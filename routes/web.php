<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Per i futuri programmatori: attenzione, io sono stato fregato, mi han chiesto così tante modifiche gratuite che ho perso 3 mesi di lavoro di moodifiche che hanno distrutto tutto il mio lavoro che ora è disordinato. E' un avviso di cortesia che lascio perchè avrei voluto saperlo io
 */

use App\Events\RequestCompleted;

Route::get('/prova', function () {

    $request = \App\Models\RentRequest::find(28);

    $request->status = 'completed';
    $request->save();

});

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'Frontend\MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'Frontend\MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'Frontend\MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'Frontend\MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'Frontend\MessagesController@update']);
});

Route::get('/admin/dashboard', 'Admin\DashboardController@index');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Auth::routes();

Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/home', 'Frontend\HomeController@index')->name('home');

Route::get('/lang/{locale}', 'Frontend\HomeController@setLang')->name('setLocale');

/** Noleggio */
Route::prefix('rent')->name('rent.')->group(function () {
    Route::get('/request/{type?}', 'Frontend\RentController@request')->name('noleggi.request');
    Route::post('/send', 'Frontend\RentController@saveRequest')->name('noleggi.save');
    Route::get('/rent-offer/{rentRequestUuid}', 'Frontend\RentController@generateOffer')->name('noleggi.offer');
    Route::get('/rent-partial-predict', 'Frontend\RentController@loadPartialPrice')->name('noleggi.partialOffer');
    Route::any('/rent-details/{rentRequestUuid}', 'Frontend\RentController@rentDetails')->name('noleggi.details');
    Route::post('/accept-offer/{rentRequest}', 'Frontend\RentController@acceptOffer')->name('noleggi.acceptOffer');
    Route::post('/final-step/{rentRequestUuid}', 'Frontend\RentController@finalStep')->name('noleggi.finalStep');
    Route::get('/confirm/{rentRequestUuid}', 'Frontend\RentController@confirm')->name('noleggi.confirm');
    Route::post('/complete/{rentRequestUuid}', 'Frontend\RentController@complete')->name('noleggi.complete');
});

Route::prefix('profile')->name('profile.')->middleware(['auth', 'can:see profile'])->group(function () {
    Route::get('/edit', 'Frontend\ProfileController@edit')->name('edit');
    Route::post('/store', 'Frontend\ProfileController@store')->name('store');
    Route::get('/requests', 'Frontend\ProfileController@requests')->name('requests');
    Route::get('/transactions', 'Frontend\ProfileController@transactions')->name('transactions');
    Route::get('/request/view/{rentRequestUuid}', 'Frontend\ProfileController@viewRequest')->name('viewRequest')->middleware(['quotePermission']);
    Route::get('/travel/view/{rentRequestUuid}', 'Frontend\ProfileController@viewTravel')->name('viewTravel')->middleware(['quotePermission']);
    Route::get('/request/delete', 'Frontend\ProfileController@deleteRequest')->name('deleteRequest');
    Route::get('/request/quotes/{rentRequestUuid}', 'Frontend\ProfileController@requestQuotes')->name('viewRequestQuotes')->middleware(['quotePermission']);
    Route::get('/request/singleQuote/{rentRequestUuid}/{quote}', 'Frontend\ProfileController@requestSingleQuote')->name('requestSingleQuote')->middleware(['quotePermission']);
    Route::any('/request/acceptQuote/{rentRequestUuid}/{quote}', 'Frontend\ProfileController@acceptQuote')->name('acceptQuote')->middleware(['quotePermission']);
});

/** Partner */
Route::prefix('partner')->name('partner.')->middleware(['auth', 'can:see partner'])->group(function () {
    Route::get('/', 'Frontend\PartnerController@index')->name('index');
    Route::get('/edit', 'Frontend\PartnerController@edit')->name('edit');
    Route::post('/store', 'Frontend\PartnerController@store')->name('store');
    Route::get('/opportunities', 'Frontend\PartnerController@opportunities')->name('opportunities');
    Route::get('/opportunity/{travel}', 'Frontend\PartnerController@opportunity')->name('opportunity')->middleware('noleggiatore');
    Route::get('/takeOpportunity/{travel}', 'Frontend\PartnerController@takeOpportunity')->name('takeOpportunity')->middleware('noleggiatore');
    Route::get('/ownedTravel/{travel}', 'Frontend\PartnerController@ownedTravel')->name('ownedTravel')->middleware('noleggiatore');
    Route::post('/ownedTravelStore/{travel}', 'Frontend\PartnerController@ownedTravelStore')->name('ownedTravelStore')->middleware('noleggiatore');
    Route::post('/ownedTravelAttach/{travel}', 'Frontend\PartnerController@fileUploads')->name('fileUploads')->middleware('noleggiatore');
    Route::get('/ownedTravelDeleteAttach/{travel}/{media}', 'Frontend\PartnerController@deleteAttachment')->name('deleteAttachment')->middleware('noleggiatore');
    Route::get('/ownedTravels', 'Frontend\PartnerController@ownedTravels')->name('ownedTravels');
});

/** Aggregati */
Route::prefix('join')->name('join.')->group(function () {
    Route::get('/', 'Frontend\JoinController@index')->name('index');
    Route::get('/ticket/{travelStep}', 'Frontend\JoinController@buyStep')->name('step')->middleware('auth');
});

/** Contact Us */
Route::get('/contact-us', 'Frontend\ContactUsController@index')->name('contactUs');
Route::get('/contact-us/submit', 'Frontend\ContactUsController@submit')->name('contactUsSubmit');

/** Contact Us */
Route::get('/faq', 'Frontend\FaqController@index')->name('faq');
Route::get('/faqs', 'Frontend\FaqController@index')->name('faqs');

/** About Us */
Route::get('/about-us', 'Frontend\AboutUsController@index')->name('aboutUs');

/** Coming Soon */
Route::get('/coming-soon', 'Frontend\ComingSoonController@index')->name('comingSoon');

/** Become Partner */
Route::get('/affiliation', 'Frontend\BecomePartnerController@index')->name('becomePartner');

/** Our Stops */
Route::get('/our-stops', 'Frontend\OurStopsController@index')->name('ourStops');
Route::get('/stops', 'Frontend\OurStopsController@index')->name('stops');

/** Our Bus */
Route::get('/our-bus', 'Frontend\OurBusController@index')->name('ourBus');
Route::get('/bus', 'Frontend\OurBusController@index')->name('bus');

/** Downloads */
Route::get('/travel/attachment/{media}', 'Frontend\DownloadsController@travelAttachment')->name('travelAttachmentDownload')->middleware(['checkDownload:travel']);

/** Socialite */
Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')
    ->name('login.provider')
    ->where('driver', implode('|', config('auth.socialite.drivers')));
Route::get('callback/{driver}', 'Auth\LoginController@handleProviderCallback')
    ->name('login.callback')
    ->where('driver', implode('|', config('auth.socialite.drivers')));

/** Payments */
Route::post('/checkout/payment/{order}/paypal', [
    'name' => 'PayPal Express Checkout',
    'as' => 'checkout.payment.paypal',
    'uses' => 'Frontend\PayPalController@checkout',
]);

Route::get('/paypal/checkout/{order}/completed', [
    'name' => 'PayPal Express Checkout',
    'as' => 'paypal.checkout.completed',
    'uses' => 'Frontend\PayPalController@completed',
]);

Route::get('/paypal/checkout/{order}/cancelled', [
    'name' => 'PayPal Express Checkout',
    'as' => 'paypal.checkout.cancelled',
    'uses' => 'Frontend\PayPalController@cancelled',
]);

Route::post('/webhook/paypal/{order?}/{env?}', [
    'name' => 'PayPal Express IPN',
    'as' => 'webhook.paypal.ipn',
    'uses' => 'Frontend\PayPalController@webhook',
]);

Route::post('/checkout/payment/{order}/stripe', [
    'name' => 'Stripe Checkout',
    'as' => 'checkout.payment.stripe',
    'uses' => 'Frontend\StripeController@checkout',
]);
