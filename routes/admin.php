<?php

Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware(['can:view administration']);

/** Permissions */
Route::prefix('permissions')->name('permissions.')->middleware('can:edit permissions')->group(function () {
    Route::get('list', 'PermissionsController@list')->name('list');
    Route::get('create', 'PermissionsController@create')->name('create');
    Route::get('update/{permission}', 'PermissionsController@update')->name('update');
    Route::get('delete/{permission}', 'PermissionsController@delete')->name('delete');
    Route::post('store', 'PermissionsController@store')->name('store');
});

/** Noleggiatori */
Route::prefix('noleggiatori')->name('noleggiatori.')->middleware('can:edit users')->group(function () {
    Route::get('list', 'NoleggiatoriController@list')->name('list');
    Route::get('create/{user}', 'NoleggiatoriController@create')->name('create');
    Route::get('update/{user}', 'NoleggiatoriController@update')->name('update');
    Route::get('delete/{user}', 'NoleggiatoriController@delete')->name('delete');
    Route::post('store', 'NoleggiatoriController@store')->name('store');
});

/** Users */
Route::prefix('users')->name('users.')->middleware('can:edit users')->group(function () {
    Route::get('list', 'UsersController@list')->name('list');
    Route::get('create', 'UsersController@create')->name('create');
    Route::get('update/{user}', 'UsersController@update')->name('update');
    Route::get('delete/{user}', 'UsersController@delete')->name('delete');
    Route::post('store', 'UsersController@store')->name('store');
});

/** Roles */
Route::prefix('roles')->name('roles.')->middleware('can:edit roles')->group(function () {
    Route::get('list', 'RolesController@list')->name('list');
    Route::get('create', 'RolesController@create')->name('create');
    Route::get('update/{role}', 'RolesController@update')->name('update');
    Route::get('delete/{role}', 'RolesController@delete')->name('delete');
    Route::post('store', 'RolesController@store')->name('store');
});

/** Markers */
Route::prefix('markers')->name('markers.')->middleware(['multiLanguage', 'can:edit markers'])->group(function () {
    Route::get('list', 'MarkersController@list')->name('list');
    Route::get('listOrphaned', 'MarkersController@orphaned')->name('orphaned');
    Route::get('orphanedDownload', 'MarkersController@orphanedDownload')->name('orphanedDownload');
    Route::get('getList', 'MarkersController@getList')->name('getList');
    Route::get('create','MarkersController@create')->name('create');
    Route::get('update/{marker}', 'MarkersController@update')->name('update');
    Route::get('delete/{marker}', 'MarkersController@delete')->name('delete');
    Route::get('search', 'MarkersController@search')->name('search');
    Route::post('store', 'MarkersController@store')->name('store');
    Route::get('distribusion/sync', 'MarkersController@distribusionSync')->name('distribusionSync')->middleware('can:update distribusion');
    Route::get('distribusion/singleSync', 'MarkersController@distribusionSingleSync')->name('distribusionSingleSync')->middleware('can:update distribusion');
    Route::get('distribusion/download/{locale}', 'MarkersController@distribusionDownload')->name('distribusionDownload')->middleware('can:update distribusion');
    Route::get('distribusion/addCounter', 'MarkersController@distribusionAddCounter')->name('distribusionAddCounter')->middleware('can:update distribusion');
    Route::get('distribusion/add', 'MarkersController@distribusionAdd')->name('distribusionAdd')->middleware('can:update distribusion');
    Route::get('distribusion/commands', 'MarkersController@distribusion')->name('distribusion')->middleware('can:update distribusion');
    Route::get('distribusion/prepareFor/{lang?}', 'MarkersController@prepareFor')->name('prepareFor')->middleware('can:update distribusion');
    Route::get('distribusion/removeToAdd', 'MarkersController@removeToAdd')->name('removeToAdd')->middleware('can:update distribusion');
});

/** Services */
Route::prefix('services')->name('services.')->middleware(['multiLanguage', 'can:edit services'])->group(function () {
    Route::get('list', 'ServicesController@list')->name('list');
    Route::get('create','ServicesController@create')->name('create');
    Route::get('update/{service}', 'ServicesController@update')->name('update');
    Route::get('delete/{service}', 'ServicesController@delete')->name('delete');
    Route::post('store', 'ServicesController@store')->name('store');
});

/** Retailers */
Route::prefix('retailers')->name('retailers.')->middleware(['multiLanguage', 'can:edit retailers'])->group(function () {
    Route::get('list', 'RetailersController@list')->name('list');
    Route::get('create','RetailersController@create')->name('create');
    Route::get('update/{retailer}', 'RetailersController@update')->name('update');
    Route::get('delete/{retailer}', 'RetailersController@delete')->name('delete');
    Route::post('store', 'RetailersController@store')->name('store');
});

/** Languages */
Route::prefix('languages')->name('languages.')->middleware(['can:edit languages'])->group(function () {
    Route::get('translations/list', 'LanguagesController@listTranslations')->name('listTranslations');
    Route::get('translations/edit/{language}/{translation}', 'LanguagesController@editTranslation')->name('editTranslation');
    Route::get('translations/new/{language}', 'LanguagesController@newTranslation')->name('newTranslation');
    Route::get('translations/duplicate/{language}/{translation}', 'LanguagesController@duplicateTranslation')->name('duplicateTranslation');
    Route::get('translations/doDuplicate/{newLang}/{translation}/{oldLang}', 'LanguagesController@doDuplicateTranslation')->name('doDuplicateTranslation');
    Route::post('translations/store/{language}', 'LanguagesController@storeTranslation')->name('storeTranslation');
});

/** Rental Request */
Route::prefix('rentRequests')->name('rentRequests.')->middleware(['can:administer rent requests'])->group(function () {
    Route::get('list', 'RentRequestsController@listNew')->name('listNew');
    Route::get('listCompleted', 'RentRequestsController@listCompleted')->name('listCompleted');
    Route::get('listSold', 'RentRequestsController@listSold')->name('listSold');
    Route::get('view/{rentRequest}', 'RentRequestsController@view')->name('view');
    Route::post('changeStatus/{rentRequest}', 'RentRequestsController@changeStatus')->name('changeStatus')->middleware(['can:change request status']);
});

/** Quotes */
Route::prefix('quotes')->name('quotes.')->middleware(['can:create quotes'])->group(function () {
    Route::get('list/{rentRequest}', 'QuotesController@listFor')->name('listFor');
    Route::get('new/{rentRequest}', 'QuotesController@create')->name('create');
    Route::get('edit/{rentRequest}/{quote}', 'QuotesController@edit')->name('edit');
    Route::get('pdf/{rentRequest}/{quote}', 'QuotesController@pdf')->name('pdf');
    Route::post('store/{rentRequest}', 'QuotesController@store')->name('store');
});

/** Promocodes */
Route::prefix('promocodes')->name('promocodes.')->middleware(['can:create promocodes'])->group(function () {
    Route::get('list', 'PromocodesController@list')->name('list');
    Route::get('new', 'PromocodesController@create')->name('create');
    Route::get('edit/{promocode}', 'PromocodesController@update')->name('edit');
    Route::post('store', 'PromocodesController@store')->name('store');
});

/** Notes */
Route::prefix('notes')->name('notes.')->middleware('can:write notes')->group(function () {
    Route::post('store', 'NotesController@store')->name('store');
});

/** Audits */
Route::prefix('audits')->name('audits.')->middleware('can:see audits')->group(function () {
    Route::post('view', 'AuditsController@view')->name('view');
});

/** Configuration */
Route::prefix('configurations')->name('configurations.')->middleware('can:edit configurations')->group(function () {
    Route::get('payments', 'SettingsController@listPayments')->name('listPayments');
    Route::get('distribusion', 'SettingsController@listDistribusion')->name('listDistribusion');
    Route::get('general', 'SettingsController@listGeneral')->name('listGeneral');
    Route::get('mail', 'SettingsController@listMail')->name('listMail');
    Route::post('store', 'SettingsController@store')->name('store');
});

/** Packages */
Route::prefix('packages')->name('packages.')->middleware(['multiLanguage', 'can:edit packages'])->group(function () {
    Route::get('list', 'PackagesController@list')->name('list');
    Route::get('create','PackagesController@create')->name('create');
    Route::get('update/{package}', 'PackagesController@update')->name('update');
    Route::get('delete/{package}', 'PackagesController@delete')->name('delete');
    Route::post('store', 'PackagesController@store')->name('store');
});

/** People Ranges */
Route::prefix('people_ranges')->name('people_ranges.')->middleware(['can:create people ranges'])->group(function () {
    Route::get('list', 'PeopleRangesController@list')->name('list');
    Route::get('new', 'PeopleRangesController@create')->name('create');
    Route::get('edit/{range}', 'PeopleRangesController@edit')->name('edit');
    Route::post('store', 'PeopleRangesController@store')->name('store');
    Route::get('delete/{range}', 'PeopleRangesController@delete')->name('delete');
});

/** Steps */
Route::prefix('steps')->name('steps.')->middleware(['can:edit steps'])->group( function() {
    Route::get('distribusionSteps/{rentRequest}', 'StepsController@distribusionSteps')->name('distribusionSteps');
    Route::get('distribusionSteps/edit/{quoteMarker}', 'StepsController@edit')->name('edit');
    Route::get('addTicket/{quoteMarker}', 'StepsController@addTicket')->name('addTicket');
    Route::get('editTicket/{quoteMarker}/{soldMarker}', 'StepsController@editTicket')->name('editTicket');
    Route::get('checkTicket/{soldMarkerUuid}', 'StepsController@checkTicket')->name('checkTicket');
    Route::post('ticket/store/{quoteMarker}', 'StepsController@storeTicket')->name('storeTicket');
    Route::post('autoGenerate/{rentRequest}', 'StepsController@autoGenerate')->name('autoGenerate');
});

/** Payments */
Route::prefix('payments')->name('payments.')->middleware(['can:see payments'])->group( function() {
    Route::get('list', 'PaymentsController@list')->name('list');
    Route::get('view/{order}', 'PaymentsController@view')->name('view');
    Route::get('edit/{order}', 'PaymentsController@edit')->name('edit')->middleware('can:edit payments');
    Route::get('create/{quote}', 'PaymentsController@create')->name('create')->middleware('can:add payments');
    Route::post('store', 'PaymentsController@store')->name('store')->middleware('can:edit payments');
});

/** Algorythm */
Route::prefix('algorithm')->name('algorithm.')->middleware(['can:edit algorithm'])->group(function () {
    Route::get('/', 'AlgorithmController@index')->name('index');
    Route::post('store', 'AlgorithmController@store')->name('store');
    Route::post('test', 'AlgorithmController@test')->name('test');
});

/** Travels */
Route::prefix('travels')->name('travels.')->middleware(['multiLanguage', 'can:edit travels'])->group(function () {
    Route::get('list', 'TravelsController@list')->name('list');
    Route::get('update/{travel}', 'TravelsController@update')->name('update');
    Route::get('create', 'TravelsController@create')->name('create');
    Route::post('fileUploads/{travel}', 'TravelsController@fileUploads')->name('fileUploads');
    Route::get('deleteAttachment/{media}', 'TravelsController@deleteAttachment')->name('deleteAttachment');
    Route::get('generate/{quote}', 'TravelsController@generate')->name('generate');
    Route::post('store', 'TravelsController@store')->name('store');
    Route::post('steps/store/{travel}', 'TravelsController@stepStore')->name('step.store');
    Route::get('steps/update/{step}', 'TravelsController@stepUpdate')->name('step.update');
    Route::get('steps/create/{travel}', 'TravelsController@stepCreate')->name('step.create');
});

/** Tickets */
Route::prefix('tickets')->name('tickets.')->middleware(['can:view tickets'])->group(function () {
    Route::get('list', 'TicketsController@list')->name('list');
    Route::get('view/{ticket}', 'TicketsController@view')->name('view');
    Route::get('update/{ticket}', 'TicketsController@update')->name('update')->middleware(['can:edit tickets']);
    Route::get('create/{travelStep?}', 'TicketsController@create')->name('create')->middleware(['can:edit tickets']);
    Route::get('check/{ticketUuid}', 'TicketsController@check')->name('check');
    Route::post('store/{step}', 'TicketsController@store')->name('store')->middleware(['can:edit tickets']);
});

/** Stops */
Route::prefix('stops')->name('stops.')->middleware(['can:view stops'])->group(function () {
    Route::get('list', 'StopsController@list')->name('list');
//    Route::get('view/{ticket}', 'TicketsController@view')->name('view');
//    Route::get('update/{ticket}', 'TicketsController@update')->name('update')->middleware(['can:edit tickets']);
    Route::get('create', 'StopsController@create')->name('create')->middleware(['can:edit stops']);
//    Route::get('check/{ticketUuid}', 'TicketsController@check')->name('check');
    Route::post('store', 'StopsController@store')->name('store')->middleware(['can:edit stops']);
    Route::get('delete/{stop}', 'StopsController@delete')->name('delete')->middleware(['can:edit stops']);
});