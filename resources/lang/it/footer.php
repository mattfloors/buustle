<?php
//File is autogenerated, do not edit or it will be overwritten
//Generated on: 04/07/2019 14:31 By andrea.dipiazza@buustle.com

return [
	'rent_a_bus' => 'Noleggia un mezzo',
	'join' => 'Aggregati',
	'contactUs' => 'Contattaci',
	'earn_with_us' => 'Guadagna con noi',
	'about_us' => 'Chi siamo',
	'our_bus' => 'I nostri mezzi',
	'our_stations' => 'Le nostre fermate',
	'faq' => 'FAQ',
	'general_conditions' => 'Condizione generali',
	'privacy' => 'Privacy',
	'cookies' => 'Cookies',
	'supported_by' => 'Supportati da',
	'developed_with' => 'Sviluppato con',
	'in_turin' => 'Torino',
	'VAT_number' => 'Partita Iva',
	'travel_license' => 'Licenza di viaggio',
];
