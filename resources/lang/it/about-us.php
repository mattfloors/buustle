<?php
//File is autogenerated, do not edit or it will be overwritten
//Generated on: 26/06/2019 10:15 By andrea.dipiazza@buustle.com

return [
	'title' => 'Chi Siamo',
	'title_sub_top' => 'La nostra mission è ottimizzare il mondo dei trasporti: efficenza e sostenibilità sono i valori a cui facciamo riferimento',
	'our_history' => 'La nostra storia',
	'our_history_text' => 'Lavorando per anni nell\'azienda di familiare di noleggio bus, Stefano, ha presto realizzato quanto fosse inefficiente il mondo dei trasporti e quanto fosse importante creare una nuova soluzione. Quando incontra Andrea, nell\'estate del 2016, innovatore con la stessa visione e studente in ambito IT. Insieme i due  decidono di fondare Buustle. Da allora, Buustle è cresciuto con il supporto di TreatBit e fondazione ITS con un vasto network di noleggiatori partner',
	'our_team' => 'Il nostro team',
	'our_team_text' => 'Siamo un team unito e appassionato di managers, commerciali, sviluppatori, designers, pensatori, atleti, viaggiatori, sognatori, esperti culinari e molto altro. Amiamo quello che facciamo e cerchiamo sempre di trasmettere questa passione a tutte le persone che fanno parte della famiglia Buustle.',
	'join_us' => 'Unisciti a noi!',
	'are_you_interested' => 'Interessato a far parte del network di Buustle?',
	'contact_ud' => 'Contattaci',
	'become_partner' => 'Diventa partner',
	'keep_in_touch' => 'Rimani informato',
	'keep_in_touch_text' => 'Registrati alla nostra newsletter per rimanere aggiornato sulle novità di Buustle e sui viaggi a cui ti puoi aggregare',
	'email_placeholder' => 'Inserisci il tuo indirizzo email',
	'accept' => 'Accetto i',
	'terms-and-condition' => 'Termini e condizioni',
	'privacy_policy' => 'la Normativa sulla privacy',
	'subscribe' => 'Registrami',
];
