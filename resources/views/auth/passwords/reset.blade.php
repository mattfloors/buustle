@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="{{ route('password.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="email">@lang('auth.login_email')*</label>
                                            <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email" placeholder="@lang('auth.login_email')" value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="password">@lang('auth.new_password')*</label>
                                            <input class="{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" id="password" name="password" placeholder="@lang('auth.new_password')" required>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="password-confirm">@lang('auth.password_confirm')*</label>
                                            <input class="{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" id="password-confirm" name="password_confirmation" placeholder="@lang('auth.password_confirmation')" required>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <input type="hidden" name="remember" value="1">
                                <input type="submit" class="bttn bttn-orange small" value="{{ __('auth.reset_password_button') }}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
