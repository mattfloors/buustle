@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="{{ route('password.email') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="email">@lang('auth.login_email')*</label>
                                            <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email" placeholder="@lang('auth.login_email')" value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <input type="hidden" name="remember" value="1">
                                <input type="submit" class="bttn bttn-orange small" value="{{ __('auth.reset_password_button') }}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection