@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="{{ route('login') }}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="email">@lang('auth.login_email')*</label>
                                            <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email" placeholder="@lang('auth.login_email')" value="{{ old('email') }}" required autofocus>
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="password">@lang('auth.login_password')*</label>
                                            <input class="{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" id="password" name="password" placeholder="@lang('auth.login_password')">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                            <br>
                                            <small>@if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert" style="color: var(--reddish-orange);">
                                                    {{ $errors->first('email') }}
                                                </span>
                                                @endif<a class="steel" href="{{ route('password.request') }}" title="@lang('auth.login_forgot_password')">@lang('auth.login_forgot_password')</a></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="checkbox">
                                    <input type="checkbox" value="1" name="terms" id="terms">
                                    <label for="terms">
                                        @lang('auth.login_accept_terms')
                                        <div class="check-button"><i></i></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                @if( $redirect !== null )
                                    <input type="hidden" name="redirect" value="{{ $redirect }}">
                                    @if( $redirect === 'rent-request' && session('request_id') )
                                        <input type="hidden" name="uuid" value="{{ session('request_id') }}">
                                    @endif
                                @endif
                                <input type="hidden" name="remember" value="1">
                                <input type="submit" class="bttn bttn-orange small" value="@lang('auth.login_button')">
                                <br>
                                <small class="text-center">@lang('auth.login_need_account') <a href="{{ route('register') }}" title="@lang('auth.register')" class="orange">@lang('auth.register')</a></small>
                                <a href="{{ route('login.provider', 'google') }}" class="bttn bttn-google small"><span>@lang('rent.step2_login_with_google')</span></a>
                                <a href="{{ route('login.provider', 'facebook') }}" class="bttn bttn-facebook small"><span>@lang('rent.step2_login_with_facebook')</span></a>
                                <a href="{{ route('login.provider', 'twitter') }}" class="bttn bttn-twitter small"><span>@lang('rent.step2_login_with_twitter')</span></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection