@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="first_name">@lang('auth.first_name')*</label>
                                            <input class="{{ $errors->has('first_name') ? ' is-invalid' : '' }}" type="text" id="first_name" name="first_name" placeholder="@lang('auth.first_name')" value="{{ old('first_name') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="last_name">@lang('auth.last_name')*</label>
                                            <input class="{{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text" id="last_name" name="last_name" placeholder="@lang('auth.last_name')" value="{{ old('last_name') }}" required>
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="email">@lang('auth.register_email')*</label>
                                            <input class="{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" name="email" placeholder="@lang('auth.register_email')" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="password">@lang('auth.login_password')*</label>
                                            <input class="{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" id="password" name="password" placeholder="@lang('auth.login_password')">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="password_confirmation">@lang('auth.password_confirmation')*</label>
                                            <input class="{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" id="password_confirmation" name="password_confirmation" placeholder="@lang('auth.password_confirmation')">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                            <br>
                                            <small><a class="steel" href="{{ route('password.request') }}" title="@lang('auth.login_forgot_password')">@lang('auth.login_forgot_password')</a></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="checkbox">
                                    <input type="checkbox" value="1" name="terms" id="terms" required>
                                    <label for="terms">
                                        @lang('auth.register_accept_terms')
                                        <div class="check-button"><i></i></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <input type="submit" class="bttn bttn-orange small" value="@lang('auth.register_button')">
                                <br>
                                <small class="text-center">@lang('auth.register_have_account') <a href="{{ route('login') }}" title="@lang('auth.login')" class="orange">@lang('auth.login')</a></small>
                                <a href="{{ route('login.provider', 'google') }}" class="bttn bttn-google small"><span>@lang('rent.step2_login_with_google')</span></a>
                                <a href="{{ route('login.provider', 'facebook') }}" class="bttn bttn-facebook small"><span>@lang('rent.step2_login_with_facebook')</span></a>
                                <a href="{{ route('login.provider', 'twitter') }}" class="bttn bttn-twitter small"><span>@lang('rent.step2_login_with_twitter')</span></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
