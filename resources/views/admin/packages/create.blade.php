@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/summernote/summernote-bs4.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Package  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Pacchetto Viaggio -@if( !empty($package->id) ) #{{ $package->id }} {{ $package->getTranslation('name', $language) }} @else Nuovo Pacchetto Viaggio @endif
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    @if( $package->id === null )
                                        <a href="{{ route(\Request::route()->getName(), ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @else
                                        <a href="{{ route(\Request::route()->getName(), ['package' => $package, 'lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @endif
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form enctype="multipart/form-data" action="{{ route('admin.packages.store', ['lang' => $language]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $package->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="sku" class="form-control-label">SKU</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="sku" value="{{ $package->sku }}" id="sku">
                                        <small class="help-block form-text">lo sku definisce i calcoli fatti per il prezzo, cambiarlo solo se si sa esattamente cosa si sta facendo</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $package->getTranslation('name', $language) }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="icon" class="form-control-label">Icona</label></div>
                                    <div class="col col-md-8">
                                        <input type="text" class="form-control" name="icon" value="{{ $package->icon }}" id="icon">
                                        <small class="help-block form-text"><a href="https://fontawesome.com/icons" target="_blank">https://fontawesome.com/icons <i class="fas fa-external-link"></i></a></small>
                                    </div>
                                    <div class="col col-md-1">
                                        @if( !empty($package->icon) )
                                            <i class="{{ $package->icon }}"></i>
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="class" class="form-control-label">Classe</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="class" id="class" class="form-control" required="required">
                                            <option value=""> - </option>
                                            @if( !empty($availableClasses) )
                                                @foreach( $availableClasses as $class )
                                                    <option value="{{ $class }}" @if($package->class === $class) selected="selected" @endif>{{ $class }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="services" class="form-control-label">Servizi Offerti</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="services[]" id="services" class="form-control" multiple="multiple">
                                            @if( !empty($services) )
                                                @foreach( $services as $service )
                                                    <option value="{{ $service->id }}" @if($package->services->contains($service->id)) selected="selected" @endif>{{ $service->getTranslation('name', \Illuminate\Support\Facades\App::getLocale()) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="multiplier" class="form-control-label">Modifica Prezzo (100% non cambia)</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <input type="number" id="multiplier" name="multiplier" placeholder="100" class="form-control" value="{{ $package->multiplier * 100 }}" step="any" required>
                                            <div class="input-group-addon"><i class="fas fa-percent"></i></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="env_class" class="form-control-label">Classe Ambientale</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="env_class" name="env_class" class="form-control" value="{{ $package->env_class }}">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="supplement" class="form-control-label">Supplemento @lang('rent.supplemento') %</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" id="supplement" name="supplement" class="form-control" value="{{ $package->supplement }}">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Attivo</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="active" name="active" value="1" class="form-check-input"@if( $package->active ) checked="checked"@endif>Il Pacchetto è attivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="banner-image" class="form-control-label">Immagine Banner</label></div>
                                    <div class="col-12 col-md-9">
                                        @if( $package->getMedia('banner-image')->count() > 0 )
                                            <img src="{{ $package->getFirstMediaUrl('banner-image') }}" alt="{{ $package->name }}">
                                        @else
                                            -
                                        @endif
                                        <br>
                                        <input type="file" id="banner-image" name="banner-image" class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="description" class="form-control-label">Descrizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="description" id="description" class="form-control" rows="5">{{ $package->getTranslation('description', $language) }}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Package -->

    @if( $package->id !== null )
        <!-- Commenti Package -->
            @include('admin.shared.notes', ['model' => 'App\Models\Package', 'id' => $package->id, 'notes' => $package->notes])
        <!-- Commenti Package -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <script src="{{ asset('storage/assets/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            $('#description').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 100,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['fullscreen', ['fullscreen']],
                    ['codeview', ['codeview']],
                    ['undo', ['undo']],
                    ['redo', ['redo']]
                ]
            });

            $('#services').select2();
        } );
    </script>

@endsection