@extends('admin.layout.default')

@section('css-header')
    @parent

    <style>
        .payment-completed {

            color: #1e7e34;

        }

        .payment-failed {

            color: #AE0E0E;

        }

        .payment-pending {

            color: #1a237e;

        }

        .payment-cancelled {

            color: #d39e00;

        }

        .myRow {

            border-bottom: 1px solid #000000;

        }

         pre {
             width: 100%;
             padding: 0;
             margin: 0;
             overflow-x: auto;
             overflow-y: hidden;
             font-size: 12px;
             line-height: 20px;
             background: #efefef;
             border: 1px solid #777;
         }
    </style>

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Ordine # {{ $order->id }}
                <span class="pull-right">
                    @if( $order->isForQuote() )
                        <a href="{{ route('admin.payments.list') }}"><i class="fas fa-backward"></i> Torna Indietro</a>
                        <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $order->product->request]) }}"><i class="far fa-external-link"></i> Vedi la Richiesta</a>
                        <a target="_blank" href="{{ route('admin.quotes.edit', ['rentRequest' => $order->product->request, 'quote' => $order->product]) }}"><i class="far fa-external-link"></i> Vedi il Preventivo</a>
                    @elseif( $order->isForTicket() )

                    @else
                        Payment Has no Product!
                    @endif
                </span>
            </h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-info" style="color:#007fff"></i> Informazioni
                @can('edit payments')
                    <span class="pull-right"><a href="{{ route('admin.payments.edit', ['order' => $order]) }}"><i class="far fa-2x fa-edit"></i></a></span>
                @endcan
            </h4>
        </div>
        <div class="card-body">
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Pagamento creato il</b>
                </div>
                <div class="col-md-8">
                    {{ \Carbon\Carbon::parse( $order->created_at )->format('d/m/Y H:i:s') }}
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Ultima Modifica</b>
                </div>
                <div class="col-md-8">
                    {{ \Carbon\Carbon::parse( $order->updated_at )->format('d/m/Y H:i:s') }}
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Prezzo Pagato</b>
                </div>
                <div class="col-md-8">
                    {{ $order->amount }} €
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Utente</b>
                </div>
                <div class="col-md-8">
                    <a href="{{ route('admin.users.update', ['user' => $order->user_id]) }}">{{ $order->user->email }}</a>
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Preventivo</b>
                </div>
                <div class="col-md-8">
                    @if( $order->isForQuote() )
                        @can('create quotes')
                            <a href="{{ route('admin.quotes.edit', ['rentRequest' => $order->product->request, 'quote' => $order->product]) }}">Quote # {{ $order->product->sku }}</a>
                        @else
                            Quote # {{ $order->product->sku }}
                        @endcan
                    @elseif( $order->isForTicket() )
                        @can('view tickets')
                            <a href="{{ route('admin.tickets.view', ['ticket' => $order->product]) }}">Ticket # {{ $order->product->id }}</a>
                        @else
                            Ticket # {{ $order->productid }}
                        @endcan
                    @else
                        ???
                    @endif
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Codice Sconto</b>
                </div>
                <div class="col-md-8">
                    @if( empty($order->promocode) )
                        <i class="fas fa-empty-set" data-toggle="tooltip" data-placement="top" title="No Promo Code"></i>
                    @else
                        @can('create promocodes')
                            <a href="{{ route('admin.promocodes.edit', ['promocode' => $order->promocode]) }}">
                                <i class="fas fa-badge-check" data-toggle="tooltip" data-placement="top" title="{{ $order->promocode->code }}" style="color: #00ff00;"></i>
                            </a>
                        @else
                            <i class="fas fa-badge-check" style="color: #00ff00;" data-toggle="tooltip" data-placement="top" title="{{ $order->promocode->code }}"></i>
                        @endcan
                    @endif
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Metodo di Pagamento</b>
                </div>
                <div class="col-md-8">
                    {{ ucfirst($order->method) }}
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Stato Pagamento</b>
                </div>
                <div class="col-md-8">
                    <span class="payment-{{ $order->payment_status }}">{{ $order->payment_status }}</span>
                </div>
            </div>
            <div class="row myRow">
                <div class="col-md-4">
                    <b>Riferimento Pagamento</b>
                </div>
                <div class="col-md-8">
                    {{ $order->transaction_id }}
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-server" style="color: #006699;"></i> Raw Data
            </h4>
        </div>
        <div class="card-body">
            @if( !empty($order->raw_data) )
                <pre>
                    {{ print_r($order->raw_data, true) }}
                </pre>
            @else
                Non ci sono informazioni ulteriori
            @endif
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-exclamation" style="color: darkred;"></i> Errori
            </h4>
        </div>
        <div class="card-body">
            @if( !empty($order->fail_message) )
                <p>{{ $order->fail_message }}</p>
            @else
                Non ci sono errori
            @endif
        </div>
    </div>

    @if( $order->id !== null )
        <!-- Commenti Pagamento -->
        @include('admin.shared.notes', ['model' => 'App\Models\Order', 'id' => $order->id, 'notes' => $order->notes])
        <!-- Commenti Pagamento -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Order', 'id' => $order->id, 'loadedModel' => $order])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('[data-toggle="tooltip"]').tooltip();

            $('.payment-created').tooltip({
                title: 'Il preventivo è stato accettato e deve essere pagato'
            });

            $('.payment-pending').tooltip({
                title: 'Il preventivo è stato accettato e il pagamento è iniziato, siamo in attesa di conferma dal service o dalla banca'
            });

            $('.payment-completed').tooltip({
                title: 'Il pagamento è andato a buon fine'
            });

            $('.payment-cancelled').tooltip({
                title: 'Il pagamento è cominciato ma è poi stato annullato dall\' utente prima della fine della transazione'
            });

            $('.payment-failed').tooltip({
                title: 'Il pagamento è fallito, guardare i dettagli per maggiori informazioni'
            });

        } );
    </script>

@endsection