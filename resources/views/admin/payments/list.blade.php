@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

    <style>
        .payment-completed {

            color: #1e7e34;

        }

        .payment-failed {

            color: #AE0E0E;

        }

        .payment-pending {

            color: #1a237e;

        }

        .payment-cancelled {

            color: #d39e00;

        }
    </style>

@endsection

@section('content')

    <!--  Payments  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Pagamenti Ricevuti
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <table id="list" class="display compact nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID Transazione</th>
                                    <th>Utente</th>
                                    <th>Ammontare (€)</th>
                                    <th>Preventivo</th>
                                    <th>Codice Sconto</th>
                                    <th>Metodo</th>
                                    <th>Status</th>
                                    <th>Aggiornato</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $orders as $o )
                                    <tr>
                                        <td>
                                            {{ $o->id }}
                                        </td>
                                        <td>
                                            {{ $o->transaction_id }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.users.update', ['user' => $o->user_id]) }}">{{ $o->user->email }}</a>
                                        </td>
                                        <td>
                                            {{ $o->amount }}
                                        </td>
                                        <td>
                                            @if( $o->isForQuote() )
                                                @can('create quotes')
                                                    <a href="{{ route('admin.quotes.edit', ['rentRequest' => $o->product->request, 'quote' => $o->product]) }}">Quote # {{ $o->product->sku }}</a>
                                                @else
                                                    Quote # {{ $o->product->sku }}
                                                @endcan
                                            @elseif( $o->isForTicket() )
                                                @can('view tickets')
                                                    <a href="{{ route('admin.tickets.view', ['ticket' => $o->product]) }}">Ticket # {{ $o->product->id }}</a>
                                                @else
                                                    Ticket # {{ $o->productid }}
                                                @endcan
                                            @else
                                                ???
                                            @endif
                                        </td>
                                        <td>
                                            @if( empty($o->promocode) )
                                                <i class="fas fa-empty-set" data-toggle="tooltip" data-placement="top" title="No Promo Code"></i>
                                            @else
                                                @can('create promocodes')
                                                    <a href="{{ route('admin.promocodes.edit', ['promocode' => $o->promocode]) }}">
                                                        <i class="fas fa-badge-check" data-toggle="tooltip" data-placement="top" title="{{ $o->promocode->code }}" style="color: #00ff00;"></i>
                                                    </a>
                                                @else
                                                    <i class="fas fa-badge-check" style="color: #00ff00;" data-toggle="tooltip" data-placement="top" title="{{ $o->promocode->code }}"></i>
                                                @endcan
                                            @endif
                                        </td>
                                        <td>
                                            {{ ucfirst($o->method) }}
                                        </td>
                                        <td>
                                            <span class="payment-{{ $o->payment_status }}">{{ $o->payment_status }}</span>
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($o->updated_at)->format('d/m/Y H:i') }}
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.payments.view', ['order' => $o]) }}" class="btn btn-outline-primary"><i class="far fa-search-dollar"></i></a>
                                            @can('edit payments')
                                                <a href="{{ route('admin.payments.edit', ['order' => $o]) }}" class="btn btn-outline-warning"><i class="far fa-file-edit"></i></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun pagamento trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>ID Transazione</th>
                                    <th>Utente</th>
                                    <th>Ammontare (€)</th>
                                    <th>Preventivo</th>
                                    <th>Codice Sconto</th>
                                    <th>Metodo</th>
                                    <th>Status</th>
                                    <th>Aggiornato</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Pagamenti -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();

            $('[data-toggle="tooltip"]').tooltip();

            $('.payment-created').tooltip({
                title: 'Il preventivo è stato accettato e deve essere pagato'
            });

            $('.payment-pending').tooltip({
                title: 'Il preventivo è stato accettato e il pagamento è iniziato, siamo in attesa di conferma dal service o dalla banca'
            });

            $('.payment-completed').tooltip({
                title: 'Il pagamento è andato a buon fine'
            });

            $('.payment-cancelled').tooltip({
                title: 'Il pagamento è cominciato ma è poi stato annullato dall\' utente prima della fine della transazione'
            });

            $('.payment-failed').tooltip({
                title: 'Il pagamento è fallito, guardare i dettagli per maggiori informazioni'
            });
        } );
    </script>
@endsection