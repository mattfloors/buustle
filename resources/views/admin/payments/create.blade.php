@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet">

    <style>
        .payment-completed {

            color: #1e7e34;

        }

        .payment-failed {

            color: #AE0E0E;

        }

        .payment-pending {

            color: #1a237e;

        }

        .payment-cancelled {

            color: #d39e00;

        }

        .myRow {

            border-bottom: 1px solid #000000;

        }

        .myRow label {

            vertical-align:middle;
            display: table-cell;

        }

        pre {
            width: 100%;
            padding: 0;
            margin: 0;
            overflow-x: auto;
            overflow-y: hidden;
            font-size: 12px;
            line-height: 20px;
            background: #efefef;
            border: 1px solid #777;
        }
    </style>

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Ordine # {{ $order->id }}
                <span class="pull-right">
                    @if( $order->isForQuote() )
                        <a href="{{ route('admin.payments.list') }}"><i class="fas fa-backward"></i> Torna Indietro</a>
                        <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $order->product->request]) }}"><i class="far fa-external-link"></i> Vedi la Richiesta</a>
                        <a target="_blank" href="{{ route('admin.quotes.edit', ['rentRequest' => $order->product->request, 'quote' => $order->product]) }}"><i class="far fa-external-link"></i> Vedi il Preventivo</a>
                    @elseif( $order->isForTicket() )

                    @else
                        Payment Has no Product!
                    @endif
                </span>
            </h4>
        </div>
    </div>

    <form action="{{ route('admin.payments.store') }}" method="post" class="form">
        @csrf
        <input type="hidden" name="id" value="{{ $order->id }}">
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-info" style="color:#007fff"></i> Informazioni
                    @if( $order->id !== null )
                        <span class="pull-right"><a href="{{ route('admin.payments.view', ['order' => $order]) }}"><i class="far fa-2x fa-eye"></i></a></span>
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Pagamento creato il</b>
                    </div>
                    <div class="col-md-8">
                        {{ \Carbon\Carbon::parse( $order->created_at )->format('d/m/Y H:i:s') }}
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Ultima Modifica</b>
                    </div>
                    <div class="col-md-8">
                        {{ \Carbon\Carbon::parse( $order->updated_at )->format('d/m/Y H:i:s') }}
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="amount"><b>Prezzo Pagato</b></label>
                    </div>
                    <div class="col-md-8">
                        <input type="number" step="any" name="amount" id="amount" class="form-control" value="{{ $order->amount }}">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="user_id"><b>Utente</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <select name="user_id" class="form-control" id="user_id">
                            @forelse( $users as $user )
                                <option value="{{ $user->id }}" @if( $user->id === $order->user_id ) selected="selected" @endif>{{ $user->email }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="quote_id"><b>Preventivo Collegato</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <input type="radio" name="selected_source" value="{{ \App\Models\Order::QUOTE }}" @if( $order->isForQuote() ) checked="checked" @endif>
                        <select name="quote_id" id="quote_id" class="form-control">
                            @forelse( $quotes as $q )
                                <option value="{{ $q->id }}" @if( $q->id === $quote_id ) selected="selected" @endif>{{ $q->sku }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="ticket_id"><b>Ticket Collegato</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <input type="radio" name="selected_source" value="{{ \App\Models\Order::TICKET }}" @if( $order->isForTicket() ) checked="checked" @endif>
                        <select name="ticket_id" id="ticket_id" class="form-control">
                            @forelse( $tickets as $t )
                                <option value="{{ $t->id }}" @if( $t->id === $ticket_id ) selected="selected" @endif>Ticket # {{ $t->id }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="promocode_id"><b>Promocode</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <select name="promocode_id" id="promocode_id" class="form-control">
                            <option value=""> - </option>
                            @forelse( $promocodes as $promocode )
                                <option value="{{ $promocode->id }}" @if( $promocode->id === $order->promocode_id ) selected="selected" @endif>{{ $promocode->code }} - {{ $promocode->reward }} @if( $promocode->data['type'] === 'flat' ) € @else % @endif</option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="method"><b>Metodo di Pagamento</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <select name="method" id="method" class="form-control">
                            @forelse( $order->methods() as $method )
                                <option value="{{ $method }}" @if( $method === $order->method ) selected="selected" @endif>{{ ucfirst($method) }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="payment_status"><b>Status Pagamento</b></label>
                    </div>
                    <div class="col-md-8" style="padding-top:5px;padding-bottom:5px;">
                        <select name="payment_status" id="payment_status" class="form-control">
                            @forelse( $order->statuses() as $status )
                                <option value="{{ $status }}" @if( $status === $order->payment_status ) selected="selected" @endif>{{ $status }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4" style="padding-top:5px;padding-bottom:5px;">
                        <label for="transaction_id"><b>Codice Transazione</b></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="transaction_id" value="{{ $order->transaction_id }}" class="form-control" id="transaction_id">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-server" style="color: #006699;"></i> Raw Data
            </h4>
        </div>
        <div class="card-body">
            @if( !empty($order->raw_data) )
                <pre>
                    {{ print_r($order->raw_data, true) }}
                </pre>
            @else
                Non ci sono informazioni ulteriori
            @endif
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-exclamation" style="color: darkred;"></i> Errori
            </h4>
        </div>
        <div class="card-body">
            @if( !empty($order->fail_message) )
                <p>{{ $order->fail_message }}</p>
            @else
                Non ci sono errori
            @endif
        </div>
    </div>

    @if( $order->id !== null )
        <!-- Commenti Pagamento -->
        @include('admin.shared.notes', ['model' => 'App\Models\Order', 'id' => $order->id, 'notes' => $order->notes])
        <!-- Commenti Pagamento -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Order', 'id' => $order->id, 'loadedModel' => $order])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <script src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('[data-toggle="tooltip"]').tooltip();

            $('.payment-created').tooltip({
                title: 'Il preventivo è stato accettato e deve essere pagato'
            });

            $('.payment-pending').tooltip({
                title: 'Il preventivo è stato accettato e il pagamento è iniziato, siamo in attesa di conferma dal service o dalla banca'
            });

            $('.payment-completed').tooltip({
                title: 'Il pagamento è andato a buon fine'
            });

            $('.payment-cancelled').tooltip({
                title: 'Il pagamento è cominciato ma è poi stato annullato dall\' utente prima della fine della transazione'
            });

            $('.payment-failed').tooltip({
                title: 'Il pagamento è fallito, guardare i dettagli per maggiori informazioni'
            });

            $("#user_id").select2();
            $("#quote_id").select2();
            $("#ticket_id").select2();
            $("#payment_status").select2();
            $("#method").select2();

        } );
    </script>

@endsection