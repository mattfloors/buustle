@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Gestione Algoritmi Pagamenti - TEST
            </h4>
        </div>
    </div>

    <form class="form" method="post" action="{{ route('admin.algorithm.test') }}">
        @csrf
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-function" style="color:#007fff"></i> Variabili Disponibili
                </h4>
            </div>
            <div class="card-body">
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::PEOPLE }}"><b>Persone</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::PEOPLE }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" value="{{ $people }}" name="{{ \App\Http\Services\RentalPrice::PEOPLE }}" id="{{ \App\Http\Services\RentalPrice::PEOPLE }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}"><b>Km Tratta</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::KM_TRATTA }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" value="{{ $km_tratta }}" step="any" name="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}" id="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}"><b>Giorni di Viaggio</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" value="{{ $days_of_travel }}" step="any" name="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}" id="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}"><b>Giorni dalla partenza</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" value="{{ $days_from_start }}" step="any" name="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}" id="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}"><b>Km al Giorno</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" step="any" value="{{ $km_per_day }}" name="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}" id="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Commissione</b>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::COMMISSIONE }}}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Tariffa</b>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::RATE }}}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="package_multiplier"><b>Moltiplicatore Pacchetto</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PACKAGE_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_multiplier"><b>Coefficiente Predizione per gruppo persone</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_PREDICT_COEFF }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_predict_multiplier"><b>Moltiplicatore Persone Predizione</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_PREDICT_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_multiplier"><b>Moltiplicatore Persone Finale</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-tachometer-alt-fastest"></i> TEST!
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-funnel-dollar" style="color: green;"></i> <b>Risultati Test</b>
            </h4>
        </div>
        <div class="card-body">

            <h2>Commissione</h2>
            Variabili disponibili:<br>
            <ul>
                @forelse( $commissioneVariables as $key => $cv )
                    <li>{{ $key }} => {{ $cv }}</li>
                @empty
                    <li>Nessun Variabile Disponibile</li>
                @endforelse
            </ul>

            <p><i>Calcolo:</i> {{ $commissione->parseCalculation() }}</p>

            <p><b>Risultato:</b> {{ $commissioneVal }}</p>

            <hr>

            <h2>Tariffa</h2>
            Variabili Disponibili:<br>
            <ul>
                @forelse( $tariffaVariables as $key => $tv )
                    <li>{{ $key }} => {{ $tv }}</li>
                @empty
                    <li>Nessun Variabile Disponibile</li>
                @endforelse
            </ul>

            <p><i>Calcolo:</i> {{ $tariffa->parseCalculation() }}</p>

            <p><b>Risultato:</b> {{ $tariffaVal }}</p>

            <hr>

            <h2>Predizione</h2>
            Variabili Disponibili:<br>
            <ul>
                @forelse( $predizioneVariables as $key => $pv )
                    <li>{{ $key }} => {{ $pv }}</li>
                @empty
                    <li>Nessun Variabile Disponibile</li>
                @endforelse
            </ul>

            <p><i>Calcolo:</i> {{ $predizione->parseCalculation() }}</p>

            <p><b>Risultato:</b> {{ $predizioneVal }}</p>

            <hr>

            <h2>Calcolo Finale</h2>
            <p><i>Calcolo:</i> {{ $finale->parseCalculation() }}</p>
            @forelse( $finalePrices as $fp )
                <p>Package: {{ $fp['package']->name }}</p><br>
                Variabili Disponibili:<br>
                <ul>
                    @forelse( $fp['vars'] as $key => $fv )
                        <li>{{ $key }} => {{ $fv }}</li>
                    @empty
                        <li>Nessuna Variabile Disponibile</li>
                    @endforelse
                </ul>

                <p><b>Risultato:</b> {{ $fp['value'] }}</p>
            @empty
            @endforelse

        </div>
    </div>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

        } );
    </script>

@endsection