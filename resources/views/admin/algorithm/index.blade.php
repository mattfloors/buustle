@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Gestione Algoritmi Pagamenti
            </h4>
        </div>
    </div>

    <form class="form" method="post" action="{{ route('admin.algorithm.test') }}">
        @csrf
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-function" style="color:#007fff"></i> Variabili Disponibili
                </h4>
            </div>
            <div class="card-body">
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::PEOPLE }}"><b>Persone</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::PEOPLE }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" name="{{ \App\Http\Services\RentalPrice::PEOPLE }}" id="{{ \App\Http\Services\RentalPrice::PEOPLE }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}"><b>Km Tratta</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::KM_TRATTA }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" step="any" name="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}" id="{{ \App\Http\Services\RentalPrice::KM_TRATTA }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}"><b>Giorni di Viaggio</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" step="any" name="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}" id="{{ \App\Http\Services\RentalPrice::DAYS_OF_TRAVEL }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}"><b>Giorni dalla partenza</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" step="any" name="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}" id="{{ \App\Http\Services\RentalPrice::DAYS_FROM_START }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}"><b>Km al Giorno</b></label>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}}
                    </div>
                    <div class="col-md-4">
                        <input type="number" step="any" name="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}" id="{{ \App\Http\Services\RentalPrice::KM_PER_DAY }}" class="form-control">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Commissione</b>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::COMMISSIONE }}}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <b>Tariffa</b>
                    </div>
                    <div class="col-md-4">
                        {{{ \App\Http\Services\RentalPrice::RATE }}}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="package_multiplier"><b>Moltiplicatore Pacchetto</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PACKAGE_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_multiplier"><b>Coefficiente Predizione per gruppo persone</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_PREDICT_COEFF }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_predict_multiplier"><b>Moltiplicatore Persone Predizione</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_PREDICT_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row myRow">
                    <div class="col-md-4">
                        <label for="people_range_multiplier"><b>Moltiplicatore Persone Finale</b></label>
                    </div>
                    <div class="col-md-4">
                        {{ \App\Http\Services\RentalPrice::PEOPLE_RANGE_MULTIPLIER }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-tachometer-alt-fastest"></i> TEST!
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    <form action="{{ route('admin.algorithm.store') }}" method="post">
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-funnel-dollar" style="color: yellow;"></i> <label for="alg_{{ \App\Models\Algorithm::COMMISSIONE }}">Calcolo Commissione</label>
                </h4>
            </div>
            <div class="card-body">
                <p>Attenzione, questo algoritmo è valido per tutte le richieste entro <a href="{{ route('admin.configurations.listGeneral') }}">{{ setting('general.days_before_start_limit') ?? 0 }} giorni</a> dalla partenza.<br>
                    Oltre questo valore la commissione applicata sarà <a href="{{ route('admin.configurations.listGeneral') }}">{{ setting('general.days_before_start_limit_price') ?? 1 }}</a></p>

                @csrf
                <input type="hidden" name="name" value="{{ \App\Models\Algorithm::COMMISSIONE }}">
                <textarea name="algorithm" class="form-control" id="alg_{{ \App\Models\Algorithm::COMMISSIONE }}" rows="4">@if( $commissione !== null){{ $commissione->algorithm }}@endif</textarea>
                @if( $commissione !== null )<small>{{ $commissione->parseCalculation() }}</small>@endif

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $commissione !== null )
        <!-- Commenti Commissione -->
        @include('admin.shared.notes', ['model' => 'App\Models\Algorithm', 'id' => $commissione->id, 'notes' => $commissione->notes])
        <!-- Commenti Commissione -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Algorithm', 'id' => $commissione->id, 'loadedModel' => $commissione])
        <!-- /Audits -->
    @endif

    <form action="{{ route('admin.algorithm.store') }}" method="post">
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-dollar-sign" style="color: darkgreen;"></i> <label for="alg_{{ \App\Models\Algorithm::TARIFFA }}">Calcolo Tariffa</label>
                </h4>
            </div>
            <div class="card-body">

                @csrf
                <input type="hidden" name="name" value="{{ \App\Models\Algorithm::TARIFFA }}">
                <textarea name="algorithm" class="form-control" id="alg_{{ \App\Models\Algorithm::TARIFFA }}" rows="4">@if( $tariffa !== null){{ $tariffa->algorithm }}@endif</textarea>
                @if( $tariffa !== null )<small>{{ $tariffa->parseCalculation() }}</small>@endif

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $tariffa !== null )
        <!-- Commenti Tariffa -->
        @include('admin.shared.notes', ['model' => 'App\Models\Algorithm', 'id' => $tariffa->id, 'notes' => $tariffa->notes])
        <!-- Commenti Tariffa -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Algorithm', 'id' => $tariffa->id, 'loadedModel' => $tariffa])
        <!-- /Audits -->
    @endif

    <form action="{{ route('admin.algorithm.store') }}" method="post">
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-low-vision" style="color: #6b0392;"></i> <label for="alg_{{ \App\Models\Algorithm::PREVISIONE }}">Calcolo Predizione Prezzo</label>
                </h4>
            </div>
            <div class="card-body">

                @csrf
                <input type="hidden" name="name" value="{{ \App\Models\Algorithm::PREVISIONE }}">
                <textarea name="algorithm" class="form-control" id="alg_{{ \App\Models\Algorithm::PREVISIONE }}" rows="4">@if( $previsione !== null){{ $previsione->algorithm }}@endif</textarea>
                @if( $previsione !== null )<small>{{ $previsione->parseCalculation() }}</small>@endif

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $previsione !== null )
        <!-- Commenti Finale -->
        @include('admin.shared.notes', ['model' => 'App\Models\Algorithm', 'id' => $previsione->id, 'notes' => $previsione->notes])
        <!-- Commenti Finale -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Algorithm', 'id' => $previsione->id, 'loadedModel' => $previsione])
        <!-- /Audits -->
    @endif

    <form action="{{ route('admin.algorithm.store') }}" method="post">
        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-sack-dollar" style="color: red;"></i> <label for="alg_{{ \App\Models\Algorithm::FINALE }}">Calcolo Finale</label>
                </h4>
            </div>
            <div class="card-body">

                @csrf
                <input type="hidden" name="name" value="{{ \App\Models\Algorithm::FINALE }}">
                <textarea name="algorithm" class="form-control" id="alg_{{ \App\Models\Algorithm::FINALE }}" rows="4">@if( $finale !== null){{ $finale->algorithm }}@endif</textarea>
                @if( $finale !== null )<small>{{ $finale->parseCalculation() }}</small>@endif

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $finale !== null )
        <!-- Commenti Finale -->
        @include('admin.shared.notes', ['model' => 'App\Models\Algorithm', 'id' => $finale->id, 'notes' => $finale->notes])
        <!-- Commenti Finale -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Algorithm', 'id' => $finale->id, 'loadedModel' => $finale])
        <!-- /Audits -->
    @endif

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

        } );
    </script>

@endsection