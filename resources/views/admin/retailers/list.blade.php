@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Retailers  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Fornitori
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    <a href="{{ route('admin.retailers.list', ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.retailers.create', ['lang' => $language]) }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Attivo</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $retailers as $retailer )
                                    <tr>
                                        <td>
                                            {{ $retailer->id }}
                                        </td>
                                        <td>
                                            {{ $retailer->getTranslation('name', $language) }}
                                        </td>
                                        <td>
                                            @if( $retailer->active )
                                                <i class="fas fa-check-circle" style="color:green"></i>
                                            @else
                                                <i class="fas fa-times-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.retailers.update', ['retailer' => $retailer, 'lang' => $language]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                                            <a href="{{ route('admin.retailers.delete', ['retailer' => $retailer, 'lang' => $language]) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i> Elimina</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun fornitore trovato</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Attivo</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Services -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection