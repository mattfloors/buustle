@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Fornitore  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Fornitore -@if( !empty($retailer->id) ) #{{ $retailer->id }} {{ $retailer->getTranslation('name', $language) }} @else Nuovo Fornitore @endif
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    @if( $retailer->id === null )
                                        <a href="{{ route(\Request::route()->getName(), ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @else
                                        <a href="{{ route(\Request::route()->getName(), ['retailer' => $retailer, 'lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @endif
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.retailers.store', ['lang' => $language]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $retailer->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $retailer->getTranslation('name', $language) }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Attivo</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="active" name="active" value="1" class="form-check-input"@if( $retailer->active ) checked="checked"@endif>Il Fornitore è attivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="description" class="form-control-label">Descrizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="description" id="description" class="form-control" rows="5">{{ $retailer->getTranslation('description', $language) }}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Fornitore -->

    @if( $retailer->id !== null )
        <!-- Commenti Marker -->
            @include('admin.shared.notes', ['model' => 'App\Models\Retailer', 'id' => $retailer->id, 'notes' => $retailer->notes])
        <!-- Commenti marker -->
    @endif
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection