@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Rent Request  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Gestione Richiesta # {{ $rentRequest->id }}
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Data Partenza</td>
                                <td>{{ \Carbon\Carbon::parse($rentRequest->getFirstStep()->start_time)->format('d/m/Y H:i') }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Luogo Partenza</td>
                                <td>{{ $rentRequest->getFirstStep()->start_address }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Stato Richiesta</td>
                                <td>{{ ucfirst($rentRequest->status) }} <i class="fas fa-question-circle" data-toggle="tooltip" id="tooltip"></i></td>
                                <td>
                                    @can('change request status')
                                        <form action="{{ route('admin.rentRequests.changeStatus', ['rentRequest' => $rentRequest]) }}" method="post" class="form-inline">
                                            @csrf
                                    @endcan
                                        <div class="form-group mb-2">
                                            <select name="status" class="form-control inline"@cannot('change request status') disabled="disabled"@endcannot>
                                                <option value="completed"@if($rentRequest->status === 'completed') selected @endif>Completed</option>
                                                <option value="closed"@if($rentRequest->status === 'closed') selected @endif>Closed</option>
                                                <option value="sold"@if($rentRequest->status === 'sold') selected @endif>Sold</option>
                                            </select>
                                        </div>
                                    @can('change request status')
                                        <div class="form-group mb-2">
                                            <button type="submit" class="btn btn-outline-warning"><i class="far fa-save"></i></button>
                                        </div>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                            <tr>
                                <td>Richiedente</td>
                                <td>{{ $rentRequest->userEmail() }}</td>
                                <td>
                                    @if( ! empty( $rentRequest->user() ) )
                                        <a class="btn btn-outline-primary" href="{{ route('admin.users.update', ['user' => $rentRequest->user]) }}"><i class="far fa-search"></i></a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Data Richiesta</td>
                                <td>{{ \Carbon\Carbon::parse($rentRequest->created_at)->format('d/m/Y H:i') }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Km Stimati</td>
                                <td>{{ $rentRequest->kilometers }} km</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Prezzo Accettato</td>
                                <td>{{ $rentRequest->accepted_price }} €</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Giorni di viaggio</td>
                                <td>{{ $rentRequest->totalDays() }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Km per Giorno</td>
                                <td>{{ round($rentRequest->kilometers / $rentRequest->totalDays(), 2) }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Offerta Emessa</td>
                                <td>{{ $rentRequest->daysBeforeStart($rentRequest->created_at) }} giorni prima della partenza</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div> <!-- /.row -->
            </div>

            <!-- Steps -->
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Step del Viaggio
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Da</th>
                                <th>A</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $rentRequest->requestSteps()->orderBy('start_time', 'ASC')->get() as $step )
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($step->start_time)->format('d/m/Y H:i') }}</td>
                                    <td>{{ $step->start_address }}</td>
                                    <td>{{ $step->end_address }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Data</th>
                                <th>Da</th>
                                <th>A</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div> <!-- /.row -->
            </div>

            <!-- Servizi -->
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Servizi Richiesti
                    </h4>
                </div>
                <div class="card-body">

                    <div class="typo-articles">

                        <ul>
                            @forelse( $rentRequest->services as $service )
                                <li><i class="{{ $service->icon }}"></i> {{ $service->name }}</li>
                            @empty
                                <li>Nessun Servizio Richiesto</li>
                            @endforelse
                        </ul>

                    </div>

                </div> <!-- /.row -->
            </div>

            <!-- Commento -->
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Commenti Aggiuntivi
                    </h4>
                </div>
                <div class="card-body">

                    <div class="typo-articles">

                        <p>
                            @if( empty( $rentRequest->comment ) )
                                Nessun Commento Lasciato
                            @else
                                {{ $rentRequest->comment }}
                            @endif
                        </p>

                    </div>

                </div> <!-- /.row -->
            </div>

            <!-- Audits -->
            @include('admin.shared.audits', ['model' => 'App\Models\RentRequest', 'id' => $rentRequest->id, 'loadedModel' => $rentRequest])
            <!-- /Audits -->

        </div><!-- /# column -->
    </div>
    <!--  /Rent Request -->

    <!-- Commenti Marker -->
        @include('admin.shared.notes', ['model' => 'App\Models\RentRequest', 'id' => $rentRequest->id, 'notes' => $rentRequest->notes])
    <!-- Commenti marker -->

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $("#tooltip").tooltip({
                html: true,
                container: 'body',
                title: '<b>Completed:</b> Richiesta Confermata ma nessun preventivo inviato<br><b>Closed:</b> Preventivo Inviato<br><b>Payed:</b> Preventivo accettato e pagato'
            });
        } );
    </script>

@endsection