@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Rent Requests  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Richieste di Preventivo da Evadere
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Passeggeri</th>
                                    <th>KM</th>
                                    <th>Partenza</th>
                                    <th>Data Partenza</th>
                                    <th>Shared</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $rentRequests as $r )
                                    <tr>
                                        <td>
                                            {{ $r->id }}
                                        </td>
                                        <td class="text-center">
                                            {{ $r->people }}
                                        </td>
                                        <td>
                                            {{ $r->kilometers }}
                                        </td>
                                        <td>
                                            {{ str_limit($r->getFirstStep()->start_address, 10) }}
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($r->getFirstStep()->start_time)->format('d/m/Y H:i') }}
                                        </td>
                                        <td>
                                            @if( $r->sharable )
                                                <i class="fas fa-check-circle" style="color:green"></i>
                                            @else
                                                <i class="fas fa-times-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.rentRequests.view', ['rentRequest' => $r]) }}" class="btn btn-outline-dark"><i class="far fa-search"></i></a>
                                            <a href="{{ route('admin.quotes.listFor', ['rentRequest' => $r]) }}" class="btn btn-outline-success"><i class="far fa-file-invoice-dollar"></i> {{ count( $r->quotes ) }}</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessuna richiesta noleggio da evadere trovata</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Passeggeri</th>
                                    <th>KM</th>
                                    <th>Partenza</th>
                                    <th>Data Partenza</th>
                                    <th>Shared</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Services -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection