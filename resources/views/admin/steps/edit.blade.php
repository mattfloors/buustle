@extends('admin.layout.default')

@section('css-header')
    @parent

    <link rel="stylesheet" href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/select2/css/select2.min.css') }}">

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Gestione singolo tragitto
                <span class="pull-right">
                    <a href="{{ route('admin.steps.distribusionSteps', ['rentRequest' => $quoteMarker->quote->request]) }}"><i class="fas fa-backward"></i> Torna Indietro</a>
                    <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $quoteMarker->quote->request]) }}"><i class="far fa-external-link"></i> Vedi la Richiesta</a>
                    <a target="_blank" href="{{ route('admin.quotes.edit', ['rentRequest' => $quoteMarker->quote->request, 'quote' => $quoteMarker->quote]) }}"><i class="far fa-external-link"></i> Vedi il Preventivo</a>
                </span>
            </h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-info" style="color:#007fff"></i> Informazioni
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <b>Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ $quoteMarker->start_marker->country }} - {{ $quoteMarker->start_marker->city_name }} - {{ $quoteMarker->start_marker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.markers.update', ['marker' => $quoteMarker->start_marker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Arrivo</b>
                </div>
                <div class="col-md-8">
                    {{ $quoteMarker->end_marker->country }} - {{ $quoteMarker->end_marker->city_name }} - {{ $quoteMarker->end_marker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.markers.update', ['marker' => $quoteMarker->end_marker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Orario Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ \Carbon\Carbon::parse( $quoteMarker->start_time )->formatLocalized('%A, %e %B %G %H:%m') }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Km Stimati</b>
                </div>
                <div class="col-md-8">
                    ~ {{ round($quoteMarker->estimated_km) }} km
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Tempo Stimato</b>
                </div>
                <div class="col-md-8">
                    ~ {{ \Carbon\CarbonInterval::seconds($quoteMarker->estimated_time)->cascade() }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Posti Disponibili</b>
                </div>
                <div class="col-md-8">
                    {{ ( $quoteMarker->slots - $quoteMarker->boughtSlots() ) }}
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-badge-dollar" style="color:#ff0000"></i> Biglietti Venduti
                <span class="pull-right">
                    <a href="{{ route('admin.steps.addTicket', ['quoteMarker' => $quoteMarker]) }}" class="btn btn-info"><i class="fas fa-plus fa-2x"></i></a>
                </span>
            </h4>
        </div>
        <div class="card-body">
            <div class="row" style="margin-top: 1.5em">
                <div class="col-md-2 text-center">#</div>
                <div class="col-md-2 text-center">Persone</div>
                <div class="col-md-2 text-center">Prezzo</div>
                <div class="col-md-2 text-center">Attraverso</div>
                <div class="col-md-2 text-center">Ref</div>
                <div class="col-md-2 text-center">
                    Azioni
                </div>
            </div>
            @forelse( $quoteMarker->soldMarkers as $soldMarker )
                <div class="row" style="margin-top: 1.5em;">
                    <div class="col-md-2 text-center">{{ $soldMarker->id }}</div>
                    <div class="col-md-2 text-center">{{ $soldMarker->slots }}</div>
                    <div class="col-md-2 text-center">{{ $soldMarker->price }} €</div>
                    <div class="col-md-2 text-center">{{ $soldMarker->through }}</div>
                    <div class="col-md-2 text-center">{{ $soldMarker->reference }}</div>
                    <div class="col-md-2 text-center">
                        <a href="{{ route('admin.steps.editTicket', ['quoteMarker' => $quoteMarker, 'soldMarker' => $soldMarker]) }}" class="btn btn-danger"><i class="far fa-file-edit"></i></a>
                        {!! QrCode::generate(route('admin.steps.checkTicket', ['soldMarkerUuid' => $soldMarker->uuid])) !!}
                    </div>
                </div>
            @empty
                <div class="row">
                    <div class="col-md-12 text-center"><b>Non ci sono biglietti venduti</b></div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center"><a href="{{ route('admin.steps.addTicket', ['quoteMarker' => $quoteMarker]) }}">Aggiungi un Biglietto Venduto</a></div>
                </div>
            @endforelse
        </div>
    </div>

    @if( $quoteMarker->id !== null )
        <!-- Commenti Marker -->
        @include('admin.shared.notes', ['model' => 'App\Models\QuoteMarker', 'id' => $quoteMarker->id, 'notes' => $quoteMarker->notes])
        <!-- Commenti marker -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\QuoteMarker', 'id' => $quoteMarker->id, 'loadedModel' => $quoteMarker])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $(".markerEdit").tooltip({
                title: 'Edita il marker. Attenzione: varrà per tutti gli step che hanno questo marker'
            });

        } );
    </script>

@endsection