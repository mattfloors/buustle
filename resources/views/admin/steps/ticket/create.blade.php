@extends('admin.layout.default')

@section('css-header')
    @parent

    <link rel="stylesheet" href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/select2/css/select2.min.css') }}">

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Gestione Biglietto Venduto
                <span class="pull-right">
                    <a href="{{ route('admin.steps.edit', ['quoteMarker' => $quoteMarker]) }}"><i class="fas fa-backward"></i> Torna Indietro</a>
                    <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $quoteMarker->quote->request]) }}"><i class="far fa-external-link"></i> Vedi la Richiesta</a>
                    <a target="_blank" href="{{ route('admin.quotes.edit', ['rentRequest' => $quoteMarker->quote->request, 'quote' => $quoteMarker->quote]) }}"><i class="far fa-external-link"></i> Vedi il Preventivo</a>
                </span>
            </h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-info" style="color:#007fff"></i> Informazioni
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <b>Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ $quoteMarker->start_marker->country }} - {{ $quoteMarker->start_marker->city_name }} - {{ $quoteMarker->start_marker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.markers.update', ['marker' => $quoteMarker->start_marker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Arrivo</b>
                </div>
                <div class="col-md-8">
                    {{ $quoteMarker->end_marker->country }} - {{ $quoteMarker->end_marker->city_name }} - {{ $quoteMarker->end_marker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.markers.update', ['marker' => $quoteMarker->end_marker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Orario Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ \Carbon\Carbon::parse( $quoteMarker->start_time )->formatLocalized('%A, %e %B %G %H:%m') }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Km Stimati</b>
                </div>
                <div class="col-md-8">
                    ~ {{ round($quoteMarker->estimated_km) }} km
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Tempo Stimato</b>
                </div>
                <div class="col-md-8">
                    ~ {{ \Carbon\CarbonInterval::seconds($quoteMarker->estimated_time)->cascade() }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Posti Disponibili</b>
                </div>
                <div class="col-md-8">
                    {{ ( $quoteMarker->slots - $quoteMarker->boughtSlots() ) }}
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('admin.steps.storeTicket', ['quoteMarker' => $quoteMarker]) }}" method="post" class="form">
        @csrf

        <div class="card">
            <div class="card-header">
                <h4 class="box-title">
                    <i class="fas fa-2x fa-badge-dollar" style="color:#ff0000"></i> Biglietto / i
                </h4>
            </div>
            <div class="card-body">

                <input type="hidden" name="id" id="id" value="{{ $soldMarker->id }}">

                <div class="form-row">
                    <div class="col-md-5"><label for="slots">Persone *</label></div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="far fa-user"></i></div>
                            <input type="number" id="slots" name="slots" step="any" min="1" value="{{ $soldMarker->slots }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-row" style="margin-top:1.5em;">
                    <div class="col-md-5"><label for="price">Prezzo *</label></div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="far fa-euro-sign"></i></div>
                            <input type="number" id="price" name="price" step="any" min="0" value="{{ $soldMarker->price }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-row" style="margin-top:1.5em;">
                    <div class="col-md-5"><label for="through">Venduto Tramite</label></div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <div id="throughInfo" class="input-group-addon"><i class="far fa-info"></i></div>
                            <input type="text" id="through" placeholder="Distribusion" name="through" value="{{ $soldMarker->through }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-row" style="margin-top:1.5em;">
                    <div class="col-md-5"><label for="reference">Identificativo Esterno</label></div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <div id="referenceInfo" class="input-group-addon"><i class="far fa-fingerprint"></i></div>
                            <input type="text" id="reference" name="reference" value="{{ $soldMarker->reference }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-row" style="margin-top:1.5em;">
                    <div class="col-md-5"><label for="owner">Proprietario</label></div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <div id="referenceInfo" class="input-group-addon"><i class="far fa-crown"></i></div>
                            <input type="text" id="owner" name="owner" value="{{ $soldMarker->owner }}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-row" style="margin-top:1.5em;">
                    <div class="col-md-12"><label for="comment">Commento Aggiuntivo (Leggibile dall'utente)</label></div>
                    <div class="col-md-12">
                        <textarea class="form-control" rows="6" name="comment" id="comment">{{ $soldMarker->comment }}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fas fa-save"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fas fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $soldMarker->id !== null )
        <!-- Commenti Marker -->
            @include('admin.shared.notes', ['model' => 'App\Models\SoldMarker', 'id' => $soldMarker->id, 'notes' => $soldMarker->notes])
        <!-- Commenti marker -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\SoldMarker', 'id' => $soldMarker->id, 'loadedModel' => $soldMarker])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $("#throughInfo").tooltip({
                title: 'Serve per segnare il metodo di vendita sia Distribusion o altro'
            });

            $("#referenceInfo").tooltip({
                title: 'Identificativo per riferimento esterno come l\'id della transazione distribusion'
            });

        } );
    </script>

@endsection