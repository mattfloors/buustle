@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Steps  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Viaggi per richiesta # {{ $rentRequest->id }} per preventivo # {{ $quote->id }}
                        <span class="pull-right">
                            <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $rentRequest]) }}"><i class="far fa-external-link"></i> Vedi la Richiesta</a>
                            <a target="_blank" href="{{ route('admin.quotes.edit', ['rentRequest' => $rentRequest, 'quote' => $quote]) }}"><i class="far fa-external-link"></i> Vedi il Preventivo</a>
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <div class="row form-group">
                                        <div class="col col-md-12">
                                            <form method="post" class="form" action="{{ route('admin.steps.autoGenerate', ['rentRequest' => $rentRequest]) }}">
                                                @csrf
                                                <div class="input-group pull-right">
                                                    <label for="slots">Capienza Autobus:</label><input type="number" name="slots" id="slots" class="form-control col-md-1 pull-right">
                                                    <div class="input-group-btn pull-right"><button class="text-right btn btn-warning pull-right" type="submit"><i class="far fa-retweet-alt"></i> Rigenera tutti</button></div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Partenza</th>
                                    <th>Arrivo</th>
                                    <th>Data</th>
                                    <th>Posti Disponibili</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $quote->markers as $m )
                                    <tr>
                                        <td>{{ $m->start_marker->city_name }} - {{ $m->start_marker->street_and_number }}</td>
                                        <td>{{ $m->end_marker->city_name }} - {{ $m->end_marker->street_and_number }}</td>
                                        <td>{{ \Carbon\Carbon::parse( $m->start_time )->formatLocalized('%A, %e %B %g %H:%m') }}</td>
                                        <td>{{ ($m->slots - $m->boughtSlots()) }} / {{ $m->slots }}</td>
                                        <td><a href="{{ route('admin.steps.edit', ['QuoteMarker' => $m]) }}" class="btn btn-outline-primary"><i class="far fa-pen-fancy"></i></a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessuno step in vendita trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Partenza</th>
                                    <th>Arrivo</th>
                                    <th>Data</th>
                                    <th>Posti Disponibili</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Steps -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection