@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Travels  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Viaggi
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    <a href="{{ route('admin.travels.list', ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-body">
                                I Viaggi sono un insieme di step fatti dallo stesso autobus.<br>
                                Possono essere derivanti da una messa in vendita di vuoti da parte di noleggiatori oppure generati da dei preventivi accettati<br>
                                I biglietti in vendita sono legati agli step di dei viaggi.<br>
                                Per <b>STEP</b> si intende la strada tra due fermate consecutive, non l'itinerario completo.<br>
                                Per <b>VIAGGIO</b> si intende l'itinerario completo del veicolo, composto da 1 o più step
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.travels.create', ['lang' => $language]) }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display compact nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Data Partenza</th>
                                    <th>Luogo Partenza</th>
                                    <th>Data Arrivo</th>
                                    <th>Luogo Arrivo</th>
                                    <th>Generato</th>
                                    <th>Status</th>
                                    <th>Attivo</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $travels as $travel )
                                    <tr>
                                        <td>
                                            {{ $travel->id }}
                                        </td>
                                        @if( !empty( $travel->getFirstStep() ) )
                                            <td data-order="{{ \Carbon\Carbon::parse($travel->getFirstStep()->start_time)->timestamp }}">
                                                {{ \Carbon\Carbon::parse($travel->getFirstStep()->start_time)->format('d/m/Y H:i') }}
                                            </td>
                                            <td>
                                                {{ $travel->getFirstStep()->startMarker->name }}
                                            </td>
                                        @else
                                            <td data-order="0">
                                                -
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        @endif

                                        @if( !empty( $travel->getLastStep() ) )
                                            <td data-order="{{ \Carbon\Carbon::parse($travel->getLastStep()->end_time)->timestamp }}">
                                                {{ \Carbon\Carbon::parse($travel->getLastStep()->end_time)->format('d/m/Y H:i') }}
                                            </td>
                                            <td>
                                                {{ $travel->getLastStep()->endMarker->name }}
                                            </td>
                                        @else
                                            <td data-order="0">
                                                -
                                            </td>
                                            <td>
                                                -
                                            </td>
                                        @endif
                                        <td>
                                            @if( $travel->quote_id !== null )
                                                Preventivo
                                            @elseif( $travel->owner_id )
                                                Noleggiatore
                                            @else
                                                ???
                                            @endif
                                        </td>
                                        <td>
                                            {{ $travel->status }}
                                        </td>
                                        <td>
                                            {{ $travel->active ? 'Sì' : 'No' }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.travels.update', ['travel' => $travel]) }}" class="btn btn-warning"><i class="far fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun Pacchetto Viaggio trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Data Partenza</th>
                                    <th>Luogo Partenza</th>
                                    <th>Data Arrivo</th>
                                    <th>Luogo Arrivo</th>
                                    <th>Generato</th>
                                    <th>Status</th>
                                    <th>Attivo</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Travels -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable({
                "order": [[ 1, "desc" ]],
                initComplete: function () {
                    this.api().columns([5, 6, 7]).every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            } );
        } );
    </script>
@endsection