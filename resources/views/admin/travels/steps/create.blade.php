@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}">

@endsection

@section('content')

    <!--  Travel  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Gestione Step -@if( !empty($step->id) ) #{{ $step->id }} @else Nuovo Step @endif
                        <span class="pull-right"><a href="{{ route('admin.travels.update', ['travel' => $travel]) }}">Torna al Viaggio <i class="fas fa-external-link"></i></a></span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.travels.step.store', ['travel' => $travel]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $step->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="start_time" class="form-control-label">Orario di Partenza</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" autocomplete="off" class="form-control" id="start_time" name="start_time" value="{{ $step->start_time }}">
                                        <small class="help-block form-text">Orario di partenza previsto. In base a questo valore lo step verrà ordinato nel viaggio</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="start_marker_id" class="form-control-label">Luogo di Partenza</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select style="width: 100%" name="start_marker_id" id="start_marker_id" class="form_control">
                                            @if( $step->start_marker_id !== null )
                                                <option value="{{ $step->startMarker->id }}" selected="selected">{{ $step->startMarker->city_name }} - {{ $step->startMarker->full_address }}</option>
                                            @else
                                                <option value=""> - </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="end_time" class="form-control-label">Orario di Arrivo</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" autocomplete="off" class="form-control" id="end_time" name="end_time" value="{{ $step->end_time }}" disabled="disabled">
                                        <small class="help-block form-text">L'orario di arrivo viene generato automaticamente</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="end_marker_id" class="form-control-label">Luogo di Arrivo</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select style="width: 100%" name="end_marker_id" id="end_marker_id" class="form_control">
                                            @if( $step->end_marker_id !== null )
                                                <option value="{{ $step->endMarker->id }}" selected="selected">{{ $step->endMarker->city_name }} - {{ $step->endMarker->full_address }}</option>
                                            @else
                                                <option value=""> - </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="slots" class="form-control-label">Slots Totali</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" min="{{ $step->boughtSlots() }}" class="form-control" id="slots" name="slots" value="{{ $step->slots }}">
                                        <small class="help-block form-text">Considera gli slot vuoti totali PRIMA della vendita di biglietti</small>
                                        @if( $step->boughtSlots() > 0 )
                                            <small class="help-block form-text" style="color:red">Attenzione, non si possono mettere meno di {{ $step->boughtSlots() }} posti in quanto già venduti</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="distribusion_code" class="form-control-label">Codice Distribusion</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" id="distribusion_code" name="distribusion_code" value="{{ $step->distribusion_code }}">
                                        <small class="help-block form-text">Se non inserito su distribusion lasciare vuoto</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="price" class="form-control-label">Prezzo per singolo posto</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fas fa-euro-sign"></i></div>
                                            <input step="any" type="number" id="price" name="price" value="{{ $step->price }}" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Travel -->

    @if( $step->id !== null )
        <!-- TravelStep Package -->
            @include('admin.shared.notes', ['model' => 'App\Models\TravelStep', 'id' => $step->id, 'notes' => $step->notes])
        <!-- TravelStep Package -->

        <!-- Audits -->
            @include('admin.shared.audits', ['model' => 'App\Models\TravelStep', 'id' => $step->id, 'loadedModel' => $step])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <script src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.full.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            let select2Options = {
                ajax: {
                    url: '{{ route('admin.markers.search') }}',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    minimumInputLength: 2
                }
            };

            let timePickerOptions = {
                step:15
            };

            $.datetimepicker.setLocale('{{ \Illuminate\Support\Facades\App::getLocale() }}');

            $("#start_time").datetimepicker(timePickerOptions);
            $("#start_marker_id").select2(select2Options);
            $("#end_marker_id").select2(select2Options);

        } );
    </script>

@endsection