@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/summernote/summernote-bs4.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/frontend/css/fileinput.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Travel  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Viaggio -@if( !empty($travel->id) ) #{{ $travel->id }} @else Nuovo Viaggio @endif
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    @if( $travel->id === null )
                                        <a href="{{ route(\Request::route()->getName(), ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @else
                                        <a href="{{ route(\Request::route()->getName(), ['travel' => $travel, 'lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @endif
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.travels.store', ['lang' => $language]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $travel->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="quote_id" class="form-control-label">Preventivo Collegato</label>
                                    </div>
                                    <div class="col col-md-1">
                                        <input type="radio" name="relazione" required class="form-control" value="quote" @if( $travel->quote_id !== null ) checked="checked" @endif>
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <select name="quote_id" class="form-control" id="quote_id">
                                            @forelse( $quotes as $quote )
                                                <option value="{{ $quote->id }}" @if( $quote->id === $travel->quote_id ) selected="selected" @endif>{{ $quote->sku }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Selezionare un preventivo collegato se il viaggio è generato da un preventivo</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="owner_id" class="form-control-label">Noleggiatore Collegato</label>
                                    </div>
                                    <div class="col col-md-1">
                                        <input type="radio" name="relazione" required class="form-control" value="owner" @if( $travel->owner_id !== null ) checked="checked" @endif>
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <select name="owner_id" class="form-control" id="owner_id">
                                            @forelse( $owners as $owner )
                                                <option value="{{ $owner->id }}" @if( $owner->id === $travel->owner_id ) selected="selected" @endif>{{ $owner->email }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Selezionare un noleggiatore collegato se il viaggio è generato da un viaggio a vuoto di un noleggiatore</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="user_id" class="form-control-label">Utente Collegato</label>
                                    </div>
                                    <div class="col col-md-1">
                                        <input type="radio" name="relazione" required class="form-control" value="user" @if( $travel->user_id !== null ) checked="checked" @endif>
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <select name="user_id" class="form-control" id="user_id">
                                            @forelse( $users as $user )
                                                <option value="{{ $user->id }}" @if( $user->id === $travel->user_id ) selected="selected" @endif>{{ $user->email }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Se il viaggio invece è proposto da un utente selezionarne uno</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="budget" class="form-control-label">Budget Retailer</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" class="form-control" name="budget" id="budget" value="{{ $travel->budget }}">
                                        <small class="help-block form-text">Il retailer non indica un noleggiatore ma un fornitore di mezzo non per forza chi richiede il viaggio</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="retailer_id" class="form-control-label">Retailer</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="retailer_id" class="form-control" id="retailer_id">
                                            <option value=""> - </option>
                                            @forelse( $retailers as $retailer )
                                                <option value="{{ $retailer->id }}" @if( $retailer->id === $travel->retailer_id ) selected="selected" @endif>{{ $retailer->name }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Il retailer non indica un noleggiatore ma un fornitore di mezzo non per forza chi richiede il viaggio</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="retailer_status" class="form-control-label">Status Retailer</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="retailer_status" class="form-control" id="retailer_status">
                                            <option value=""> - </option>
                                            @forelse( \App\Models\Travel::rs() as $rs )
                                                <option value="{{ $rs }}" @if( $rs === $travel->retailer_status ) selected="selected" @endif>{{ ucfirst($rs) }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Indica se la proposta del retailer è stata accettata</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="status" class="form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="status" class="form-control" id="status">
                                            @forelse( $travel->statuses() as $status )
                                                <option value="{{ $status }}" @if( $travel->status === $status ) selected="selected" @endif>{{ ucfirst($status) }}</option>
                                            @empty
                                                <option value=""> - </option>
                                            @endforelse
                                        </select>
                                        <small class="help-block form-text">Stato del Viaggio</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Condiviso</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="shared" class="form-check-label ">
                                                    <input type="checkbox" id="shared" name="shared" value="1" class="form-check-input"@if( $travel->shared ) checked="checked"@endif>Il Viaggio è condiviso
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Attivo</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="active" name="active" value="1" class="form-check-input"@if( $travel->active ) checked="checked"@endif>Il Viaggio è attivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Blocca Notifiche</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="notified" name="notified" value="1" class="form-check-input"@if( $travel->notified ) checked="checked"@endif>Le Notifiche Retailer sono Bloccate
                                                </label>
                                            </div>
                                            <small class="help-block form-text">
                                                Se selezionato il sistema non manderà le notifiche ai noleggiatori delle aree interessate con l'informazione "nuovo viaggio nella tua area"<br>
                                                <span class="color-red">
                                                    ATTENZIONE: Il sistema automaticamente seleziona questa checkbox se sono già state inviate notifiche, attenzione a deselezionarlo
                                                </span>
                                            </small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="telefono_autista" class="form-control-label">Numero Telefono Autista</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" step="any" class="form-control" name="telefono_autista" id="telefono_autista" value="{{ $travel->telefono_autista }}">
                                        <small class="help-block form-text">Il noleggiatore ha la posisbilità di modificare questo dato</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="targa_mezzo" class="form-control-label">Targa del Mezzo</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" step="any" class="form-control" name="targa_mezzo" id="targa_mezzo" value="{{ $travel->targa_mezzo }}">
                                        <small class="help-block form-text">Il noleggiatore ha la posisbilità di modificare questo dato</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="description" class="form-control-label">Descrizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="description" id="description" class="form-control" rows="5">{{ $travel->getTranslation('description', $language) }}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>

            <div class="card">
                <form action="{{ route('admin.travels.fileUploads', ['travel' => $travel]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h4 class="card-title">File Allegati Pubblici</h4>
                    </div>
                    <div class="card-body">

                        <table id="public_files_table" class="display compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nome File</th>
                                <th>Tipo</th>
                                <th>Dimensione</th>
                                <th>Caricato</th>
                                <th>Azioni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $travel->getMedia('public_files') as $file )
                                <tr>
                                    <td>{{ $file->file_name }}</td>
                                    <td>{{ $file->mime_type }}</td>
                                    <td>{{ $file->size / 1000 }} KB</td>
                                    <td>{{ \Carbon\Carbon::parse($file->updated_at)->format('d/m/Y H:i') }}</td>
                                    <td>
                                        <a href="{{ route('travelAttachmentDownload', ['media' => $file]) }}" class="btn btn-primary"><i class="fas fa-download"></i></a>
                                        <a href="{{ route('admin.travels.deleteAttachment', ['media' => $file]) }}" class="btn btn-danger"><i class="fas fa-times"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Nessun file Caricato</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nome File</th>
                                <th>Tipo</th>
                                <th>Dimensione</th>
                                <th>Caricato</th>
                                <th>Azioni</th>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="form-group">
                            <input type="hidden" name="file_upload_folder" value="public">
                            <label class="col-sm-3 control-label" for="public_files">
                                Carica dei file pubblici
                            </label>
                            <div class="col-sm-9">
                            <span class="btn btn-default btn-file">
                                <input id="public_files" name="files[]" type="file" multiple>
                            </span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Invia
                        </button>
                    </div>
                </form>
            </div>

            <div class="card">
                <form action="{{ route('admin.travels.fileUploads', ['travel' => $travel]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h4 class="card-title">File Allegati Privati</h4>
                    </div>
                    <div class="card-body">

                        <table id="hidden_files_table" class="display compact" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nome File</th>
                                <th>Tipo</th>
                                <th>Dimensione</th>
                                <th>Caricato</th>
                                <th>Azioni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $travel->getMedia('hidden_files') as $file )
                                <tr>
                                    <td>{{ $file->file_name }}</td>
                                    <td>{{ $file->mime_type }}</td>
                                    <td>{{ $file->size / 1000 }} KB</td>
                                    <td>{{ \Carbon\Carbon::parse($file->updated_at)->format('d/m/Y H:i') }}</td>
                                    <td>
                                        <a href="{{ route('travelAttachmentDownload', ['media' => $file]) }}" class="btn btn-primary"><i class="fas fa-download"></i></a>
                                        <a href="{{ route('admin.travels.deleteAttachment', ['media' => $file]) }}" class="btn btn-danger"><i class="fas fa-times"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Nessun file Caricato</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nome File</th>
                                <th>Tipo</th>
                                <th>Dimensione</th>
                                <th>Caricato</th>
                                <th>Azioni</th>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="form-group">
                            <input type="hidden" name="file_upload_folder" value="hidden">
                            <label class="col-sm-3 control-label" for="hidden_files">
                                Carica dei file privati
                            </label>
                            <div class="col-sm-12">
                            <span class="btn btn-default btn-file">
                                <input id="hidden_files" name="files[]" type="file" multiple>
                            </span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Invia
                        </button>
                    </div>
                </form>
            </div>

            @if( $travel->id !== null )
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Steps</h4>
                        <span class="pull-right">
                            <a href="{{ route('admin.travels.step.create', ['travel' => $travel]) }}" class="btn btn-primary"><i class="fas fa-calendar-plus"></i> Crea un nuovo Step</a>
                        </span>
                    </div>
                    <div class="card-body">
                        <table id="list" class="display compact nowrap" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Data Partenza</th>
                                <th>Luogo Partenza</th>
                                <th>Data Arrivo</th>
                                <th>Luogo Arrivo</th>
                                <th>Posti Disponibili</th>
                                <th>Prezzo</th>
                                <th>Azioni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse( $travel->steps as $step )
                                <tr>
                                    <td>
                                        {{ $step->id }}
                                    </td>
                                    <td data-order="{{ \Carbon\Carbon::parse($step->start_time)->timestamp }}">
                                        {{ \Carbon\Carbon::parse($step->start_time)->format('d/m/Y H:i') }}
                                    </td>
                                    <td>
                                        {{ $step->startMarker->name }}
                                    </td>
                                    <td data-order="{{ \Carbon\Carbon::parse($step->end_time)->timestamp }}">
                                        {{ \Carbon\Carbon::parse($step->end_time)->format('d/m/Y H:i') }}
                                    </td>
                                    <td>
                                        {{ $step->endMarker->name }}
                                    </td>
                                    <td>
                                        {{ ( $step->slots - $step->boughtSlots() ) }} / {{ $step->slots }}
                                    </td>
                                    <td>
                                        {{ $step->price }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.travels.step.update', ['step' => $step]) }}" class="btn btn-warning"><i class="far fa-pencil-alt"></i></a>
                                        <a href="{{ route('admin.tickets.list', ['travel_step_id' => $step->id]) }}" class="btn btn-primary"><i class="far fa-ticket-alt"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>Nessun Pacchetto Viaggio trovato</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Data Partenza</th>
                                <th>Luogo Partenza</th>
                                <th>Data Arrivo</th>
                                <th>Luogo Arrivo</th>
                                <th>Posti Disponibili</th>
                                <th>Prezzo</th>
                                <th>Azioni</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Steps</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            Gli step saranno editabili e visibili quando il viaggio sarà creato<br>
                            Ovvero prima creare il viaggio con i dati, una volta salvato sarà possibile lavorare agli step
                        </p>
                    </div>
                </div>
            @endif
        </div><!-- /# column -->
    </div>
    <!--  /Travel -->

    @if( $travel->id !== null )
        <!-- Commenti Package -->
            @include('admin.shared.notes', ['model' => 'App\Models\Travel', 'id' => $travel->id, 'notes' => $travel->notes])
        <!-- Commenti Package -->

        <!-- Audits -->
            @include('admin.shared.audits', ['model' => 'App\Models\Travel', 'id' => $travel->id, 'loadedModel' => $travel])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <script src="{{ asset('storage/assets/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/fileinput.theme.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#description').summernote({
                placeholder: 'Description',
                tabsize: 2,
                height: 100,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['fullscreen', ['fullscreen']],
                    ['codeview', ['codeview']],
                    ['undo', ['undo']],
                    ['redo', ['redo']]
                ]
            });

            $('#quote_id').select2();
            $('#owner_id').select2();
            $('#user_id').select2();

            $("#list").dataTable({
                "order": [[ 1, "asc" ]],
            });

            $("#public_files").fileinput({
                theme: "fas",
                maxFileSize: 2048
            });

            $("#public_files_table").DataTable();

            $("#hidden_files").fileinput({
                theme: "fas",
                maxFileSize: 2048
            });

            $("#hidden_files_table").DataTable();

        } );
    </script>

@endsection