<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li @if($menuActive === 'dashboard') class="active" @endif>
                    <a href="{{ route('admin.dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li>
                    <a href="/logs"><i class="menu-icon fa fa-books"></i>Logs </a>
                </li>
                <li class="menu-title">Strumenti</li><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown @if($menuActive === 'configurations') show @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon far fa-cog"></i>Configurazione</a>
                    <ul class="sub-menu children dropdown-menu @if($menuActive === 'configurations') show @endif">
                        <li @if($subMenuActive === 'payments') class="active" @endif><i class="far fa-credit-card"></i><a href="{{ route('admin.configurations.listPayments') }}">Pagamenti</a></li>
                        <li @if($subMenuActive === 'distribusion') class="active" @endif><i class="far fa-car"></i><a href="{{ route('admin.configurations.listDistribusion') }}">Distribusion</a></li>
                        <li @if($subMenuActive === 'mail') class="active" @endif><i class="far fa-mailbox"></i><a href="{{ route('admin.configurations.listMail') }}">E-Mail Provider</a></li>
                        <li @if($subMenuActive === 'general') class="active" @endif><i class="far fa-user-hard-hat"></i><a href="{{ route('admin.configurations.listGeneral') }}">Generale</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown @if($menuActive === 'languages') show @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon far fa-flag"></i>Lingue</a>
                    <ul class="sub-menu children dropdown-menu @if($menuActive === 'languages') show @endif">
                        <li @if($subMenuActive === 'translations') class="active" @endif><i class="fas fa-flag"></i><a href="{{ route('admin.languages.listTranslations') }}">Traduzioni</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown @if($menuActive === 'users') show @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon far fa-users-cog"></i>Utenti</a>
                    <ul class="sub-menu children dropdown-menu @if($menuActive === 'users') show @endif">
                        <li @if($subMenuActive === 'users') class="active" @endif><i class="fas fa-users"></i><a href="{{ route('admin.users.list') }}">Utenti</a></li>
                        <li @if($subMenuActive === 'roles') class="active" @endif><i class="fas fa-user-shield"></i><a href="{{ route('admin.roles.list') }}">Ruoli</a></li>
                        <li @if($subMenuActive === 'permissions') class="active" @endif><i class="fas fa-lock-alt"></i><a href="{{ route('admin.permissions.list') }}">Permessi</a></li>
                    </ul>
                </li>
                <li @if($menuActive === 'noleggiatori') class="active" @endif>
                    <a href="{{ route('admin.noleggiatori.list') }}"><i class="menu-icon fa fa-handshake"></i>Noleggiatori </a>
                </li>
                <li @if($menuActive === 'travels') class="active" @endif>
                    <a href="{{ route('admin.travels.list') }}"><i class="menu-icon far fa-bus"></i>Viaggi </a>
                </li>
                <li @if($menuActive === 'tickets') class="active" @endif>
                    <a href="{{ route('admin.tickets.list') }}"><i class="menu-icon far fa-ticket-alt"></i>Tickets </a>
                </li>
                <li @if($menuActive === 'payments') class="active" @endif>
                    <a href="{{ route('admin.payments.list') }}"><i class="menu-icon far fa-money-check-alt"></i>Pagamenti </a>
                </li>
                <li @if($menuActive === 'markers') class="active" @endif>
                    <a href="{{ route('admin.markers.list') }}"><i class="menu-icon far fa-map-marker-alt"></i>Markers </a>
                </li>
                <li @if($menuActive === 'stops') class="active" @endif>
                    <a href="{{ route('admin.stops.list') }}"><i class="menu-icon far fa-map-marker-alt"></i>Stops </a>
                </li>
                <li @if($menuActive === 'distribusion') class="active" @endif>
                    <a href="{{ route('admin.markers.distribusion') }}"><i class="menu-icon fas fa-sync-alt"></i>Distribusion </a>
                </li>
                <li class="menu-title">Viaggi</li><!-- /.menu-title -->
                <li @if($menuActive === 'promocodes') class="active" @endif>
                    <a href="{{ route('admin.promocodes.list') }}"><i class="menu-icon fas fa-barcode"></i>Codici Sconto </a>
                </li>
                <li class="menu-item-has-children dropdown @if($menuActive === 'rentRequests') show @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon far fa-file-invoice"></i>Richieste Preventivi</a>
                    <ul class="sub-menu children dropdown-menu @if($menuActive === 'rentRequests') show @endif">
                        <li @if($subMenuActive === 'rent_requests_new') class="active" @endif><i class="far fa-calendar-plus"></i><a href="{{ route('admin.rentRequests.listNew') }}">Da Evadere @if($requestsToEvade > 0)<span class="badge badge-primary">{{ $requestsToEvade }}</span>@endif</a></li>
                        <li @if($subMenuActive === 'rent_requests_completed') class="active" @endif><i class="far fa-hourglass"></i><a href="{{ route('admin.rentRequests.listCompleted') }}">Con Offerte @if($requestsWithOffers > 0)<span class="badge badge-dark">{{ $requestsWithOffers }}</span>@endif</a></li>
                        <li @if($subMenuActive === 'rent_requests_sold') class="active" @endif><i class="far fa-money-check"></i><a href="{{ route('admin.rentRequests.listSold') }}">Vendute @if($requestsSold > 0)<span class="badge badge-success">{{ $requestsSold }}</span>@endif</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown @if($menuActive === 'travel_settings') show @endif">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon far fa-cogs"></i>Settings</a>
                    <ul class="sub-menu children dropdown-menu @if($menuActive === 'travel_settings') show @endif">
                        <li @if($subMenuActive === 'travel_services') class="active" @endif><i class="fas fa-layer-plus"></i><a href="{{ route('admin.services.list') }}">Servizi</a></li>
                        <li @if($subMenuActive === 'travel_retailers') class="active" @endif><i class="fas fa-bus-alt"></i><a href="{{ route('admin.retailers.list') }}">Fornitori</a></li>
                        <li @if($subMenuActive === 'travel_packages') class="active" @endif><i class="fas fa-box-heart"></i><a href="{{ route('admin.packages.list') }}">Pacchetti Viaggio</a></li>
                        <li @if($subMenuActive === 'travel_people_ranges') class="active" @endif><i class="fas fa-user-edit"></i><a href="{{ route('admin.people_ranges.list') }}">Range Persone</a></li>
                        @can('edit algorithm')
                            <li @if($subMenuActive === 'travel_alg') class="active" @endif><i class="fas fa-function"></i><a href="{{ route('admin.algorithm.index') }}">Algoritmi Prezzi</a></li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>