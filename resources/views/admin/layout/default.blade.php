<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rent Buustle V2</title>
    <meta name="description" content="Rent Buustle - Admin Panel">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('css-header')
        <link rel="apple-touch-icon" href="{{ asset('storage/images/favicon-16x16.png') }}">
        <link rel="shortcut icon" href="{{ asset('storage/images/favicon-16x16.png') }}">

        <link rel="stylesheet" href="{{ asset('storage/assets/css/dist/normalize.min.css') }}">
        <link rel="stylesheet" href="{{ asset('storage/assets/css/dist/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('storage/assets/font-awesome/css/all.min.css') }}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="{{ asset('storage/assets/flags/css/flag-icon.css') }}">
        <link rel="stylesheet" href="{{ asset('storage/assets/css/cs-skin-elastic.css') }}">
        <link rel="stylesheet" href="{{ asset('storage/assets/css/style.css') }}">
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    @show

</head>

<body @if( !$openMenu ) class="open" @endif>

<!-- Left Panel -->
@include('admin.layout.default.left-sidebar')
<!-- /#left-panel -->

<!-- Right Panel -->
<div id="right-panel" class="right-panel">
    <!-- Header-->
    @include('admin.layout.default.header')
    <!-- /#header -->

    <!-- Content -->
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">

            @yield('content')

        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->

    <div class="clearfix"></div>

    <!-- Footer -->
    @include('admin.layout.default.footer')
    <!-- /.site-footer -->

</div>
<!-- /#right-panel -->

<!-- Scripts -->
@section('js-scripts')
    <script src="{{ asset('storage/assets/js/jQuery.min.js') }}"></script>
    <script src="{{ asset('storage/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('storage/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('storage/assets/js/jquery.matchHeight.min.js') }}"></script>
    <script src="{{ asset('storage/assets/js/main.js') }}"></script>
@show

<!--Local Stuff-->
@yield('added-js')

<!-- SweetAlert -->
@include('sweetalert::alert')

</body>
</html>