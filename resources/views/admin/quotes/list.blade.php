@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Preventivi  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Preventivi per richiesta # {{ $rentRequest->id }}
                        <span class="pull-right">
                            <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $rentRequest]) }}"><i class="far fa-external-link"></i> Torna alla richiesta</a>
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.quotes.create', ['rentRequest' => $rentRequest]) }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>SKU</th>
                                    <th>Passeggeri</th>
                                    <th>Prezzo (€)</th>
                                    <th>Status</th>
                                    <th>Valido Al</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $quotes as $q )
                                    <tr>
                                        <td>
                                            {{ $q->id }}
                                        </td>
                                        <td>
                                            {{ $q->sku }}
                                        </td>
                                        <td>
                                            {{ $q->people }}
                                        </td>
                                        <td>
                                            {{ $q->price }}
                                        </td>
                                        <td>
                                            {{ ucfirst($q->status) }}
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($q->valid_until)->format('d/m/Y') }}
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.quotes.edit', ['rentRequest' => $rentRequest, 'quote' => $q]) }}" class="btn btn-outline-success"><i class="far fa-file-edit"></i></a>
                                            <a href="{{ route('admin.quotes.pdf', ['rentRequest' => $rentRequest, 'quote' => $q]) }}" class="btn btn-outline-success"><i class="far fa-file-pdf"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun preventivo trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>SKU</th>
                                    <th>Passeggeri</th>
                                    <th>Prezzo</th>
                                    <th>Status</th>
                                    <th>Valido Al</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Preventivi -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection