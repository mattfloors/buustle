@extends('admin.layout.default')

@section('css-header')
    @parent

    <link rel="stylesheet" href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/select2/css/select2.min.css') }}">

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                @if( $quote->id === null )
                    Nuovo Preventivo
                @else
                    Modifica il preventivo ID {{ $quote->sku }}
                @endif
                <span class="pull-right">
                    <a target="_blank" href="{{ route('admin.rentRequests.view', ['rentRequest' => $rentRequest]) }}"><i class="far fa-external-link"></i> Torna alla richiesta</a>
                </span>
            </h4>
        </div>
    </div>

    <form class="form" action="{{ route('admin.quotes.store', ['rentRequest' => $rentRequest]) }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $quote->id }}">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Dati di Base</strong>
            </div>
            <div class="card-body">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="sku" class="form-control-label">SKU</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-fingerprint"></i></div>
                            <input type="text" id="sku" name="sku" placeholder="" value="{{ $quote->sku }}" class="form-control" required>

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="price" class="form-control-label">Prezzo</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-euro-sign"></i></div>
                            <input type="number" step="any" id="price" name="price" placeholder="" value="{{ $quote->price ?? $rentRequest->accepted_price }}" class="form-control" required>

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="budget" class="form-control-label">Budget</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-euro-sign"></i></div>
                            <input type="number" step="any" id="budget" name="budget" placeholder="" value="{{ $quote->budget }}" class="form-control">

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="people" class="form-control-label">Persone</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-users"></i></div>
                            <input type="number" id="people" name="people" placeholder="" value="{{ $quote->people ?? $rentRequest->people }}" class="form-control" required>

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="valid_until" class="form-control-label">Preventivo valido fino al</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-calendar-exclamation"></i></div>
                            <input type="date" id="valid_until" name="valid_until" placeholder="" value="{{ \Carbon\Carbon::parse($quote->valid_until)->format('Y-m-d') }}" class="form-control" required>

                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="status" class="form-control-label">Status Preventivo</label>
                    </div>
                    <div class="col col-md-9">
                        <select name="status" id="status" class="form-control">
                            <option value="draft" @if( empty($quote->status) || $quote->status === 'draft' ) selected="selected" @endif>Bozza</option>
                            <option value="completed" @if( $quote->status === 'completed' ) selected="selected" @endif>Inviato</option>
                            <option value="accepted" @if( $quote->status === 'accepted' ) selected="selected" @endif>Accettato</option>
                            <option value="payed" @if( $quote->status === 'payed' ) selected="selected" @endif>Pagato</option>
                            <option value="deleted" @if( $quote->status === 'deleted' ) selected="selected" @endif>Eliminato</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong  class="card-title">Fornitore</strong>
            </div>
            <div class="card-body">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="retailer" class="form-control-label">Fornitore</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <select name="retailer" id="retailer" class="form-control">
                                <option value=""> - </option>
                                @forelse( $retailers as $retailer )
                                    <option value="{{ $retailer->id }}" @if($quote->retailer_id === $retailer->id) selected="selected" @endif>{{ $retailer->name }}</option>
                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="retailer_price" class="form-control-label">Pagamento Fornitore</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-euro-sign"></i></div>
                            <input type="number" step="any" id="retailer_price" name="retailer_price" placeholder="" value="{{ $quote->retailer_price }}" class="form-control">

                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="bus_seats" class="form-control-label">Posti Bus</label>
                    </div>
                    <div class="col md-9">
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fas fa-chair-office"></i></div>
                            <input type="number" id="bus_seats" name="bus_seats" placeholder="" value="{{ $quote->bus_seats }}" class="form-control">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Servizi</h4>
            </div>
            <div class="card-body">
                <div class="row form-group" style="margin-left: 15px;">
                    @forelse( $services as $service )
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label for="service-{{ $service->id }}" class="form-check-label ">
                                    <input type="checkbox" id="service-{{ $service->id }}" name="services[]" value="{{ $service->id }}" class="form-check-input" @if( ($quote->id !== null && $quote->services->contains($service->id)) || ($quote->id === null && $rentRequest->services->contains($service->id) ) ) checked="checked" @endif>
                                    <i class="{{ $service->icon }}"></i> {{ $service->name }}
                                </label>
                            </div>
                        </div>
                    @empty
                        <div class="col-md-12">Nessun Servizio Disponibile</div>
                    @endforelse
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong class="card-title">Markers</strong>
            </div>
            <div class="card-body">
                <a class="btn btn-outline-success" data-toggle="collapse" href="#steps" role="button" aria-expanded="false" aria-controls="steps">Step Richiesti <i class="fas fa-eye"></i></a>
                <div class="collapse" id="steps">
                    <br>
                    <ol style="padding-left: 4%">
                        @forelse( $rentRequest->requestSteps as $step )
                            <li>
                                <ul style="padding-left:4%">
                                    <li>Partenza: <b>{{ $step->start_address }}</b> il <u>{{ \Carbon\Carbon::parse($step->start_time)->formatLocalized('%A, %e %B %g') }} alle {{ \Carbon\Carbon::parse($step->start_time)->format('H:i') }}</u></li>
                                    <li>Arrivo: <b>{{ $step->end_address }}</b></li>
                                </ul>
                            </li>
                        @empty
                            @lang('rent.error_no_steps')
                        @endforelse
                    </ol>
                </div>

                <hr>

                <div id="selMarkers">
                    <?php $mI = 0; ?>
                    @forelse( $quote->markers as $marker )
                        <div class="form-row" id="step-{{ $mI }}" style="margin-top:3%">
                            <div class="col col-md-4">
                                <select class="form-control" name="start_marker[{{ $mI }}]" id="start_marker-{{ $mI }}">
                                    <option value="{{ $marker->start_marker->id }}" selected="selected">{{ $marker->start_marker->city_name }} - {{ $marker->start_marker->name }}</option>
                                </select>
                            </div>
                            <div class="col col-md-3">
                                <input type="text" autocomplete="off" value="{{ $marker->start_time }}" class="form-control" name="time[{{ $mI }}]" id="time-{{ $mI }}">
                            </div>
                            <div class="col col-md-4">
                                <select class="form-control" name="end_marker[{{ $mI }}]" id="end_marker-{{ $mI }}">
                                    @if( !empty ( $marker->end_marker ) )
                                        <option value="{{ $marker->end_marker->id }}" selected="selected">{{ $marker->end_marker->city_name }} - {{ $marker->end_marker->name }}</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col col-md-1">
                                <div class="btn btn-dark btn-disabled"><i class="fas fa-times-circle"></i></div>
                            </div>
                        </div>
                        <?php $mI++; ?>
                    @empty
                        <div class="form-row" id="step-0">
                            <div class="col col-md-4">
                                <select class="form-control" name="start_marker[0]" id="start_marker-0">
                                    <option value="">Seleziona un Marker</option>
                                </select>
                            </div>
                            <div class="col col-md-3">
                                <input type="text" class="form-control" name="time[0]" id="time-0">
                            </div>
                            <div class="col col-md-4">
                                <select class="form-control" name="end_marker[0]" id="end_marker-0">
                                    <option value="">Seleziona un Marker</option>
                                </select>
                            </div>
                            <div class="col col-md-1">
                                <div class="btn btn-dark btn-disabled"><i class="fas fa-times-circle"></i></div>
                            </div>
                        </div>
                    @endforelse
                </div>

                <hr>

                <div class="col-md-12">
                    <button type="button" id="addMarker" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Aggiungi Marker</button>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong class="card-title">Pacchetto Viaggio Richiesto</strong>
            </div>
            <div class="card-body">
                <div class="form-row">
                    @forelse( $packages as $package )
                        <div class="col col-md-3">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="package_id" value="{{ $package->id }}" id="package-{{ $package->id }}" @if( ($quote->id === null && $rentRequest->package_id === $package->id) || $quote->package_id === $package->id ) checked="checked" @endif>
                                <label class="form-check-label" for="package-{{ $package->id }}">
                                    <i class="{{ $package->icon }}"></i> {{ $package->name }}
                                </label>
                            </div>
                        </div>
                    @empty
                        Nessun Pacchetto Viaggio Disponibile
                    @endforelse
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong class="card-title">Costi Aggiuntivi (€)</strong>
            </div>
            <div class="card-body">
                <div id="pricesContainer">
                    <?php $pi = 0; ?>
                    @forelse( $quote->prices as $price )
                        <div style="margin-top: 3%; margin-left:3%;" class="form-row" id="price-{{ $pi }}">
                            <div class="col col-md-8 form-check">
                                <input type="text" name="price-reason[{{ $pi }}]" class="form-control" value="{{ $price->reason }}" id="price-reason[{{ $pi }}]">
                            </div>
                            <div class="col md-3">
                                <input type="number" step="any" class="form-control" name="price-price[{{ $pi }}]" id="info-content-{{ $pi }}" value="{{ $price->price }}">
                            </div>
                            <div class="col-md-1"><button class="btn btn-danger" id="deletePrice-{{ $pi }}" type="button"><i class="fas fa-times-circle"></i></button></div>
                        </div>
                        <?php $pi++; ?>
                    @empty

                    @endforelse
                </div>

                <hr>

                <div class="col-md-12">
                    <button type="button" id="addPrice" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Aggiungi Costo</button>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong class="card-title">Informazioni Addizionali</strong>
            </div>
            <div class="card-body">
                <div id="infosContainer">
                    <?php $ii = 0; ?>
                    @forelse( $quote->infos as $infoRow )
                        <div style="margin-top: 3%; margin-left:3%;" class="form-row" id="info-{{ $ii }}">
                            <div class="col col-md-3 form-check">
                                <input type="checkbox" class="form-check-input" name="info-included[{{ $ii }}]" id="info-included[{{ $ii }}]" @if($infoRow->included) checked="checked" @endif>
                                <label class="form-check-label" for="info-included[{{ $ii }}]">
                                    Incluso
                                </label>
                            </div>
                            <div class="col md-8">
                                <input type="text" class="form-control" name="info-content[{{ $ii }}]" id="info-content-{{ $ii }}" placeholder="contenuto" value="{{ $infoRow->infos }}">
                            </div>
                            <div class="col-md-1"><button class="btn btn-danger" id="deleteInfo-{{ $ii }}" type="button"><i class="fas fa-times-circle"></i></button></div>
                        </div>
                        <?php $ii++; ?>
                    @empty

                    @endforelse
                </div>

                <hr>

                <div class="col-md-12">
                    <button type="button" id="addInfo" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Aggiungi Informazione</button>
                </div>

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <strong class="card-title">Commento per l'Utente</strong>
            </div>
            <div class="card-body">

                <div class="form-group">
                    <textarea class="form-control" rows="8" name="comment" id="comment">{{ $quote->comment }}</textarea>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
    </form>

    @if( $quote->id !== null )
        <!-- Commenti Marker -->
        @include('admin.shared.notes', ['model' => 'App\Models\Quote', 'id' => $quote->id, 'notes' => $quote->notes])
        <!-- Commenti marker -->

        <!-- Audits -->
        @include('admin.shared.audits', ['model' => 'App\Models\Quote', 'id' => $quote->id, 'loadedModel' => $quote])
        <!-- /Audits -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('storage/assets/select2/js/select2.min.js') }}" type="text/javascript"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            let markerCount = parseInt('{{ $mI + 1 }}');
            let markersContainer = $("#selMarkers");

            let infosContainer = $("#infosContainer");
            let infosCount = parseInt('{{ $ii }}');

            let priceContainer = $("#pricesContainer");
            let priceCount = parseInt('{{ $pi }}');

            let select2Options = {
                ajax: {
                    url: '{{ route('admin.markers.search') }}',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    minimumInputLength: 2
                }
            };

            let timePickerOptions = {
                step:15
            };

            $.datetimepicker.setLocale('{{ \Illuminate\Support\Facades\App::getLocale() }}');
            @for( $im = 0; $im <= $mI; $im++ )
                $('#time-{{ $im }}').datetimepicker(timePickerOptions);
                $('#start_marker-{{ $im }}').select2(select2Options);
                $('#end_marker-{{ $im }}').select2(select2Options);
            @endfor

            $("#addMarker").on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                addMarker();

                markerCount += 1;
            });

            $("#addInfo").on('click', function(e) {

                e.preventDefault();
                e.stopPropagation();

                let infoTemplate = addTemplateInfo();

                infosContainer.append(infoTemplate);

                bindInfoElements(infosCount);

                infosCount += 1;

            });

            $("#addPrice").on('click', function(e) {

                e.preventDefault();
                e.stopPropagation();

                let priceTemplate = addTemplatePrice();

                priceContainer.append(priceTemplate);

                bindPriceElements(priceCount);

                priceCount += 1;

            });

            function addMarker() {

                let rowToAdd = addTemplate();

                markersContainer.append(rowToAdd);

                bindElements(markerCount);

            }

            function bindInfoElements( rowId ) {

                $("#deleteInfo-" + rowId).on('click', function(e) {

                    e.preventDefault();
                    e.stopPropagation();

                    $("#info-" + rowId).remove();

                });

            }

            function bindPriceElements( rowId ) {

                $("#deletePrice-" + rowId).on('click', function(e) {

                    e.preventDefault();
                    e.stopPropagation();

                    $("#price-" + rowId).remove();

                });

            }

            function bindElements( rowId ) {

                $("#deleteButton-" + rowId).on('click', function(e) {

                    e.preventDefault();
                    e.stopPropagation();

                    $("#step-" + rowId).remove();

                });

                $("#start_marker-" + rowId).select2(select2Options);
                $("#end_marker-" + rowId).select2(select2Options);

                $("#time-" + rowId).datetimepicker(timePickerOptions);

            }

            function addTemplateInfo() {

                return $("<div style=\"margin-top: 3%;\" class=\"form-row\" id=\"info-" + infosCount + "\">\n" +
                    "<div class=\"col col-md-3 form-check\">\n" +
                    "<input type=\"checkbox\" class=\"form-check-input\" name=\"info-included[" + infosCount + "]\" id=\"info-included[" + infosCount + "]\">\n" +
                    "<label class=\"form-check-label\" for=\"info-included[" + infosCount + "]\">\n" +
                    "Incluso\n" +
                    "</label>" +
                    "</div>\n" +
                    "<div class=\"col md-8\">\n" +
                    "<input type=\"text\" class=\"form-control\" name=\"info-content[" + infosCount + "]\" id=\"info-content-" + infosCount + "\" placeholder=\"contenuto\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-1\"><button class=\"btn btn-danger\" id=\"deleteInfo-" + infosCount + "\" type=\"button\"><i class=\"fas fa-times-circle\"></i></button></div>\n" +
                    "</div>");

            }

            function addTemplatePrice() {

                return $("<div style=\"margin-top: 3%; margin-left:3%;\" class=\"form-row\" id=\"price-" + priceCount + "\">\n" +
                    "<div class=\"col col-md-8 form-check\">\n" +
                    "<input type=\"text\" placeholder=\"Ragione Costo\" name=\"price-reason[" + priceCount + "]\" class=\"form-control\" value=\"\" id=\"price-reason[" + priceCount + "]\">\n" +
                    "</div>\n" +
                    "<div class=\"col md-3\">\n" +
                    "<input placeholder=\"10\" type=\"number\" step=\"any\" class=\"form-control\" name=\"price-price[" + priceCount + "]\" id=\"info-content-" + priceCount + "\" value=\"\">\n" +
                    "</div>\n" +
                    "<div class=\"col-md-1\"><button class=\"btn btn-danger\" id=\"deletePrice-" + priceCount + "\" type=\"button\"><i class=\"fas fa-times-circle\"></i></button></div>\n" +
                    "</div>");

            }

            function addTemplate() {

                return $("<div style=\"margin-top: 3%;\" class=\"form-row\" id=\"step-" + markerCount + "\">\n" +
                    "<div class=\"col col-md-4\">\n" +
                    "<select class=\"form-control\" name=\"start_marker[" + markerCount + "]\" id=\"start_marker-" + markerCount + "\">\n" +
                    "<option value=\"\">Seleziona un Marker</option>\n" +
                    "</select>\n" +
                    "</div>\n" +
                    "<div class=\"col col-md-3\">\n" +
                    "<input type=\"text\" autocomplete=\"off\" class=\"form-control\" name=\"time[" + markerCount + "]\" id=\"time-" + markerCount + "\">\n" +
                    "</div>\n" +
                    "<div class=\"col col-md-4\">\n" +
                    "<select class=\"form-control\" name=\"end_marker[" + markerCount + "]\" id=\"end_marker-" + markerCount + "\">\n" +
                    "<option value=\"\">Seleziona un Marker</option>\n" +
                    "</select>\n" +
                    "</div>\n" +
                    "<div class=\"col col-md-1\">\n" +
                    "<button type=\"button\" id='deleteButton-" + markerCount + "' class=\"btn btn-danger deleteMarker\" data-id='" + markerCount + "'><i class=\"fas fa-times-circle\"></i></button>\n" +
                    "</div>\n" +
                    "</div>");

            }
        } );
    </script>

@endsection