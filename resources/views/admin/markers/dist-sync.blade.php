@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    Synced data from {{ $start }} to {{ $newStart }} out of total of {{ $totalToSync }}

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    @if( $allData === $take )
        <script>
            window.location.href = '{!! $route !!}';
        </script>
    @endif

@endsection