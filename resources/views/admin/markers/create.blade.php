@extends('admin.layout.default')

@section('css-header')
    @parent

    <link rel="stylesheet" href="{{ asset('storage/assets/leaflet/leaflet.css') }}" />
    <style>
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')

    <!--  Marker  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Marker -@if( !empty($marker->id) ) #{{ $marker->id }} {{ $marker->getTranslation('name', $language) }} @else Nuovo Marker @endif
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    @if( $marker->id === null )
                                        <a href="{{ route(\Request::route()->getName(), ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @else
                                        <a href="{{ route(\Request::route()->getName(), ['marker' => $marker, 'lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @endif
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.markers.store', ['lang' => $language]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $marker->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $marker->getTranslation('name', $language) }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Attivo</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="active" name="active" value="1" class="form-check-input"@if( $marker->active ) checked="checked"@endif>Il Marker è attivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="value_type" class="form-control-label">Tipo Valore</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="value_type" id="value_type" class="form-control">
                                            <option value="flat"@if( $marker->value_type === 'flat' ) selected="selected"@endif>Valore fisso ( € )</option>
                                            <option value="percent"@if( $marker->value_type === 'percent' ) selected="selected"@endif>Percentuale sul costo del viaggio ( % )</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="value" class="form-control-label">Valore</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" class="form-control" name="value" value="{{ $marker->value }}" id="value">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="distribusion_id" class="form-control-label">Distribusion ID</label></div>
                                    <div class="col col-md-5">
                                        <input type="text" class="form-control" name="distribusion_id" value="{{ $marker->distribusion_id }}" id="distribusion_id">
                                    </div>
                                    <div class="col col-md-4">
                                        @if( $marker->id !== null )
                                            <button class="btn btn-primary" type="button" id="syncDistribusion"><i class="fal fa-sync-alt"></i> Sync</button>
                                        @else
                                            <button class="btn btn-primary" type="button" id="syncDistribusion" disabled="disabled"><i class="fal fa-sync-alt"></i> Sync</button>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="description" class="form-control-label">Descrizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="description" id="description" class="form-control" rows="5">{{ $marker->getTranslation('description', $language) }}</textarea>
                                    </div>
                                </div>

                                <!-- <div id="map-container"></div> -->
                                <div id="map"></div>

                                <div class="row form-group" style="margin-top: 5px;">
                                    <div class="col col-md-3"><label for="full_address" class="form-control-label">Indirizzo</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="search" value="{{ $marker->full_address }}" class="form-control" id="full_address" name="full_address" placeholder="Indirizzo">
                                    </div>
                                    <input type="hidden" value="" name="raw_data" id="raw_data">
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="lat" class="form-control-label">Latitudine</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" class="form-control" id="lat" name="lat" value="{{ $marker->lat }}">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="lng" class="form-control-label">Longitudine</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" class="form-control" id="lng" name="lng" value="{{ $marker->lng }}">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="street_and_number" class="form-control-label">Strada</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" value="{{ $marker->getTranslation('street_and_number', $language) }}" class="form-control" id="street_and_number" name="street_and_number" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="city_name" class="form-control-label">Città</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" value="{{ $marker->getTranslation('city_name', $language) }}" id="city_name" name="city_name" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="zip_code" class="form-control-label">ZIP</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" id="zip_code" name="zip_code" value="{{ $marker->zip_code }}" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="country" class="form-control-label">Nazione</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" id="country" name="country" value="{{ $marker->getTranslation('country', $language) }}" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="country_code" class="form-control-label">Codice Nazione</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" id="country_code" name="country_code" value="{{ $marker->country_code }}" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="type" class="form-control-label">Tipo</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" id="type" name="type" value="{{ $marker->type }}" readonly>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Marker -->

    @if( $marker->id !== null )
        <!-- Commenti Marker -->
            @include('admin.shared.notes', ['model' => 'App\Models\Marker', 'id' => $marker->id, 'notes' => $marker->notes])
        <!-- Commenti marker -->
    @endif
@endsection

@section('js-scripts')
    @parent

    <!-- <script src="{{ asset('storage/assets/leaflet/leaflet.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/places.js@1.15.5"></script> -->

@endsection

@section('added-js')
    <script>

        var lat = document.getElementById('lat');
        var lng = document.getElementById('lng');
        var streetAndNumber = document.getElementById('street_and_number');
        var cityName = document.getElementById('city_name');
        var zipCode = document.getElementById('zip_code');
        var type = document.getElementById('type');
        var country = document.getElementById('country');
        var countryCode = document.getElementById('country_code');
        var rawData = document.getElementById('raw_data');

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: @if( $marker->id ) {{ $marker->lat }}, @else 45.0, @endif
                    lng: @if( $marker->id ) {{ $marker->lng }}, @else 7.65, @endif
                },
                zoom: @if( $marker->id ) 17, @else 5, @endif
            });
            var input = document.getElementById('full_address');

            var autocomplete = new google.maps.places.Autocomplete(input);

            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
                let ac = place.address_components;
                let l = ac.length;
                let nomeCitta = '';
                let nomePaese = '';
                let nomeCittaS = '';

                for ( i = 0 ; i < l; i++ ) {

                    if ( inArray('route', ac[i].types) ) {

                        streetAndNumber.value = ac[i].long_name;

                    }

                    if ( inArray('administrative_area_level_3', ac[i].types) ) {

                        nomePaese = ac[i].long_name;

                    }

                    if ( inArray('administrative_area_level_2', ac[i].types) ) {

                        nomeCitta = ac[i].long_name;

                    }

                    if ( inArray('administrative_area_level_2', ac[i].types) ) {

                        nomeCitta = ac[i].long_name;
                        nomeCittaS = ac[i].short_name;

                    }

                    if ( inArray('country', ac[i].types) ) {

                        country.value = ac[i].long_name;
                        countryCode.value = ac[i].short_name;

                    }

                    if ( inArray('postal_code', ac[i].types) ) {

                        zipCode.value = ac[i].long_name;

                    }

                }

                cityName.value = nomePaese + ', ' + nomeCitta + ' (' + nomeCittaS + ')';
                type.value = ac[0].types[0];

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                if ( place.geometry.location ) {

                    lat.value = place.geometry.location.lat();
                    lng.value = place.geometry.location.lng();

                }
            });

            function inArray( needle, haystack ) {

                let length = haystack.length;
                for(var i = 0; i < length; i++) {
                    if(haystack[i] == needle) return true;
                }
                return false;

            }

            @if( $marker->id )
            marker.setPosition({lat: {{ $marker->lat }}, lng: {{ $marker->lng }} });
            @endif
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjVBna1BziI1odEaNZBv2AO9B2Y8MNOwU&libraries=places&callback=initMap"
            async defer></script>
@endsection