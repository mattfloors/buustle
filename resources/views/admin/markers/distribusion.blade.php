@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Distribusion  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Gestione Marker Distribusion
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="alert alert-warning" role="alert">
                            Attenzione, se non sapete come usare queste funzioni non fatelo, potreste distruggere tutto
                        </div>

                        <hr>

                        <div class="container">
                            <label for="dataInfo">Informazioni Sistema <span id="workingMsg"><i class="fas fa-cog fa-spin"></i> Working...</span></label>
                            <br>
                            <div class="progress mb-2" id="progressBarDiv">
                                <div class="progress-bar bg-info" id="progressBar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                            </div>
                            <br>
                            <textarea id="dataInfo" class="form-control" rows="6"></textarea>

                            <hr>

                            Questo bottone non è pericoloso, permette di scaricare tutte le stazioni di distribusion dentro il database ,a non sovrascrive dati interni.<br>
                            <p class="red font-weight-bold">Prima di aggiornare i dati è necessario usare questa funzione</p>
                            <button type="button" data-message="Comincio Download da Distribusion..." data-url="{{ route('admin.markers.distribusionDownload', ['locale' => 'it']) }}" id="reloadRawDataIt" class="actionButton btn btn-primary"><i class="fas fa-download"></i> Scarica i Dati da Distribusion | IT</button>
                            <button type="button" data-message="Comincio Download da Distribusion..." data-url="{{ route('admin.markers.distribusionDownload', ['locale' => 'en']) }}" id="reloadRawDataEn" class="actionButton btn btn-primary"><i class="fas fa-download"></i> Scarica i Dati da Distribusion | EN</button>

                            <hr>

                            Cerca nuove stazioni
                            <p class="red font-weight-bold color-red">Cerca tra i dati di distribusion stazioni mancanti nel sistema</p>
                            <button type="button" data-message="Preparo l'aggiunta di dati" data-url="{{ route('admin.markers.prepareFor', ['lang' => 'it']) }}" id="prepareForIT" class="actionButton btn btn-primary"><i class="fas fa-search-plus"></i> Controlla nuovi dati (IT)</button>
                            <button type="button" data-message="Preparo l'aggiunta di dati" data-url="{{ route('admin.markers.prepareFor', ['lang' => 'en']) }}" id="prepareForEN" class="actionButton btn btn-primary"><i class="fas fa-search-plus"></i> Controlla nuovi dati (EN)</button>


                            <hr>

                            Inserisci le stazioni mancanti (<span id="missingStationsEN">{{ $missingEN }}</span> EN e <span id="missingStationsIT">{{ $missingIT }}</span> IT da aggiungere)<br>
                            <button type="button" data-message="Aggiungo Dati" data-url="{{ route('admin.markers.distribusionAddCounter') }}" id="addItems" class="actionButton btn btn-primary"><i class="fas fa-layer-plus"></i> Aggiungi i Dati da Distribusion</button>

                            <hr>

                            Resync
                            <p class="red font-weight-bold color-red">Tutte le modifiche fatte verranno perse in maniera irrecuperabile</p>
                            <button type="button" data-message="Resync totale dei dati" data-url="{{ route('admin.markers.distribusionSync', ['lang' => 'it', 'start' => 0, 'take' => 25]) }}" id="resyncIT" class="actionButton btn btn-danger"><i class="fas fa-sync"></i> Resync totale | IT</button>
                            <button type="button" data-message="Resync totale dei dati" data-url="{{ route('admin.markers.distribusionSync', ['lang' => 'en', 'start' => 0, 'take' => 25]) }}" id="resyncEN" class="actionButton btn btn-danger"><i class="fas fa-sync"></i> Resync totale | EN</button>

                        </div>

                    </div>
                </div> <!-- /.row -->
            </div>

        </div><!-- /# column -->
    </div>
    <!--  /Distribusion -->

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            let infoBox = $("#dataInfo");
            let reloadRawDataIt = $("#reloadRawDataIt");
            let reloadRawDataEn = $("#reloadRawDataEn");
            let addItems = $("#addItems");
            let resyncIT = $("#resyncIT");
            let resyncEN = $("#resyncEN");
            let prepareIT = $("#prepareForIT");
            let prepareEN = $('#prepareForEN');
            let workingMessage = $("#workingMsg");
            let progressBarDiv = $("#progressBarDiv");
            let progressBar = $("#progressBar");
            let missingStationsIT = $("#missingStationsIT");
            let missingStationsEN = $("#missingStationsEN");

            workingMessage.hide();
            progressBarDiv.hide();

            prepareIT.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                let url = prepareIT.data('url');

                $.get({
                    url: url,
                    success: function(data) {

                        infoBox.empty();
                        infoBox.append("Cerco nuovi dati per IT");

                        if( data.status !== 'success' ) {

                            infoBox.append("\nSi è verificato un errore");

                        } else {

                            infoBox.append("\nTrovati " + data.new + " nuovi markers da inserire");
                            missingStationsIT.text(data.new);

                        }

                    }
                });
            });

            prepareEN.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                let url = prepareEN.data('url');

                $.get({
                    url: url,
                    success: function(data) {

                        infoBox.empty();
                        infoBox.append("Cerco nuovi dati per EN");

                        if( data.status !== 'success' ) {

                            infoBox.append("\nSi è verificato un errore");

                        } else {

                            infoBox.append("\nTrovati " + data.new + " nuovi markers da inserire");
                            missingStationsEN.text(data.new);

                        }

                    }
                });
            });

            resyncIT.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                let url = resyncIT.data('url');

                $('html, body').animate({ scrollTop: 0 }, 'fast');
                workingMessage.show();

                infoBox.empty();
                infoBox.append("Sync for IT");
                progressBar.attr('aria-valuenow', 0 + '%').css('width', 0 + '%');
                progressBarDiv.show();

                syncIt(url);
            });

            resyncEN.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                let url = resyncIT.data('url');

                $('html, body').animate({ scrollTop: 0 }, 'fast');
                workingMessage.show();

                infoBox.empty();
                infoBox.append("Sync for EN");
                progressBar.attr('aria-valuenow', 0 + '%').css('width', 0 + '%').text('0%');
                progressBarDiv.show();

                syncIt(url);
            });

            addItems.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $('html, body').animate({ scrollTop: 0 }, 'fast');
                workingMessage.show();

                infoBox.empty();
                infoBox.append("Adding Items...");
                progressBar.attr('aria-valuenow', 0 + '%').css('width', 0 + '%').text('0%');
                progressBarDiv.show();

                addIt('{!! route('admin.markers.distribusionAdd', ['start' => 0, 'take' => 25]) !!}');

            });

            reloadRawDataIt.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                buttonIsClicked($(this));
            });

            reloadRawDataEn.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                buttonIsClicked($(this));
            });

            function addIt( url ) {

                $.get({
                    url: url,
                    success: function( data ) {
                        if ( parseInt(data.newStart) > parseInt(data.totalToSync) ) {

                            infoBox.prepend("Added " + data.totalToSync + " out of " + data.totalToSync + "\n");
                            infoBox.prepend("DONE!\n");
                            progressBar.attr('aria-valuenow', data.barProgress + '%').css('width', data.barProgress + '%').text(data.barProgress + '%');
                            workingMessage.hide();

                            removeToAdd();

                        } else {

                            infoBox.prepend("Added " + data.newStart + " out of " + data.totalToSync + "\n");
                            progressBar.attr('aria-valuenow', data.barProgress + '%').css('width', data.barProgress + '%').text(data.barProgress + '%');

                            addIt( data.newRoute );

                        }
                    }
                });

            }

            function removeToAdd() {

                $.get({
                    url: '{!! route('admin.markers.removeToAdd') !!}',
                    success: function(data) {
                        missingStationsEN.text('0');
                        missingStationsIT.text('0');

                        infoBox.prepend("Queue Cleared\n");
                    }
                });

            }

            function syncIt(url) {

                $.get(url, function( data ) {

                    if ( parseInt(data.newStart) > parseInt(data.totalToSync) ) {

                        infoBox.prepend("Synced " + data.totalToSync + " out of " + data.totalToSync + "\n");
                        infoBox.prepend("DONE!\n");
                        progressBar.attr('aria-valuenow', data.barProgress + '%').css('width', data.barProgress + '%').text(data.barProgress + '%');
                        workingMessage.hide();

                    } else {

                        infoBox.prepend("Synced " + data.newStart + " out of " + data.totalToSync + "\n");
                        progressBar.attr('aria-valuenow', data.barProgress + '%').css('width', data.barProgress + '%').text(data.barProgress + '%');

                        syncIt( data.newRoute );

                    }

                });

            }

            function buttonIsClicked( button ) {

                let oldVal = button[0].innerHTML;
                let message = button.data('message');
                let url = button.data('url');

                button[0].innerHTML = loadingHtmlContent();
                infoBox.empty();

                button.attr('disabled', true);
                infoBox.append(message);

                $.get({
                    url: url,
                    success: function ( data ) {

                        infoBox.append("\n" + data.message);

                    }
                }).done(function() {
                    button.attr('disabled', false);
                    button[0].innerHTML = oldVal;
                });

            }

            /**
             * HTML content for loading
             * @returns {string}
             */
            function loadingHtmlContent() {

                return "<i class=\"fas fa-cog fa-spin\"></i> Working...";

            }
        } );
    </script>

@endsection