@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Markers  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Markers non Collegati con Distribusion
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    <a href="{{ route('admin.markers.orphaned', ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-dark" href="{{ route('admin.markers.orphanedDownload', ['language' => $language]) }}"><i class="fa fa-file-download"></i> Scarica CSV</a>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Indirizzo</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse( $markers as $marker )
                                        <tr>
                                            <td>{{ $marker->getTranslation('name', $language) }}</td>
                                            <td>{{ $marker->getTranslation('street_and_number', $language) }}</td>
                                            <td>{{ $marker->lat }}</td>
                                            <td>{{ $marker->lng }}</td>
                                            <td>
                                                <a href="{{ route('admin.markers.update', ['marker' => $marker, 'lang' => $language]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                                                <a href="{{ route('admin.markers.delete', ['marker' => $marker, 'lang' => $language]) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i> Elimina</a>
                                            </td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Nome</th>
                                    <th>Indirizzo</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ruoli -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable({
            });
        } );
    </script>
@endsection