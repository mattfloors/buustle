@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Markers  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Markers
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    <a href="{{ route('admin.markers.list', ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-dark" href="{{ route('admin.markers.orphaned') }}"><i class="fa fa-barcode-scan"></i> Markers non Collegati</a>
                                    <a class="text-right btn btn-primary" href="{{ route('admin.markers.create', ['lang' => $language]) }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Indirizzo</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Guardia</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ruoli -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{!! route('admin.markers.getList') !!}?lang={{ $language }}"
                },
                columns: [
                    { data: 'id' },
                    { data: 'nome' },
                    { data: 'indirizzo' },
                    { data: 'azioni',
                        orderable: false,
                        searchable: false
                    }
                ],
            });
        } );
    </script>
@endsection