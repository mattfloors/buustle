@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Retailers  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <i class="fas fa-barcode"></i> Codici Sconto
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.promocodes.create') }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sconto</th>
                                    <th>Emailed</th>
                                    <th>Uso Singolo</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $promocodes as $promocode )
                                    <tr>
                                        <td>
                                            {{ $promocode->code }}
                                        </td>
                                        <td>
                                            {{ $promocode->reward }}
                                            @if( $promocode->data['type'] === 'flat' )
                                                €
                                            @else
                                                %
                                            @endif
                                        </td>
                                        <td>
                                            @if( !empty($promocode->data['emailed']) )
                                                {{ $promocode->data['emailed'] }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if( $promocode->is_disposable )
                                                <i class="fas fa-check-circle" style="color:green"></i>
                                            @else
                                                <i class="fas fa-times-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.promocodes.edit', ['promocode' => $promocode]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun codice sconto trovato</td>
                                        <td>-</td>
                                        <td></td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Sconto</th>
                                    <th>Emailed</th>
                                    <th>Uso Singolo</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Services -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection