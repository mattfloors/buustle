@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Promocode  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <i class="fas fa-barcode"></i> Gestione Codice Sconto -@if( !empty($promocode->id) ) #{{ $promocode->id }} {{ $promocode->code }} @else Nuovo Codice Sconto @endif
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.promocodes.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $promocode->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="type" class="form-control-label">Tipo di Sconto</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="type" id="type" class="form-control">
                                            <option value="flat" @if($promocode->data['type'] === 'flat') selected="selected" @endif>Flat (€)</option>
                                            <option value="perc" @if($promocode->data['type'] === 'perc') selected="selected" @endif>Percentuale (% sul totale)</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="reward" class="form-control-label">Valore</label></div>
                                    <div class="col col-md-9">
                                        <input type="number" step="any" name="reward" class="form-control" value="{{ $promocode->reward }}" id="reward">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="emailed" class="form-control-label">Inviato</label></div>
                                    <div class="col col-md-9">
                                        <input type="text" class="form-control" name="emailed" value="{{ $promocode->data['emailed'] }}" id="emailed">
                                        <small class="help-block form-text">Inviare il codice sconto ad un indirizzo email dopo la creazione</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="disposable" class="form-control-label">Uso Singolo</label>
                                    </div>
                                    <div class="col md-9">
                                        <input class="form-control" type="checkbox" name="disposable" id="disposable" value="1" @if( $promocode->is_disposable ) checked="checked" @endif>
                                        <small class="help-block form-text">Se selezionato il codice sarà usabile una volta sola altrimenti sarà utilizzabile una volta per utente</small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="expires_at" class="form-control-label">Scadenza</label></div>
                                    <div class="col col-md-9">
                                        <input type="date" class="form-control" name="expires_at" value="{{ \Carbon\Carbon::parse($promocode->expires_at)->format('Y-m-d') }}" id="expires_at">
                                        <small class="help-block form-text">Lasciare vuoto per infinito</small>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Promocode -->

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection