@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Noleggiatori  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Noleggiatori </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.stops.create') }}"><i class="fa fa-plus-circle"></i> Add new stop</a>
                                </div>
                            </div>
                            <table id="list" class="display compact nowrap" style="width:100%" data-page-length="25">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>City Name</th>
                                    <th>Stop Name</th>
                                    <th>Link</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $stops as $stop )
                                    <tr>
                                        <td>
                                            {{ $stop->id }}
                                        </td>
                                        <td>
                                            {{ $stop->city_name }}
                                        </td>
                                        <td>
                                            {{$stop->stops_name}}
                                        </td>
                                        <td>
                                            {{$stop->link}}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.stops.delete', ['stop' => $stop]) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                <tr>
                                    <th>#</th>
                                    <th>City Name</th>
                                    <th>Stop Name</th>
                                    <th>Link</th>
                                </tr>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Noleggiatori -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection