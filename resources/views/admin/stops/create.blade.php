@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <!--  People Range  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Add new stop</h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.stops.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="city_name" class="form-control-label">City Name</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="city_name"  id="city_name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="stop_name" class="form-control-label">Stop Name</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="stop_name"  id="stop_name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="link" class="form-control-label">Link</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="link"  id="link">
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ruolo -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {

            $("#permissions").select2({});

        } );
    </script>
@endsection