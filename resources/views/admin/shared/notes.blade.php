@can('write notes')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <i class="far fa-envelope-open-text"></i> Lascia una nota
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.notes.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $id }}">
                                <input type="hidden" name="model" value="{{ $model }}">

                                <div class="row form-group">
                                    <div class="col col-md-12"><label for="text" class="form-control-label">Testo Nota</label></div>
                                    <div class="col-12 col-md-12">
                                        <textarea name="text" id="text" class="form-control" rows="6"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Invia la nota
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->

                <div class="row">
                    @forelse( $notes->sortByDesc('created_at') as $note )
                        <div class="col-md-12">
                            <div class="card border border-primary">
                                <div class="card-header">
                                    <strong class="card-title">[{{ \Carbon\Carbon::parse($note->created_at)->format('d/m/Y H:i') }}] da {{ !empty($note->author) ? $note->author->email : 'Anonymous' }}</strong>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">{{ $note->content }}</p>
                                </div>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div><!-- /# column -->
    </div>
@endcan