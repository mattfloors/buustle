@extends('admin.layout.default')

@section('css-header')
    @parent

    <style>
        pre {
            background: #f4f4f4;
            border: 1px solid #ddd;
            border-left: 3px solid #f36d33;
            color: #666;
            page-break-inside: avoid;
            font-family: monospace;
            font-size: 15px;
            line-height: 1.6;
            margin-bottom: 1.6em;
            max-width: 100%;
            overflow: auto;
            padding: 1em 1.5em;
            display: block;
            word-wrap: break-word;
        }
    </style>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            @if ( count( $audits ) > 0 )
                @foreach( $audits->reverse() as $au )
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">[{{ \Carbon\Carbon::parse($au->getMetadata()['audit_updated_at'])->format('d/m/Y H:i') }}] - <u>{{ $au->getMetadata()['user_email'] }}</u> => <b>{{ $au->getMetadata()['audit_event'] }}</b></strong>
                        </div>
                        <div class="card-body">
                            <h4>Metadata <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#auditMetadata-{{ $au->id }}" aria-expanded="false" aria-controls="collapseAudit-{{ $au->id }}"><i class="fal fa-search-plus"></i></button></h4>
                            <div class="collapse" id="auditMetadata-{{ $au->id }}">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Chiave</th>
                                            <th>Valore</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $au->getMetadata() as $amKey => $amValue )
                                            <tr>
                                                <td>{{ $amKey }}</td>
                                                <td>{{ $amValue }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Chiave</th>
                                            <th>Valore</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <hr>

                            <h4>Dati</h4>
                            <pre>
                                {{ print_r($au->getModified(), true) }}
                            </pre>
                        </div>
                    </div>
                @endforeach
            @endif

        </div><!-- /# column -->
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection