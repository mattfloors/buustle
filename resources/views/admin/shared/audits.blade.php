@if( count( $loadedModel->audits ) > 0 )
    <?php
    $audit = $loadedModel->audits()->latest()->first();
    $auditMetadata = $audit->getMetadata();
    ?>
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Storico Modifiche ({{ count($loadedModel->audits) }} presenti)
                <span class="pull-right">
                    <form class="form-inline" method="post" action="{{ route('admin.audits.view') }}">
                        @csrf
                        <input type="hidden" name="model" value="{{ $model }}">
                        <input type="hidden" name="id" value="{{ $id }}">
                        <button type="submit" class="btn btn-primary align-right">Guarda Tutti</button>
                    </form>
                </span>
            </h4>
        </div>
        <div class="card-body">

            <div class="typo-articles">

                <h4>Ultima Modifica</h4>

                <hr>
                <ul style="padding-left: 2%;">
                    <li><b>{{ $auditMetadata['audit_event'] }}</b> in data <i>{{ \Carbon\Carbon::parse($auditMetadata['audit_updated_at'])->format('d/m/Y H:i') }}</i> per mano di <u>{{ \App\User::find($auditMetadata['user_id'])['email'] }}</u></li>
                    @if( count( $audit->getModified() ) )
                        <ul style="padding-left: 4%;">
                            @foreach( $audit->getModified() as $aKey => $aValue )
                                <li>
                                    <b>{{ $aKey }}</b>
                                    <ul style="padding-left: 4%;">
                                        @if( array_key_exists('old', $aValue) )
                                            <li><b>(old)</b> => {{ print_r($aValue['old'], true) }}</li>
                                        @else
                                            <li>-</li>
                                        @endif
                                        @if( array_key_exists('new', $aValue) )
                                            <li><b>(new)</b> => {{ print_r($aValue['new'], true) }}</li>
                                        @else
                                            <li>-</li>
                                        @endif
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </ul>

            </div>

        </div> <!-- /.row -->
    </div>
@endif