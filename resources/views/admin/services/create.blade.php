@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Marker  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Servizio -@if( !empty($service->id) ) #{{ $service->id }} {{ $service->getTranslation('name', $language) }} @else Nuovo Servizio @endif
                        <span class="pull-right">Cambia Lingua:
                            @forelse($app_langs as $al)
                                @if( $al !== $language )
                                    @if( $service->id === null )
                                        <a href="{{ route(\Request::route()->getName(), ['lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @else
                                        <a href="{{ route(\Request::route()->getName(), ['service' => $service, 'lang' => $al]) }}"><span class="flag-icon flag-icon-{{ $al }}"></span></a>
                                    @endif
                                @endif
                            @empty
                                -
                            @endforelse
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.services.store', ['lang' => $language]) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $service->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $service->getTranslation('name', $language) }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="icon" class="form-control-label">Icona</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="icon" value="{{ $service->icon }}" id="icon">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="price" class="form-control-label">Prezzo</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" class="form-control" name="price" value="{{ $service->price }}" id="price">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Attivo</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="active" class="form-check-label ">
                                                    <input type="checkbox" id="active" name="active" value="1" class="form-check-input"@if( $service->active ) checked="checked"@endif>Il Servizio è attivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="icon-checked" class="form-control-label">Icona Selezionata</label></div>
                                    <div class="col-12 col-md-9">
                                        @if( $service->getMedia('icon-checked')->count() > 0 )
                                            <img class="checked" src="{{ $service->getFirstMediaUrl('icon-checked') }}" alt="{{ $service->name }}" title="{{ $service->name }}">
                                        @else
                                            -
                                        @endif
                                        <input type="file" name="icon-checked" id="icon-checked" class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="icon-unchecked" class="form-control-label">Icona Non Selezionata</label></div>
                                    <div class="col-12 col-md-9">
                                        @if( $service->getMedia('icon-unchecked')->count() > 0 )
                                            <img class="unchecked" src="{{ $service->getFirstMediaUrl('icon-unchecked') }}" alt="{{ $service->name }}" title="{{ $service->name }}">
                                        @else
                                            -
                                        @endif
                                        <input type="file" name="icon-unchecked" id="icon-unchecked" class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="description" class="form-control-label">Descrizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="description" id="description" class="form-control" rows="5">{{ $service->getTranslation('description', $language) }}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Marker -->

    @if( $service->id !== null )
        <!-- Commenti Marker -->
            @include('admin.shared.notes', ['model' => 'App\Models\Service', 'id' => $service->id, 'notes' => $service->notes])
        <!-- Commenti marker -->
    @endif
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection