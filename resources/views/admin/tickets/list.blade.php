@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Tickets  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        Tickets
                        @if( $step !== null )
                            <span class="pull-right">
                                <a href="{{ route('admin.tickets.list') }}"><i class="fas fa-eye"></i> Guarda Tutti</a>
                                <a target="_blank" href="{{ route('admin.travels.update', ['travel' => $step->travel]) }}"><i class="far fa-external-link"></i> Vedi il Viaggio</a>
                                <a target="_blank" href="{{ route('admin.travels.step.update', ['step' => $step]) }}"><i class="far fa-external-link"></i> Vedi lo Step</a>
                            </span>
                        @endif
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            @if( $step !== null )
                                <span class="pull-right" style="margin-bottom: 2em;">
                                    <a class="btn btn-primary" href="{{ route('admin.tickets.create', ['travelStep' => $step]) }}"><i class="far fa-plus-octagon"></i> Crea nuovo Biglietto</a>
                                </span>
                            @endif

                            <table id="list" class="display compact nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Viaggio</th>
                                    <th>Partenza</th>
                                    <th>Arrivo</th>
                                    <th>Partenza Viaggio</th>
                                    <th>Arrivo Previsto</th>
                                    <th>Posti</th>
                                    <th>Prezzo</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $tickets as $ticket )
                                    <tr>
                                        <td>{{ $ticket->step->travel->id }}</td>
                                        <td>{{ $ticket->step->startMarker->city_name }} - {{ $ticket->step->startMarker->street_and_number }}</td>
                                        <td>{{ $ticket->step->endMarker->city_name }} - {{ $ticket->step->endMarker->street_and_number }}</td>
                                        <td data-order="{{ \Carbon\Carbon::parse( $ticket->step->start_time )->timestamp }}">{{ \Carbon\Carbon::parse( $ticket->step->start_time )->formatLocalized('%A, %e %B %g %H:%m') }}</td>
                                        <td data-order="{{ \Carbon\Carbon::parse( $ticket->step->end_time )->timestamp }}">{{ \Carbon\Carbon::parse( $ticket->step->end_time )->formatLocalized('%A, %e %B %g %H:%m') }}</td>
                                        <td>{{ $ticket->slots }}</td>
                                        <td data-order="{{ $ticket->price }}">{{ $ticket->price }} €</td>
                                        <td>
                                            <a href="{{ route('admin.tickets.view', ['ticket' => $ticket]) }}" class="btn btn-outline-primary"><i class="far fa-eye"></i></a>
                                            <a href="{{ route('admin.tickets.update', ['ticket' => $ticket]) }}" class="btn btn-outline-warning"><i class="far fa-pen-fancy"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun biglietto trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Viaggio</th>
                                    <th>Partenza</th>
                                    <th>Arrivo</th>
                                    <th>Partenza Viaggio</th>
                                    <th>Arrivo Previsto</th>
                                    <th>Posti</th>
                                    <th>Prezzo</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Tickets -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable({
                "order": [[ 3, "desc" ]],
                initComplete: function () {
                    this.api().columns([0]).every( function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            });
        } );
    </script>
@endsection