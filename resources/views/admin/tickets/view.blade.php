@extends('admin.layout.default')

@section('css-header')
    @parent

    <link rel="stylesheet" href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/select2/css/select2.min.css') }}">

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="box-title">
                Gestione Biglietto Venduto
                <span class="pull-right">
                    <a target="_blank" href="{{ route('admin.travels.step.update', ['step' => $step]) }}"><i class="fas fa-backward"></i> Torna allo Step</a>
                    <a target="_blank" href="{{ route('admin.travels.update', ['travel' => $step->travel]) }}"><i class="far fa-external-link"></i> Vedi il Viaggio</a>
                </span>
            </h4>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-info" style="color:#007fff"></i> Informazioni
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <b>Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ $step->startMarker->country }} - {{ $step->startMarker->city_name }} - {{ $step->startMarker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.travels.step.update', ['step' => $step->startMarker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Arrivo</b>
                </div>
                <div class="col-md-8">
                    {{ $step->endMarker->country }} - {{ $step->endMarker->city_name }} - {{ $step->endMarker->street_and_number }}
                    <a class="markerEdit" href="{{ route('admin.travels.step.update', ['step' => $step->endMarker]) }}"><i class="far fa-map-marker-edit" style="color:yellowgreen"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Orario Partenza</b>
                </div>
                <div class="col-md-8">
                    {{ \Carbon\Carbon::parse( $step->start_time )->formatLocalized('%A, %e %B %G %H:%m') }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Km Stimati</b>
                </div>
                <div class="col-md-8">
                    ~ {{ round($step->estimated_km) }} km
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Tempo Stimato</b>
                </div>
                <div class="col-md-8">
                    ~ {{ \Carbon\CarbonInterval::seconds($step->estimated_time)->cascade() }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b>Posti Disponibili</b>
                </div>
                <div class="col-md-8">
                    {{ ( $step->slots - $step->boughtSlots() ) }}
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="box-title">
                <i class="fas fa-2x fa-badge-dollar" style="color:#ff0000"></i> Biglietto / i
            </h4>
        </div>
        <div class="card-body">

            <div class="form-row">
                <div class="col-md-5"><label for="slots">Persone</label></div>
                <div class="col-md-7">
                    {{ $ticket->slots }}
                </div>
            </div>

            <div class="form-row" style="margin-top:1.5em;">
                <div class="col-md-5"><label for="price">Prezzo Pagato</label></div>
                <div class="col-md-7">
                    {{ $ticket->price }} €
                </div>
            </div>

            <div class="form-row" style="margin-top:1.5em;">
                <div class="col-md-5"><label for="through">Venduto Tramite</label></div>
                <div class="col-md-7">
                    {{ $ticket->through }}
                </div>
            </div>

            <div class="form-row" style="margin-top:1.5em;">
                <div class="col-md-5"><label for="reference">Identificativo Esterno</label></div>
                <div class="col-md-7">
                    {{ $ticket->reference }}
                </div>
            </div>

            <div class="form-row" style="margin-top:1.5em;">
                <div class="col-md-5"><label for="owner">Proprietario</label></div>
                <div class="col-md-7">
                    {{ $ticket->owner }}
                </div>
            </div>

            <div class="form-row" style="margin-top:1.5em;">
                <div class="col-md-12"><label for="comment">Commento Aggiuntivo</label></div>
                <div class="col-md-12">
                    <p>{{ $ticket->comment }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! QrCode::generate(route('admin.tickets.check', ['ticketUuid' => $ticket->uuid])) !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $("#throughInfo").tooltip({
                title: 'Serve per segnare il metodo di vendita sia Distribusion o altro'
            });

            $("#referenceInfo").tooltip({
                title: 'Identificativo per riferimento esterno come l\'id della transazione distribusion'
            });

        } );
    </script>

@endsection