@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')

    <!--  Noleggiatori  -->
    <form action="{{ route('admin.noleggiatori.store') }}" method="post" class="form-horizontal">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title">Gestione Noleggiatore -@if( !empty($user->id) ) #{{ $user->id }} {{ $user->email }} @else Nuovo Noleggiatore @endif</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $user->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="partita_iva" class="form-control-label">Partita IVA</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-credit-card-front"></i></div>
                                            <input class="form-control" type="text" id="partita_iva" name="partita_iva" value="{{ $user->noleggiatoreProfile->partita_iva }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="paypal_account" class="form-control-label">Account Paypal</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fab fa-cc-paypal"></i></div>
                                            <input class="form-control" type="text" id="paypal_account" name="paypal_account" value="{{ $user->noleggiatoreProfile->paypal_account }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="stripe_account" class="form-control-label">Account Stripe</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fab fa-cc-stripe"></i></div>
                                            <input class="form-control" type="text" id="stripe_account" name="stripe_account" value="{{ $user->noleggiatoreProfile->stripe_account }}">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div> <!-- /.row -->
                    <div class="card-body"></div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><label for="regions">Regioni di Esercizio</label></h4>
                    </div>

                    <div class="card-body">
                        <select class="form-control" name="regions[]" id="regions" multiple>
                            @forelse( $regions as $region )
                                <option value="{{ $region->id }}" @if( $user->regions->contains($region->id) ) selected="selected" @endif>{{ $region->name_it }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>

                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><label for="retailer_id">Retailer Collegato</label></h4>
                    </div>

                    <div class="card-body">
                        <select class="form-control" name="retailer_id" id="retailer_id">
                            @forelse( $retailers as $retailer )
                                <option value="{{ $retailer->id }}" @if( $user->noleggiatoreProfile->retailer_id === $retailer->id ) selected="selected" @endif>{{ $retailer->name }}</option>
                            @empty
                                <option value=""> - </option>
                            @endforelse
                        </select>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Salva
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </div>
            </div><!-- /# column -->
        </div>

    </form>
    <!--  /Noleggiatori -->

    @if( $user->id !== null )
        <!-- Commenti Marker -->
        @include('admin.shared.notes', ['model' => 'App\User', 'id' => $user->id, 'notes' => $user->notes])
        <!-- Commenti marker -->
    @endif

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {

            $("#retailer_id").select2({});
            $("#regions").select2({});

        } );
    </script>
@endsection