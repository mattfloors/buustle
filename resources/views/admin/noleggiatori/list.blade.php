@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Noleggiatori  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Noleggiatori </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <table id="list" class="display compact nowrap" style="width:100%" data-page-length="25">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Ruoli</th>
                                    <th>Verificato</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $users as $user )
                                    <tr>
                                        <td>
                                            {{ $user->id }}
                                        </td>
                                        <td>
                                            {{ $user->email }}
                                        </td>
                                        <td>
                                            @if( !empty($user->getRoleNames()) )
                                                @foreach( $user->getRoleNames() as $role )
                                                    <span class="badge badge-success">{{ $role }}</span>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td align="center">
                                            @if( $user->email_verified_at !== null )
                                                <i class="fas fa-shield-check" style="color:green"></i>
                                            @else
                                                <i class="fas fa-times-hexagon" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.noleggiatori.update', ['user' => $user]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-user-edit"></i></a>
                                            <a href="{{ route('admin.noleggiatori.delete', ['user' => $user]) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <th>-</th>
                                        <td>Nessun utente trovato</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Ruolo</th>
                                    <th>Verificato</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Noleggiatori -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection