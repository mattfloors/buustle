@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Marker  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Gestione Traduzioni -@if( !empty($file) ) {{ $file }} @else Nuova Traduzione @endif
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.languages.storeTranslation', ['language' => $language]) }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        @if( empty($file) )
                                            <input type="text" class="form-control" name="name" value="" id="name">
                                        @else
                                            <input type="text" class="form-control" name="name" value="{{ $file }}" id="name" readonly>
                                        @endif
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-md-12">
                                        <h4>Traduzioni</h4>
                                    </div>
                                </div>

                                <?php $i = 0; ?>
                                <div id="translationsList">
                                    @forelse( $data as $codice => $traduzione )
                                        <div class="row form-group" id="langLine-{{ $i }}">
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="key[{{ $i }}]" value="{{ $codice }}" required>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control" name="data[{{ $i }}]" value="{{ $traduzione }}" required>
                                            </div>
                                            <div class="col-12 col-md-1">
                                                <button type="button" data-delete="{{ $i }}" id="delete-{{ $i }}" class="btn btn-danger deleteButton text-center"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    @empty
                                        <div class="row" id="empty-data">
                                            <div class="col-md-12 text-center">Nessuna traduzione disponibile</div>
                                        </div>
                                    @endforelse
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"></div>
                                    <div class="col-12 col-md-9">
                                        <button type="button" class="btn btn-success" id="addTranslation"><i class="fa fa-plus"></i> Aggiungi Traduzione</button>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                @if( !empty($rawData) )
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card-body">

                                <h4>Raw Data</h4>

                                <textarea class="form-control" id="rawData" rows="6" style="width:100%" readonly>{{ $rawData }}</textarea>

                            </div>

                        </div>
                    </div>
                @endif
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Translations -->
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            let translationsDiv = $("#translationsList");
            let count = parseInt('{{ $i }}');

            $( ".deleteButton" ).each(function( index ) {
                $(this).on('click', function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    deleteRow($(this).data('delete'));
                });
            });

            $("#addTranslation").on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                translationsDiv.append(newLine());

                $("#delete-" + count).on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();

                    deleteRow($(this).data('delete'));
                });

                if( count === 0 ) {

                    $("#empty-data").remove();

                }

                count++;
            });

            function newLine() {

                let myData = '<div class="row form-group" id="langLine-' + count + '">\n' +
                        '<div class="col-md-2">\n' +
                            '<input type="text" class="form-control" name="key[' + count + ']" value="" required>\n' +
                        '</div>\n' +
                        '<div class="col-12 col-md-9">\n' +
                            '<input type="text" class="form-control" name="data[' + count + ']" value="" required>\n' +
                        '</div>\n' +
                        '<div class="col-12 col-md-1">\n' +
                            '<button type="button" data-delete="' + count + '" id="delete-' + count + '" class="btn btn-danger deleteButton text-center"><i class="fa fa-times"></i></button>\n' +
                        '</div>\n' +
                    '</div>';

                return $(myData);

            }

            function deleteRow(idToRemove) {

                if( idToRemove !== undefined ) {

                    let rowToRemove = $("#langLine-" + idToRemove);

                    if( rowToRemove !== undefined ) {

                        rowToRemove.remove();

                    }

                }

            }
        } );
    </script>
@endsection