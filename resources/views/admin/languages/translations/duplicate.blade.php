@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Marker  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">
                        <span class="flag-icon flag-icon-{{ $language }}"></span> Duplica Traduzione - {{ $file }}
                    </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">

                            @forelse( config('translatable.app_locales') as $l )
                                @if( $l !== $language )
                                    <a class="text-right btn btn-outline-primary" href="{{ route('admin.languages.doDuplicateTranslation', ['newLang' => $l, 'oldLang' => $language, 'translation' => $file]) }}"><span class="flag-icon flag-icon-{{ $l }}"></span> Duplica</a>
                                    <hr>
                                @endif
                            @empty
                                -
                            @endforelse

                        </div>

                    </div>
                </div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Translations -->
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection