@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  Languages  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title"> Traduzioni</h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    @forelse( config('translatable.app_locales') as $l )
                                        <a class="text-right btn btn-outline-primary" href="{{ route('admin.languages.newTranslation', ['language' => $l]) }}"><span class="flag-icon flag-icon-{{ $l }}"></span> Aggiungi Nuovo</a>
                                    @empty
                                        -
                                    @endforelse
                                </div>
                            </div>
                            <table id="list" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th><i class="fas fa-flag"></i></th>
                                    <th>Nome</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $languages as $language => $files )
                                    @if( !empty($files) )
                                        @foreach( $files as $file )
                                            <tr>
                                                <td align="center" class="text-center">
                                                    <span class="flag-icon flag-icon-{{ $language }}"></span>
                                                </td>
                                                <td>
                                                    {{ $file }}
                                                </td>
                                                <td align="center">
                                                    @if( $file !== 'validation' )
                                                        <a href="{{ route('admin.languages.editTranslation', ['language' => $language, 'translation' => $file]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                                                        <a href="{{ route('admin.languages.duplicateTranslation', ['language' => $language, 'translation' => $file]) }}" class="btn btn-outline-dark btn-sm"><i class="fa fa-copy"></i> Duplica</a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>Nessuna traduzione trovata</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><i class="fas fa-flag"></i></th>
                                    <th>Nome</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Languages -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();

            $(".deleteTranslation").confirm();
        } );
    </script>
@endsection