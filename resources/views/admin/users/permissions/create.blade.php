@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <!--  Permessi  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Gestione Permesso </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.permissions.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $permission->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $permission->name }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="guard_name" class="form-control-label">Guardia</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" placeholder="web" class="form-control" name="guard_name" value="{{ $permission->guard_name }}" id="guard_name">
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Permessi -->

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
        } );
    </script>
@endsection