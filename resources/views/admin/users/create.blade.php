@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')

    <!--  Utenti  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Gestione Utente -@if( !empty($user->id) ) #{{ $user->id }} {{ $user->email }} @else Nuovo Utente @endif</h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.users.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $user->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Username</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $user->name }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="email" class="form-control-label">Email</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-envelope-square"></i></div>
                                            <input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Controlli</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="checkbox">
                                                <label for="email_verified" class="form-check-label ">
                                                    <input type="checkbox" id="email_verified" name="email_verified" value="1" class="form-check-input"@if( $user->email_verified_at !== null ) checked="checked"@endif>Email Confermata
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="password" class="form-control-label">Password</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-user-lock"></i></div>
                                            <input class="form-control" type="password" id="password" name="password" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="password_confirmation" class="form-control-label">Conferma Password</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-user-lock"></i></div>
                                            <input class="form-control" type="password" id="password_confirmation" name="password_confirmation" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="roles" class="form-control-label">Ruolo</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="roles[]" id="roles" class="form-control" required multiple>
                                            @if( !empty( $roles ) )
                                                @foreach( $roles as $role )
                                                    <option value="{{ $role->name }}" @if($user->hasRole($role->name)) selected="selected"@endif>{{ $role->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="first_name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-user-circle"></i></div>
                                            <input class="form-control" type="text" id="first_name" name="first_name" value="{{ $user->profile->first_name }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="last_name" class="form-control-label">Cognome</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-user-circle"></i></div>
                                            <input class="form-control" type="text" id="last_name" name="last_name" value="{{ $user->profile->last_name }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="phone" class="form-control-label">Numero di Telefono</label></div>
                                    <div class="col-12 col-md-9">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fal fa-phone"></i></div>
                                            <input class="form-control" type="text" id="phone" name="phone" value="{{ $user->profile->phone }}">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Users -->

    @if( $user->id !== null )
        <!-- Commenti Marker -->
        @include('admin.shared.notes', ['model' => 'App\User', 'id' => $user->id, 'notes' => $user->notes])
        <!-- Commenti marker -->
    @endif

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {

            $("#roles").select2({});

        } );
    </script>
@endsection