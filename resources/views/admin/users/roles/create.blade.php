@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <!--  Ruolo  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Gestione Ruolo -@if( !empty($role->id) ) #{{ $role->id }} {{ $role->name }} @else Nuovo Ruolo @endif </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.roles.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $role->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class="form-control-label">Nome</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" class="form-control" name="name" value="{{ $role->name }}" id="name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="guard_name" class="form-control-label">Guardia</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" placeholder="web" class="form-control" name="guard_name" value="{{ $role->guard_name }}" id="guard_name">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="permissions" class="form-control-label">Permessi</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="permissions[]" id="permissions" multiple="multiple" class="form-control">
                                            @if( !empty( $permissions ) )
                                                @foreach( $permissions as $permission )
                                                    <option value="{{ $permission->id }}" @if($role->hasPermissionTo($permission->name, $permission->guard_name)) selected="selected"@endif>{{ $permission->guard_name }} | {{ $permission->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ruolo -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {

            $("#permissions").select2({});

        } );
    </script>
@endsection