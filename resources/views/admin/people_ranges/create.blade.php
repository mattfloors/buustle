@extends('admin.layout.default')

@section('css-header')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <!--  People Range  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">Gestione Range persone -@if( !empty($range->id) ) #{{ $range->id }} {{ $range->min }} => {{ $range->max }} @else Nuovo Range Persone @endif </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{ route('admin.people_ranges.store') }}" method="post" class="form-horizontal">
                            @csrf

                            <div class="card-body">

                                <input type="hidden" name="id" value="{{ $range->id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="min" class="form-control-label">Persone Minime</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" min="0" class="form-control" name="min" value="{{ $range->min }}" id="min">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="max" class="form-control-label">Persone Massime</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" min="0" class="form-control" name="max" value="{{ $range->max }}" id="max">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="coeff_predizione" class="form-control-label">Coefficiente Calcolo Predizione</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" min="0" class="form-control" name="coeff_predizione" value="{{ $range->coeff_predizione }}" id="coeff_predizione">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="multiplier" class="form-control-label">Moltiplicatore Predizione Prezzo</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" min="0" class="form-control" name="multiplier" value="{{ $range->multiplier }}" id="multiplier">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="price_multiplier" class="form-control-label">Moltiplicatore Prezzo Finale</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="number" step="any" min="0" class="form-control" name="price_multiplier" value="{{ $range->price_multiplier }}" id="multiplier">
                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Salva
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>

                        </form>

                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ruolo -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {

            $("#permissions").select2({});

        } );
    </script>
@endsection