@extends('admin.layout.default')

@section('css-header')
    @parent
    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <!--  People Ranges  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="box-title">People Ranges </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="row" style="padding-bottom:2em;">
                                <div class="col-md-12 text-right">
                                    <a class="text-right btn btn-primary" href="{{ route('admin.people_ranges.create') }}"><i class="fa fa-plus-circle"></i> Aggiungi Nuovo</a>
                                </div>
                            </div>
                            <table id="list" class="display compact nowrap" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Min</th>
                                    <th>Max</th>
                                    <th>Coeff. Predizione</th>
                                    <th>Multiplier Predizione</th>
                                    <th>Multiplier Finale</th>
                                    <th>Azioni</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse( $ranges as $range )
                                    <tr>
                                        <td>
                                            {{ $range->id }}
                                        </td>
                                        <td>
                                            {{ $range->min }}
                                        </td>
                                        <td>
                                            {{ $range->max }}
                                        </td>
                                        <td>
                                            {{ $range->coeff_predizione }}
                                        </td>
                                        <td>
                                            {{ $range->multiplier }}
                                        </td>
                                        <td>
                                            {{ $range->price_multiplier }}
                                        </td>
                                        <td align="center">
                                            <a href="{{ route('admin.people_ranges.edit', ['range' => $range]) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                                            <a href="{{ route('admin.people_ranges.delete', ['range' => $range]) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i> Elimina</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Nessun range trovato</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Min</th>
                                    <th>Max</th>
                                    <th>Coeff. Predizione</th>
                                    <th>Multiplier Predizione</th>
                                    <th>Multiplier Finale</th>
                                    <th>Azioni</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Ranges -->

@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            $('#list').DataTable();
        } );
    </script>
@endsection