@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <form class="form" action="{{ route('admin.configurations.store') }}" method="POST">
        @csrf

        <input type="hidden" name="group" value="payments">

        <!-- Paypal -->
        <div class="card">

            <div class="card-header">
                <strong class="card-title"><i class="fab fa-cc-paypal"></i> Paypal Settings</strong>
            </div>
            <div class="card-body">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_sandbox_mode]" class="form-control-label">Paypal in Sandbox</label>
                    </div>
                    <div class="col md-9">
                        <input type="checkbox" name="config[paypal_sandbox_mode]" id="config[paypal_sandbox_mode]" value="1" @if( (bool) setting('payments.paypal_sandbox_mode') === true ) checked="checked" @endif>
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_username_sandbox]" class="form-control-label">Paypal Sandbox Username</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_username_sandbox]" name="config[paypal_username_sandbox]" value="{{ setting('payments.paypal_username_sandbox') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_password_sandbox]" class="form-control-label">Paypal Sandbox Password</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_password_sandbox]" name="config[paypal_password_sandbox]" value="{{ setting('payments.paypal_password_sandbox') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_signature_sandbox]" class="form-control-label">Paypal Sandbox Signature</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_signature_sandbox]" name="config[paypal_signature_sandbox]" value="{{ setting('payments.paypal_signature_sandbox') }}">
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_username_live]" class="form-control-label">Paypal Live Username</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_username_live]" name="config[paypal_username_live]" value="{{ setting('payments.paypal_username_live') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_password_live]" class="form-control-label">Paypal Live Password</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_password_live]" name="config[paypal_password_live]" value="{{ setting('payments.paypal_password_live') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[paypal_signature_live]" class="form-control-label">Paypal Live Signature</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[paypal_signature_live]" name="config[paypal_signature_live]" value="{{ setting('payments.paypal_signature_live') }}">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
        <!-- /Paypal -->

        <!-- Stripe -->
        <div class="card">

            <div class="card-header">
                <strong class="card-title"><i class="fab fa-cc-stripe"></i> Stripe Settings</strong>
            </div>
            <div class="card-body">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[stripe_test_mode]" class="form-control-label">Stripe in Test Mode</label>
                    </div>
                    <div class="col md-9">
                        <input type="checkbox" name="config[stripe_test_mode]" id="config[stripe_test_mode]" value="1" @if( (bool) setting('payments.stripe_test_mode') === true ) checked="checked" @endif>
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[stripe_test_public]" class="form-control-label">Stripe Test Public Key</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[stripe_test_public]" name="config[stripe_test_public]" value="{{ setting('payments.stripe_test_public') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[stripe_test_private]" class="form-control-label">Stripe Test Private Key</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[stripe_test_private]" name="config[stripe_test_private]" value="{{ setting('payments.stripe_test_private') }}">
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[stripe_live_public]" class="form-control-label">Stripe Live Public Key</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[stripe_live_public]" name="config[stripe_live_public]" value="{{ setting('payments.stripe_live_public') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[stripe_live_private]" class="form-control-label">Stripe Live Private Key</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[stripe_live_private]" name="config[stripe_live_private]" value="{{ setting('payments.stripe_live_private') }}">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
        <!-- /Stripe -->

    </form>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
        } );
    </script>
@endsection