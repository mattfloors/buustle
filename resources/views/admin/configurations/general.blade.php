@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <form class="form" action="{{ route('admin.configurations.store') }}" method="POST">
        @csrf

        <input type="hidden" name="group" value="general">

        <!-- Distribusion -->
        <div class="card">

            <div class="card-header">
                <strong class="card-title"><i class="fas fa-user-hard-hat"></i> General Settings</strong>
            </div>
            <div class="card-body">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[contact_email]" class="form-control-label">Email di contatto</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[contact_email]" id="config[contact_email]" value="{{ setting('general.contact_email') }}">
                        <small class="help-block form-text">Email cui arriveranno i contatti degli utenti</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[sender_email]" class="form-control-label">Email di invio</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[sender_email]" id="config[sender_email]" value="{{ setting('general.sender_email') }}">
                        <small class="help-block form-text">Email da cui arriveranno le email dal sito (esempio: noreply@buustle.com)</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[sender_name]" class="form-control-label">Nome Invio</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[sender_name]" id="config[sender_name]" value="{{ setting('general.sender_name') }}">
                        <small class="help-block form-text">Nome di chi invia le email (esempio: Buustle)</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[phone_number]" class="form-control-label">Numero di Telefono</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[phone_number]" id="config[phone_number]" value="{{ setting('general.phone_number') }}">
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[shared_discount]" class="form-control-label">Percentuale di sconto per condivisione</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[shared_discount]" id="config[shared_discount]" value="{{ setting('general.shared_discount') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[max_per_bus]" class="form-control-label">Massimo persone per veicolo</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[max_per_bus]" id="config[max_per_bus]" value="{{ setting('general.max_per_bus') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[min_per_bus]" class="form-control-label">Minimo persone per veicolo</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[min_per_bus]" id="config[min_per_bus]" value="{{ setting('general.min_per_bus') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[draft_expire]" class="form-control-label">Ore validità Richiesta Incompleta</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[draft_expire]" id="config[draft_expire]" value="{{ setting('general.draft_expire') }}">
                        <small class="help-block form-text">Numero di ore una richiesta non completa viene mantenuta nel database (consigliato: 24)</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[days_before_start_limit]" class="form-control-label">Giorni dalla Partenza Limite</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[days_before_start_limit]" id="config[days_before_start_limit]" value="{{ setting('general.days_before_start_limit') }}">
                        <small class="help-block form-text">Giorni dalla partenza dellla richiesta limite oltre i quali viene applicato un forfettario (esempio 60)</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[days_before_start_limit_price]" class="form-control-label">Prezzo se più di tot giorni</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[days_before_start_limit_price]" id="config[days_before_start_limit_price]" value="{{ setting('general.days_before_start_limit_price') }}">
                        <small class="help-block form-text">Valore di commissione forfettaria per richieste oltre il limite di giorni dalla partenza (esempio 7)</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[min_days_for_request]" class="form-control-label">Minimo giorni dalla richiesta</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[min_days_for_request]" id="config[min_days_for_request]" value="{{ setting('general.min_days_for_request') }}">
                        <small class="help-block form-text">Minimo di giorni di tempo per poter evadere la richiesta</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[min_days_for_retailer_offer]" class="form-control-label">Minimo Giorni per Noleggiatori</label>
                    </div>
                    <div class="col md-9">
                        <input type="number" step="any" class="form-control" name="config[min_days_for_retailer_offer]" id="config[min_days_for_retailer_offer]" value="{{ setting('general.min_days_for_retailer_offer') }}">
                        <small class="help-block form-text">Minimo di giorni di tempo dalla partenza perchè i noleggiatori possano fare una proposta</small>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
        <!-- /Distribusion -->

    </form>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
        } );
    </script>
@endsection