@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <form class="form" action="{{ route('admin.configurations.store') }}" method="POST">
        @csrf

        <input type="hidden" name="group" value="distribusion">

        <!-- Distribusion -->
        <div class="card">

            <div class="card-header">
                <strong class="card-title"><i class="fas fa-car"></i> Distribusion Settings</strong>
            </div>
            <div class="card-body">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[distribusion_test_mode]" class="form-control-label">Test Mode</label>
                    </div>
                    <div class="col md-9">
                        <input type="checkbox" name="config[distribusion_test_mode]" id="config[distribusion_test_mode]" value="1" @if( (bool) setting('distribusion.distribusion_test_mode') === true ) checked="checked" @endif>
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[api_url_test]" class="form-control-label">Test API URL</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[api_url_test]" id="config[api_url_test]" value="{{ setting('distribusion.api_url_test') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[api_key_test]" class="form-control-label">Test API KEY</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[api_key_test]" name="config[api_key_test]" value="{{ setting('distribusion.api_key_test') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[buustle_code_test]" class="form-control-label">Test Buustle Code</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[buustle_code_test]" name="config[buustle_code_test]" value="{{ setting('distribusion.buustle_code_test') }}">
                        <small class="help-block form-text">Codice di riferimento che identifica Buustle nel sistema Distribusion</small>
                    </div>
                </div>

                <hr>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[api_url_live]" class="form-control-label">Live API URL</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[api_url_live]" id="config[api_url_live]" value="{{ setting('distribusion.api_url_live') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[api_key_live]" class="form-control-label">Live API KEY</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[api_key_live]" name="config[api_key_live]" value="{{ setting('distribusion.api_key_live') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[buustle_code_live]" class="form-control-label">Live Buustle Code</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" id="config[buustle_code_live]" name="config[buustle_code_live]" value="{{ setting('distribusion.buustle_code_live') }}">
                        <small class="help-block form-text">Codice di riferimento che identifica Buustle nel sistema Distribusion</small>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
        <!-- /Distribusion -->

    </form>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
        } );
    </script>
@endsection