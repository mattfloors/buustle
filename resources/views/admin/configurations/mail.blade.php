@extends('admin.layout.default')

@section('css-header')
    @parent

@endsection

@section('content')

    <form class="form" action="{{ route('admin.configurations.store') }}" method="POST">
        @csrf

        <input type="hidden" name="group" value="email">

        <!-- Email -->
        <div class="card">

            <div class="card-header">
                <strong class="card-title"><i class="fas fa-mailbox"></i> Mail Settings</strong>
            </div>
            <div class="card-body">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[driver]" class="form-control-label">Driver</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[driver]" id="config[driver]" value="{{ setting('email.driver') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[host]" class="form-control-label">Host</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[host]" id="config[host]" value="{{ setting('email.host') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[port]" class="form-control-label">Porta</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[port]" id="config[port]" value="{{ setting('email.port') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[encryption]" class="form-control-label">Encrypt</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[encryption]" id="config[encryption]" value="{{ setting('email.encryption') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[username]" class="form-control-label">Username</label>
                    </div>
                    <div class="col md-9">
                        <input type="text" class="form-control" name="config[username]" id="config[username]" value="{{ setting('email.username') }}">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="config[password]" class="form-control-label">Password</label>
                    </div>
                    <div class="col md-9">
                        <input type="password" class="form-control" name="config[password]" id="config[password]" value="{{ setting('email.password') }}">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa-save"></i> Salva
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </div>
        <!-- /Email -->

    </form>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')
    <script>
        jQuery(document).ready(function($) {
        } );
    </script>
@endsection