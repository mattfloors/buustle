<html>
<head>
    <style>
        @font-face {
            font-family: 'F37Ginger';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.svg') }}') format('svg');
            /* Legacy iOS */
        }
        @font-face {
            font-family: 'F37Ginger-Regular';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.svg') }}') format('svg');
            /* Legacy iOS */
        }
        @font-face {
            font-family: 'F37Ginger-Bold';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.svg') }}') format('svg');
            /* Legacy iOS */
        }
        body {
            font-family: F37Ginger-Light;
            color: #292559;
            font-size: 14px;
        }
        p {
            margin: 0pt;
        }
        td {
            vertical-align: top;
        }
        table{
            font-size: 14pt;
        }
        table thead tr td {
            text-align: left;
            font-size: 20pt;
            font-family: F37Ginger-Bold;
        }
        table tbody{
            border-top: 2px solid #292559;
        }
        table tbody tr{
            border-bottom: 1px solid #292559;
        }
        table tbody tr td{
            font-size: 10pt;
        }
        .exc{
            border-bottom: 1px solid #292559;
            font-family: F37Ginger-Bold;
            font-size: 14pt !important;
        }
        .yellow{
            background-color: #eddfa2;
        }
        .yellow tbody tr{
            border-bottom: 1px solid #ffffff;
        }

    </style>
</head>
<body>
<table width="100%" cellpadding="10">
    <tr>
        <td width="45%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="45%" style="text-align: right; font-family: F37Ginger-Light;">
            <img src="{{ asset('storage/assets/mail/images/logo-buustle.png') }}" width="200" style="max-width: 200px; width: 200px;">
            <br />
            <span style="font-family: F37Ginger-Bold;">Buustle srl</span>
            <br /><br />
            Via Alfonso Lamarmora 16
            <br />
            10128 - Torino
            <br />
            P.IVA: 11568420019
            <br />
            +39 3929393264
        </td>
    </tr>
</table>
<br />

@yield('pdf.content')

</body>
</html>