@extends('pdf.layout.base')

@section('pdf.content')

    <table width="100%" cellpadding="10">
        <tr>
            <td width="45%" style="text-align: left; font-family: F37Ginger-Light;">
                <br />Quote # {{ $quote->sku }} Invoice,
                <br />@lang('pdf.operatore'):
                <br />@lang('pdf.to_top') {{ $quote->user->profile->first_name }} {{ $quote->user->profile->last_name }}
                <br /><br />@lang('pdf.top_description'):
            </td>
        </tr>
    </table>
    <br />
    <table class="table" width="100%" style="border-collapse: collapse;" cellpadding="20">
        <thead>
        <tr>
            <td>@lang('pdf.quote_route') a</td>
        </tr>
        </thead>
        <tbody>
        @forelse( $quote->markers as $marker )
            <tr>
                <td style="width:100%"><hr></td>
            </tr>
            <tr>
                <td>@lang('pdf.quote_start_step') {{ $marker->start_marker->city_name }} - {{ $marker->start_marker->name }}</td>
            </tr>
            <tr>
                <td>@lang('pdf.quote_start_date') {{ \Carbon\Carbon::parse($marker->start_time)->format('d/m/Y') }}</td>
            </tr>
            <tr>
                <td>@lang('pdf.quote_start_time') {{ \Carbon\Carbon::parse($marker->start_time)->format('H:i') }}</td>
            </tr>
            <tr>
                <td>@lang('pdf.end_route') {{ $marker->end_marker->city_name }} - {{ $marker->end_marker->name }}</td>
            </tr>
        @empty
        @endforelse
        <tr>
            <td>@lang('pdf.quote_people'): {{ $quote->people }}</td>
        </tr>
        <tr class="exc">
            <td>@lang('pdf.quote_notes')<br><hr>
                @if( empty($quote->comment) )
                    <i>@lang('pdf.invoice_no_comment')</i>
                @else
                    <i>{{ $quote->comment }}</i>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
    <table class="yellow" width="100%" style="border-collapse: collapse;" cellpadding="20">
        <tbody>
        <tr>
            <td width="50%">
                @lang('pdf.quote_price') {{ $quote->price }}<br>
                @if( !$quote->shared ) @lang('pdf.invoice_exclusive') @else @lang('pdf.invoice_shared') @endif<br>
                (@lang('pdf.quote_VAT_included'))
            </td>
            <td width="50%" style="border-left: 1px solid #ffffff">

            </td>
        </tr>
    </table><br>
    <table width="100%" style="border-collapse: collapse;" cellpadding="20">
        <tbody style="border: none">
        <tr style="border-color: #7b868c;">
            <td>
                <p style="color: #7b868c; font-size: 14pt; line-height: 1.5;">*@lang('pdf.invoice_shared_description')</p><br>
            </td>
        </tr>
        </tbody>
    </table><br>
    @if ( !empty($quote->infos ) )
        <table width="100%" style="border-collapse: collapse;" cellpadding="20">
            <thead>
            <tr>
                <td style="font-size: 14pt">@lang('pdf.invoice_infos')</td>
            </tr>
            </thead>
        </table>
        <table width="100%" style="border-collapse: collapse;" cellpadding="10">
            <tbody style="border: none">
            @forelse ( $quote->infos as $info )
                @if( $info->included )
                    <tr style="border: none">
                        <td width="2%"></td>
                        <td width="2%">➔</td>
                        <td width="96%">{{ $info->infos }}</td>
                    </tr>
                @endif
            @endforelse
            </tbody>
        </table><br><hr>
        <table width="100%" style="border-collapse: collapse;" cellpadding="20">
            <thead>
            <tr>
                <td style="font-size: 14pt">@lang('pdf.invoice_not_included'):</td>
            </tr>
            </thead>
        </table>
        <table width="100%" style="border-collapse: collapse;" cellpadding="10">
            <tbody style="border: none">
            @forelse ( $quote->infos as $info )
                @if( !$info->included )
                    <tr style="border: none">
                        <td width="2%"></td>
                        <td width="2%">➔</td>
                        <td width="96%">{{ $info->infos }}</td>
                    </tr>
                @endif
            @endforelse
            </tbody>
        </table><br><hr>
    @endif

    @if ( !empty($quote->prices ) )
        <table width="100%" style="border-collapse: collapse;" cellpadding="20">
            <thead>
            <tr>
                <td style="font-size: 14pt">@lang('pdf.invoice_additional_costs')</td>
            </tr>
            </thead>
        </table>
        <table width="100%" style="border-collapse: collapse;" cellpadding="10">
            <tbody style="border: none">
            @forelse ( $quote->prices as $price )
                <tr style="border: none">
                    <td width="2%">{{ $price->price }}€</td>
                    <td width="2%">➔</td>
                    <td width="96%">{{ $info->reason }}</td>
                </tr>
            @endforelse
            </tbody>
        </table><br><hr>
    @endif

    <table>
        <tbody>
        <tr>
            <td style="font-size: 14pt"><br>
                @lang('pdf.invoice_additional_description')
            </td>
        </tr>
        </tbody>
    </table>
    <table style="padding-bottom: 100px;" width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr>
            <td width="20%" style="border-bottom: 1px solid #292559">@lang('pdf.signature_date') <br><br><br></td>
            <td width="60%"></td>
            <td width="20%" style="border-bottom: 1px solid #292559">@lang('pdf.signature_signature') <br><br><br></td>
        </tr>
        </tbody>
    </table>

@endsection