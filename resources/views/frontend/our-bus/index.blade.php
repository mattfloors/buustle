@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

<div class="menu-mobile-overlay"></div>

<div class="banner-bg">
    <picture class="banner">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/i-nostri-mezzi.svg') }}" media="(max-width: 767px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/i-nostri-mezzi-desktop.svg') }}" media="(min-width: 768px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/i-nostri-mezzi.svg') }}">
        <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/i-nostri-mezzi.svg') }}" alt="Buustle" title="Buustle">
    </picture>
    <div class="inner">
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                        <h1 class="title mbottom-0">@lang('our-bus.title')</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-ivory i-nostri-mezzi">
    <div class="container padding-60">
        <div class="row">
            <div class="col-xs-12">
                <div class="scheda03">
                    <form class="row">
                        @forelse( $packages as $package )
                            <!-- single -->
                                <div class="col-xs-12 col-sm-4 inner main-container" id="{{ $package->id }}">
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                                                <div class="item">
                                                    <div class="radio">
                                                        <input type="radio" value="{{ $package->id }}" id="package-{{ $package->id }}" name="package">
                                                        <label for="package-{{ $package->id }}">
                                                            <div class="top">
                                                                <i class="{{ $package->icon }} radio-img" title="{{ $package->name }}"></i>
                                                                <span class="name">{{ $package->name }}</span>
                                                                <span class="caret"></span>
                                                                <span class="supplemento">+ {{ $package->supplement }}€/Km</span>
                                                            </div>
                                                            <div class="img-container">
                                                                @if( $package->getMedia('banner-image')->count() > 0 )
                                                                    <img src="{{ $package->getFirstMediaUrl('banner-image') }}" alt="{{ $package->name }}" title="{{ $package->name }}">
                                                                @else
                                                                    <img src="{{ asset('storage/assets/frontend/images/bus.jpg') }}" alt="{{ $package->name }}" title="{{ $package->name }}">
                                                                @endif
                                                            </div>
                                                            <div class="text-container">
                                                                <p>{!! $package->description !!}</p>
                                                                <div class="classe-ambientale">
                                                                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/classe-ambientale.svg') }}" alt="@lang('rent.step3_environment_class')" title="@lang('rent.step3_environment_class')">
                                                                    <span>@lang('rent.step3_environment_class'): <span class="aqua">{{ $package->env_class }}</span> </span>
                                                                </div>
                                                            </div>
                                                            <div class="plus" id="plus-items-{{ $package->id }}">
                                                                <br><br>
                                                                <span class="title">@lang('rent.step3_services')</span>
                                                                <span class="subtitle">@lang('rent.step3_services_desc'):</span>
                                                                <ul>
                                                                    @forelse( $package->services as $service )
                                                                        <li>
                                                                            <div class="checkbox">
                                                                                <input type="checkbox" name="services-{{ $package->id }}[]" value="{{ $service->id }}" id="service-{{ $package->id }}-{{ $service->id }}">
                                                                                <label for="service-{{ $package->id }}-{{ $service->id }}">
                                                                                    <div class="check-container">
                                                                                        <img class="unchecked" src="{{ ($service->getMedia('icon-unchecked')->count() > 0) ? $service->getFirstMediaUrl('icon-unchecked') : asset('storage/assets/frontend/images/icons-svg/carrozzina.svg') }}" alt="{{ $service->name }}" title="{{ $service->name }}">
                                                                                        <img class="checked" src="{{ ($service->getMedia('icon-checked')->count() > 0) ? $service->getFirstMediaUrl('icon-checked') : asset('storage/assets/frontend/images/icons-svg/carrozzina-checked.svg') }}" alt="{{ $service->name }}" title="{{ $service->name }}">
                                                                                    </div>
                                                                                    <span>{{ $service->name }}</span>
                                                                                </label>
                                                                            </div>
                                                                        </li>
                                                                    @empty
                                                                        <li>@lang('rent.step3_no_services_for_package')</li>
                                                                    @endforelse
                                                                </ul>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- /single -->
                        @empty
                        @endforelse
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('content')

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

    <script>
        $(document).ready(function(){

            $('.plus').hide();

            $('.main-container').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    $('#plus-items-' + $(this).attr('id')).slideToggle();

                });
            });

        });
    </script>

@endsection
