@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente">
            <img src="{{ $noleggiatore->getFirstMedia('logo') ? $noleggiatore->getFirstMedia('logo')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ $noleggiatore->retailer->name }}" title="{{ $noleggiatore->retailer->name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ $noleggiatore->retailer->name }}<a href="{{ route('partner.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('partner.edit')" title="@lang('partner.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ $noleggiatore->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ $noleggiatore->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="bg-ivory">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-offset-2">
                        <ul class="nav-profile">
                            <li><a href="{{ route('partner.index') }}" title="@lang('partner.rent_requests_area')">@lang('partner.rent_requests_area')</a></li>
                            <li class="active"><a href="{{ route('partner.ownedTravels') }}" title="@lang('partner.rent_requests_accepted')">@lang('partner.rent_requests_accepted')</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-ivory">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="scheda01 scheda01-b padding-0-60">
                            <div class="row">
                                @forelse( $travels as $travel )
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 inner">
                                        <div>
                                            <a href="{{ route( 'partner.ownedTravel', ['travel' => $travel] ) }}">
                                                <div class="row">
                                                    <div class="col-xs-10 col-xs-offset-1">
                                                        <div class="row viaggi">
                                                            <div class="col-xs-5">
                                                                <span class="citta">{{ $travel->getFirstStep()->startMarker->region->name_it }}</span>
                                                                <span class="indirizzo">{{ trim($travel->getFirstStep()->startMarker->name) }}</span>
                                                                <span class="data">{{ \Carbon\Carbon::parse($travel->getFirstStep()->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                            </div>
                                                            <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="_" title="_"></div>
                                                            <div class="col-xs-5">
                                                                <span class="citta">{{ $travel->getLastStep()->endMarker->region->name_it }}</span>
                                                                <span class="indirizzo">{{ trim($travel->getLastStep()->endMarker->name) }}</span>
                                                                <span class="data">{{ \Carbon\Carbon::parse($travel->getLastStep()->endMarker->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row credito-posti">
                                                            <div class="col-xs-6">
                                                                <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/passeggeri-2.svg') }}" alt="@lang('profile.rent_confirmed_people')" title="@lang('profile.rent_confirmed_people')"><span class="steel">{{ $travel->people }}</span></div>
                                                                <span class="per-biglietto">@lang('profile.rent_confirmed_people')</span>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/bus-2.svg') }}" alt="@lang('profile.rent_vehicle_requested')" title="@lang('profile.rent_vehicle_requested')"><span>{{ $travel->package->name }}</span></div>
                                                                <span class="per-biglietto">@lang('profile.rent_vehicle_requested')</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @empty
                                    @lang('profile.no_active_requests')
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
