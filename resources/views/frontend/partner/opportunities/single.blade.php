@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente">
            <img src="{{ $noleggiatore->getFirstMedia('logo') ? $noleggiatore->getFirstMedia('logo')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ $noleggiatore->retailer->name }}" title="{{ $noleggiatore->retailer->name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ $noleggiatore->retailer->name }}<a href="{{ route('partner.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('partner.edit')" title="@lang('partner.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ $noleggiatore->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ $noleggiatore->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="bg-ivory padding-60">

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h2>
                                                    @lang('travels.quote.view_title') # {{ $travel->id }}
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.base_data')</h3>
                                                <hr>
                                                <div class="col col-md-3">
                                                    @lang('travels.sku')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->uuid }}
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.price')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->budget }} €
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.people')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->people }}
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.package')
                                                </div>
                                                <div class="col md-9">
                                                    @if( !empty($travel->package) )
                                                        <i class="{{ $travel->package->icon }}"></i> {{ $travel->package->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.services')</h3>
                                                <hr>
                                                @forelse( $travel->services as $service )
                                                    <div class="col-md-12"><i class="{{ $service->icon }}"></i> {{ $service->name }}</div>
                                                @empty
                                                    <div class="col-md-12">@lang('travels.no_services_asked')</div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.stop_points')</h3>
                                                <hr>
                                                @forelse( $travel->steps as $step )
                                                    <div class="row" style="margin-top:3%">
                                                        <div class="col-md-4">
                                                            {{ $step->startMarker->name }}
                                                        </div>
                                                        <div class="col-md-4">
                                                            {{ \Carbon\Carbon::parse($step->start_time)->formatLocalized('%A, %e %B %g') }} {{ \Carbon\Carbon::parse($step->start_time)->format('H:i') }}
                                                        </div>
                                                        <div class="col-md-4">
                                                            {{ $step->endMarker->name }}
                                                        </div>
                                                    </div>
                                                @empty
                                                    -
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.infos')</h3>
                                                <hr>
                                                @forelse( $travel->orderedInfos() as $info )
                                                    <div class="row" style="margin-top:3%">
                                                        <div class="col-md-12">
                                                            @if($info->included)
                                                                <span style="color:green;font-weight:bold"><i class="fas fa-check"></i> {{ $info->infos }}</span>
                                                            @else
                                                                <span style="color:darkred;font-weight:bold"><i class="fas fa-times"></i> {{ $info->infos }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @empty
                                                    -
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.added_prices')</h3>
                                                <hr>
                                                @forelse( $travel->prices as $price )
                                                    <div class="row" style="margin-top:3%">
                                                        <div class="col-md-8">
                                                            {{ $price->reason }}
                                                        </div>
                                                        <div class="col-md-4">
                                                            {{ $price->price }} €
                                                        </div>
                                                    </div>
                                                @empty
                                                    -
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <a href="{{ route('partner.takeOpportunity', ['travel' => $travel]) }}" class="bttn bttn-orange small">@lang('travels.partner_am_interested')</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br>

        </div>

    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
