@extends('frontend.layout.main')

@section('added-css')
    @parent

    <link href="{{ asset('storage/assets/DataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/frontend/css/fileinput.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente">
            <img src="{{ $noleggiatore->getFirstMedia('logo') ? $noleggiatore->getFirstMedia('logo')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ $noleggiatore->retailer->name }}" title="{{ $noleggiatore->retailer->name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ $noleggiatore->retailer->name }}<a href="{{ route('partner.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('partner.edit')" title="@lang('partner.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ $noleggiatore->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ $noleggiatore->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="bg-ivory padding-60">

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h2>
                                                    @lang('travels.quote.view_title') # {{ $travel->id }}
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.base_data')</h3>
                                                <hr>
                                                <div class="col col-md-3">
                                                    @lang('travels.sku')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->uuid }}
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.price')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->budget }} €
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.people')
                                                </div>
                                                <div class="col md-9">
                                                    {{ $travel->people }}
                                                </div>
                                                <br>
                                                <div class="col col-md-3">
                                                    @lang('travels.package')
                                                </div>
                                                <div class="col md-9">
                                                    @if( !empty($travel->package) )
                                                        <i class="{{ $travel->package->icon }}"></i> {{ $travel->package->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.services')</h3>
                                                <hr>
                                                @forelse( $travel->services as $service )
                                                    <div class="col-md-12"><i class="{{ $service->icon }}"></i> {{ $service->name }}</div>
                                                @empty
                                                    <div class="col-md-12">@lang('travels.no_services_asked')</div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.stop_points')</h3>
                                                <hr>
                                                @forelse( $travel->steps as $step )
                                                    <div class="row" style="margin-top:3%">
                                                        <div class="col-md-4">
                                                            {{ $step->startMarker->name }}
                                                        </div>
                                                        <div class="col-md-4">
                                                            {{ \Carbon\Carbon::parse($step->start_time)->formatLocalized('%A, %e %B %g') }} {{ \Carbon\Carbon::parse($step->start_time)->format('H:i') }}
                                                        </div>
                                                        <div class="col-md-4">
                                                            {{ $step->endMarker->name }}
                                                        </div>
                                                    </div>
                                                @empty
                                                    -
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.infos')</h3>
                                                <hr>
                                                @forelse( $travel->orderedInfos() as $info )
                                                    <div class="row" style="margin-top:3%">
                                                        <div class="col-md-12">
                                                            @if($info->included)
                                                                <span style="color:green;font-weight:bold"><i class="fas fa-check"></i> {{ $info->infos }}</span>
                                                            @else
                                                                <span style="color:darkred;font-weight:bold"><i class="fas fa-times"></i> {{ $info->infos }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @empty
                                                    -
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.add_infos')</h3>
                                                <hr>
                                                <form action="{{ route('partner.ownedTravelStore', ['travel' => $travel]) }}" method="POST">
                                                    @csrf
                                                    <label for="telefono_autista">@lang('parnter.add_telefono_guidatore')</label>
                                                    <input type="text" class="form-control" name="telefono_autista" id="telefono_autista" value="{{ $travel->telefono_autista }}">
                                                    <br>
                                                    <label for="targa_mezzo">@lang('parnter.add_targa_mezzo')</label>
                                                    <input type="text" class="form-control" name="targa_mezzo" id="targa_mezzo" value="{{ $travel->targa_mezzo }}">
                                                    <br>
                                                    <input type="submit" value="@lang('partner.send_infos')" class="bttn bttn-orange">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.add_hidden_files')</h3>
                                                <hr>
                                                <form action="{{ route('partner.fileUploads', ['travel' => $travel]) }}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <table id="hidden_files_table" class="display compact" style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>@lang('partner.travel_file_name')</th>
                                                            <th>@lang('partner.travel_file_type')</th>
                                                            <th>@lang('partner.travel_file_size')</th>
                                                            <th>@lang('partner.travel_file_uploaded_at')</th>
                                                            <th>@lang('partner.travel_file_actions')</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse( $travel->getMedia('hidden_files') as $file )
                                                            <tr>
                                                                <td>{{ $file->file_name }}</td>
                                                                <td>{{ $file->mime_type }}</td>
                                                                <td>{{ $file->size / 1000 }} KB</td>
                                                                <td>{{ \Carbon\Carbon::parse($file->updated_at)->format('d/m/Y H:i') }}</td>
                                                                <td>
                                                                    <a href="{{ route('travelAttachmentDownload', ['media' => $file]) }}" class="btn btn-primary"><i class="fas fa-download"></i></a>
                                                                    <a href="{{ route('partner.deleteAttachment', ['travel' => $travel, 'media' => $file]) }}" class="btn btn-danger"><i class="fas fa-times"></i></a>
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="4">@lang('partner.hidden_files_no_files')</td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>@lang('partner.travel_file_name')</th>
                                                            <th>@lang('partner.travel_file_type')</th>
                                                            <th>@lang('partner.travel_file_size')</th>
                                                            <th>@lang('partner.travel_file_uploaded_at')</th>
                                                            <th>@lang('partner.travel_file_actions')</th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>

                                                    <div class="form-group">
                                                        <input type="hidden" name="file_upload_folder" value="hidden">
                                                        <label class="col-sm-3 control-label" for="hidden_files">
                                                            @lang('partner.hidden_files_no_files')
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <span class="btn btn-default btn-file">
                                                                <input id="hidden_files" name="files[]" type="file" multiple>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <h3>@lang('travels.add_public_files')</h3>
                                                <hr>
                                                <form action="{{ route('partner.fileUploads', ['travel' => $travel]) }}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <table id="hidden_files_table" class="display compact" style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>@lang('partner.travel_file_name')</th>
                                                            <th>@lang('partner.travel_file_type')</th>
                                                            <th>@lang('partner.travel_file_size')</th>
                                                            <th>@lang('partner.travel_file_uploaded_at')</th>
                                                            <th>@lang('partner.travel_file_actions')</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse( $travel->getMedia('public_files') as $file )
                                                            <tr>
                                                                <td>{{ $file->file_name }}</td>
                                                                <td>{{ $file->mime_type }}</td>
                                                                <td>{{ $file->size / 1000 }} KB</td>
                                                                <td>{{ \Carbon\Carbon::parse($file->updated_at)->format('d/m/Y H:i') }}</td>
                                                                <td>
                                                                    <a href="{{ route('travelAttachmentDownload', ['media' => $file]) }}" class="btn btn-primary"><i class="fas fa-download"></i></a>
                                                                    <a href="{{ route('partner.deleteAttachment', ['travel' => $travel, 'media' => $file]) }}" class="btn btn-danger"><i class="fas fa-times"></i></a>
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="4">@lang('partner.public_files_no_files')</td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>@lang('partner.travel_file_name')</th>
                                                            <th>@lang('partner.travel_file_type')</th>
                                                            <th>@lang('partner.travel_file_size')</th>
                                                            <th>@lang('partner.travel_file_uploaded_at')</th>
                                                            <th>@lang('partner.travel_file_actions')</th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>

                                                    <div class="form-group">
                                                        <input type="hidden" name="file_upload_folder" value="public">
                                                        <label class="col-sm-3 control-label" for="public_files">
                                                            @lang('partner.public_files_no_files')
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <span class="btn btn-default btn-file">
                                                                <input id="public_files" name="files[]" type="file" multiple>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
            <br>

        </div>

    </div>
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/fileinput.theme.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {

            $("#public_files").fileinput({
                theme: "fas",
                maxFileSize: 2048
            });

            $("#public_files_table").DataTable();

            $("#hidden_files").fileinput({
                theme: "fas",
                maxFileSize: 2048
            });

            $("#hidden_files_table").DataTable();

        } );
    </script>

@endsection
