@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
<div class="container row">
    <div class="col-md-12">
        <h4>Ci sono {{ count( $travels ) }} richieste nella tua area</h4>
    </div>

    <ul>
        @forelse( $travels as $travel )
            <li><a href="{{ route( 'partner.opportunity', ['travel' => $travel] ) }}">Vedi Viaggio</a></li>
        @empty
            <li>No Viaggi</li>
        @endforelse
    </ul>
</div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
