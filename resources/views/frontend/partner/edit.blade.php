@extends('frontend.layout.main')

@section('added-css')
    @parent

    <link href="{{ asset('storage/assets/select2/css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente">
            <img src="{{ $noleggiatore->getFirstMedia('logo') ? $noleggiatore->getFirstMedia('logo')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ $noleggiatore->retailer->name }}" title="{{ $noleggiatore->retailer->name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ $noleggiatore->retailer->name }}<a href="{{ route('partner.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('partner.edit')" title="@lang('partner.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ $noleggiatore->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ $noleggiatore->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="bg-ivory padding-60">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form action="{{ route('partner.store') }}" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="name">@lang('partner.name')</label>
                                                <input class="" type="text" id="name" name="name" placeholder="@lang('partner.name')" value="{{ $noleggiatore->retailer->name }}" required autofocus>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="description">@lang('partner.description')</label>
                                                <textarea name="description" id="description" class="">{{ $noleggiatore->retailer->description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="phone">@lang('partner.phone')</label>
                                                <input class="" type="text" id="phone" name="phone" placeholder="@lang('partner.phone')" value="{{ $noleggiatore->phone }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="address">@lang('partner.address')</label>
                                                <input class="" type="text" id="address" name="address" placeholder="@lang('partner.address')" value="{{ $noleggiatore->address }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="partita_iva">@lang('partner.partita_iva')</label>
                                                <input type="text" name="partita_iva" value="{{ $noleggiatore->partita_iva }}" id="partita_iva" placeholder="@lang('partner.partita_iva')">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="paypal_account">@lang('partner.paypal_account')</label>
                                                <input type="text" name="paypal_account" value="{{ $noleggiatore->paypal_account }}" id="paypal_account" placeholder="@lang('partner.paypal_account')">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="stripe_account">@lang('partner.stripe_account')</label>
                                                <input type="text" name="stripe_account" value="{{ $noleggiatore->stripe_account }}" id="stripe_account" placeholder="@lang('partner.stripe_account')">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="logo">@lang('partner.logo')</label>
                                                <input type="file" name="logo" class="" id="logo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label for="regions">@lang('partner.regions')</label>
                                                <select name="regions[]" class="form-control" id="regions" multiple>
                                                    @forelse( $regions as $region )
                                                        <option value="{{ $region->id }}" @if( \Illuminate\Support\Facades\Auth::user()->regions->contains($region->id) ) selected="selected" @endif>{{ $region->name_it }}</option>
                                                    @empty
                                                        <option value=""> - </option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <input type="submit" class="bttn bttn-orange small" value="{{ __('partner.save_profile') }}">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/select2/js/select2.min.js') }}"></script>

@endsection

@section('added-js')

    <script>
        jQuery(document).ready(function($) {

            $("#regions").select2({});

        } );
    </script>

@endsection