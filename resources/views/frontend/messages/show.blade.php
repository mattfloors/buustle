@extends('frontend.layout.messages')

@section('content')
    <div class="col-md-6">
        <h1>{{ $thread->subject }}</h1>
        @each('frontend.messages.partials.messages', $thread->messages, 'message')

        @include('frontend.messages.partials.form-message')
    </div>
@stop