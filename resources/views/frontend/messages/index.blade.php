@extends('frontend.layout.messages')

@section('content')
    @include('frontend.messages.partials.flash')

    @each('frontend.messages.partials.thread', $threads, 'thread', 'frontend.messages.partials.no-threads')
@stop