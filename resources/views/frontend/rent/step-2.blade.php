@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="padding-30-60 bg-ivory">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <img src="{{ asset('storage/assets/frontend/images/tappe/step2.svg') }}" alt="@lang('rent.step2')" title="@lang('rent.step2')">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <form action="{{ route('rent.noleggi.details', ['rentRequestUuid' => $rentRequest->uuid]) }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span class="title-center-grey mtop-30">@lang('rent.step2_select_rent_mode')</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 tipologia-noleggio">
                                        <div class="inner">
                                            <div class="row">
                                                <div class="col-xs-9 col-xs-offset-2 col-sm-9 col-sm-offset-2 col-md-9 col-md-offset-2 exc">
                                                    <div class="radio">
                                                        <input type="radio" value="1" id="sharable-yes" name="sharable">
                                                        <label for="sharable-yes">
                                                            @lang('rent.step2_shared')
                                                            <div class="radio-button"><i></i></div>
                                                        </label>
                                                    </div>
                                                    <p>@lang('rent.step2_shared_description')</p>
                                                    <span class="prezzo">@lang('rent.step2_shared_starting_from'): <span class="line-through">{{ round($suggestedPrice, 2) }} €</span> <span class="sale">{{ round(($suggestedPrice / 100) * ( 100 - setting('general.shared_discount') ), 2) }} €</span></span>
                                                    <small>@lang('rent.step2_bonus_from_tickets_sold')</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 tipologia-noleggio">
                                        <div class="inner">
                                            <div class="row">
                                                <div class="col-xs-9 col-xs-offset-2 col-sm-9 col-sm-offset-2 col-md-9 col-md-offset-2 exc">
                                                    <div class="radio">
                                                        <input type="radio" value="0" id="sharable-no" name="sharable">
                                                        <label for="sharable-no">
                                                            @lang('rent.step2_not_shared')
                                                            <div class="radio-button"><i></i></div>
                                                        </label>
                                                    </div>
                                                    <p>@lang('rent.step2_not_shared_description')</p>
                                                    <span class="prezzo">@lang('rent.step2_not_shared_starting_from'): <span class="price">{{ round($suggestedPrice, 2) }} €</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span class="title-center-grey">@lang('rent.step2_your_details')</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 tipologia-noleggio">
                                        <div class="inner">
                                            <div class="row">
                                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-6 input">
                                                            <label for="first_name">@lang('rent.step2_form_name')*</label>
                                                            <input type="text" id="first_name" name="first_name" placeholder="@lang('rent.step2_form_name_placeholder')" @if(\Illuminate\Support\Facades\Auth::check()) value="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" @endif>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 input">
                                                            <label for="last_name">@lang('rent.step2_form_surname')*</label>
                                                            <input type="text" id="last_name" name="last_name" placeholder="@lang('rent.step2_form_surname_placeholder')" @if(\Illuminate\Support\Facades\Auth::check()) value="{{ \Illuminate\Support\Facades\Auth::user()->profile->last_name }}" @endif>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 input">
                                                            <label for="email">@lang('rent.step2_form_email')*</label>
                                                            <input type="email" id="email" name="email" placeholder="@lang('rent.step2_form_email_placeholder')" @if(\Illuminate\Support\Facades\Auth::check()) value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" @endif>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 input">
                                                            <label for="phone">@lang('rent.step2_form_phone')*</label>
                                                            <input type="tel" id="phone" name="phone" placeholder="@lang('rent.step2_form_phone_placeholder')" @if(\Illuminate\Support\Facades\Auth::check()) value="{{ \Illuminate\Support\Facades\Auth::user()->profile->phone }}" @endif>
                                                        </div>
                                                        @if( !\Illuminate\Support\Facades\Auth::check() )
                                                            <div class="col-sm-6 col-md-6 input">
                                                                <label for="password">@lang('rent.step2_form_password')*</label>
                                                                <input type="password" id="password" name="password">
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 input">
                                                                <label for="password_confirmation">@lang('rent.step2_form_password_confirm')*</label>
                                                                <input type="password" id="password_confirmation" name="password_confirmation">
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if( !\Illuminate\Support\Facades\Auth::check() )
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                            <div class="checkbox">
                                                <input type="checkbox" value="terms" id="terms">
                                                <label for="terms">
                                                    @lang('rent.step2_accept') <a href="#" title="@lang('rent.step2_terms_and_conditions')">@lang('rent.step2_terms_and_conditions')</a> @lang('global.and') <a href="#" title="@lang('rent.step2_privacy')">@lang('rent.step2_privacy')</a>
                                                    <div class="check-button"><i></i></div>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <input type="checkbox" value="newsletter" id="newsletter">
                                                <label for="newsletter">
                                                    @lang('rent.step2_subscribe_newsletter')
                                                    <div class="check-button"><i></i></div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                        <input type="submit" class="bttn bttn-orange small" value="@lang('rent.step2_send_form')">
                                        @if( !\Illuminate\Support\Facades\Auth::check() )
                                            <br>
                                            <small class="text-center">@lang('rent.step2_already_have_account') <a href="{{ route('login', ['redirect' => 'rent-request']) }}" title="@lang('rent.step2_do_login')" class="orange">@lang('rent.step2_do_login')</a></small>
                                            <a href="{{ route('login.provider', 'google') }}?redirect=rent-request" class="bttn bttn-google small"><span>@lang('rent.step2_login_with_google')</span></a>
                                            <a href="{{ route('login.provider', 'facebook') }}?redirect=rent-request" class="bttn bttn-facebook small"><span>@lang('rent.step2_login_with_facebook')</span></a>
                                            <a href="{{ route('login.provider', 'twitter') }}?redirect=rent-request" class="bttn bttn-twitter small"><span>@lang('rent.step2_login_with_twitter')</span></a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
