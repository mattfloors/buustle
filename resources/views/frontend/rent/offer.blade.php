@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

<!-- content tabs -->
<div class="container row">

  <div class="col-md-12">
        <h2>Offerta</h2>
  </div>

    <div class="col-md-12 text-left">
        <h4>La tua Richiesta</h4>
        <b>Persone: {{ $rentRequest->people }}</b><br>
        <b>Km Stimati: ~{{ $rentRequest->kilometers }} km.</b><br>
        <b>Servizi:</b><br>
        @if ( !empty($rentRequest->services) )
            <ul>
                @foreach( $rentRequest->services as $service )
                    <li><i class="{{ $service->icon }}"></i> {{ $service->name }}</li>
                @endforeach
            </ul>
        @else
            <ul>
                <li>Nessun servizio Richiesto</li>
            </ul>
        @endif
        <b>Commento:</b><br>
        <p>{{ $rentRequest->comment }}</p>

        <hr>

        <b>Step:</b><br>
        <ol>
            @forelse( $rentRequest->requestSteps as $step )
                <li>
                    <ul>
                        <li>Partenza: <b>{{ $step->start_address }}</b> il <u>\</u></li>
                        <li>Arrivo: <b>{{ $step->end_address }}</b></li>
                    </ul>
                </li>
            @empty
                @lang('rent.error_no_steps')
            @endforelse
        </ol>
        
    </div>

    <div class="col-md-12">
        <h4>Offerte</h4>
        <p>Proposta di prezzo: {{ round($suggestedPrice, 2) }} €</p><br>
        <p>Proposta Prezzo Scontato: {{ round(($suggestedPrice / 100) * 97, 2) }} €</p>
    </div>

    <div class="col-md-6">
        <form class="form" method="post" action="{{ route('rent.noleggi.acceptOffer', ['rentRequest' => $rentRequest]) }}">
            @csrf

            <input type="hidden" name="sharable" value="1">
            <button type="submit" class="btn btn-success">@lang('rent.accept_to_share')</button>
        </form>
    </div>

    <div class="col-md-6">
        <form class="form" method="post" action="{{ route('rent.noleggi.acceptOffer', ['rentRequest' => $rentRequest]) }}">
            @csrf

            <input type="hidden" name="sharable" value="0">
            <button type="submit" class="btn btn-dark">@lang('rent.refuse_to_share')</button>
        </form>
    </div>

</div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
