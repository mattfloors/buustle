@extends('frontend.layout.main')

@section('added-css')
    @parent
    
@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-conferma.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-conferma-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-conferma.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-conferma.svg') }}" alt="Buustle" title="Buustle">
        </picture>
    </div>
    <div class="container conferma padding-0-60">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
                <h1>@lang('thank_you.title')</h1>
                <p>@lang('thank_you.description')</p>
                <br>
                <a href="{{ route('profile.requests') }}" class="bttn bttn-orange" title="Torna al profilo"><span>@lang('thank_you.back_to_profile')</span></a>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
