<div class="row text-center">
    <a @if($type === 'a') href="javascript:void(0);" @else href="{{ route('rent.noleggi.request', ['type' => 'a']) }}" @endif class="btn btn-primary @if($type === 'a') disabled @endif">Andata</a>
    <a @if($type === 'r') href="javascript:void(0);" @else href="{{ route('rent.noleggi.request', ['type' => 'r']) }}" @endif class="btn btn-primary @if($type === 'r') disabled @endif">Andata e Ritorno</a>
    <!-- <a @if($type === 't') href="javascript:void(0);" @else href="{{ route('rent.noleggi.request', ['type' => 't']) }}" @endif class="btn btn-primary @if($type === 't') disabled @endif">Tour</a>-->
</div>