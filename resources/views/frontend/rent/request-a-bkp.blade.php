@extends('frontend.layout.main')

@section('added-css')
    @parent

    <link href="{{ asset('storage/assets/datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <style>
        #google_map {

            height:100%;

        }

        body.dragging, body.dragging * {
            cursor: move !important;
        }

        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        ol.example li.placeholder {
            position: relative;
            /** More li styles **/
        }
        ol.example li.placeholder:before {
            position: absolute;
            /** Define arrowhead **/
        }
    </style>
@endsection

@section('content')
<div id="noleggia-landing">
    <form id="noleggio-viaggio" method="POST" action="{{ route('rent.noleggi.save') }}">
        @csrf

        <input type="hidden" name="type" value="{{ $type }}">

      <!-- title -->
      <div class="section heading">
        <div class="wrapper">
          <h1 class="payoff black">Noleggia il tuo Bus - Andata</h1>
          <p class="paragraph">bla bla</p>
        </div>
      </div>
      <!-- end title -->

        @include('frontend.rent.shared.form-types')

    <!-- content tabs -->
        <div class="container row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="label">Città di Partenza</label>
                    <input class="form-control" id="noleggia_tour_partenza" type="text" placeholder="Inserisci città di Partenza"/>

                    <input type="hidden" name="posizione_w-partenza" id="tour_posizione_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="citta_w-partenza" id="tour_citta_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="via_w-partenza" id="tour_via_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="lat_w-partenza" id="tour_lat_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="log_w-partenza" id="tour_lng_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="stato_w-partenza" id="tour_stato_w-partenza" class="hidden quote_hidden_input form-control">
                    <input type="hidden" name="provincia_w-partenza" id="tour_provincia_w-partenza" class="hidden quote_hidden_input form-control">

                </div>
                <div class="form-group">
                    <label class="label">Città di Arrivo</label>
                    <input class="form-control" id="noleggia_tour_arrivo" type="text"
                           placeholder="Inserisci città di Arrivo"/>
                    <input type="hidden" name="posizione_w-arrivo" id="tour_posizione_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="citta_w-arrivo" id="tour_citta_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="via_w-arrivo" id="tour_via_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="lat_w-arrivo" id="tour_lat_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="log_w-arrivo" id="tour_lng_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="stato_w-arrivo" id="tour_stato_w-arrivo" class="hidden form-control">
                    <input type="hidden" name="provincia_w-arrivo" id="tour_provincia_w-arrivo" class="hidden form-control">

                </div>


                <ol class="listOfStep" id="tappe-intermedie">

                </ol>

                <div class="form-group">
                    <label class="label">Persone Confermate</label>
                    <input type="number" id="people" name="people" min="1" placeholder="1" class="form-control" required>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <a href="javascript:void(0);" id="aggiungiTappa" class="btn btn--link"><span class="icon icon--plus"></span>Aggiungi tappa</a>
            </div>

            <input type="hidden" name="tour_waypoints" id="tour_waypoints" value="0">

            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="box counter-distance">
                    <p class="paragraph">
                        Chilometri totali Stimati
                        <strong class="map_km_mob"></strong>
                        <input type="hidden" name="km_tratta" id="km_tratta">
                    </p>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-outline-dark" type="submit">Salva</button>
            </div>

        </div>
        <!-- end content tabs -->

        <div class="container row">
            <div class="col-md-12" style="height:350px;">
                <div id="google_map"></div>
            </div>
        </div>

    </form>
</div>
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/js/sortable.min.js') }}"></script>
@endsection

@section('added-js')
    <?php if ($_SERVER['SERVER_NAME'] == 'www.rent.buustle.com' || $_SERVER['SERVER_NAME'] == 'rent.buustle.com') :?>
    <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyBjVBna1BziI1odEaNZBv2AO9B2Y8MNOwU&libraries=places"></script>
    <?php else : ?>
    <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyDvFmEA0NWFv2Celnf_2qPmhAGcQjQReSw&libraries=places"></script>
    <?php endif; ?>

    <script>
        $(document).ready(function () {
            var map;

            var check_partenza = 0;
            var check_arrivo = 0;
            var check_citta = 0;

            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;

            var options = {
                componentRestrictions: {country: ['it', 'fr', 'de', 'ch', 'at']},
            };

            var tour_tappa_id = 0;
            var tour_points_div = $("#tappe-intermedie");
            var fire_element = $("#aggiungiTappa");

            var listOfSortables = $("ol.listOfStep").sortable({
                onDrop: function ($item, container, _super, event) {

                    $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                    $("body").removeClass(container.group.options.bodyClass);
                    calculateAndDisplayRoute2(directionsService, directionsDisplay);

                }
            });

            listOfSortables.sortable("enable");

            $('#noleggio-viaggio').on('beforeValidate', function (event, messages, deferreds) {
                $('#loader_fade').fadeIn(100);
            });

            function noleggiaInput() {

                $('#soloandata input').attr("required", true);

                if (!check_partenza || !check_arrivo) {

                    $(document).keypress(function (event) {

                        if (event.keyCode === 10 || event.keyCode === 13)
                            event.preventDefault();
                    });

                }

                $('.nav-tabs').find('li').click(function () {

                    check_partenza = 0;
                    check_arrivo = 0;
                    directionsDisplay.set('directions', null);

                    $('#selects input').val('');

                    map.setZoom(5);
                    map.setCenter({lat: 41.909986, lng: 12.3959116});

                    var tab = $(this).attr('id');

                    $('#selects input').removeAttr('required');
                    $('#selects input').attr('disabled', true);
                    $('#noleggia_submit').attr('name', 'noleggia_tour');
                    $('#noleggiatour input').attr("required", true).removeAttr('disabled');
                    $('.not_required').find('input').removeAttr('required');
                    $('.date_not_required, #noleggia_tour_w2, #noleggia_tour_w3').removeAttr('required');

                });

            }

            /** starting with partenza / arrivo */
            var tour_partenza_input = document.getElementById('noleggia_tour_partenza');
            var tour_arrivo_input = document.getElementById('noleggia_tour_arrivo');

            var tour_noleggia_partenza = new google.maps.places.Autocomplete(tour_partenza_input, options);
            var tour_noleggia_arrivo = new google.maps.places.Autocomplete(tour_arrivo_input, options);

            /** initializing map */
            map = new google.maps.Map(document.getElementById('google_map'), {
                zoom: 5,
                center: {lat: 41.909986, lng: 12.3959116},
                scrollwheel: false
            });

            directionsDisplay.setMap(map);

            /** initializing start and stop */
            tour_noleggia_partenza.addListener('place_changed', function () {

                check_partenza = 1;

                addDataInput2(tour_noleggia_partenza, 'partenza', tour_partenza_input);

                if (check_arrivo && check_arrivo && check_citta) {
                    calculateAndDisplayRoute2(directionsService, directionsDisplay);
                }

            });

            tour_noleggia_arrivo.addListener('place_changed', function () {

                check_arrivo = 1;

                addDataInput2(tour_noleggia_arrivo, 'arrivo', tour_arrivo_input);

                if (check_partenza && check_arrivo && check_citta) {
                    calculateAndDisplayRoute2(directionsService, directionsDisplay);
                }

            });

            /**
             * Clicking on the "add waypoint" button
             */
            fire_element.on('click', function(e) {
                e.preventDefault();
                generateTourStop();
            });

            /**
             * handles the waypoint row generation
             */
            function generateTourStop() {
                /**
                 * 1 - add template to view
                 * 2 - bind to element id autocomplete
                 * 3 - bind data and datepicker
                 * 4 - bind deleter
                 * 5 - bind return
                 * 6 - +1 to tour counter
                 * 7 - Saving max waypoints
                 */

                /** 1 **/
                var templateHeader = getTourTemplate(tour_tappa_id);
                tour_points_div.append(templateHeader);

                /** 2 */
                var step_text_input = document.getElementById('noleggia_tappa-' + tour_tappa_id);
                var step_place_complete = new google.maps.places.Autocomplete(step_text_input, options);
                var thisId = tour_tappa_id;
                step_place_complete.addListener('place_changed', function () {

                    check_arrivo = 1;

                    addDataInput2(step_place_complete, thisId, step_text_input);

                    if (check_partenza && check_arrivo && check_citta) {
                        calculateAndDisplayRoute2(directionsService, directionsDisplay);
                    }

                });

                /** 3 */

                /** 4 */
                $("#delete_waypoint-" + tour_tappa_id).on('click', function(e) {
                    e.preventDefault();

                    var idToDelete = $(this).data('id');

                    deleteWaypoint(idToDelete);
                });

                /** 5 */
                $("#tappa_ritorno-" + tour_tappa_id).on('click', function(e) {
                    e.preventDefault();

                    var idToReturn = $(this).data('id');

                    bindToReturn(idToReturn);
                });

                /** 6 */
                tour_tappa_id = tour_tappa_id + 1;

                /** 7 */
                $("#tour_waypoints").val(tour_tappa_id);

                listOfSortables.sortable("refresh");
            }

            /**
             * Takes start + endpoint
             * Loads waypoints
             * Create route
             * calculate kms
             **/
            function calculateAndDisplayRoute2(directionsService, directionsDisplay) {

                //Pulisci la mappa
                directionsDisplay.set('directions', null);

                var placeIdStart = document.getElementById('tour_posizione_w-partenza').value;
                var placeIdEnd = document.getElementById('tour_posizione_w-arrivo').value;
                var waypts = [];
                var i = 0;
                var count = 0;

                $('.noleggia_waypoint').each(function () {
                    if ($(this).val() !== '') {
                        count = count + 1;
                    }
                });

                $('.noleggia_waypoint').each(function () {
                    var myInputId = $(this).data('id');
                    if ($(this).val() !== '') {
                        //ok, taking last placeIdEnd and putting it into waypoints and set this waypoint as last spot
                        if(i === 0) {
                            waypts.push({
                                location: $("#noleggia_tour_arrivo").val(),
                                stopover: true
                            });

                            i = i + 1;
                        }

                        if(i < count) {
                            waypts.push({
                                location: $(this).val(),
                                stopover: true
                            });
                        } else {
                            placeIdEnd = $("#tour_posizione_w-" + myInputId).val();
                        }

                        i = i + 1;
                    }
                });

                i = 0;

                directionsService.route({
                    origin:  {'placeId': placeIdStart},
                    destination:  {'placeId': placeIdEnd},
                    waypoints: waypts,
                    optimizeWaypoints: false,
                    travelMode: 'DRIVING'
                }, function (response, status) {

                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                        var km_total = 0;
                        for (var i = 0; i < route.legs.length; i++) {
                            var routeSegment = i + 1;
                            km_total += (route.legs[i].distance.value) / 1000;
                        }
                        km_total = Math.round(km_total);
                        $('#map_km').html(km_total + ' km');
                        $('.map_km_mob').html(km_total);
                        $('#km_tratta').val(km_total);
                    } else {
                        console.log(response);
                    }
                });

            }

            /**
             * Adding the waypoint to the list
             * Adds google data to the hidden input fields
             * Checks everything is fine
             **/
            function addDataInput2(input, id, field) {

                var place = input.getPlace();

                document.getElementById('tour_posizione_w-' + id).value = place.place_id;

                if (place.types[0] !== 'country' && place.types[0].indexOf("administrative_area") < 0) {
                    check_citta = 1;
                } else {
                    check_citta = 0;
                }

                if (check_citta) {

                    $.each(place.address_components, function () {
                        var str = $(field).val().split(',');

                        if (this.types[0] === 'locality') {
                            var element = document.getElementById('tour_citta_w-' + id).value = this.long_name;
                        }

                        if (this.types[0] === "country") {
                            document.getElementById('tour_stato_w-' + id).value = this.short_name;
                        }

                        if (this.types[0] === "administrative_area_level_2") {
                            document.getElementById('tour_provincia_w-' + id).value = this.short_name;

                        }

                        if (element == null) {
                            document.getElementById('tour_citta_w-' + id).value = str[0];
                        }

                    });

                    var lat = place.geometry.location.lat;
                    var lng = place.geometry.location.lng;

                    $('#tour_lat_w-' + id).val(lat);
                    $('#tour_lng_w-' + id).val(lng);

                    document.getElementById('tour_via_w-' + id).value = $(field).val();

                } else {

                    $(field).val("");
                    $(field).attr('placeholder', 'Sii più preciso!');

                }

            }

            /**
             * Handling deletion of a tour waypoint
             * Basic stuff just check that everything is fine and remove bindings
             * @param id
             */
            function deleteWaypoint(id) {
                $("#tappa-" + id).remove();
                calculateAndDisplayRoute2(directionsService, directionsDisplay);
            }

            function bindToReturn(id) {
                $("#noleggia_tappa-" + id).val($("#noleggia_tour_partenza").val());
                $("#tour_posizione_w-" + id).val($("#tour_posizione_w-partenza").val());
                $("#tour_citta_w-" + id).val($("#tour_citta_w-partenza").val());
                $("#tour_via_w-" + id).val($("#tour_via_w-partenza").val());
                $("#tour_lat_w-" + id).val($("#tour_lat_w-partenza").val());
                $("#tour_lng_w-" + id).val($("#tour_lng_w-partenza").val());
                $("#tour_stato_w-" + id).val($("#tour_stato_w-partenza").val());
                $("#tour_provincia_w-" + id).val($("#tour_provincia_w-partenza").val());

                calculateAndDisplayRoute2(directionsService, directionsDisplay)
            }

            /**
             * Returns the row template for the waypoint generated with an id to reference it afterwards
             * @param id
             * @returns {jQuery|HTMLElement}
             */
            function getTourTemplate(id) {
                var myTemplate = "<li class=\"classepercount\" id=\"tappa-" + id + "\">" +

                    "<div class=\"form-group\">\n"+
                    "<input data-id=\"" + id + "\" id=\"noleggia_tappa-" + id + "\" class=\"input form-control noleggia_waypoint\" type=\"text\" placeholder=\"Destinazione\">"+
                    "<input id=\"tour_posizione_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_posizione_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_citta_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_citta_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_via_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_via_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_lat_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_lat_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_lng_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_log_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_stato_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_stato_w[" + id + "]\" required=\"required\" type=\"hidden\">" +
                    "<input id=\"tour_provincia_w-" + id + "\" class=\"quote_hidden_input\" name=\"new_provincia_w[" + id + "]\" required=\"required\" type=\"hidden\">"+
                    "</div>"+

                    "<div class=\"form-group\">" +
                    "<a class='btn btn--link btn--discard' href=\"javascript:void(0);\" id=\"delete_waypoint-" + id + "\" data-id=\"" + id + "\"><span class='icon icon--delete'></span>Cancella</a>" +
                    "</div></li>";

                return $(myTemplate);
            }

            $(function () {
                noleggiaInput();
            });
        });
    </script>
@endsection
