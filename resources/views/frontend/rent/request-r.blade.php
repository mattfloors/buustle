@extends('frontend.layout.main')

@section('added-css')
    @parent

    <link href="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.min.css') }}" rel="stylesheet">

    <style>
        #map {

            height:400px;

        }
    </style>
@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-noleggio.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-noleggio-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-noleggio.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-noleggio.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title">@lang('rent.request_step1_a_title')</h1>
                            <h2 class="subtitle">@lang('rent.request_step1_a_subtitle')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="padding-30-60 bg-ivory" id="trip">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <img src="{{ asset('storage/assets/frontend/images/tappe/step1.svg') }}" alt="@lang('rent.step1')" title="@lang('rent.step1')">
                        </div>
                    </div>
                    <div class="row mtop-60">
                        <div class="col-xs-12 col-sm-5 col-md-5">
                            <div class="tab-noleggio">
                                <a href="{{ route('rent.noleggi.request', ['type' => 'r']) }}#trip" class="text-center active">@lang('rent.button_form_return')</a>
                                <a href="{{ route('rent.noleggi.request', ['type' => 'a']) }}#trip" class="text-center">@lang('rent.button_form_only_way')</a>
                            </div>
                            <form action="{{ route('rent.noleggi.save') }}" method="post">
                                @csrf
                                <input type="hidden" name="type" value="r">
                                <div class="form-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                            <div class="tappe" id="tappe-aggiuntive">
                                                <div class="input partenza sortIgnore" id="step-0">
                                                    <label for="w-0">@lang('rent.form_label_starting_point')</label>
                                                    <input class="rent_steps" type="text" required="required" data-id="0" id="w-0" name="w[]" placeholder="@lang('rent.form_placeholder_starting_point')" value="">
                                                    <input id="lat_w-0" name="lat_w[]" value="" required="required" type="hidden">
                                                    <input id="lng_w-0" name="lng_w[]" value="" required="required" type="hidden">
                                                </div>

                                                <div class="input arrivo singleMarker sortIgnore" id="step-e">
                                                    <label for="w-e">@lang('rent.form_label_ending_point')</label>
                                                    <input class="rent_steps" type="text" name="w[]" required="required" data-id="e" id="w-e" placeholder="@lang('rent.form_placeholder_ending_point')" value="">
                                                    <input id="lat_w-e" name="lat_w[]" value="" required="required" type="hidden">
                                                    <input id="lng_w-e" name="lng_w[]" value="" required="required" type="hidden">
                                                </div>
                                                <div class="input tappa sortIgnore" id="div-add-marker">
                                                    <label><a href="javascript:void(0);" id="aggiungiTappa" title="@lang('rent.form_button_add_marker_alt')">@lang('rent.form_button_add_marker')</a></label>
                                                </div>
                                            </div>
                                            <div class="select-container mtop-30">
                                                <label for="people">@lang('rent.form_label_people')</label>
                                                <select id="people" name="people" class="selectpicker" title="@lang('rent.form_title_people')" required>
                                                    @forelse( $peopleRanges as $range )
                                                        <option value="{{ $range->id }}">@lang('global.from') {{ $range->min }} @lang('global.to') {{ $range->max }} @lang('rent.people')</option>
                                                    @empty
                                                        <option value="">-</option>
                                                    @endforelse
                                                    <option value="more">@lang('rent.more_people')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0">
                                        <div class="parziale">
                                            <div class="km">
                                                <span>@lang('rent.form_info_km_total'):</span>
                                                <span id="map_km_mob">-</span>
                                                <input type="hidden" name="km_tratta" id="km_tratta">
                                            </div>
                                            <div class="prezzo">
                                                <span>@lang('rent.form_info_predicted_price'):</span>
                                                <span id="predicted_price">-</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                                        <input type="submit" class="bttn bttn-orange small" value="@lang('rent.form_button_continue')">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container noleggia-content">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <h2>@lang('rent.step1_subsection_title')</h2>
                <p>@lang('rent.step1_subsection_subtitle')</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/gruppi.svg') }}" alt="@lang('rent.step1_group_travels')" title="@lang('rent.step1_group_travels')"></div>
                            <h3>@lang('rent.step1_group_travels')</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/scuola.svg') }}" alt="@lang('rent.step1_school_trips')" title="@lang('rent.step1_school_trips')"></div>
                            <h3>@lang('rent.step1_school_trips')</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/festival.svg') }}" alt="@lang('rent.step1_events_and_festivals')" title="@lang('rent.step1_events_and_festivals')"></div>
                            <h3>@lang('rent.step1_events_and_festivals')</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="@lang('rent.step1_sport_events')" title="@lang('rent.step1_sport_events')"></div>
                            <h3>@lang('rent.step1_sport_events')</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/lavoro.svg') }}" alt="@lang('rent.step1_corporate_meetings')" title="@lang('rent.step1_corporate_meetings')"></div>
                            <h3>@lang('rent.step1_corporate_meetings')</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/matrimonio.svg') }}" alt="@lang('rent.step1_wedding_cerimonies')" title="@lang('rent.step1_wedding_cerimonies')"></div>
                            <h3>@lang('rent.step1_wedding_cerimonies')</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/js/lib/datetime/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/html5sortable.min.js') }}"></script>
@endsection

@section('added-js')

    @if( env('APP_ENV') === 'local' )
        <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyDvFmEA0NWFv2Celnf_2qPmhAGcQjQReSw&libraries=places"></script>
    @else
        <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyBjVBna1BziI1odEaNZBv2AO9B2Y8MNOwU&libraries=places"></script>
    @endif

    <script>
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var nextId = 1;
        var totalRouteKm = 0;
        var peopleSelect = document.getElementById('people');

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 5,
                center: {lat: 41.909986, lng: 12.3959116}
            });
            directionsDisplay.setMap(map);

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        }

        /**
         * returns an array of waypoints
         * @returns {array}
         */
        function getWaypoints() {

            let elements = document.getElementsByClassName('rent_steps');
            let returnArray = [];

            for ( let e = 0; e < elements.length; e++ ) {

                if ( e !== (elements.length - 1 ) && e !== 0 ) {

                    returnArray.push({
                        location: elements[e].value,
                        stopover: true
                    });

                }

            }

            return returnArray;
        }

        /**
         * returns the id of the last input element
         * @returns {string}
         */
        function firstStepId() {

            let elements = document.getElementsByClassName('rent_steps');
            let firstStep = (elements.length > 0) ? elements[0].id : 'w-0';

            return firstStep.toString();

        }

        /**
         * returns the id of the last input element
         * @returns {string}
         */
        function lastStepId() {

            /*let elements = document.getElementsByClassName('rent_steps');
            let lastElement = (elements.length > 0) ? elements[elements.length - 1].id : 'w-0';

            return lastElement.toString();*/

            return 'w-e';

        }

        /**
         * Calculate and display the route based on params
         * @param directionsService
         * @param directionsDisplay
         */
        function calculateAndDisplayRoute(directionsService, directionsDisplay) {

            let waypts = getWaypoints();

            if(document.getElementById(firstStepId()).value === '' || lastStepId() === '' || document.getElementById(lastStepId()).value === '') {

                return;

            }

            directionsService.route({
                origin: document.getElementById(firstStepId()).value,
                destination: document.getElementById(lastStepId()).value,
                waypoints: waypts,
                optimizeWaypoints: false,
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                    let route = response.routes[0];

                    totalRouteKm = 0;

                    // For each route, display summary information.
                    for (var i = 0; i < route.legs.length; i++) {
                        totalRouteKm += parseInt(route.legs[i].distance.value / 1000);
                    }

                    if( totalRouteKm > 0 ) {

                        document.getElementById('km_tratta').value = totalRouteKm;
                        document.getElementById('map_km_mob').innerText = totalRouteKm.toString() + " KM";

                        if ( peopleSelect.selectedIndex !== undefined && parseInt(peopleSelect.options[peopleSelect.selectedIndex].value) > 0 ) {

                            updatePrice( parseInt(peopleSelect.options[peopleSelect.selectedIndex].value), totalRouteKm );

                        }

                    }


                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }

        function updatePrice( people, km ) {

            $("#predicted_price").empty().html('<i class="fas fa-sync fa-spin"></i>');
            let end_lat = document.getElementById('lat_' + lastStepId()).value;
            let end_lng = document.getElementById('lng_' + lastStepId()).value;
            let start_lat = document.getElementById('lat_' + firstStepId()).value;
            let start_lng = document.getElementById('lng_' + firstStepId()).value;

            jQuery.get({
                url: '{!! route('rent.noleggi.partialOffer') !!}?people=' + people + '&km=' + km + '&start_lat=' + start_lat + '&start_lng=' + start_lng + '&end_lat=' + end_lat + '&end_lng=' + end_lng,
                context: document.body,
                success: function( data ){
                    if ( data.status === 'success' ) {

                        $("#predicted_price").empty().text('~ ' + data.valueRounded + ' €');

                    } else {

                        $("#predicted_price").empty().text(data.message);

                    }
                }
            });

        }

        $(document).ready(function() {

            let createStepButton = $("#aggiungiTappa");
            let deleteButtons = $(".deleteButtons");
            let lastButton = $("#div-add-marker");
            let autocompleteOptions = {
                componentRestrictions: {country: ['it', 'fr', 'de', 'ch', 'at']},
            };
            let people = $("#people");
            let sortableRules = {
                items: ':not(.sortIgnore)',
                forcePlaceholderSize: true
            };
            let lastElement = $("#step-e");

            sortable("#tappe-aggiuntive", sortableRules);

            sortable("#tappe-aggiuntive")[0].addEventListener('sortupdate', function(e) {
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            });

            $.datetimepicker.setLocale('{{ \Illuminate\Support\Facades\App::getLocale() }}');

            bindGoogleSearch( 0 );
            bindGoogleSearch( 'e' );
            //bindGoogleSearch( 1 );

            deleteButtons.each( function( index ) {

                let idToRemove = $( this ).data('id');

                $( this ).on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    deleteRouteStep(idToRemove);
                });

            } );

            createStepButton.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                createNewStep();
            });

            people.change(function() {

                let totalKm = $("#km_tratta").val();

                if ( totalKm !== 'undefined' && parseInt(totalKm) > 0 && $( this ).val() !== '' && parseInt($( this ).val()) > 0 ) {

                    updatePrice( parseInt($( this ).val()), totalKm );

                }
            });

            function createNewStep() {

                createAndAppendStepHtml(lastElement, nextId);

                nextId += 1;

            }

            function createAndAppendStepHtml( el, id ) {

                let template = getStepTemplate(id);

                template.insertBefore(el);

                bindDeleteElement( id );

                bindGoogleSearch( id );

                sortable("#tappe-aggiuntive");

            }

            function bindGoogleSearch( id ) {

                let input = document.getElementById('w-' + id);

                let autocomplete = new google.maps.places.Autocomplete(input, autocompleteOptions);

                autocomplete.addListener('place_changed', function () {

                    bindAdditionalPlaceData( id, autocomplete.getPlace() );

                    calculateAndDisplayRoute(directionsService, directionsDisplay);

                });

            }

            function bindAdditionalPlaceData( id, data ) {

                $("#lat_w-" + id).val(data.geometry.location.lat);
                $("#lng_w-" + id).val(data.geometry.location.lng);

            }

            function bindDeleteElement( id ) {

                $("#delete-step-" + id).on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    deleteRouteStep( id );
                });

            }

            function deleteRouteStep( id ) {

                $("#step-" + id).remove();

                calculateAndDisplayRoute(directionsService, directionsDisplay);

            }

            function getStepTemplate( id ) {

                return $("<div class=\"input arrivo singleMarker\" id=\"step-" + id + "\">\n" +
                    "<div class=\"delete\" id=\"delete-step-" + id + "\"><img src=\"{{ asset('storage/assets/frontend/images/icons-svg/delete.svg') }}\" alt=\"@lang('rent.delete_step')\" title=\"@lang('rent.delete_step')\"></div>" +
                    "<label for=\"w-" + id + "\">@lang('rent.form_label_additional_point')</label>\n" +
                    "<input class=\"rent_steps\" type=\"text\" name=\"w[]\" required=\"required\" data-id=\"" + id + "\" id=\"w-" + id + "\" placeholder=\"@lang('rent.form_placeholder_additional_point')\" value=\"\">\n" +
                    "<input id=\"lat_w-" + id + "\" name=\"lat_w[]\" value=\"\" required=\"required\" type=\"hidden\">\n" +
                    "<input id=\"lng_w-" + id + "\" name=\"lng_w[]\" value=\"\" required=\"required\" type=\"hidden\">\n" +
                    "</div>");

            }

            initMap();

        });

    </script>

@endsection
