@extends('frontend.layout.main')

@section('added-css')
    @parent
    
@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-30-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <img src="{{ asset('storage/assets/frontend/images/tappe/step4.svg') }}" alt="@lang('rent.step4')" title="@lang('rent.step4')">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <form action="{{ route('rent.noleggi.complete', ['rentRequestUuid' => $rentRequest->uuid]) }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span class="title-center-grey mtop-30">@lang('rent.step4_check_confirm')</span>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="scheda01">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                                    <div class="inner">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-xs-10 col-xs-offset-1">
                                                                    <div class="row viaggi" style="height: auto;margin-bottom: 10px">
                                                                        <div class="col-xs-5">
                                                                            <span class="citta">{{ \App\Http\Services\Maps::loadCityName($rentRequest->getFirstStep()->start_lat, $rentRequest->getFirstStep()->start_lng) }}</span>
                                                                            <span class="indirizzo">{{ $startAddress }}</span>
                                                                            <span class="data">{{ \Carbon\Carbon::parse($rentRequest->getFirstStep()->start_time)->formatLocalized('%A, %e %B %G - %H:%m') }}</span>
                                                                        </div>
                                                                        <div class="col-xs-1"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                                        <div class="col-xs-5">
                                                                            <span class="citta">{{ \App\Http\Services\Maps::loadCityName($lastLat, $lastLon) }}</span>
                                                                            <span class="indirizzo">{{ $lastAddress }}</span>
                                                                            <span class="data">{{ \Carbon\Carbon::parse($lastTime)->formatLocalized('%A, %e %B %G - %H:%m') }}</span>
                                                                        </div>
                                                                        @if( count( $steps ) > 1 )
                                                                        <div class="col-xs-1"><a data-toggle="collapse" href="#steps" role="button" aria-expanded="true" aria-controls="steps"><i id="steps_chevron" class="icon icon-chevron-down"></i></a></div>
                                                                        <div class="row"></div>
                                                                        <div class="tappe collapse" id="steps">
                                                                            @foreach( $steps as $step )
                                                                                <div class="input arrivo box-input" >
                                                                                    <label for="w-{{ (!$loop->last) ? $loop->iteration : 'e' }}">{{ ($loop->first) ? __('rent.step3_starting_point') : __('rent.step3_intermediate_step') }}</label>
                                                                                    <input id="w-{{ (!$loop->last) ? $loop->iteration : 'e' }}" class="rent_steps" type="text" value="{{ $step->start_address }}" readonly>
                                                                                    <hr>
                                                                                    <div class="data-ora">
                                                                                        <div class="data mydata">
                                                                                                  <span class="add-on">
                                                                                                    <input class="dataForBored" type="text" value="{{\Carbon\Carbon::parse($step->start_time)->format('d/m/y')}}" readonly>
                                                                                                  </span>
                                                                                        </div>
                                                                                        <div class="ora mytime">
                                                                                                  <span class="add-on">
                                                                                                    <input class="timeForBored" type="text" value="{{\Carbon\Carbon::parse($step->start_time)->format('H:m')}}" readonly>
                                                                                                  </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                @if( $loop->last && !$isWayBack )
                                                                                    <div class="input arrivo box-input" >
                                                                                        <label for="w-{{ (!$loop->last) ? $loop->iteration : 'ee' }}">{{ __('rent.step3_ending_point') }}</label>
                                                                                        <input id="w-{{ (!$loop->last) ? $loop->iteration : 'ee' }}" class="rent_steps" type="text" value="{{ $step->end_address }}" readonly>
                                                                                        <hr>
                                                                                        <div class="data-ora">
                                                                                            <div class="data mydata">
                                                                                                  <span class="add-on">
                                                                                                    <input class="dataForBored" type="text" value="{{\Carbon\Carbon::parse($step->end_time)->format('d/m/y')}}" readonly>
                                                                                                  </span>
                                                                                            </div>
                                                                                            <div class="ora mytime">
                                                                                                  <span class="add-on">
                                                                                                    <input class="timeForBored" type="text" value="{{\Carbon\Carbon::parse($step->end_time)->format('H:m')}}" readonly>
                                                                                                  </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="row opzioni">
                                                                        <div class="col-xs-4">
                                                                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/km.svg') }}" alt="Km" title="Km">
                                                                            <span>@lang('step4.km_total')<br>km</span>
                                                                            <span class="big">{{ $rentRequest->kilometers }}</span>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/individuo-2.svg') }}" alt="@lang('step4.passengers_confirmed')" title="@lang('step4.passengers_confirmed')">
                                                                            <span>@lang('step4.passengers')<br>@lang('step4.confirmed')</span>
                                                                            <span class="big">{{ $rentRequest->people }}</span>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/bus-2.svg') }}" alt="@lang('step4.veichle_requested')" title="@lang('step4.veichle_requested')">
                                                                            <span>@lang('step4.veichle')<br>@lang('step4.requested')</span>
                                                                            <span class="big">{{ $rentRequest->package->getTranslation('name', \Illuminate\Support\Facades\App::getLocale()) }}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <hr>
                                                                </div>
                                                                <div class="col-xs-10 col-xs-offset-1">
                                                                    <div class="row opzioni dettaglio">
                                                                        <div class="col-xs-4">
                                                                            <span class="exc">@lang('step4.price')<br>@lang('step4.predicted')*</span>
                                                                            <span class="big orange">{{ $rentRequest->accepted_price }}€</span>
                                                                            <span class="small">~ {{ round($rentRequest->accepted_price / $rentRequest->people) }}€/@lang('step4.for_people')</span>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <span class="exc">@lang('step4.spots')<br>@lang('step4.available')</span>
                                                                            <span class="big">{{ $rentRequest->getBaseFreeSlots() }}</span>
                                                                        </div>
                                                                        @if( $rentRequest->sharable )
                                                                            <div class="col-xs-4">
                                                                                <span class="exc">@lang('step4.earnings')<br>@lang('step4.predicted_earning')**</span>
                                                                                <span class="big aqua">{{ $rentRequest->getBaseFreeSlots() * 15 }}€</span>
                                                                                <span class="small">15€/@lang('step4.for_ticket')</span>
                                                                            </div>
                                                                            <div class="col-xs-12">
                                                                                <div class="extra">
                                                                                    <small>*@lang('step4.warning_ztl')</small>
                                                                                    <small>**@lang('step4.warning_all_tickets_sold')</small>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="inner mtop-30">
                                                        <div class="row">
                                                            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                                                <div class="row">
                                                                    <div class="col-xs-12 input">
                                                                        <label for="nome">@lang('step4.contact_name')*</label>
                                                                        <input type="text" id="nome" name="nome" value="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" required>
                                                                    </div>
                                                                    <div class="col-xs-12 input">
                                                                        <label for="email">@lang('step4.contact_email')*</label>
                                                                        <input type="email" id="email" name="email" value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                        <input type="submit" class="bttn bttn-orange small" value="Continua">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent
    <script>
        $('.collapse').on('show.bs.collapse', function(){
            $("#steps_chevron").removeClass("icon-chevron-down").addClass("icon-chevron-up");
        }).on('hide.bs.collapse', function(){
            $("#steps_chevron").removeClass("icon-chevron-up").addClass("icon-chevron-down");
        });
    </script>
@endsection

@section('added-js')

@endsection
