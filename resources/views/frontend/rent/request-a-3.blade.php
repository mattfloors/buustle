@extends('frontend.layout.main')

@section('added-css')
    @parent

    <!--<link rel="stylesheet" href="{{ asset('storage/assets/datepicker/css/bootstrap-datepicker3.min.css') }}">-->
    <link rel="stylesheet" href="{{ asset('storage/assets/frontend/css/bootstrap-datetimepicker.min.css') }}">

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-30-60 step3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <img src="{{ asset('storage/assets/frontend/images/tappe/step3.svg') }}"
                                 alt="@lang('rent.step3')" title="@lang('rent.step3')">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="{{ route('rent.noleggi.finalStep', ['rentRequestUuid' => $rentRequest->uuid]) }}"
                                  method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span class="title-center-grey mtop-30">@lang('rent.step3_mode_details')</span>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-box">
                                            <div class="row">
                                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-6">
                                                            <div class="tappe" id="tappeContainer">
                                                                <div class="input partenza box-input sortIgnore"
                                                                     id="box-input-0">
                                                                    <label for="w-0">@lang('rent.step3_starting_point')</label>
                                                                    <input name="w[]" id="w-0" type="text"
                                                                           value="{{ $rentRequest->requestSteps[0]->start_address }}">
                                                                    <input type="hidden" name="lat_w[]" id="lat_w-0"
                                                                           class="hidden quote_hidden_input form-control"
                                                                           value="{{ $rentRequest->requestSteps[0]->start_lat }}">
                                                                    <input type="hidden" name="lng_w[]" id="lng_w-0"
                                                                           class="hidden quote_hidden_input form-control"
                                                                           value="{{ $rentRequest->requestSteps[0]->start_lng }}">
                                                                    <hr>
                                                                    <div class="data-ora">
                                                                        <div class="data mydata">
                                                                          <span class="add-on">
                                                                            <input class="dataForBored"
                                                                                   autocomplete="off"
                                                                                   name="start-date[0]"
                                                                                   id="start-date-0"
                                                                                   data-format="dd/MM/yyyy" type="text"
                                                                                   placeholder="@lang('step3.form_input_data')"
                                                                                   required>
                                                                          </span>
                                                                        </div>
                                                                        <div class="ora mytime">
                                                                          <span class="add-on">
                                                                            <input class="timeForBored"
                                                                                   autocomplete="off"
                                                                                   name="start-time[0]"
                                                                                   id="start-time-0"
                                                                                   data-format="hh:mm:ss" type="text"
                                                                                   placeholder="@lang('step3.form_input_time')"
                                                                                   required>
                                                                          </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if( count( $rentRequest->requestSteps ) > 0 )
                                                                    <?php
                                                                    $i = 1;
                                                                    $totRequests = count($rentRequest->requestSteps);
                                                                    ?>

                                                                    @foreach( $rentRequest->requestSteps as $step )
                                                                        @php
                                                                            $stepId = ($i < $totRequests || $isWayBack) ? $i : 'e';
                                                                            $stepName = ($i < $totRequests || $isWayBack) ? __('rent.step3_intermediate_step') : __('rent.step3_ending_point');
                                                                        @endphp
                                                                        <div class="input arrivo box-input @if( $i === $totRequests && !$isWayBack) sortIgnore @endif"
                                                                             id="box-input-{{ $stepId }}"
                                                                             data-inputid="{{ $stepId }}">
                                                                            <label for="w-{{ $stepId }}">@if( $i === $totRequests && $isWayBack ){{__('rent.step3_ending_point_wayback')}}@else{{ $stepName }}@endif</label>
                                                                            <input id="w-{{ $stepId }}" name="w[]"
                                                                                   data-id="{{ $stepId }}"
                                                                                   class="rent_steps" type="text"
                                                                                   value="{{ $step->end_address }}">
                                                                            <input type="hidden" name="lat_w[]"
                                                                                   id="lat_w-{{ $stepId }}"
                                                                                   class="hidden quote_hidden_input form-control"
                                                                                   value="{{ $step->end_lat }}">
                                                                            <input type="hidden" name="lng_w[]"
                                                                                   id="lng_w-{{ $stepId }}"
                                                                                   class="hidden quote_hidden_input form-control"
                                                                                   value="{{ $step->end_lng }}">
                                                                            @if ( $i < $totRequests || $isWayBack)
                                                                                <hr>
                                                                                <div class="data-ora">
                                                                                    <div class="data mydata">
                                                                                      <span class="add-on">
                                                                                        <input class="dataForBored"
                                                                                               autocomplete="off"
                                                                                               name="start-date[]"
                                                                                               id="start-date-{{ $i }}"
                                                                                               data-format="dd/MM/yyyy"
                                                                                               type="text"
                                                                                               placeholder="@lang('step3.form_input_data')"
                                                                                               required>
                                                                                      </span>
                                                                                    </div>
                                                                                    <div class="ora mytime">
                                                                                      <span class="add-on">
                                                                                        <input class="timeForBored"
                                                                                               autocomplete="off"
                                                                                               name="start-time[]"
                                                                                               id="start-time-{{ $i }}"
                                                                                               data-format="hh:mm:ss"
                                                                                               type="text"
                                                                                               placeholder="@lang('step3.form_input_time')"
                                                                                               required>
                                                                                      </span>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div>

                                                                        <?php $i++; ?>
                                                                    @endforeach
                                                                @endif
                                                                <div class="input tappa" id="addTappaContainer">
                                                                    <label><a href="javascript:void(0);" id="addTappa"
                                                                              title="@lang('rent.step3_add_marker')">@lang('rent.step3_add_marker')</a></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6">
                                                            <div class="input-container">
                                                                <label for="people">@lang('rent.step3_confirmed_passengers')</label>
                                                                <input class="form-control" type="number" name="people"
                                                                       id="people" min="{{ $peopleRange->min }}"
                                                                       max="{{ $peopleRange->max }}"
                                                                       value="{{ round(($peopleRange->min + $peopleRange->max) / 2) }}"
                                                                       required>
                                                            </div>
                                                            <div class="select-container">
                                                                <label for="travel_type">@lang('rent.step3_travel_type')</label>
                                                                <select id="travel_type" name="travel_type"
                                                                        class="selectpicker"
                                                                        title="@lang('rent.step3_select_option')">
                                                                    <option selected value="@lang('rent.step3_group_travel')">@lang('rent.step3_group_travel')</option>
                                                                    <option value="@lang('rent.step3_school_trip')">@lang('rent.step3_school_trip')</option>
                                                                    <option value="@lang('rent.step3_music_festival')">@lang('rent.step3_music_festival')</option>
                                                                    <option value="@lang('rent.step3_sport_event')">@lang('rent.step3_sport_event')</option>
                                                                    <option value="@lang('rent.step3_company_meeting')">@lang('rent.step3_company_meeting')</option>
                                                                    <option value="@lang('rent.step3_wedding_cerimony')">@lang('rent.step3_wedding_cerimony')</option>
                                                                </select>
                                                            </div>
                                                            <div class="input">
                                                                <label for="comment">@lang('rent.step3_additional_comment')</label>
                                                                <textarea name="comment"
                                                                          placeholder="@lang('rent.step3_additional_comment_placeholder')"
                                                                          id="comment"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span class="title-center-grey">@lang('rent.select_bus_offer')</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="scheda03">
                                            <div class="row">
                                                @forelse( $packages as $key =>  $package )
                                                    <div class="col-xs-12 col-sm-4 inner">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                                                                    <div class="item">
                                                                        <div class="radio">
                                                                            <input type="radio"
                                                                                   value="{{ $package->id }}"
                                                                                   id="package-{{ $package->id }}"
                                                                                   name="package"
                                                                                    @php
                                                                                        if($key === 0) echo 'checked';
                                                                                    @endphp
                                                                            >
                                                                            <label for="package-{{ $package->id }}">
                                                                                <div class="top">
                                                                                    <i class="{{ $package->icon }} radio-img"
                                                                                       title="{{ $package->name }}"></i>
                                                                                    <span class="name">{{ $package->name }}</span>
                                                                                    <span class="caret"></span>
                                                                                    <span class="supplemento">+ {{ $package->supplement }} @lang('rent.supplemento') %</span>
                                                                                </div>
                                                                                <div class="img-container">
                                                                                    @if( $package->getMedia('banner-image')->count() > 0 )
                                                                                        <img src="{{ $package->getFirstMediaUrl('banner-image') }}"
                                                                                             alt="{{ $package->name }}"
                                                                                             title="{{ $package->name }}">
                                                                                    @else
                                                                                        <img src="{{ asset('storage/assets/frontend/images/bus.jpg') }}"
                                                                                             alt="{{ $package->name }}"
                                                                                             title="{{ $package->name }}">
                                                                                    @endif
                                                                                </div>
                                                                                <div class="text-container">
                                                                                    <p>{!! $package->description !!}</p>
                                                                                    <div class="classe-ambientale">
                                                                                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/classe-ambientale.svg') }}"
                                                                                             alt="@lang('rent.step3_environment_class')"
                                                                                             title="@lang('rent.step3_environment_class')">
                                                                                        <span>@lang('rent.step3_environment_class'): <span
                                                                                                    class="aqua">{{ $package->env_class }}</span> </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="plus">
                                                                                    <br><br>
                                                                                    <span class="title">@lang('rent.step3_services')</span>
                                                                                    <span class="subtitle">@lang('rent.step3_services_desc'):</span>
                                                                                    <ul>
                                                                                        @forelse( $package->services as $service )
                                                                                            <li>
                                                                                                <div class="checkbox">
                                                                                                    <input type="checkbox"
                                                                                                           name="services-{{ $package->id }}[]"
                                                                                                           value="{{ $service->id }}"
                                                                                                           id="service-{{ $package->id }}-{{ $service->id }}">
                                                                                                    <label for="service-{{ $package->id }}-{{ $service->id }}">
                                                                                                        <div class="check-container">
                                                                                                            <img class="unchecked"
                                                                                                                 src="{{ ($service->getMedia('icon-unchecked')->count() > 0) ? $service->getFirstMediaUrl('icon-unchecked') : asset('storage/assets/frontend/images/icons-svg/carrozzina.svg') }}"
                                                                                                                 alt="{{ $service->name }}"
                                                                                                                 title="{{ $service->name }}">
                                                                                                            <img class="checked"
                                                                                                                 src="{{ ($service->getMedia('icon-checked')->count() > 0) ? $service->getFirstMediaUrl('icon-checked') : asset('storage/assets/frontend/images/icons-svg/carrozzina-checked.svg') }}"
                                                                                                                 alt="{{ $service->name }}"
                                                                                                                 title="{{ $service->name }}">
                                                                                                        </div>
                                                                                                        <span>{{ $service->name }}</span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </li>
                                                                                        @empty
                                                                                            <li>@lang('rent.step3_no_services_for_package')</li>
                                                                                        @endforelse
                                                                                    </ul>
                                                                                </div>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <div class="center">@lang('rent.step3_no_available_packages')</div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                        <input type="submit" class="bttn bttn-orange small" value="Continua">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/html5sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/moment.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('storage/assets/frontend/js/datetimepicker/bootstrap-datetimepicker.js') }}"></script>
@endsection

@section('added-js')

    @if( env('APP_ENV') === 'local' )
        <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyDvFmEA0NWFv2Celnf_2qPmhAGcQjQReSw&libraries=places"></script>
    @else
        <script src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyBjVBna1BziI1odEaNZBv2AO9B2Y8MNOwU&libraries=places"></script>
    @endif

    <script>
        $(document).ready(function () {
            const formatDay = 'DD/MM/YYYY';
            const formatTime = 'HH:mm';
            var addTappa = $("#addTappa");
            let autocompleteOptions = {
                componentRestrictions: {country: ['it', 'fr', 'de', 'ch', 'at']},
            };
            let date = new Date();
            date.setDate(date.getDate() + 7);
            let date2 = new Date();
            date2.setDate(date2.getDate() + 20);
            let dateTimePickerSetup = {
                format: formatDay,
                minDate: moment(date).startOf('day')
            };
            let dateTimePickerSetupTime = {
                format: formatTime,
                date: moment('12:00', formatTime)
            };
            let addedRoute = 0;
            let sortableRules = {
                items: ':not(.sortIgnore)',
                forcePlaceholderSize: true,
            };
            let nextIndex = parseInt('{{ $totRequests + 1 }}');
            $('.dataForBored').datetimepicker(dateTimePickerSetup);
            $('.timeForBored').datetimepicker(dateTimePickerSetupTime);
            sortable("#tappeContainer", sortableRules);
            $('#tappeContainer').on('click', function (event) {
                if ($(event.target).attr('class') === 'dataForBored') {
                    setDynamicDate(event.target.id, event);
                }
                if ($(event.target).attr('class') === 'timeForBored') {
                    setDynamicTime(event.target.id);
                }
            });
            sortable('#tappeContainer')[0].addEventListener('sortupdate', function (e) {
                let items = e.detail.destination.items;
                let currentIndex = e.detail.destination.index;
                let prevElement = $(`#${e.detail.item.previousElementSibling.id}`)
                let prevInputDate = prevElement.find('.dataForBored')
                let prevInputTime = prevElement.find('.timeForBored')
                if (e.detail.destination.elementIndex > e.detail.origin.elementIndex) {
                    let prevInputId = parseInt(prevInputDate.attr('id').replace(/\D+/g, ""));
                    for (let i = currentIndex, k = prevInputId; i < items.length; i++, k++) {
                        let item = $(`#${items[i].id}`);
                        let inputLowerDate = item.find('.dataForBored')
                        let inputLowerTime = item.find('.timeForBored')
                        if (inputLowerDate.val()) {
                            let dateFromCurrentInput = moment(inputLowerDate.val(), formatDay);
                            if (dateFromCurrentInput.isBefore(moment(prevInputDate.val(), 'D/M/YYYY)'))) {
                                inputLowerDate.data('DateTimePicker').minDate(prevInputDate.val())
                                inputLowerDate.data('DateTimePicker').date(prevInputDate.val())
                            }
                            let timeFromCurrentInput = moment(inputLowerTime.val(), formatTime)
                            if (timeFromCurrentInput.isBefore(moment(prevInputTime.val(), formatTime))) {
                                inputLowerTime.data('DateTimePicker').minDate(prevInputTime.val().toString());
                                inputLowerTime.data('DateTimePicker').date(prevInputTime.val().toString());
                            }
                            inputLowerDate.attr('id', `start-date-${k + 1}`);
                            inputLowerTime.attr('id', `start-time-${k + 1}`);
                            if (nextIndex === k + 1) {
                                nextIndex++
                            }
                        }
                    }
                } else {
                    let currentElement = $(`#${items[currentIndex].id}`)
                    let currentInputDate = currentElement.find('.dataForBored')
                    let currentInputTime = currentElement.find('.timeForBored')
                    let currentInputId = parseInt(currentInputDate.attr('id').replace(/\D+/g, ""));
                    currentInputDate.data('DateTimePicker').minDate(moment(prevInputDate.val(), formatDay))
                    currentInputTime.data('DateTimePicker').minDate(prevInputTime.val().toString())
                    for (let i = currentIndex + 1, k = currentInputId;
                         i <= items.length;
                         i++, ++k) {
                        if (items[i] && items[i].id !== 'addTappaContainer') {
                            let item = $(`#${items[i].id}`);
                            let inputLowerDate = item.find('.dataForBored')
                            let inputLowerTime = item.find('.timeForBored')
                            let currentDateInput = moment(currentInputDate.val(), formatDay);
                            if (currentDateInput.isAfter(inputLowerDate.val())) {
                                inputLowerDate.data('DateTimePicker').date(currentDateInput)
                            }
                            inputLowerDate.data('DateTimePicker').minDate(currentDateInput)
                            inputLowerDate.attr('id', `start-date-${k + 1}`)
                            let currentTimeInput = moment(currentInputTime.val(), formatTime)
                            inputLowerTime.data('DateTimePicker').minDate(currentInputTime.val().toString());
                            if (currentTimeInput.isAfter(moment(inputLowerTime.val(), 'H:mm'))) {
                                inputLowerTime.data('DateTimePicker').date(currentInputTime.val().toString());
                            }
                            inputLowerTime.attr('id', `start-time-${k + 1}`)
                            if (nextIndex === k + 1) {
                                nextIndex++
                            }
                        }
                    }
                }
            })
            addTappa.on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (addedRoute === 0) {
                    Swal.fire({
                        title: '{{ __('rent.add_tappa_alert_title') }}',
                        text: "{{ __('rent.add_tappa_alert_text') }}",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{ __('rent.add_tappa_alert_OK') }}'
                    }).then((result) => {
                        if (result.value) {
                            addedRoute = 1;
                            addTappaOK();
                        }
                    });
                } else {
                    addTappaOK();
                }
            });
            function setDynamicTime(id) {
                $(`#${id}`).datetimepicker().on('dp.change', function (ev) {
                    let numId = parseInt(id.replace(/\D+/g, ""));
                    for (let i = nextIndex; i >= 0; i--) {
                        let selectorNextTime = $(`#start-time-${i}`);
                        if (selectorNextTime.val() && i !== 1 && i > numId) {
                            let currentSelector = $(`#${id}`);
                            let currentDateTime = moment(currentSelector.val(), formatTime);
                            if (currentSelector.val()) {
                                selectorNextTime.data('DateTimePicker').minDate(currentSelector.val().toString());
                                if (currentDateTime.isAfter(moment(selectorNextTime.val(), formatTime))) {
                                    selectorNextTime.data('DateTimePicker').date(currentSelector.val().toString());
                                }
                            }
                        }
                    }
                });
            }
            function setDynamicDate(id) {
                $(`#${id}`).datetimepicker().on('dp.change', function (ev) {
                    let numId = parseInt(id.replace(/\D+/g, ""));
                    let currentInputTime = $(`#start-time-${numId}`)
                    if(ev.date.isAfter(ev.oldDate) && currentInputTime.val()) {
                        currentInputTime.data('DateTimePicker').minDate(false)
                        currentInputTime.data('DateTimePicker').maxDate(moment('23:59',formatTime))
                    }
                    for (let i = numId; i >= 0; i--) {
                        let prevInputDate = $(`#start-date-${i-1}`);
                        if(prevInputDate.val()) {
                            let prevDate = moment(prevInputDate.val(), formatDay);
                            if(ev.date.isSame(prevDate)) {
                                let prevInputTime = $(`#start-time-${i-1}`);
                                currentInputTime.data('DateTimePicker').minDate(moment(prevInputTime.val().toString(),formatTime))
                                currentInputTime.data('DateTimePicker').maxDate(moment('23:59',formatTime))
                                break;
                            }
                        }
                    }
                    for (let i = nextIndex; i >= 0; i--) {
                        let selectorNextDate = $(`#start-date-${i}`);
                        if (selectorNextDate.val() && i !== 1 && i > numId) {
                            let currentSelector = $(`#${id}`);
                            if (currentSelector.val()) {
                                let currentDateTime = moment(currentSelector.val(), formatDay);
                                if (currentDateTime.isAfter(selectorNextDate.val())) {
                                    selectorNextDate.data('DateTimePicker').date(currentDateTime)
                                }
                                selectorNextDate.data('DateTimePicker').minDate(currentDateTime)
                            }
                        }
                    }
                });
            }
            function addTappaOK() {
                //let lastMarkerId = getLastInputId();
                let lastMarkerId = nextIndex - 1;
                //generateTimeStart(lastMarkerId);
                generateNewMarker((lastMarkerId + 1));
                bindDeleteButton((lastMarkerId + 1));
                nextIndex += 1;
            }
            bindGoogleSearch(0);
            $(".rent_steps").each(function (index) {
                bindGoogleSearch($(this).data('id'));
            });
            function bindDeleteButton(index) {
                $("#delete-step-" + index).on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $("#box-input-" + index).remove();
                    $("#time-box-" + index).remove();
                })
            }
            function bindGoogleSearch(id) {
                let input = document.getElementById('w-' + id);
                let autocomplete = new google.maps.places.Autocomplete(input, autocompleteOptions);
                autocomplete.addListener('place_changed', function () {
                    bindAdditionalPlaceData(id, autocomplete.getPlace());
                });
            }
            function bindAdditionalPlaceData(id, data) {
                $("#lat_w-" + id).val(data.geometry.location.lat);
                $("#lng_w-" + id).val(data.geometry.location.lng);
            }
            function getLastInputId() {
                if ($('.arrivo').length) {
                    return parseInt($('.arrivo').last().data('inputid'));
                }
                return 1;
            }
            function generateTimeStart(index) {
                let lastBox = $("#box-input-" + index);
                let elementExists = document.getElementById("time-box-" + index);
                if (!elementExists) {
                    let timeBox = timeTemplate(index);
                    lastBox.append(timeBox);
                    $('.dataForBored').datetimepicker(dateTimePickerSetup);
                    $('.timeForBored').datetimepicker(dateTimePickerSetupTime);
                }
            }
            function generateNewMarker(index) {
                let minDateSelector;
                let minTimeSelector;
                for (let i = index; i >= 0; i--) {
                    if (!minDateSelector || !minDateSelector.val()) {
                        minDateSelector = $(`#start-date-${i}`);
                        minTimeSelector = $(`#start-time-${i}`);
                    }
                }
                if (minTimeSelector.val()) {
                    dateTimePickerSetupTime.minDate = moment(minTimeSelector.val(), formatTime);
                    dateTimePickerSetupTime.date = moment(minTimeSelector.val(), formatTime);
                    dateTimePickerSetupTime.maxDate = moment('23:59', formatTime);
                }
                dateTimePickerSetup.minDate = moment(minDateSelector.val(), formatDay);
                let lastStep = $("#box-input-e");
                if (!lastStep.length) {
                    let index = $("#tappeContainer").children().length - 1;
                    lastStep = $($("#tappeContainer").children().get(index))
                }

                let markerBox = markerTemplate(index);
                markerBox.insertBefore(lastStep);
                $('.dataForBored').datetimepicker(dateTimePickerSetup);
                $('.timeForBored').datetimepicker(dateTimePickerSetupTime);
                sortable("#tappeContainer", sortableRules);
                bindGoogleSearch(index);
            }
            /*function markerTemplate( index ) {
                return $("<div class=\"input arrivo box-input\" id=\"box-input-" + index + "\" data-inputid=\"" + index + "\">\n" +
                    "<div class=\"delete\" id=\"delete-step-" + index + "\"><img src=\"{{ asset('storage/assets/frontend/images/icons-svg/delete.svg') }}\" alt=\"@lang('rent.delete_step')\" title=\"@lang('rent.delete_step')\"></div>" +
                    "<label for=\"w-" + index + "\">@lang('rent.step3_ending_point')</label>\n" +
                    "<input data-id=\"" + index + "\" class=\"rent_steps\" id=\"w-" + index + "\" name=\"w[]\" type=\"text\" value=\"\"  required>\n" +
                    "<input type=\"hidden\" name=\"lat_w[]\" id=\"lat_w-" + index + "\" class=\"hidden quote_hidden_input form-control\" value=\"\">\n" +
                    "<input type=\"hidden\" name=\"lng_w[]\" id=\"lng_w-" + index + "\" class=\"hidden quote_hidden_input form-control\" value=\"\">\n" +
                    "</div>");
            }*/
            function markerTemplate(index) {
                return $("<div class=\"input arrivo box-input\" id=\"box-input-" + index + "\" data-inputid=\"" + index + "\">\n" +
                    "<div class=\"delete\" id=\"delete-step-" + index + "\"><img src=\"{{ asset('storage/assets/frontend/images/icons-svg/delete.svg') }}\" alt=\"@lang('rent.delete_step')\" title=\"@lang('rent.delete_step')\"></div>" +
                    "<label for=\"w-" + index + "\">@lang('rent.step3_intermediate_step')</label>\n" +
                    "<input data-id=\"" + index + "\" class=\"rent_steps\" id=\"w-" + index + "\" name=\"w[]\" type=\"text\" value=\"\"  required>\n" +
                    "<input type=\"hidden\" name=\"lat_w[]\" id=\"lat_w-" + index + "\" class=\"hidden quote_hidden_input form-control\" value=\"\">\n" +
                    "<input type=\"hidden\" name=\"lng_w[]\" id=\"lng_w-" + index + "\" class=\"hidden quote_hidden_input form-control\" value=\"\">\n" +
                    "<div id=\"time-box-" + index + "\"><hr>\n" +
                    "<div class=\"data-ora\">\n" +
                    "<div class=\"data mydata\">\n" +
                    "<span class=\"add-on\">\n" +
                    "<input class=\"dataForBored\" autocomplete=\"off\" name=\"start-date[]\" id=\"start-date-" + index + "\" data-format=\"dd/MM/yyyy\" type=\"text\" placeholder=\"@lang('step3.form_input_data')\"  required>\n" +
                    "</span>\n" +
                    "</div>\n" +
                    "<div class=\"ora mytime\">\n" +
                    "<span class=\"add-on\">\n" +
                    "<input class=\"timeForBored\" autocomplete=\"off\" name=\"start-time[]\" id=\"start-time-" + index + "\" data-format=\"hh:mm:ss\" type=\"text\" placeholder=\"@lang('step3.form_input_time')\"  required>\n" +
                    "</span>\n" +
                    "</div>\n" +
                    "</div></div>" +
                    "</div>");
            }
            /*function timeTemplate( index ) {
                return $("<div id=\"time-box-" + index + "\"><hr>\n" +
                    "<div class=\"data-ora\">\n" +
                    "<div class=\"data mydata\">\n" +
                    "<span class=\"add-on\">\n" +
                    "<input autocomplete=\"off\" name=\"start-date[" + index + "]\" id=\"start-date-" + index + "\" data-format=\"dd/MM/yyyy\" type=\"text\" placeholder=\"@lang('step3.form_input_data')\"  required>\n" +
                    "</span>\n" +
                    "</div>\n" +
                    "<div class=\"ora mytime\">\n" +
                    "<span class=\"add-on\">\n" +
                    "<input autocomplete=\"off\" name=\"start-time[" + index + "]\" id=\"start-time-" + index + "\" data-format=\"hh:mm:ss\" type=\"text\" placeholder=\"@lang('step3.form_input_time')\"  required>\n" +
                    "</span>\n" +
                    "</div>\n" +
                    "</div></div>");
            }*/
        });
    </script>

@endsection