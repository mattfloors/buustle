@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

<div class="menu-mobile-overlay"></div>


<div class="bg-ivory noleggiatori">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-xs-12 title">
                        <span class="orange">@lang('become-partner.title')</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 inner">
                        <div>
                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/noleggiatori.svg') }}" alt="@lang('become-partner.join_us')" title="@lang('become-partner.join_us')">
                            <p>@lang('become-partner.text_1')</p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    <br><a title="@lang('become-partner.join_us')" href="#" class="bttn bttn-orange "><span>@lang('become-partner.join_us')</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 inner">
                        <div>
                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/sedili.svg') }}" alt="@lang('become-partner.share_empty_seats')" title="@lang('become-partner.share_empty_seats')">
                            <p>@lang('become-partner.share_empty_seats_text')</p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    <br><a title="@lang('become-partner.share_empty_seats')" href="#" class="bttn bttn-orange "><span>@lang('become-partner.share_empty_seats')</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container vantaggi-noleggiatori">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0">
            <h2>@lang('become-partner.allow_to')</h2>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 inner">
                    <div>
                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/green-copy.svg') }}" alt="@lang('become-partner.optimize')" title="@lang('become-partner.optimize')">
                        <p>@lang('become-partner.optimize_text_1') <br>@lang('become-partner.optimize_text_2')</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 inner">
                    <div>
                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/aumentare.svg') }}" alt="@lang('become-partner.increase')" title="@lang('become-partner.increase')">
                        <p>@lang('become-partner.increse_text_1') <br>@lang('become-partner.increase_text_2')</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 inner">
                    <div>
                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/preventivo.svg') }}" alt="@lang('become-partner.auto')" title="@lang('become-partner.auto')">
                        <p>@lang('become-partner.auto_text_1') <br>@lang('become-partner.auto_text_2')</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 inner">
                    <div>
                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/green.svg') }}" alt="@lang('become-partner.reduce')" title="@lang('become-partner.reduce')">
                        <p>@lang('become-partner.reduce_text_1') <br>@lang('become-partner.reduce_text_2')</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 inner">
                    <div>
                        <img src="{{ asset('storage/assets/frontend/images/icons-svg/network.svg') }}" alt="@lang('become-partner.network')" title="@lang('become-partner.network')">
                        <p>@lang('become-partner.network_text_1') <br>@lang('become-partner.network_text_2')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-aqua padding-60 text-center network">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h3>@lang('become-partner.safety_and_professionality_title')</h3>
                <p>@lang('become-partner.safety_and_professionality_text')</p>
            </div>
        </div>
    </div>
</div>

<div class="rimani-informato">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                <img src="{{ asset('storage/assets/frontend/images/icons-svg/mail.svg') }}" alt="@lang('become-partner.keep_in_touch')" title="@lang('become-partner.keep_in_touch')">
                <h3>@lang('become-partner.keep_in_touch')</h3>
                <p>@lang('become-partner.keep_in_touch_text')</p>
                <form action="{{ route('subscribers.store') }}" method="post" class="newsletter">
                    @csrf
                    <input type="email" name="" placeholder="@lang('about-us.email_placeholder')">
                    <div class="checkbox">
                        <input type="checkbox" value="terms" id="terms" required>
                        <label for="terms">
                            @lang('about-us.accept') <a href="#" title="@lang('about-us.terms-and-condition')">@lang('about-us.terms-and-condition')</a> @lang('global.and') <a href="#" title="@lang('about-us.privacy_policy')">@lang('about-us.privacy_policy')</a>
                            <div class="check-button"><i></i></div>
                        </label>
                    </div>
                    <input type="submit" class="bttn bttn-orange " value="@lang('about-us.subscribe')">
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
