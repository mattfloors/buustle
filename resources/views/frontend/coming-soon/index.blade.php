@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title mbottom-0">@lang('comingsoon.title')</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory padding-60 faqs">
        <div class="container">
            <div class="row">
                <h1>Coming Soon...</h1>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
