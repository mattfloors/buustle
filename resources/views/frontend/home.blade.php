@extends('frontend.layout.main')

@section('content')
    <div class="menu-mobile-overlay"></div>
    <picture class="banner">
        <source srcset="{{ asset('storage/assets/frontend/images/mobile_homepage_gif.gif') }}" media="(max-width: 767px)">
        <source srcset="{{ asset('storage/assets/frontend/images/desktop_homepage_gif.gif') }}" media="(min-width: 768px)">
        <source srcset="{{ asset('storage/assets/frontend/images/mobile_homepage_gif.gif') }}">
        <img srcset="{{ asset('storage/assets/frontend/images/mobile_homepage_gif.gif') }}" alt="Buustle" title="Buustle">
    </picture>

    <div class="container top-button-hp">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="{{ route('rent.noleggi.request') }}" title="Noleggia un mezzo" class="bttn bttn-orange bttn-image">
                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/bottone-bus.svg') }}" alt="@lang('home.rent_a_bus')" title="@lang('home.rent_a_bus')">
                            <span>@lang('home.rent_a_bus')</span>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="{{ route('comingSoon') }}" title="Aggregati ad altri" class="bttn bttn-red bttn-image">
                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/bottone-aggregati.svg') }}" alt="@lang('home.rent_join')" title="@lang('home.rent_join')">
                            <span>@lang('home.rent_join')</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="come-funziona">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0">
                    <div class="flex-container">
                        <div class="flex-item">
                            <div class="inner">
                                <div>
                                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/diagramma.svg') }}" alt="Come funziona Buustle" title="Come funziona Buustle">
                                </div>
                            </div>
                        </div>
                        <div class="flex-item">
                            <div class="inner">
                                <div>
                                    <h2>@lang('home.how_it_works')</h2>
                                    <p>@lang('home.how_it_works_text')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="soluzione">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>@lang('home.buustle_is_for_you_title')</h2>
                    <div class="scheda02">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="inner">
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <img src="{{ asset('storage/assets/frontend/images/icons-svg/renters.svg') }}" alt="@lang('home.rent_a_bus')" title="@lang('home.rent_a_bus')">
                                                <p>@lang('home.buustle_is_for_you_text_1')</p>
                                                <a href="{{ route('rent.noleggi.request') }}" class="bttn bttn-orange " title="Noleggia un mezzo"><span>@lang('home.rent_a_bus')</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="inner">
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <img src="{{ asset('storage/assets/frontend/images/icons-svg/individuo.svg') }}" alt="@lang('home.travel_with_us')" title="@lang('home.travel_with_us')">
                                                <p>@lang('home.buustle_is_for_you_text_2')</p>
                                                <a href="{{ route('rent.noleggi.request') }}" class="bttn bttn-orange " title="@lang('home.travel_with_us')"><span>@lang('home.travel_with_us')</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="inner">
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <img src="{{ asset('storage/assets/frontend/images/icons-svg/noleggiatori.svg') }}" alt="@lang('home.earn_with_us')" title="@lang('home.earn_with_us')">
                                                <p>@lang('home.buustle_is_for_you_text_3')</p>
                                                <a href="{{ route('becomePartner') }}" class="bttn bttn-orange " title="@lang('home.earn_with_us')"><span>@lang('home.earn_with_us')</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container vantaggi">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0">
                <h2>@lang('home.buustle_benefits_title')</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/affidabile.svg') }}" alt="@lang('home.alt_fast')" title="@lang('home.alt_fast')"><span>@lang('home.alt_fast')</span></div>
                            <p>@lang('home.buustle_benefits_text_1')</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/cheap.svg') }}" alt="@lang('home.alt_cheaper')" title="@lang('home.alt_cheaper')"><span>@lang('home.alt_cheaper')</span></div>
                            <p>@lang('home.buustle_benefits_text_2')</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sostenibilita.svg') }}" alt="@lang('home.alt_sustainable')" title="@lang('home.alt_sustainable')"><span>@lang('home.alt_sustainable')</span></div>
                            <p>@lang('home.buustle_benefits_text_3')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--<div class="propone">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Buustle propone...</h2>
                    <div class="scheda01">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 inner">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row event">
                                                    <div class="col-xs-3"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-9"><p>Vieni a sciare con noi! Il pullman di Skiday ti porta sulla neve per una giornata di sci in compagnia.</p></div>
                                                </div>
                                                <div class="row viaggi">
                                                    <div class="col-xs-5">
                                                        <span class="citta">Torino</span>
                                                        <span class="indirizzo">Via Pico Pallo 23</span>
                                                        <span class="data">15 Aprile 2019 - 7:00</span>
                                                    </div>
                                                    <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-5">
                                                        <span class="citta">Bardonecchia</span>
                                                        <span class="indirizzo">Campo Smith</span>
                                                        <span class="data">15 Aprile 2019 - 9:00</span>
                                                    </div>
                                                </div>
                                                <div class="row credito-posti">
                                                    <div class="col-xs-6">
                                                        <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="Credito" title="Credito"><span>23€</span></div>
                                                        <span class="per-biglietto">Per biglietto</span>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="Posti disponibili" title="Posti disponibili"><span><b>2</b>/30</span></div>
                                                        <span class="per-biglietto">Posti disponibili</span>
                                                    </div>
                                                </div>
                                                <a href="#" class="bttn bttn-orange " title="Aggregati!"><span>Aggregati!</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 inner">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row event">
                                                    <div class="col-xs-3"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-9"><p>Vieni a sciare con noi! Il pullman di Skiday ti porta sulla neve per una giornata di sci in compagnia.</p></div>
                                                </div>
                                                <div class="row viaggi">
                                                    <div class="col-xs-5">
                                                        <span class="citta">San Benedetto del Tronto</span>
                                                        <span class="indirizzo">Via Roma 2</span>
                                                        <span class="data">28 MAggio 2019 - 17:00</span>
                                                    </div>
                                                    <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-5">
                                                        <span class="citta">Bari</span>
                                                        <span class="indirizzo">Aereoporto Palese</span>
                                                        <span class="data">28 Maggio 2019 - 23:00</span>
                                                    </div>
                                                </div>
                                                <div class="row credito-posti">
                                                    <div class="col-xs-6">
                                                        <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="Credito" title="Credito"><span>23€</span></div>
                                                        <span class="per-biglietto">Per biglietto</span>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="Posti disponibili" title="Posti disponibili"><span><b>2</b>/30</span></div>
                                                        <span class="per-biglietto">Posti disponibili</span>
                                                    </div>
                                                </div>
                                                <a href="#" class="bttn bttn-orange " title="Aggregati!"><span>Aggregati!</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 inner">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row event">
                                                    <div class="col-xs-3"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-9"><p>Vieni a sciare con noi! Il pullman di Skiday ti porta sulla neve per una giornata di sci in compagnia.</p></div>
                                                </div>
                                                <div class="row viaggi">
                                                    <div class="col-xs-5">
                                                        <span class="citta">Roma</span>
                                                        <span class="indirizzo">Via Torino 58</span>
                                                        <span class="data">10 Giugno 2019 - 7:00</span>
                                                    </div>
                                                    <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-5">
                                                        <span class="citta">Firenze</span>
                                                        <span class="indirizzo">Stazione Centrale FS</span>
                                                        <span class="data">10 Giugno 2019 - 10:00</span>
                                                    </div>
                                                </div>
                                                <div class="row credito-posti">
                                                    <div class="col-xs-6">
                                                        <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="Credito" title="Credito"><span>23€</span></div>
                                                        <span class="per-biglietto">Per biglietto</span>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="Posti disponibili" title="Posti disponibili"><span><b>2</b>/30</span></div>
                                                        <span class="per-biglietto">Posti disponibili</span>
                                                    </div>
                                                </div>
                                                <a href="#" class="bttn bttn-orange " title="Aggregati!"><span>Aggregati!</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 inner">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row event">
                                                    <div class="col-xs-3"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-9"><p>Vieni a sciare con noi! Il pullman di Skiday ti porta sulla neve per una giornata di sci in compagnia.</p></div>
                                                </div>
                                                <div class="row viaggi">
                                                    <div class="col-xs-5">
                                                        <span class="citta">Torino</span>
                                                        <span class="indirizzo">Via Pico Pallo 23</span>
                                                        <span class="data">15 Aprile 2019 - 7:00</span>
                                                    </div>
                                                    <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-5">
                                                        <span class="citta">Milano Malpensa</span>
                                                        <span class="indirizzo">Terminal 1</span>
                                                        <span class="data">15 Aprile 2019 - 9:00</span>
                                                    </div>
                                                </div>
                                                <div class="row credito-posti">
                                                    <div class="col-xs-6">
                                                        <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="Credito" title="Credito"><span>23€</span></div>
                                                        <span class="per-biglietto">Per biglietto</span>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="Posti disponibili" title="Posti disponibili"><span><b>2</b>/30</span></div>
                                                        <span class="per-biglietto">Posti disponibili</span>
                                                    </div>
                                                </div>
                                                <a href="#" class="bttn bttn-orange " title="Aggregati!"><span>Aggregati!</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <div class="standard">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                    <h3>@lang('home.our_standard')</h3>
                    <p>@lang('home.our_standard_text')</p>
                </div>
            </div>
        </div>
    </div>

    <div class="rimani-informato">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/mail.svg') }}" alt="@lang('home.keep_in_touch')" title="@lang('home.keep_in_touch')">
                    <h3>@lang('home.keep_in_touch')</h3>
                    <p>@lang('home.keep_in_touch_text')</p>
                    <form action="{{ route('subscribers.store') }}" method="post" class="newsletter">
                        @csrf
                        <input type="email" name="" placeholder="@lang('about-us.email_placeholder')l">
                        <div class="checkbox">
                            <input type="checkbox" value="terms" id="terms" required>
                            <label for="terms">
                                @lang('about-us.accept') <a href="#" title="@lang('about-us.terms-and-condition')">@lang('about-us.terms-and-condition')</a> @lang('global.and') <a href="#" title="@lang('about-us.privacy_policy')">@lang('about-us.privacy_policy')</a>
                                <div class="check-button"><i></i></div>
                            </label>
                        </div>
                        <input type="submit" class="bttn bttn-orange " value="@lang('about-us.subscribe')">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection