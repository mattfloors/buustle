@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

<div class="menu-mobile-overlay"></div>

<div class="banner-bg">
    <picture class="banner">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/le-nostre-fermate.svg') }}" media="(max-width: 767px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/le-nostre-fermate-desktop.svg') }}" media="(min-width: 768px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/le-nostre-fermate.svg') }}">
        <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/le-nostre-fermate.svg') }}" alt="Buustle" title="Buustle">
    </picture>
    <div class="inner">
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                        <h1 class="title mbottom-0">@lang('our-stops.title')</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-ivory padding-60 fermate">
    <div class="container">
        <div class="row">
            @foreach($grouped_stops as $city_name=>$data)
            <div class="col-xs-12 col-sm-4 col-md-4 item">
                <h2 class="orange">{{$city_name}}</h2>
                @foreach($data as $stop)
                <p><a href="{{$stop['link']}}" style="color: var(--steel)">{{$stop['stop_name']}}</a></p>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
</div>

@section('content')

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
