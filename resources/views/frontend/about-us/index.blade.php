@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

<div class="menu-mobile-overlay"></div>

<div class="banner-bg">
    <picture class="banner">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/chi-siamo.svg') }}" media="(max-width: 767px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/chi-siamo-desktop.svg') }}" media="(min-width: 768px)">
        <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/chi-siamo.svg') }}">
        <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/chi-siamo.svg') }}" alt="Buustle" title="Buustle">
    </picture>
    <div class="inner">
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                        <h1 class="title mbottom-0">@lang('about-us.title')</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-ivory chi-siamo">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-xs-12 title">
                        <span class="dark-blue-grey">@lang('about-us.title_sub_top').</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/storia.svg') }}" alt="@lang('about-us.our_history')" title="@lang('about-us.our_history')"><h2 class="orange">@lang('about-us.our_history')</h2></div>
                            <p>@lang('about-us.our_history_text')</p>
                        </div>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 inner">
                        <div>
                            <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/team.svg') }}" alt="@lang('about-us.our_team')" title="@lang('about-us.our_team')"><h2 class="orange">@lang('about-us.our_team')</h2></div>
                            <p>@lang('about-us.our_team_text')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<picture class="banner">
    <source srcset="{{ asset('storage/assets/frontend/images/foto-di-gruppo.jpg') }}" media="(max-width: 767px)">
    <source srcset="{{ asset('storage/assets/frontend/images/foto-di-gruppo.jpg') }}" media="(min-width: 768px)">
    <source srcset="{{ asset('storage/assets/frontend/images/foto-di-gruppo.jpg') }}">
    <img srcset="{{ asset('storage/assets/frontend/images/foto-di-gruppo.jpg') }}" alt="Buustle" title="Buustle">
</picture>

<div class="bg-aqua padding-60 text-center network">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h3>@lang('about-us.join_us')</h3>
                <p>@lang('about-us.are_you_interested') <br><span class="dark-blue-grey">@lang('about-us.contact_ud')</span> @lang('global.on') {{ setting('general.contact_email') }} <br>@lang('global.or') <span class="dark-blue-grey">@lang('about-us.become_partner').</span></p>
            </div>
        </div>
    </div>
</div>

<div class="rimani-informato">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                <img src="{{ asset('storage/assets/frontend/images/icons-svg/mail.svg') }}" alt="@lang('about-us.keep_in_touch')" title="@lang('about-us.keep_in_touch')">
                <h3>@lang('about-us.keep_in_touch')</h3>
                <p>@lang('about-us.keep_in_touch_text')</p>
                <form action="{{ route('subscribers.store') }}" method="post" class="newsletter">
                    @csrf
                    <input type="email" name="" placeholder="@lang('about-us.email_placeholder')">
                    <div class="checkbox">
                        <input type="checkbox" value="terms" id="terms" required>
                        <label for="terms">
                            @lang('about-us.accept') <a href="#" title="@lang('about-us.terms-and-condition')">@lang('about-us.terms-and-condition')</a> @lang('global.and') <a href="#" title="@lang('about-us.privacy_policy')">@lang('about-us.privacy_policy')</a>
                            <div class="check-button"><i></i></div>
                        </label>
                    </div>
                    <input type="submit" class="bttn bttn-orange " value="@lang('about-us.subscribe')">
                </form>
            </div>
        </div>
    </div>
</div>

@section('content')

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
