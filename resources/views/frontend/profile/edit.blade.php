@extends('frontend.layout.main')

@section('added-css')
    <link rel="stylesheet" href="{{ asset('storage/assets/css/croppie.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/frontend/css/croppie.css') }}">
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente" style="background-image: url("");">
            <img src="{{ \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar') ? \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" title="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ \Illuminate\Support\Facades\Auth::user()->email }}<a href="{{ route('profile.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('profile.edit')" title="@lang('profile.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory padding-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <form action="{{ route('profile.store') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="first_name">@lang('profile.first_name')</label>
                                            <input class="" type="text" id="first_name" name="first_name" placeholder="@lang('profile.first_name')" value="{{ $user->profile->first_name }}" required autofocus>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="last_name">@lang('profile.last_name')</label>
                                            <input class="" type="text" id="last_name" name="last_name" placeholder="@lang('profile.last_name')" value="{{ $user->profile->last_name }}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="phone">@lang('profile.phone')</label>
                                            <input class="" type="text" id="phone" name="phone" placeholder="@lang('profile.phone')" value="{{ $user->profile->phone }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <label for="phone">@lang('profile.address')</label>
                                            <input class="" type="text" id="address" name="address" placeholder="@lang('profile.address')" value="{{ $user->profile->address }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                    <div class="border-box">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 input">
                                                <label class="cabinet center-block">
                                                    <figure>
                                                        <img src="" class="gambar img-responsive img-thumbnail"
                                                             id="item-img-output"/>
                                                        <figcaption><i class="fa fa-camera"></i></figcaption>
                                                    </figure>
                                                    <input type="file" class="item-img file center-block"
                                                           name="img_avatar"/>
                                                    <input id="avatar" class="avatar" type="hidden" name="avatar" value=''></p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <input type="submit" class="bttn bttn-orange small" value="{{ __('profile.save_profile') }}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Cortar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent
    <script src="{{ asset('storage/assets/frontend/js/croppie.min.js') }}"></script>
    <script>
        // Start upload preview image
        $(".gambar").attr("src", $('.utente img').attr('src'));
        var $uploadCrop,
            tempFilename,
            rawImg,
            imageId;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $('#cropImagePop').modal('show');
                    rawImg = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                swal("Desculpe, seu navegador não suporta o FileReader API.");
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 270,
                height: 270,
                type: 'circle'
            },
            enforceBoundary: false,
            enableExif: true
        });
        $('#cropImagePop').on('shown.bs.modal', function () {
            // alert('Shown pop');
            $uploadCrop.croppie('bind', {
                url: rawImg
            }).then(function () {
                console.log('jQuery bind complete');
            });
        });

        $('.item-img').on('change', function () {
            imageId = $(this).data('id');
            tempFilename = $(this).val();
            $('#cancelCropBtn').data('id', imageId);
            readFile(this);
        });
        $('#cropImageBtn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'base64',
                format: 'jpeg',
                size: {width: 270, height: 270}
            }).then(function (resp) {
                $('#item-img-output').attr('src', resp);

                $('#avatar').val(resp);
                $('#cropImagePop').modal('hide');
            });
        });
    </script>
@endsection

@section('added-js')

@endsection