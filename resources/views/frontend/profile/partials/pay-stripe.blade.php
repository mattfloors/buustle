
<div class="col-xs-10 col-xs-offset-1">

    <form action="{{ route('checkout.payment.stripe', ['order' => encrypt($order->id)]) }}" method="POST">
        @csrf
        <input type="hidden" name="order_id" value="{{ encrypt($order->id) }}">
        <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="{{ setting('payments.stripe_test_mode') ? setting('payments.stripe_test_public') : setting('payments.stripe_live_public') }}"
                data-amount="{{ $order->amount * 100 }}"
                data-name="Buustle ordine noleggio"
                data-description="Noleggio # {{ $rentRequest->uuid }} Preventivo {{ $quote->sku }}"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto"
                data-currency="eur">
        </script>
    </form>

</div>