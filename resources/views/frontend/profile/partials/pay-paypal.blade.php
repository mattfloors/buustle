<div class="col-xs-10 col-xs-offset-1">

    <form method="POST" action="{{ route('checkout.payment.paypal', ['order' => encrypt($order->id)]) }}">
        @csrf
        <button class="btn btn-outline-primary bttn btn-info">
            <i class="fab fa-cc-paypal"></i> @lang('payments.pay_with_paypal')
        </button>
    </form>

</div>