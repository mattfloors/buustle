<form action="{{ route('profile.acceptQuote', ['rentRequestUuid' => $request->uuid, 'quote' => $quote]) }}" method="post">
    @csrf
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 title-orange-icon">
            <img src="{{ asset('storage/assets/frontend/images/icons-svg/portafoglio.svg') }}" alt="Dettagli pagamento" title="Dettagli pagamento"><span class="orange">Dettagli pagamento</span>
        </div>
    </div>
    <div class="scheda03">
        <div class="row">
            <div class="col-xs-12 inner">
                <div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                            <div class="item">
                                <div class="radio">
                                    <input type="radio" value="{{ \App\Models\Order::STRIPE }}" id="stripe" name="payment_type">
                                    <label for="stripe">
                                        <div>
                                            <span class="steel">@lang('profile.quote_pay_credit_card')</span>
                                            <!--<span class="caret"></span>-->
                                            <div class="radio-button"><i></i></div>
                                        </div>
                                        <!--<div class="plus">
                                            <div class="row">
                                                <div class="col-xs-12 input">
                                                    <label for="nome-carta">Nome sulla carta*</label>
                                                    <input type="text" id="nome-carta" name="nome-carta" value="" placeholder="Scrivi il tuo nome">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="numero-carta">Numero carta*</label>
                                                    <input type="text" id="numero-carta" name="numero-carta" value="" placeholder="Scrivi il numero della carta">
                                                </div>
                                                <div class="col-xs-6 input">
                                                    <label for="scadenza-carta">Scadenza*</label>
                                                    <input type="text" id="scadenza-carta" name="scadenza-carta" value="" placeholder="(MM/YY)">
                                                </div>
                                                <div class="col-xs-6 input">
                                                    <label for="codice-carta">CVC*</label>
                                                    <input type="text" id="codice-carta" name="codice-carta" value="" placeholder="XXX">
                                                </div>
                                            </div>
                                        </div>-->
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 inner">
                <div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                            <div class="item">
                                <div class="radio">
                                    <input type="radio" value="{{ \App\Models\Order::BONIFICO }}" id="bonifico" name="payment_type">
                                    <label for="bonifico">
                                        <div>
                                            <span class="steel">@lang('profile.quote_pay_money_transfer')</span>
                                            <div class="radio-button"><i></i></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 inner">
                <div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                            <div class="item">
                                <div class="radio">
                                    <input type="radio" value="{{ \App\Models\Order::PAYPAL }}" id="paypal" name="payment_type">
                                    <label for="paypal">
                                        <div>
                                            <span class="steel"><img src="{{ asset('storage/assets/frontend/images/icons-svg/paypal.svg') }}" alt="PayPal" title="PayPal"></span>
                                            <div class="radio-button"><i></i></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                    <input type="submit" class="bttn bttn-orange disabled" value="@lang('profile.quote_accept')">
                </div>
            </div>
        </div>
    </div>
</form>