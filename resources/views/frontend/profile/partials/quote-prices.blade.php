<div class="col-xs-12">
    <hr>
</div>
<div class="col-xs-10 col-xs-offset-1">
    <div class="row opzioni dettaglio">
        @foreach( $quote->prices as $price )
            <div class="col-xs-8">
                <span class="exc">{{ $price->reason }}</span>
            </div>
            <div class="col-xs-4">
                <span class="exec">{{ $price->price }}€</span>
            </div>
        @endforeach
    </div>
</div>