<div class="col-xs-12">
    <hr>
</div>
<div class="col-xs-10 col-xs-offset-1">
    <div class="row opzioni dettaglio">
        <div class="col-xs-4">
            <span class="exc">@lang('profile.quote_free')<br>@lang('profile.quote_slots')</span>
            <span class="big">{{ $quote->totalFreeSlots() }}</span>
        </div>
        <div class="col-xs-4">
            <span class="exc">@lang('profile.quote_earning')<br>@lang('profile.quote_potential')*</span>
            <span class="big">{{ $quote->totalFreeSlots() * 15 }}€</span>
        </div>
        <div class="col-xs-12">
            <div class="extra">
                <small>*@lang('profile.quote_potential_earning_explanation')</small>
            </div>
        </div>
    </div>
</div>