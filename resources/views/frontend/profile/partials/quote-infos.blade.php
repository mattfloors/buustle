<div class="col-xs-12">
    <hr>
</div>
<div class="col-xs-10 col-xs-offset-1">
    <div class="row opzioni dettaglio">
        @foreach( $quote->infos as $info )
            <div class="col-xs-8">
                <span class="exc">{{ $info->infos }}</span>
            </div>
            <div class="col-xs-4">
                @if( $info->included )
                    <span class="exec" style="color:green;font-weight:bold;">@lang('profile.quote_info_included')</span>
                @else
                    <span class="exec" style="color:darkred;font-weight:bold;">@lang('profile.quote_info_not_included')</span>
                @endif
            </div>
        @endforeach
    </div>
</div>