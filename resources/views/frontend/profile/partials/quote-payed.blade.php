 <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 title-orange-icon">
        <img src="{{ asset('storage/assets/frontend/images/icons-svg/portafoglio.svg') }}" alt="Dettagli pagamento" title="Dettagli pagamento"><span class="orange">Dettagli pagamento</span>
    </div>
</div>
<div class="scheda03">
    <div class="row">
        <div class="col-xs-12 inner">
            <div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1">
                        @lang('profile.quote_already_payed')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>