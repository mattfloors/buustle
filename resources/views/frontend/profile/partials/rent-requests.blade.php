<div class="bg-ivory">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="scheda01 scheda01-b padding-0-60">
                    <div class="row">
                        @forelse( $requests as $request )
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 inner step">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div class="row viaggi">
                                                <div class="col-xs-5">
                                                    <span class="citta">{{ $request->getFirstStep()->onlyCityNameStart() }}</span>
                                                    <span class="indirizzo">{{ trim($request->getFirstStep()->start_address) }}</span>
                                                    <span class="data">{{ \Carbon\Carbon::parse($request->getFirstStep()->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                </div>
                                                <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="_" title="_"></div>
                                                <div class="col-xs-5">
                                                    <span class="citta">{{ $request->getLastStep()->onlyCityNameEnd() }}</span>
                                                    <!--<span class="indirizzo">{{ trim($request->getLastStep()->end_address) }}</span>
                                                    <span class="data">{{ \Carbon\Carbon::parse($request->getLastStep()->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>-->
                                                </div>
                                            </div>
                                            <div class="row itinerario excd">
                                                <div class="col-xs-12">
                                                    <span class="title"><img src="{{ asset('storage/assets/frontend/images/icons-svg/andata.svg') }}" alt="@lang('profile.request_forwardroute')" title="@lang('profile.request_forwardroute')"> @lang('profile.request_forwardroute')</span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <table class="scroll">
                                                        <thead>
                                                        <tr>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/luogo.svg') }}" alt="@lang('profile.request_start')" title="@lang('profile.request_start')"><span>@lang('profile.request_start')</span></th>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/calendario-piccolo.svg') }}" alt="@lang('profile.request_date')" title="@lang('profile.request_date')"><span>@lang('profile.request_date')</span></th>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/orologio-piccolo.svg') }}" alt="@lang('profile.request_time')" title="@lang('profile.request_time')"><span>@lang('profile.request_time')</span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse( $request->requestSteps as $step )
                                                            <tr>
                                                                <td><span>{{ $step->onlyCityNameStart() }}</span></td>
                                                                <td><span>{{ \Carbon\Carbon::parse($step->start_time)->format('d.m.y') }}</span></td>
                                                                <td><span>{{ \Carbon\Carbon::parse($step->start_time)->format('H:i') }}</span></td>
                                                            </tr>
                                                        @empty
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row itinerario excd">
                                                <div class="col-xs-12">
                                                    <span class="title"><img src="{{ asset('storage/assets/frontend/images/icons-svg/ritorno.svg') }}" alt="Ritorno" title="Ritorno"> @lang('profile.request_backroute')</span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <table class="scroll">
                                                        <thead>
                                                        <tr>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/luogo.svg') }}" alt="@lang('profile.request_end')" title="@lang('profile.request_end')"><span>@lang('profile.request_end')</span></th>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/calendario-piccolo.svg') }}" alt="@lang('profile.request_date')" title="@lang('profile.request_date')"><span>@lang('profile.request_date')</span></th>
                                                            <th><img src="{{ asset('storage/assets/frontend/images/icons-svg/orologio-piccolo.svg') }}" alt="@lang('profile.request_time')" title="@lang('profile.request_time')"><span>@lang('profile.request_time')</span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><span>{{ $request->getLastStep()->onlyCityNameEnd() }}</span></td>
                                                            <td><span>{{ \Carbon\Carbon::parse($request->getLastStep()->end_time)->format('d.m.y') }}</span></td>
                                                            <td><span>{{ \Carbon\Carbon::parse($request->getLastStep()->end_time)->format('H:i') }}</span></td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 excd">
                                            <hr>
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1 excd">
                                            <div class="row credito-posti">
                                                <div class="col-xs-6">
                                                    <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/passeggeri-2.svg') }}" alt="@lang('profile.rent_confirmed_people')" title="@lang('profile.rent_confirmed_people')"><span class="steel">{{ $request->people }}</span></div>
                                                    <span class="per-biglietto">@lang('profile.rent_confirmed_people')</span>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/bus-2.svg') }}" alt="@lang('profile.rent_vehicle_requested')" title="@lang('profile.rent_vehicle_requested')"><span>{{ $request->package->name }}</span></div>
                                                    <span class="per-biglietto">@lang('profile.rent_vehicle_requested')</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 excd">
                                            <hr>
                                        </div>
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div class="row mezzo">
                                                <div class="col-xs-12">
                                                    <div class="span"> <span>@lang('profile.request_status'): <exc class="green-blue">{{ $request->getStupidStatus() }}</exc></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 excd">
                                            <hr class="mbottom-0">
                                        </div>
                                        <div class="col-xs-12 acquista excd">
                                            <div>
                                                <div class="row">
                                                    <div class="col-xs-10 col-xs-offset-1">
                                                        <div class="row">
                                                            @if( $request->status === \App\Models\RentRequest::STATUS_COMPLETED )
                                                                <div class="col-xs-5"><a data-uuid="{{ $request->uuid }}" href="#" class="deleteRequest bttn bttn-danger btn-danger">@lang('profile.rent_request_delete')</a></div>
                                                                <div class="col-xs-5 col-xs-offset-1"><a href="{{ route('contactUs') }}" title="@lang('profile.contact_us')" class="bttn bttn-orange"><span>@lang('profile.contact_us')</span></a></div>
                                                            @elseif( $request->status === \App\Models\RentRequest::STATUS_CLOSED )
                                                                <div class="col-xs-5"><a href="{{ route('profile.viewRequestQuotes', ['rentRequestUuid' => $request->uuid]) }}" class="payRequest bttn bttn-danger">@lang('profile.rent_request_pay')</a></div>
                                                            @elseif( $request->status === \App\Models\RentRequest::STATUS_SOLD )
                                                                <div class="col-xs-5"><a href="{{ route('profile.viewTravel', ['rentRequestUuid' => $request->uuid]) }}" class="payRequest bttn bttn-danger">@lang('profile.travel_infos')</a></div>
                                                                <div id="fb-root"></div>
                                                                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=674355302996126&autoLogAppEvents=1"></script>
                                                                <div class="col-xs-5 col-xs-offset-1 fb-share-button" data-href="https://www.buustle.com" data-layout="button" data-size="large"><a class="bttn bttn-orange" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.buustle.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><span>@lang('profile.share')</span></a></div>
                                                            @else
                                                                <div class="col-xs-5">&nbsp;</div>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            @lang('profile.no_active_requests')
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>