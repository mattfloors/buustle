<div class="col-xs-10 col-xs-offset-1">
    <div class="row viaggi">
        <div class="col-xs-12">
            <h4>@lang('profile.quote_proposed_steps')</h4>
        </div>
    </div>
</div>
<div class="col-xs-10 col-xs-offset-1">
    @forelse( $quote->markers as $marker )
        <div class="row viaggi">
            <div class="col-xs-5">
                <span class="citta">{{ $marker->start_marker->city_name }}</span>
                <span class="indirizzo">{{ trim($marker->start_marker->name) }}</span>
                <span class="data">{{ \Carbon\Carbon::parse($marker->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
            </div>
            <div class="col-xs-2"><i class="fa fa-arrow-alt-from-left"></i></div>
            <div class="col-xs-5">
                <span class="citta">{{ $marker->end_marker->city_name }}</span>
                <span class="indirizzo">{{ trim($marker->end_marker->name) }}</span>
                <span class="data">{{ \Carbon\Carbon::parse($marker->start_time)->addSeconds($marker->estimated_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
            </div>
        </div>
    @empty
        -
    @endforelse
</div>