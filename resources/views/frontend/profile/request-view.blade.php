@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
<div class="container row">
    <div class="col-md-12">
        <h2>@lang('requests.view_title')</h2>

        <div class="card">
            <div class="card-body">
                <h4 class="box-title">
                    @lang('requests.view_requests') # {{ $rentRequest->id }}
                </h4>
            </div>
            <div class="row">
                <div class="col-lg-12">

                    <table class="table">
                        <tbody>
                        <tr>
                            <td>@lang('requests.start_date')</td>
                            <td>{{ \Carbon\Carbon::parse($rentRequest->getFirstStep()->start_time)->format('d/m/Y H:i') }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.start_place')</td>
                            <td>{{ $rentRequest->getFirstStep()->start_address }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>@lang('requests.request_status')</td>
                            <td>{{ ucfirst($rentRequest->status) }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.user')</td>
                            <td>{{ $rentRequest->userEmail() }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.request_date')</td>
                            <td>{{ \Carbon\Carbon::parse($rentRequest->created_at)->format('d/m/Y H:i') }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.km_estimate')</td>
                            <td>{{ $rentRequest->kilometers }} km</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.accepted_price')</td>
                            <td>{{ $rentRequest->accepted_price }} €</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.travel_days')</td>
                            <td>{{ $rentRequest->totalDays() }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.km_per_day')</td>
                            <td>{{ round($rentRequest->kilometers / $rentRequest->totalDays(), 2) }}</td>
                        </tr>
                        <tr>
                            <td>@lang('requests.offer_created')</td>
                            <td>{{ $rentRequest->daysBeforeStart($rentRequest->created_at) }} @lang('requests.days_before_start')</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div> <!-- /.row -->
        </div>

        <!-- Steps -->
        <div class="card">
            <div class="card-body">
                <h4 class="box-title">
                    @lang('requests.steps_title')
                </h4>
            </div>
            <div class="row">
                <div class="col-lg-12">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('requests.data_title')</th>
                            <th>@lang('requests.place_from')</th>
                            <th>@lang('requests.place_to')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $rentRequest->requestSteps()->orderBy('start_time', 'ASC')->get() as $step )
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($step->start_time)->format('d/m/Y H:i') }}</td>
                                <td>{{ $step->start_address }}</td>
                                <td>{{ $step->end_address }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>@lang('requests.data_title')</th>
                            <th>@lang('requests.place_from')</th>
                            <th>@lang('requests.place_to')</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div> <!-- /.row -->
        </div>

        <!-- Servizi -->
        <div class="card">
            <div class="card-body">
                <h4 class="box-title">
                    @lang('requests.package')
                </h4>
            </div>
            <div class="card-body">

                <div class="typo-articles">

                    <i class="{{ $rentRequest->package->icon }}"></i> {{ $rentRequest->package->getTranslation('name', \Illuminate\Support\Facades\App::getLocale()) }}

                </div>

            </div> <!-- /.row -->
        </div>

        <!-- Commento -->
        <div class="card">
            <div class="card-body">
                <h4 class="box-title">
                    @lang('requests.added_comment_title')
                </h4>
            </div>
            <div class="card-body">

                <div class="typo-articles">

                    <p>
                        @if( empty( $rentRequest->comment ) )
                            @lang('requests.no_comments')
                        @else
                            {{ $rentRequest->comment }}
                        @endif
                    </p>

                </div>

            </div> <!-- /.row -->
        </div>
    </div>
</div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
