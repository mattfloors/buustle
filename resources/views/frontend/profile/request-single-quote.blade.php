@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
<div class="menu-mobile-overlay"></div>

<div class="bg-ivory padding-30-60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="scheda01">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                            <div class="inner">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div class="row viaggi">
                                                <div class="col-xs-5">
                                                    <span class="citta">{{ $request->getFirstStep()->onlyCityNameStart() }}</span>
                                                    <span class="indirizzo">{{ trim($request->getFirstStep()->start_address) }}</span>
                                                    <span class="data">{{ \Carbon\Carbon::parse($request->getFirstStep()->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                </div>
                                                <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                <div class="col-xs-5">
                                                    <span class="citta">{{ $request->getLastStep()->onlyCityNameEnd() }}</span>
                                                    <span class="indirizzo">{{ trim($request->getLastStep()->end_address) }}</span>
                                                    <span class="data">{{ \Carbon\Carbon::parse($request->getLastStep()->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        @include('frontend.profile.partials.quote-proposed-steps')

                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div class="row opzioni dettaglio">
                                                <div class="col-xs-12">
                                                    <br>
                                                    <span class="exc dark-blue-grey">@lang('profile.quote_required_to_pay')</span>
                                                    <span class="big orange">{{ $quote->price }}€</span>
                                                </div>
                                            </div>
                                        </div>
                                        @if( (bool) $request->sharable === true )
                                            @include('frontend.profile.partials.quote-request-shared')
                                        @endif
                                        @if( !empty($quote->infos) )
                                            @include('frontend.profile.partials.quote-infos')
                                        @endif
                                        @if( !empty($quote->prices) )
                                            @include('frontend.profile.partials.quote-prices')
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if( $quote->status === \App\Models\Quote::STATUS_COMPLETED || $quote->status === \App\Models\Quote::STATUS_ACCEPTED )
                                @include('frontend.profile.partials.quote-payment-form')
                            @elseif($quote->status === \App\Models\Quote::STATUS_PAYED)
                                @include('frontend.profile.partials.quote-payed')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
