@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente" style="background-image: url("");">
            <img src="{{ $userAvatar ? $userAvatar->getFullUrl() : asset('storage/assets/frontend/images/utente.png')  }}" alt="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" title="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ \Illuminate\Support\Facades\Auth::user()->email }}<a href="{{ route('profile.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('profile.edit')" title="@lang('profile.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ivory">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav-profile">
                            <li class="active"><a href="#" title="@lang('profile.rent_requests')">@lang('profile.rent_requests')</a></li>
                            <!--<li><a href="#" title="@lang('profile.tickets')">@lang('profile.tickets')</a></li>
                            <li><a href="#" title="@lang('profile.suggestions')">@lang('profile.suggestions')</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @include('frontend.profile.partials.rent-requests')

    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
