@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente" style="background-image: url("");">
            <img src="{{ \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar') ? \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" title="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">{{ \Illuminate\Support\Facades\Auth::user()->email }}<a href="{{ route('profile.edit') }}" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('profile.edit')" title="@lang('profile.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->phone ?? '-' }}</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>{{ \Illuminate\Support\Facades\Auth::user()->profile->address ?? '-' }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory padding-60">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            <h2>@lang('profile.transactions_title')</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

    @forelse($orders as $order)

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            @lang('profile.transaction_date_open') {{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y H:i') }}<br>
                                            @lang('profile.transaction_date_closed') {{ \Carbon\Carbon::parse($order->updated_at)->format('d/m/Y H:i') }}
                                            <hr>
                                            @lang('profile.transaction_method') {{ __('profile.transaction_method_' . $order->method) }}<br>
                                            @lang('profile.transaction_status') {{ __('profile.transaction_status_' . $order->payment_status) }}
                                            <hr>
                                            @lang('profile.transaction_for') @if( $order->isForTicket() ) @lang('profile.transaction_for_ticket') @else @lang('profile.transaction_for_quote') @endif<br>
                                            @lang('profile.transaction_amount') {{ $order->amount }} €
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

    @empty

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="border-box">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 input">
                                            @lang('profile.no_transactions')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

    @endforelse

    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection