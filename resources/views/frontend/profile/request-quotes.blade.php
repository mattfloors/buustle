@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="profilo-utente">
        <picture class="banner-menu">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="utente" style="background-image: url("");">
            <img src="{{ asset('storage/assets/frontend/images/utente.png') }}" alt="Marti" title="Marti">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="name">Martina<a href="#" title="Edit"><img src="{{ asset('storage/assets/frontend/images/icons-svg/edit.svg') }}" alt="@lang('profile.edit')" title="@lang('profile.edit')"></a></span>
                    <ul class="info">
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/mail-profile.svg') }}" alt="Email" title="Email"><span>vicine.marti@gmail.com</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/telefono.svg') }}" alt="Telefono" title="Telefono"><span>3489167930</span></li>
                        <li><img src="{{ asset('storage/assets/frontend/images/icons-svg/casa.svg') }}" alt="Indirizzo" title="Indirizzo"><span>Via Matteotti 89, Ciriè, Torino, 10073</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-ivory">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav-profile">
                            <li class="active"><a href="#" title="@lang('profile.rent_requests')">@lang('profile.rent_requests')</a></li>
                            <li><a href="#" title="@lang('profile.tickets')">@lang('profile.tickets')</a></li>
                            <li><a href="#" title="@lang('profile.suggestions')">@lang('profile.suggestions')</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-ivory">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="scheda01 scheda01-b padding-0-60">
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="list" class="display" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('quote.travelers')</th>
                                            <th>@lang('quote.price') (€)</th>
                                            <th>@lang('quote.status')</th>
                                            <th>@lang('quote.quote_valid_until')</th>
                                            <th>@lang('quote.actions')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse( $rentRequest->quotes as $q )
                                            <tr>
                                                <td>
                                                    {{ $q->sku }}
                                                </td>
                                                <td>
                                                    {{ $q->people }}
                                                </td>
                                                <td>
                                                    {{ $q->price }}
                                                </td>
                                                <td>
                                                    {{ ucfirst($q->status) }}
                                                </td>
                                                <td>
                                                    {{ \Carbon\Carbon::parse($q->valid_until)->format('d/m/Y') }}
                                                </td>
                                                <td align="center">
                                                    <a href="{{ route('profile.requestSingleQuote', ['rentRequestUuid' => $rentRequest->uuid, 'quote' => $q]) }}" class="btn btn-outline-success"><i class="fas fa-info-square"></i></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>@lang('requests.no_quotes')</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('quote.travelers')</th>
                                            <th>@lang('quote.price') (€)</th>
                                            <th>@lang('quote.status')</th>
                                            <th>@lang('quote.quote_valid_until')</th>
                                            <th>@lang('quote.actions')</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
