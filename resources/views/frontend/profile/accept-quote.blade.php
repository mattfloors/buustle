@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="bg-ivory padding-30-60">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="scheda01">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <div class="inner">
                                    <div>
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row viaggi">
                                                    <div class="col-xs-5">
                                                        <span class="citta">{{ $rentRequest->getFirstStep()->onlyCityNameStart() }}</span>
                                                        <span class="indirizzo">{{ trim($rentRequest->getFirstStep()->start_address) }}</span>
                                                        <span class="data">{{ \Carbon\Carbon::parse($rentRequest->getFirstStep()->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                    </div>
                                                    <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="Sport" title="Sport"></div>
                                                    <div class="col-xs-5">
                                                        <span class="citta">{{ $rentRequest->getLastStep()->onlyCityNameEnd() }}</span>
                                                        <span class="indirizzo">{{ trim($rentRequest->getLastStep()->end_address) }}</span>
                                                        <span class="data">{{ \Carbon\Carbon::parse($rentRequest->getLastStep()->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-10 col-xs-offset-1">
                                                <div class="row opzioni dettaglio">
                                                    <div class="col-xs-12">
                                                        <br>
                                                        <span class="exc dark-blue-grey">@lang('profile.quote_required_to_pay')</span>
                                                        <span class="big orange">{{ $quote->price }}€</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if( $rentRequest->sharable === true )
                                                @include('frontend.profile.partials.quote-request-shared')
                                            @endif

                                            @if( $order->method === \App\Models\Order::PAYPAL )
                                                @include('frontend.profile.partials.pay-paypal')
                                            @endif
                                            @if( $order->method === \App\Models\Order::STRIPE )
                                                @include('frontend.profile.partials.pay-stripe')
                                            @endif
                                            @if( $order->method === \App\Models\Order::BONIFICO )
                                                @include('frontend.profile.partials.pay-bonifico')
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
