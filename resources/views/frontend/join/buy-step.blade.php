@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title">@lang('join.buy_step_title')</h1>
                            <h2 class="subtitle">@lang('join.buy_step_subtitle')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory aggregati step01">
        <div class="container">
            <div class="scheda01 scheda01-b">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                        <div class="inner end">
                            <div>
                                <div class="id-viaggio">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <span>@lang('join.step_id'): {{ $travelStep->travel->uuid }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <div class="row viaggi">
                                            <div class="col-xs-5">
                                                <span class="citta">{{ $travelStep->startMarker->name }}</span>
                                                <span class="indirizzo">{{ $travelStep->startMarker->street_and_number }}</span>
                                                <span class="data">{{ \Carbon\Carbon::parse($travelStep->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                            </div>
                                            <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="_" title="_"></div>
                                            <div class="col-xs-5">
                                                <span class="citta">{{ $travelStep->endMarker->name }}</span>
                                                <span class="indirizzo">{{ $travelStep->endMarker->street_and_number }}</span>
                                                <span class="data">{{ \Carbon\Carbon::parse($travelStep->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 excd">
                                        <hr>
                                    </div>
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <div class="row credito-posti">
                                            <div class="col-xs-6">
                                                <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="Credito" title="Credito"><span>{{ $travelStep->price }}€</span></div>
                                                <span class="per-biglietto">@lang('join.per_ticket')</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="@lang('join.free_slots')" title="@lang('join.free_slots')"><span><b>{{ $travelStep->slots - $travelStep->boughtSlots() }}</b>/{{ $travelStep->slots }}</span></div>
                                                <span class="per-biglietto">@lang('join.free_slots')</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 excd">
                                        <hr>
                                    </div>
                                    <div class="col-xs-10 col-xs-offset-1 excd">
                                        <div class="row mezzo">
                                            <div class="col-xs-12">
                                                <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/bus-2.svg') }}" alt="@lang('join.package')" title="@lang('join.package')"><span>@lang('join.package'): <span class="orange">{{ $travelStep->travel->package->name }}</span></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 excd">
                                        <hr>
                                    </div>
                                    <div class="col-xs-10 col-xs-offset-1 excd">
                                        <div class="event">
                                            <img src="{{ asset('storage/assets/frontend/images/icons-svg/festival.svg') }}" alt="Festival" title="Festival">
                                            <span>{{ $travelStep->travel->travel_type }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 title-orange-icon">
                                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/portafoglio.svg') }}" alt="@lang('join.payment_details')" title="@lang('join.payment_details')"><span class="orange">@lang('join.payment_details')</span>
                                </div>
                            </div>
                            <div class="inner exc">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-xs-12 input">
                                                    <label for="nome-carta">Nome sulla carta*</label>
                                                    <input type="text" id="nome-carta" name="nome-carta" value="" placeholder="Scrivi il tuo nome">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="numero-carta">Numero carta*</label>
                                                    <input type="text" id="numero-carta" name="numero-carta" value="" placeholder="Scrivi il numero della carta">
                                                </div>
                                                <div class="col-xs-6 input">
                                                    <label for="scadenza-carta">Scadenza*</label>
                                                    <input type="text" id="scadenza-carta" name="scadenza-carta" value="" placeholder="(MM/YY)">
                                                </div>
                                                <div class="col-xs-6 input">
                                                    <label for="codice-carta">CVC*</label>
                                                    <input type="text" id="codice-carta" name="codice-carta" value="" placeholder="XXX">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 title-orange-icon">
                                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/passeggeri.svg') }}" alt="@lang('join.person_1_details')" title="@lang('join.person_1_details')"><span class="orange">@lang('join.person_1_details')</span>
                                </div>
                            </div>
                            <div class="inner exc">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-xs-12 input">
                                                    <label for="nome-passeggero-1">Nome*</label>
                                                    <input type="text" id="nome-passeggero-1" name="nome-passeggero-1" value="" placeholder="Scrivi il nome">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="email-passeggero-1">Email*</label>
                                                    <input type="email" id="email-passeggero-1" name="email-passeggero-1" value="" placeholder="Scrivi l'indirizzo email">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="telefono-passeggero-1">Telefono*</label>
                                                    <input type="text" id="telefono-passeggero-1" name="telefono-passeggero-1" value="" placeholder="Scrivi il numero di telefono">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 title-orange-icon">
                                    <img src="{{ asset('storage/assets/frontend/images/icons-svg/passeggeri.svg') }}" alt="I tuoi dettagli" title="I tuoi dettagli"><span class="orange">I tuoi dettagli</span>
                                </div>
                            </div>
                            <div class="inner exc">
                                <div>
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-xs-12 input">
                                                    <label for="nome-passeggero-principale">Nome*</label>
                                                    <input type="text" id="nome-passeggero-principale" name="nome-passeggero-principale" value="" placeholder="Scrivi il tuo nome">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="email-passeggero-principale">Email*</label>
                                                    <input type="email" id="email-passeggero-principale" name="email-passeggero-principale" value="" placeholder="Scrivi il tuo indirizzo email">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="password-passeggero-principale">Password*</label>
                                                    <input type="password" id="password-passeggero-principale" name="password-passeggero-principale" value="" placeholder="Scrivi la tua password">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="conferma-password-passeggero-principale">Conferma password*</label>
                                                    <input type="password" id="conferma-password-passeggero-principale" name="conferma-password-passeggero-principale" value="" placeholder="Scrivi di nuovo la tua password">
                                                </div>
                                                <div class="col-xs-12 input">
                                                    <label for="telefono-passeggero-principale">Telefono*</label>
                                                    <input type="text" id="telefono-passeggero-principale" name="telefono-passeggero-principale" value="" placeholder="Scrivi il tuo numero di telefono">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                    <div class="checkbox">
                                        <input type="checkbox" value="terms" id="terms">
                                        <label for="terms">
                                            Acetto i <a href="#" title="Termini & Condizioni">Termini & Condizioni</a> e la <a href="#" title="Normativa sulla Privacy">Normativa sulla Privacy</a>
                                            <div class="check-button"><i></i></div>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <input type="checkbox" value="nwslt" id="nwslt">
                                        <label for="nwslt">
                                            Iscrivimi alla newsletter
                                            <div class="check-button"><i></i></div>
                                        </label>
                                    </div>
                                    <input type="submit" class="bttn bttn-orange " value="Conferma acquisto">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
