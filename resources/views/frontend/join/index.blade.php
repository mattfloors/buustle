@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')

    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-noleggio-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/background-aggregati.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title">@lang('join.main_title')</h1>
                            <h2 class="subtitle">@lang('join.main_sub_title')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory padding-30-60" id="noleggia">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="search-filter">
                        <form action="{{ route('join.index') }}" method="get">
                            @csrf
                            <div class="top">
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                        <div class="input-search">
                                            <input name="q" id="q" type="search" placeholder="@lang('join.find_placeholder')">
                                        </div>
                                        <div class="filter">
                                            <img class="unclick active" src="{{ asset('storage/assets/frontend/images/icons-svg/filter-unclick.svg') }}" alt="@lang('join.placeholder_filters')" title="@lang('join.placeholder_filters')">
                                            <img class="click" src="{{ asset('storage/assets/frontend/images/icons-svg/filter-click.svg') }}" alt="@lang('join.placeholder_filters')" title="@lang('join.placeholder_filters')">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="border-box padding-30-0">
                                        <div class="row">
                                            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                                <h3>@lang('join.filter_action'):</h3>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 input">
                                                        <label for="start_place">@lang('join.select_start_place')</label>
                                                        <input type="text" id="start_place" value="{{ $request->input('start_place', '') }}" name="start_place" placeholder="@lang('join.select_start_place_placeholder')">
                                                        <input type="hidden" name="start_lat" id="start_lat" value="{{ $request->input('start_lat') }}">
                                                        <input type="hidden" name="start_lng" id="start_lng" value="{{ $request->input('start_lng') }}">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 input">
                                                        <label for="end_place">@lang('join.select_end_place')</label>
                                                        <input type="text" id="end_place" value="{{ $request->input('end_place', '') }}" name="end_place" placeholder="@lang('join.select_end_place_placeholder')">
                                                        <input type="hidden" name="end_lat" id="end_lat" value="{{ $request->input('end_lat') }}">
                                                        <input type="hidden" name="end_lng" id="end_lng" value="{{ $request->input('end_lng') }}">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 input data-ora">
                                                        <label for="start_date">@lang('join.select_start_date')</label>
                                                        <div class="data joinSearchDate">
                                                          <span class="add-on">
                                                            <input autocomplete="off" value="{{ $request->input('start_date') }}" id="start_date" name="start_date" data-format="dd/MM/yyyy" type="text" placeholder="@lang('join.select_start_date_placeholder')">
                                                          </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 select-container">
                                                        <label for="people">@lang('join.select_people')</label>
                                                        <input type="number" id="people" name="people" value="{{ $request->input('people') ?: 1 }}" min="1" class="form-control">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 select-container">
                                                        <label for="travel_type">@lang('join.travel_type')</label>
                                                        <select id="travel_type" name="travel_type" class="selectpicker" title="@lang('rent.step3_select_option')">
                                                            <option value="@lang('rent.step3_group_travel')">@lang('rent.step3_group_travel')</option>
                                                            <option value="@lang('rent.step3_school_trip')">@lang('rent.step3_school_trip')</option>
                                                            <option value="@lang('rent.step3_music_festival')">@lang('rent.step3_music_festival')</option>
                                                            <option value="@lang('rent.step3_sport_event')">@lang('rent.step3_sport_event')</option>
                                                            <option value="@lang('rent.step3_company_meeting')">@lang('rent.step3_company_meeting')</option>
                                                            <option value="@lang('rent.step3_wedding_cerimony')">@lang('rent.step3_wedding_cerimony')</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12"><button type="submit">invia</button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="scheda01 scheda01-b">
                <div class="row">
                    @forelse($travelSteps as $travelStep)
                        <div class="col-xs-12 col-sm-4 inner">
                            <div>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <div class="row event">
                                            <div class="col-xs-3"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sport.svg') }}" alt="@lang('join.sport')" title="@lang('join.sport')"></div>
                                            <div class="col-xs-9"><p>{!! $travelStep->travel->description !!}</p></div>
                                        </div>
                                        <div class="row viaggi">
                                            <div class="col-xs-5">
                                                <span class="citta">{{ $travelStep->startMarker->name }}</span>
                                                <span class="indirizzo">{{ $travelStep->startMarker->street_and_number }}</span>
                                                <span class="data">{{ \Carbon\Carbon::parse($travelStep->start_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                            </div>
                                            <div class="col-xs-2"><img src="{{ asset('storage/assets/frontend/images/icons-svg/freccia-doppio-senso.svg') }}" alt="@lang('join.route')" title="@lang('join.route')"></div>
                                            <div class="col-xs-5">
                                                <span class="citta">{{ $travelStep->endMarker->name }}</span>
                                                <span class="indirizzo">{{ $travelStep->endMarker->street_and_number }}</span>
                                                <span class="data">{{ \Carbon\Carbon::parse($travelStep->end_time)->formatLocalized('%e %B %g - %H:%m') }}</span>
                                            </div>
                                        </div>
                                        <div class="row credito-posti">
                                            <div class="col-xs-6">
                                                <div class="span"><img src="{{ asset('storage/assets/frontend/images/icons-svg/soldi.svg') }}" alt="@lang('join.credit')" title="@lang('join.credit')"><span>{{ $travelStep->price }}€</span></div>
                                                <span class="per-biglietto">@lang('join.for_ticket')</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="span posti"><img src="{{ asset('storage/assets/frontend/images/icons-svg/sedile.svg') }}" alt="@lang('join.free_slots')" title="@lang('join.free_slots')"><span><b>{{ $travelStep->slots - $travelStep->boughtSlots() }}</b>/{{ $travelStep->slots }}</span></div>
                                                <span class="per-biglietto">@lang('join.free_slots')</span>
                                            </div>
                                        </div>
                                        <a href="{{ route('join.step', ['step' => $travelStep]) }}" class="bttn bttn-orange " title="@lang('join.go_title')"><span>@lang('join.go')</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-md-12 text-center">@lang('join.no_travels_found')</div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvFmEA0NWFv2Celnf_2qPmhAGcQjQReSw&libraries=places&callback=initPlaces&language={{ \Illuminate\Support\Facades\App::getLocale() }}" async defer></script>

@endsection

@section('added-js')

    <script>

        function initPlaces() {
            var input_start = document.getElementById('start_place');
            var input_end = document.getElementById('end_place');
            var autocomplete_start = new google.maps.places.Autocomplete(input_start);
            var autocomplete_end = new google.maps.places.Autocomplete(input_end);

            // Set the data fields to return when the user selects a place.
            autocomplete_start.setFields(
                ['address_components', 'geometry', 'icon', 'name']
            );
            autocomplete_end.setFields(
                ['address_components', 'geometry', 'icon', 'name']
            );

            autocomplete_start.addListener('place_changed', function() {
                let place = autocomplete_start.getPlace();
                if (!place.geometry) {
                    window.alert("@lang('join.no_details_autocomplete'): '" + place.name + "'");
                    return;
                }

                $("#start_lat").val(place.geometry.location.lat);
                $("#start_lng").val(place.geometry.location.lng);
            });

            autocomplete_end.addListener('place_changed', function() {
                let place = autocomplete_end.getPlace();
                if (!place.geometry) {
                    window.alert("@lang('join.no_details_autocomplete'): '" + place.name + "'");
                    return;
                }

                $("#end_lat").val(place.geometry.location.lat);
                $("#end_lng").val(place.geometry.location.lng);
            });
        }

        $(document).ready(function(){
            let nowTemp = new Date();
            let now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            $('.joinSearchDate').datetimepicker({
                pickTime: false,
                startDate: now
            });
        });
    </script>

@endsection
