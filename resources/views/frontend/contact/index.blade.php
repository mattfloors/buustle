@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/contattaci.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/contattaci-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/contattaci.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/contattaci.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title mbottom-0">@lang('contact-us.title')</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-ivory contattaci">
        <div class="container padding-30-60">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="charcoal-grey">@lang('contact-us.leave_message_or_call') {{ setting('general.phone_number') }}.</h2>
                    <p class="exc">@lang('contact-us.service_always_open')</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{ route('contactUsSubmit') }}">
                        <div class="form-box">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 input">
                                            <label for="nome">@lang('contact-us.first_name')*</label>
                                            <input type="text" id="nome" name="nome" placeholder="@lang('contact-us.first_name_placeholder')" required>
                                        </div>
                                        <div class="col-sm-6 col-md-6 input">
                                            <label for="cognome">@lang('contact-us.last_name')*</label>
                                            <input type="text" id="cognome" name="cognome" placeholder="@lang('contact-us.last_name_placeholder')" required>
                                        </div>
                                        <div class="col-sm-6 col-md-6 input">
                                            <label for="email">@lang('contact-us.email')*</label>
                                            <input type="email" id="email" name="email" placeholder="@lang('contact-us.email')" required>
                                        </div>
                                        <div class="col-sm-6 col-md-6 input">
                                            <label for="telefono">@lang('contact-us.phone')*</label>
                                            <input type="tel" id="telefono" name="telefono" placeholder="@lang('contact-us.phone_placeholder')">
                                        </div>
                                        <div class="input col-xs-12">
                                            <label for="altro">@lang('contact-us.content')</label>
                                            <textarea name="content" placeholder="@lang('contact-us.content_placeholder')" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                                <input type="submit" class="bttn bttn-orange small" value="@lang('contact-us.send')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
