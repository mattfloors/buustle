<!DOCTYPE html>
<html lang="{{ \Illuminate\Support\Facades\App::getLocale() }}">

@include('frontend.layout.shared.head')

<body class="">

    @include('frontend.layout.shared.header')

    @include('frontend.layout.shared.sidebar')

    @yield('content')

    @include('frontend.layout.shared.footer')

    @section('js-scripts')
        <!-- jQuery CDN -->
        <script src="{{ asset('storage/assets/frontend/js/jQuery/3.3.1/jquery.min.js') }}"></script>
        <!-- Bootstrap JS -->
        <script src="{{ asset('storage/assets/frontend/js/bootstrap/3.3.7/bootstrap.min.js') }}"></script>
        <script src="{{ asset('storage/assets/frontend/js/bootstrap-select.min.js') }}"></script>
        <!--<script src="{{ asset('storage/assets/frontend/js/bootstrap-datetimepicker.min.js') }}"></script>-->
        <!-- Slick Slider CSS -->
        <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/slick-slide.min.js') }}"></script>
        <!-- font awesome -->
        <link rel="stylesheet" href="{{ asset('storage/assets/font-awesome/css/all.min.css') }}">
        <!-- Our js -->
        <script type="text/javascript" src="{{ asset('storage/assets/frontend/js/main.js') }}"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140483564-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-140483564-1');
        </script>

        <!-- SweetAlert -->
        @include('sweetalert::alert')
    @show

    @yield('added-js')

</body>

</html>
