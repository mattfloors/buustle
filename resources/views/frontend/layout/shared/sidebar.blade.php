<!-- Sidebar  -->
<div id="sidebar">
    <!--     <div class="menu-no-logged">
          <img class="banner-menu" src="images/banner-bg/profile-menu.svg" alt="Accedi" title="Accedi">
          <a href="#" class="bttn bttn-orange "><span>Accedi</span></a>
        </div> -->
    <div class="menu-logged">
        <img class="banner-menu"
             src="{{ asset('storage/assets/frontend/images/banner-bg/profile-menu-logged.svg') }}"
             alt="Profilo" title="Profilo">
        <div class="utente" style="background-image: url("");">
            <img src="{{
    \Illuminate\Support\Facades\Auth::user() && \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar')
    ? \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar')->getFullUrl()
    : asset('storage/assets/frontend/images/utente.png') }}"
                 alt="{{ \Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->profile->first_name : 'User' }}" title="{{ \Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->profile->first_name : 'User' }}">
        </div>
        <span class="name">{{ \Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->profile->first_name : 'User' }}</span>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="menu">
                        @if ( \Illuminate\Support\Facades\Auth::check() )
                            @if( \Illuminate\Support\Facades\Auth::user()->can('view administration'))
                                <li>
                                    <a href="{{ route('admin.dashboard') }}" title="Amministrazione">Amministrazione</a>
                                </li>
                            @endif
                            @if( \Illuminate\Support\Facades\Auth::user()->isValidNoleggiatore() )
                                <li>
                                    <a href="{{ route('partner.index') }}" title="@lang('header.partner')">@lang('header.partner')</a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('profile.requests') }}" title="@lang('header.profile')">@lang('header.profile')</a>
                            </li>
                            <li>
                                <a href="{{ route('profile.transactions') }}" title="@lang('header.transactions')">@lang('header.transactions')</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" class="red" title="@lang('global.logout_title')">@lang('global.logout')</a>
                            </li>
                        @else
                            <li class="menu-no-logged">
                                <a href="{{ route('login') }}" class="bttn bttn-orange "><span>@lang('global.login')</span></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="container">
            <nav class="nav-menu">
                <ul>
                    <li class="lang">
                        <ul>
                            <li @if(\Illuminate\Support\Facades\App::getLocale() === 'it') class="active" @endif>
                                <a href="{{ route('setLocale', ['locale' => 'it']) }}" title="ITA">@lang('header.lang_ITA')</a>
                            </li>
                            <span>/</span>
                            <li @if(\Illuminate\Support\Facades\App::getLocale() === 'en') class="active" @endif>
                                <a href="{{ route('setLocale', ['locale' => 'en']) }}" title="EN">@lang('header.lang_EN')</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('rent.noleggi.request') }}" title="@lang('header.rent_request_title')">@lang('header.rent_request')</a>
                    </li>
                    <li>
                        <a href="{{ route('aboutUs') }}" title="@lang('header.about_us')">@lang('header.about_us')</a>
                    </li>
                    <li>
                        <a href="{{ route('contactUs') }}" title="@lang('header.contact_us_title')">@lang('header.contact_us')</a>
                    </li>
                    <li>
                        <a href="{{ route('becomePartner') }}" title="@lang('header.earn_with_us_title')">@lang('header.earn_with_us')</a>
                    </li>
                </ul>
            </nav>
            <ul class="submenu">
                <li><a href="{{ route('aboutUs') }}" title="@lang('footer.about_us')">@lang('footer.about_us')</a></li>
                <li><a href="{{ route('ourBus') }}" title="@lang('footer.our_bus')">@lang('footer.our_bus')</a></li>
                <li><a href="{{ route('ourStops') }}" title="@lang('footer.our_stations')">@lang('footer.our_stations')</a></li>
                <li><a href="{{ route('faq') }}" title="@lang('footer.faq')">@lang('footer.faq')</a></li>
                <li><a href="#" title="@lang('footer.general_conditions')">@lang('footer.general_conditions')</a></li>
                <li><a href="#" title="@lang('footer.privacy')">@lang('footer.privacy')</a></li>
                <li><a href="#" title="@lang('footer.cookies')">@lang('footer.cookies')</a></li>
            </ul>
        </div>
    </div>
</div>