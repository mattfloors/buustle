<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Buustle</title>

    <!-- Bootstrap CSS -->
    <link href="{{ asset('storage/assets/frontend/css/bootstrap/3.3.7/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('storage/assets/frontend/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link rel="stylesheet" href="{{ asset('storage/assets/frontend/css/slick-slider-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/frontend/css/slick-slider.min.css') }}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ asset('storage/assets/frontend/css/style.css') }}">

    @yield('added-css')
</head>