
<footer class="footer hidden-sm hidden-md hidden-lg">
    fvdfvdvdfv
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
                    <img class="logo-footer" src="{{ asset('storage/assets/frontend/images/logo-buustle-footer.svg') }}" alt="Buustle" title="Buustle">
                    <ul class="social">
                        <li>
                            <a href="#" title="Facebook" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/facebook.svg') }}" alt="Facebook" title="Facebook"></a>
                        </li>
                        <li>
                            <a href="#" title="Instagram" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/instagram.svg') }}" alt="Instagram" title="Instagram"></a>
                        </li>
                        <li>
                            <a href="#" title="Twitter" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/twitter.svg') }}" alt="Twitter" title="Twitter"></a>
                        </li>
                        <li>
                            <a href="#" title="Linkedin" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/linked-in.svg') }}" alt="Linkedin" title="Linkedin"></a>
                        </li>
                    </ul>
                    <div class="info">
                        <span>Via Lamarmora 16, 10128, TO</span>
                        <span>@lang('footer.VAT_number') 11568420019</span>
                        <span>@lang('footer.travel_license') n. 0034009/16</span>
                    </div>
                    <div class="contatti">
                        <span>info@buustle.com</span>
                        <span>+39 347 5747 58 38</span>
                    </div>
                    <div class="support">
                        <span>@lang('footer.supported_by')</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <br><br>
                            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenza Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" ></a><br><br>@lang('footer.license') <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="credits">
        <span>@lang('footer.developed_with') <img src="{{ asset('storage/assets/frontend/images/icons-svg/favorite-24-px.svg') }}" alt="Cuore" title="Cuore"> @lang('footer.in_turin')</span>
    </div>
</footer>
<footer class="footer hidden-xs">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img class="logo-footer" src="{{ asset('storage/assets/frontend/images/logo-buustle-footer.svg') }}" alt="Buustle" title="Buustle">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <ul class="nav-footer">
                                <li><a href="{{ route('rent.noleggi.request') }}" title="@lang('footer.rent_a_bus')">@lang('footer.rent_a_bus')</a></li>
                                <li><a href="{{ route('comingSoon') }}" title="@lang('footer.join')">@lang('footer.join')</a></li>
                                <li><a href="{{ route('contactUs') }}" title="@lang('footer.contactUs')">@lang('footer.contactUs')</a></li>
                                <li><a href="{{ route('becomePartner') }}" title="@lang('footer.earn_with_us')">@lang('footer.earn_with_us')</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <ul class="nav-footer">
                                <li><a href="{{ route('aboutUs') }}" title="@lang('footer.about_us')">@lang('footer.about_us')</a></li>
                                <li><a href="{{ route('ourBus') }}" title="@lang('footer.our_bus')">@lang('footer.our_bus')</a></li>
                                <li><a href="{{ route('ourStops') }}" title="@lang('footer.our_stations')">@lang('footer.our_stations')</a></li>
                                <li><a href="{{ route('faq') }}" title="@lang('footer.faq')">@lang('footer.faq')</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <ul class="nav-footer">
                                <li><a href="#" title="@lang('footer.general_conditions')">@lang('footer.general_conditions')</a></li>
                                <li><a href="#" title="@lang('footer.privacy')">@lang('footer.privacy')</a></li>
                                <li><a href="#" title="@lang('footer.cookies')">@lang('footer.cookies')</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="info">
                                <span>Via Lamarmora 16, 10128, TO</span>
                                <span>Partita Iva 11568420019</span>
                                <span>Licenza di viaggio n. 0034009/16</span>
                            </div>
                            <div class="contatti">
                                <span>{{ setting('general.contact_email') }}</span>
                                <span>+39 3929393264</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="support">
                                <span>@lang('footer.supported_by')</span>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5">
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/buustle/" title="Facebook" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/facebook.svg') }}" alt="Facebook" title="Facebook"></a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/buustle/" title="Instagram" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/instagram.svg') }}" alt="Instagram" title="Instagram"></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/buustle" title="Twitter" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/twitter.svg') }}" alt="Twitter" title="Twitter"></a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/buustle-srl/" title="Linkedin" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/linked-in.svg') }}" alt="Linkedin" title="Linkedin"></a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCqbmGG89UzkjL9FmnbHIzGQ" title="Youtube" target="_blank"><img src="{{ asset('storage/assets/frontend/images/icons-svg/youtube.svg') }}" alt="Youtube" title="Youtube"></a>
                                </li>
                            </ul>
                            <div class="row">
                                <div class="col-xs-12">
                                    <br><br>
                                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenza Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" ></a><br><br>Quest'opera è distribuita con Licenza <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="credits">
        <span>@lang('footer.developed_with') <img src="{{ asset('storage/assets/frontend/images/icons-svg/favorite-24-px.svg') }}" alt="Cuore" title="Cuore"> @lang('footer.in_turin')</span>
    </div>
</footer>