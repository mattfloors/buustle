<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
                <button type="button" class="menu-mobile-icon hidden-md hidden-lg">
                    <div class="icon-bars collapsed">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                </button>
                <a href="{{ route('home') }}" title="Buustle" class="logo-container"><img class="logo-header" src="{{ asset('storage/assets/frontend/images/logo-buustle.svg') }}" alt="Buustle" title="Buustle"></a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-9 hidden-sm hidden-xs">
                <nav class="main-menu">
                    <ul>
                        <li class="lang">
                            <ul>
                                <li @if(\Illuminate\Support\Facades\App::getLocale() === 'it') class="active" @endif>
                                    <a href="{{ route('setLocale', ['locale' => 'it']) }}" title="ITA">@lang('header.lang_ITA')</a>
                                </li>
                                <span>/</span>
                                <li @if(\Illuminate\Support\Facades\App::getLocale() === 'en') class="active" @endif>
                                    <a href="{{ route('setLocale', ['locale' => 'en']) }}" title="EN">@lang('header.lang_EN')</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('rent.noleggi.request') }}" title="@lang('header.rent_request_title')">@lang('header.rent_request')</a>
                        </li>
                        <li>
                            <a href="{{ route('aboutUs') }}" title="@lang('header.about_us')">@lang('header.about_us')</a>
                        </li>
                        <li>
                            <a href="{{ route('contactUs') }}" title="@lang('header.contact_us_title')">@lang('header.contact_us')</a>
                        </li>
                        <li>
                            <a href="{{ route('becomePartner') }}" title="@lang('header.earn_with_us_title')">@lang('header.earn_with_us')</a>
                        </li>
                        @if ( \Illuminate\Support\Facades\Auth::check() )
                            <li class="menu-logged">
                                <div class="utente" style="background-image: url("");">
                                  <img src="{{ \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar') ? \Illuminate\Support\Facades\Auth::user()->getFirstMedia('avatar')->getFullUrl() : asset('storage/assets/frontend/images/utente.png') }}" alt="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}" title="{{ \Illuminate\Support\Facades\Auth::user()->profile->first_name }}">
                                </div>
                                <ul class="menu">
                                    @if( \Illuminate\Support\Facades\Auth::user()->can('view administration'))
                                        <li>
                                            <a href="{{ route('admin.dashboard') }}" title="Amministrazione">Amministrazione</a>
                                        </li>
                                    @endif
                                    @if( \Illuminate\Support\Facades\Auth::user()->isValidNoleggiatore() )
                                        <li>
                                            <a href="{{ route('partner.index') }}" title="@lang('header.partner')">@lang('header.partner')</a>
                                        </li>
                                    @endif
                                  <li>
                                    <a href="{{ route('profile.requests') }}" title="@lang('header.profile')">@lang('header.profile')</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('profile.transactions') }}" title="@lang('header.transactions')">@lang('header.transactions')</a>
                                  </li>
                                  <li>
                                    <a href="{{ route('logout') }}" class="red" title="@lang('global.logout_title')">@lang('global.logout')</a>
                                  </li>
                                </ul>
                            </li>
                        @else
                            <li class="menu-no-logged">
                                <a href="{{ route('login') }}" class="bttn bttn-orange "><span>@lang('global.login')</span></a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>