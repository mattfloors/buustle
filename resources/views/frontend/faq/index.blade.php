@extends('frontend.layout.main')

@section('added-css')
    @parent

@endsection

@section('content')
    <div class="menu-mobile-overlay"></div>

    <div class="banner-bg">
        <picture class="banner">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}" media="(max-width: 767px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq-desktop.svg') }}" media="(min-width: 768px)">
            <source srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}">
            <img srcset="{{ asset('storage/assets/frontend/images/banner-bg/faq.svg') }}" alt="Buustle" title="Buustle">
        </picture>
        <div class="inner">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                            <h1 class="title mbottom-0">@lang('faqs.title')</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-ivory padding-60 faqs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_1')</h2>
                    <p>@lang('faq.answer_1')</p>
                </div>
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_2')</h2>
                    <p>@lang('faq.answer_2')</p>
                </div>
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_3')</h2>
                    <p>@lang('faq.answer_3')</p>
                </div>
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_4')</h2>
                    <p>@lang('faq.answer_4')</p>
                </div>
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_5')</h2>
                    <p>@lang('faq.answer_5')</p>
                </div>
                <div class="col-xs-12 col-sm-6 item">
                    <h2 class="orange">@lang('faq.question_6')</h2>
                    <p>@lang('faq.answer_6')</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    @parent

@endsection

@section('added-js')

@endsection
