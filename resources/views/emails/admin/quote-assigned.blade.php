@extends('emails.layouts.base')

@section('top.title')
    Quote Assigned
@endsection

@section('top.preview')
    Quote Assigned Preview Message
@endsection

@section('content.main')
    Content Main Quote Assigned
@endsection

@section('content.footer')
    Content Footer Quote Assigned
@endsection