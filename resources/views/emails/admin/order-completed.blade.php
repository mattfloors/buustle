@extends('emails.layouts.base')

@section('top.title')
    Order Completed
@endsection

@section('top.preview')
    Order Completed Preview Message
@endsection

@section('content.main')
    Content Main Order Completed
@endsection

@section('content.footer')
    Content Footer Order Completed
@endsection