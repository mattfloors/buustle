@extends('emails.layouts.base')

@section('top.title')
    Quote Accepted
@endsection

@section('top.preview')
    Quote Accepted Preview Message
@endsection

@section('content.main')
    Content Main Quote Accepted
@endsection

@section('content.footer')
    Content Footer Quote Accepted
@endsection