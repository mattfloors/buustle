@extends('emails.layouts.base')

@section('top.title')
    Travel Assigned
@endsection

@section('top.preview')
    Travel Assigned Preview Message
@endsection

@section('content.main')
    Content Main Travel Assigned
@endsection

@section('content.footer')
    Content Footer Travel Assigned
@endsection