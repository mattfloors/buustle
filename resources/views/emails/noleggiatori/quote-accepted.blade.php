@extends('emails.layouts.base')

@section('top.title')
    Quote Accepted in your Area
@endsection

@section('top.preview')
    Quote Accepted in your Area Preview Message
@endsection

@section('content.main')
    Content Main Quote Accepted in your Area
@endsection

@section('content.footer')
    Content Footer Quote Accepted in your Area
@endsection