@extends('emails.layouts.base')

@section('top.title')
    Travel Waiting in your Area
@endsection

@section('top.preview')
    Travel Waiting in your Area Preview Message
@endsection

@section('content.main')
    Content Main Travel Waiting in your Area
@endsection

@section('content.footer')
    Content Footer Travel Waiting in your Area
@endsection