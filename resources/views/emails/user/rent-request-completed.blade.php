@extends('emails.layouts.base')

@section('top.title')
    Rent Request Completed
@endsection

@section('top.preview')
    Rent Preview Message
@endsection

@section('content.main')
    Content Main request completed
@endsection

@section('content.footer')
    Content Footer request completed
@endsection