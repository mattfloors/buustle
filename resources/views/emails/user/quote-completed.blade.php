@extends('emails.layouts.base')

@section('top.title')
    Quote Completed
@endsection

@section('top.preview')
    Quote Preview Message
@endsection

@section('content.main')
    Content Main Quote completed
@endsection

@section('content.footer')
    Content Footer Quote completed
@endsection