<!DOCTYPE html>
<html>
<head>

    <title>Buustle - @yield('top.title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style type="text/css">
        @font-face {
            font-family: 'F37Ginger';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Light/F37Ginger-Light.svg') }}') format('svg');
            /* Legacy iOS */
        }
        @font-face {
            font-family: 'F37Ginger-Regular';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Regular/F37Ginger-Regular.svg') }}') format('svg');
            /* Legacy iOS */
        }
        @font-face {
            font-family: 'F37Ginger-Bold';
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.eot') }}');
            /* IE9 Compat Modes */
            src: url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.woff2') }}') format('woff2'), /* Super Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.woff') }}') format('woff'), /* Pretty Modern Browsers */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.ttf') }}') format('truetype'), /* Safari, Android, iOS */ url('{{ asset('storage/assets/mail/fonts/F37Ginger-Family/F37Ginger-Bold/F37Ginger-Bold.svg') }}') format('svg');
            /* Legacy iOS */
        }
        .title{
            font-size: 24px;
        }
        .text{
            font-size: 18px;
            line-height: 28px;
        }
        .big{
            font-size: 30px;
        }
        .tblCell{
            padding: 40px;
        }
        table, tbody, tr, td{
            border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; width: 100%;
        }
        .mediaquery{
            width: 767px;
            margin-left: auto !important;
            margin-right: auto !important;
        }
        @media only screen and (max-width: 767px){
            table{
                width: 100% !important;
            }
            .title{
                font-size: 18px;
            }
            .text{
                font-size: 14px;
            }
            .big{
                font-size: 24px;
            }
            .tblCell{
                text-align: center;
                padding: 30px 10px;
            }
            .tblCell .title{
                font-size: 14px;
            }
            .tblCell img{
                margin-right: auto !important;
                margin-left: auto !important;
            }
            .tblCell .big{
                margin-right: auto !important;
                margin-left: auto !important;
            }
            .tblCell *{
                display: block;
                text-align: center;
            }
        }
    </style>
</head>
<body style="height: 100% !important; margin: 0; padding: 0; width: 100% !important; min-width: 100%;">
<table width="100%" bgcolor="#f5f5f5" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
    <tbody style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
    <tr style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
        <td width="100%" align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
            <table width="100%" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                <tbody>
                <tr>
                    <td>
                        <table style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;" align="center" width="100%">
                            <tbody>
                            <tr>
                                <td width="100%" align="center">
                                    <table width="100%" align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                        <tbody>
                                        <tr>
                                            <td width="100%" align="center">
                                                <div>
                                                    <table width="100%" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" style="padding: 20px;">
                                                                <img src="{{ asset('storage/assets/mail/images/logo-buustle.png') }}" width="200" style="max-width: 200px; display: block; width: 200px;">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div>
                                                    <table class="mediaquery" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <table class="mediaquery" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="mediaquery" align="center">
                                                                            <img src="{{ asset('storage/assets/mail/images/artboard.jpg') }}" style="max-width: 100%; display: block;">
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div>
                                                    <table class="mediaquery" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <table align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <table style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; padding-top:60px;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="font-family: 'F37Ginger';">
                                                                                        <div style="text-align: center;">
                                                                                            <span class="title" style="margin-bottom: 30px; display: block; font-family: 'F37Ginger'; color: #7b868c;">
                                                                                                Buustle
                                                                                            </span>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <img src="{{ asset('storage/assets/mail/images/gruppi.png') }}" width="150" style="max-width: 150px; display: block; margin-bottom: 40px;">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-family: 'F37Ginger'; font-size: 14px; color: #7b868c;">
                                                                                        <div>
                                                                                            <p class="text" style=" font-family: 'F37Ginger'; color: #7b868c; text-align: justify; -webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 50px; -moz-column-gap: 50px; column-gap: 50px; margin-bottom: 0;">
                                                                                                @yield('content.main')                                                                                            </p>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div>
                                                    <table class="mediaquery" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; padding-top: 60px">
                                                        <tbody>
                                                        <tr>
                                                            <td class="mediaquery" align="center">
                                                                <img src="{{ asset('storage/assets/mail/images/informazioni.jpg') }}" style="max-width: 100%; display: block;">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" valign="top" align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                <div>
                                                    <table width="100%" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; background-color: #ffffff; margin-top: 60px;">
                                                        <tbody width="100%">
                                                        <tr>
                                                            <td>
                                                                <table class="mediaquery" align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; text-align:center;">
                                                                    <tbody width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0;">
                                                                                <tbody width="100%">
                                                                                <tr>
                                                                                    <td align="center" style="padding: 30px; height:40px;">
                                                                                        <a href="" target="_blank" style="display: inline-block; margin: 0 40px; background-color: #7b868c; border-radius: 100%;" target="_blank">
                                                                                            <img src="https://ui.benchmarkemail.com/images/editor/socialicons/tw_btn.png" alt="Twitter" style="display: block;" border="0" width="40" height="40">
                                                                                        </a>
                                                                                        <a href="" target="_blank" style="display: inline-block; margin: 0 40px; background-color: #7b868c; border-radius: 100%;" target="_blank">
                                                                                            <img src="https://ui.benchmarkemail.com/images/editor/socialicons/fb_btn.png" alt="Facebook" style="display: block;" border="0" width="40" height="40">
                                                                                        </a>
                                                                                        <a href="" target="_blank" style="display: inline-block; margin: 0 40px; background-color: #7b868c; border-radius: 100%;" target="_blank">
                                                                                            <img src="https://ui.benchmarkemail.com/images/editor/socialicons/gp_btn.png" alt="Google Plus" style="display: block;" border="0" width="40" height="40">
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table width="100%" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; background-color: #ffffff; border-top: 1px solid #eee">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <table class="mediaquery" align="center" style="border: none; border-collapse: unset; padding: 0; margin: 0; border-spacing: 0; padding-bottom: 60px; padding-top: 60px;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="width: 50%">
                                                                            @yield('content.footer')
                                                                        </td>
                                                                        <td style="width: 50%">
                                                                            <span class="text" style="text-align: center;  display: block; font-family: 'F37Ginger'; color: #c1c5c8;">Già supportati da:</span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>