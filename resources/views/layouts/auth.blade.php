
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Buustle</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ asset('storage/images/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{ asset('storage/images/favicon-16x16.png') }}">

    <link rel="stylesheet" href="{{ asset('storage/assets/css/dist/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/css/dist/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('storage/assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('storage/assets/css/style.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>
<body class="bg-dark">
<div class="sufee-login d-flex align-content-center flex-wrap">

    @yield('content')

</div>
<script src="{{ asset('storage/assets/js/jQuery.min.js') }}"></script>
<script src="{{ asset('storage/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('storage/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('storage/assets/js/jquery.matchHeight.min.js') }}"></script>
<script src="{{ asset('storage/assets/js/main.js') }}"></script>
<script src="{{ asset('storage/assets/js/sweetalert.min.js') }}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>
</html>
