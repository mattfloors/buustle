<?php

return [

    /*
     * If a translation has not been set for a given locale, use this locale instead.
     */
    'fallback_locale' => 'it',

    /*
     * Array of possible locales
     */
    'app_locales' => ['it', 'en'],
];
