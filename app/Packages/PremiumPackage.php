<?php

namespace App\Packages;

use App\Http\Services\RentalPrice;

class PremiumPackage extends AbstractPackage
{

    /**
     * @return float
     */
    public function calculatePartialPrice(): float
    {
        $rentalPrice = new RentalPrice();

        $fromPeopleRange = $rentalPrice->calculatePartialPrice( $this->getPeopleRange(), $this->km, 0 );

        return $fromPeopleRange * $this->multiplier;
    }

    public function calculatePrice(): float
    {
        return 150.00;
    }

}