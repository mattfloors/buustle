<?php

namespace App\Packages;

use App\Models\PeopleRange;

abstract class AbstractPackage
{

    /** @var int $people */
    protected $people;

    /** @var float $km */
    protected $km;

    /** @var string $startDate */
    protected $startDate;

    /** @var string $endDate */
    protected $endDate;

    /** @var PeopleRange $peopleRange */
    protected $peopleRange;

    /** @var float $multiplier */
    protected $multiplier = 1.00;

    /**
     * Calculates the price based on people and kms
     * @return float
     */
    abstract public function calculatePrice() :float;

    /**
     * Calculates the price based on kms and peopleRange
     * @return float
     */
    abstract public function calculatePartialPrice() :float;

    /**
     * @param int $people
     * @return AbstractPackage
     */
    public function setPeople( int $people ) :self
    {

        $this->people = $people;

        return $this;

    }

    /**
     * @param float $km
     * @return AbstractPackage
     */
    public function setKm( float $km ) :self
    {

        $this->km = $km;

        return $this;

    }

    /**
     * @return int
     */
    public function getPeople(): int
    {
        return $this->people;
    }

    /**
     * @return float
     */
    public function getKm(): float
    {
        return $this->km;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return AbstractPackage
     */
    public function setStartDate(string $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return AbstractPackage
     */
    public function setEndDate(string $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return PeopleRange
     */
    public function getPeopleRange(): PeopleRange
    {
        return $this->peopleRange;
    }

    /**
     * @param PeopleRange $peopleRange
     * @return AbstractPackage
     */
    public function setPeopleRange(PeopleRange $peopleRange): self
    {
        $this->peopleRange = $peopleRange;

        return $this;
    }

    /**
     * @return float
     */
    public function getMultiplier(): float
    {
        return $this->multiplier;
    }

    /**
     * @param float $multiplier
     * @return AbstractPackage
     */
    public function setMultiplier(float $multiplier): self
    {
        $this->multiplier = $multiplier;

        return $this;
    }

}