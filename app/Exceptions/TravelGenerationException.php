<?php

namespace App\Exceptions;

use Exception;

class TravelGenerationException extends Exception
{
    public function report()
    {
        \Log::debug('Errore nella generazione del viaggio!');
    }
}
