<?php

namespace App\Exceptions;

use Exception;

class TravelPriceCalculationException extends Exception
{
    public function report()
    {
        \Log::debug('Errore nel calcolo prezzo');
    }
}
