<?php

namespace App\Console\Commands;

use App\Models\RentRequest;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PruneRentRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rentRequest:prune';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $hoursMin = setting('general.draft_expire') ?? 24;

        $rentRequests = RentRequest::where('status', '=', 'draft')->where('updated_at', '<=', Carbon::now()->subHours($hoursMin)->toDateTimeString())->get();

        $this->info('Found ' . $rentRequests->count() . ' Rent Requests to Prune');

        if ($rentRequests->count() > 0) {

            $bar = $this->output->createProgressBar($rentRequests->count());

            $bar->start();

            foreach ($rentRequests as $rentRequest) {

                $rentRequest->delete();

                usleep(25000);
                $bar->advance();

            }

        }

        $this->info('Finished!');

    }
}
