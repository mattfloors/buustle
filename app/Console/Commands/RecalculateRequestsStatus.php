<?php

namespace App\Console\Commands;

use App\Models\Quote;
use App\Models\RentRequest;
use Illuminate\Console\Command;

class RecalculateRequestsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rentRequest:recalculate
                            {--with-quotes : also recalculate status of all quotes before the rent requests}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate status of all rent requests, may take a while';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $withQuotes = $this->option('with-quotes');

        $rentRequestQuery = RentRequest::all();
        $rentRequestCount = RentRequest::count();

        $this->line('Found ' . $rentRequestCount . ' Rent Requests to realign');

        if ($withQuotes) {

            $quotesQuery = Quote::all();
            $quotesCount = Quote::count();

            $this->line('Found ' . $quotesCount . ' Quotes to realign');

            $this->info('Working on quotes...');

            if ($quotesCount > 0) {

                $bar = $this->output->createProgressBar($quotesCount);

                $bar->start();

                foreach ($quotesQuery as $quote) {

                    $this->callSilent('quote:setStatus', [
                        'quoteId' => $quote->id
                    ]);

                    usleep(250000);
                    $bar->advance();

                }

                $bar->finish();
                $this->line('');

            } else {

                $this->line('No quotes to work on');

            }

            $this->info('Finished working on quotes');

        }

        $this->info('Working on Rent Requests now...');

        if ($rentRequestCount > 0) {

            $bar = $this->output->createProgressBar($rentRequestCount);

            $bar->start();

            foreach ($rentRequestQuery as $rentRequest) {

                $this->callSilent('rentRequest:setStatus', [
                    'rentRequestId' => $rentRequest->id
                ]);

                usleep(250000);
                $bar->advance();

            }

            $bar->finish();
            $this->line('');

        } else {

            $this->info('No rent Requests to work on');

        }

        $this->info('Done!');
    }
}
