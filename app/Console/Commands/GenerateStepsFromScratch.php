<?php

namespace App\Console\Commands;

use App\Models\Quote;
use App\Models\QuoteMarker;
use App\Models\SoldMarker;
use Exception;
use Illuminate\Console\Command;

class GenerateStepsFromScratch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'steps:generate
                            {quoteId : The id of the quote}
                            {slots : The number of total vehicle slots}
                            {--force : Force the regeneration even if already generated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate steps from quote';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quoteId = $this->argument('quoteId');
        $force = $this->option('force');
        $slots = (int)$this->argument('slots');

        try {

            if ($slots < 1) {

                throw new Exception('Slots < 1');

            }

            $quote = Quote::where('status', '=', 'payed')->where('id', '=', $quoteId)->firstOrFail();
            $this->info('Quote id ' . $quoteId . ' Found');

            if ($quote->boughtSteps() > 0 && !$force) {

                throw new Exception('Steps have been already sold for some of the steps');

            }

            $quoteMarkers = QuoteMarker::where('quote_id', '=', $quoteId)->get();
            $this->info('Found ' . count($quoteMarkers) . ' quote markers');

            if (!empty($quoteMarkers)) {

                $this->info('Cycling through all the markers...');

                foreach ($quoteMarkers as $qm) {

                    $qm->slots = $slots - $quote->people;
                    $qm->estimated_km = $qm->calculateKm();
                    $qm->estimated_time = $qm->calculateTime();
                    $qm->save();

                    $this->line('... quote marker # ' . $qm->id . ' has had its free slots set to ' . $qm->slots . ' ...');

                    SoldMarker::where('quote_marker_id', '=', $qm->id)->delete();

                }

            }

            $this->info('done!');

        } catch (Exception $e) {

            $this->error($e->getMessage());

        }
    }
}
