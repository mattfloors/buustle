<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Quote;
use Exception;
use Illuminate\Console\Command;

class SetQuoteStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quote:setStatus
                            {quoteId : Id of the quote}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the status of the quote based on the payment';

    protected $orderStatuses = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderStatuses = [
            Order::PAYMENT_CREATED => [
                'value' => 0,
                'to' => Quote::STATUS_COMPLETED
            ],
            Order::PAYMENT_CANCELLED => [
                'value' => 1,
                'to' => Quote::STATUS_ACCEPTED
            ],
            Order::PAYMENT_PENDING => [
                'value' => 1,
                'to' => Quote::STATUS_ACCEPTED
            ],
            Order::PAYMENT_COMPLETED => [
                'value' => 2,
                'to' => Quote::STATUS_PAYED
            ],
            Order::PAYMENT_FAILED => [
                'value' => 1,
                'to' => Quote::STATUS_ACCEPTED
            ]
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quoteId = $this->argument('quoteId');

        try {

            $this->info('Working on quote id ' . $quoteId);

            $quote = Quote::findOrFail($quoteId);

            if (empty ($quote->orders)) {

                $this->info('No orders found, exiting');

                return true;

            }

            $lastOrderStatus = Order::PAYMENT_CREATED;

            foreach ($quote->orders as $order) {

                if ($this->orderStatuses[$order->payment_status]['value'] > $this->orderStatuses[$lastOrderStatus]['value']) {

                    $this->info('Nuovo status ordine ' . $order->payment_status . ' maggiore di ' . $lastOrderStatus);

                    $lastOrderStatus = $order->payment_status;

                }

            }

            $quoteStatus = $this->orderStatuses[$lastOrderStatus]['to'];
            $this->info('Nuovo status Preventivo: ' . $quoteStatus);

            if ($quote->setNewStatus($quoteStatus)) {

                $this->info('New status found and set as ' . $quoteStatus . ' for the quote');

            } else {

                $this->info('Status already valid for quote, unchanged');

            }

        } catch (Exception $e) {

            $this->error($e->getMessage());

        }
    }
}
