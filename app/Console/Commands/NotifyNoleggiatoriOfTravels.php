<?php

namespace App\Console\Commands;

use App\Models\Travel;
use App\Notifications\Noleggiatori\Travels\NoleggiatoriTravelInRegion;
use App\User;
use Illuminate\Console\Command;

class NotifyNoleggiatoriOfTravels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'travels:notifyNoleggiatori
                           {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notifications for travels to retailers if needed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->line('Starting scheduled notification for Retailers');

        if( $this->option('force') ) {

            $this->info('Forced activated, it will take some times...');

        }

        $this->line('[1] => Loading all the elegible travels');

        $travels = $this->loadTravels();

        if( count($travels) > 0 ) {

            $this->line('Found ' . count($travels) . ' travels');

            foreach( $travels as $travel ) {

                $this->line('Working on travel #' . $travel->id);

                if ( $this->sendForTravel( $travel ) ) {

                    $this->line( 'Travel Notifications sent correctly' );

                    $travel->notified = true;

                    $travel->save();

                } else {

                    $this->warn('Travel encountered an error on notifications');

                }

                usleep(250000);

            }

        } else {

            $this->info('No travels found to notify');

        }

        $this->info('Done!');

    }

    protected function loadTravels()
    {

        $force = $this->option('force');

        $query = Travel::whereNull('retailer_id')->where('active', '=', true)->where('status', '=', 'accepted');

        if ( !$force ) {

            $query->where('notified', '=', false);

        }

        return $query->get();

    }

    protected function sendForTravel( Travel $travel ) :bool
    {

        $listOfRegionIds = [];

        if ( $travel->steps->count() > 0 ) {

            foreach( $travel->steps as $step ) {

                if ( $step->startMarker->region_id === null ) {

                    $step->startMarker->recalculateRegion();

                }

                if ( $step->endMarker->region_id === null ) {

                    $step->endMarker->recalculateRegion();

                }

                if ( !in_array( (int) $step->startMarker->region_id, $listOfRegionIds, true ) ) {

                    $listOfRegionIds[] = $step->startMarker->region_id;

                }

                if ( !in_array( (int) $step->endMarker->region_id, $listOfRegionIds, true ) ) {

                    $listOfRegionIds[] = $step->endMarker->region_id;

                }

            }

        } else {

            $this->error('Travel has no steps!');

            return false;

        }

        $usersToNotify = [];

        if ( count( $listOfRegionIds ) > 0 ) {

            $this->line('Sending notifications for regions: ' . implode(',', $listOfRegionIds));

            $usersToNotify = User::select(['users.id', 'users.email'])->role(config('permission.retailer_group'))
                ->join('noleggiatore_region', function ($join) use ($listOfRegionIds) {
                    $join->on('users.id', '=', 'noleggiatore_region.noleggiatore_id')
                        ->whereIn('noleggiatore_region.region_id', $listOfRegionIds);
                })
                ->groupBy(['users.id', 'users.email'])
                ->get();
        } else {

            $this->error('Travel has no regions!');

            return false;

        }

        if ( count( $usersToNotify ) > 0 ) {

            $this->line('Found ' . count( $usersToNotify ) . ' users to notify');

            foreach( $usersToNotify as $user ) {

                $user->notify(new NoleggiatoriTravelInRegion( $travel ) );

            }

        } else {

            $this->warn('Travel has no elegible retailers to notify');

            return false;

        }

        return true;

    }
}
