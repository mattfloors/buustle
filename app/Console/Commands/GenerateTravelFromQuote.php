<?php

namespace App\Console\Commands;

use App\Exceptions\TravelGenerationException;
use App\Models\Quote;
use App\Models\Travel;
use App\Models\TravelInfos;
use App\Models\TravelPrice;
use App\Models\TravelStep;
use Exception;
use Illuminate\Console\Command;

class GenerateTravelFromQuote extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'travel:generate
                            {quoteId : The id of the quote}
                            {status  : The travel status [draft, completed, accepted]}
                            {price?  : The price to set up for each step}
                            {--force : Force the regeneration even if already generated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the travel from a quote';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function handle() :int
    {
        $quoteId = $this->argument('quoteId');
        $force = $this->option('force');
        $status = $this->argument('status');
        $price = $this->argument('price') ? (float)$this->argument('price') : null;

        try {

            $this->line('Creating travel for quote # ' . $quoteId);
            $quote = Quote::findOrFail($quoteId);

            $this->line('quote found, sku: ' . $quote->sku);

            if ($quote->status !== Quote::STATUS_PAYED && !$force) {

                throw new TravelGenerationException('Lo status del viaggio non è \'' . Quote::STATUS_PAYED . '\' riprovare con --force');

            }

            if ($quote->travels->count() > 0 && !$force) {

                throw new TravelGenerationException('Il viaggio per il preventivo è già presente, per rigenerarlo usare --force');

            }

            if ( $quote->markers->count() === 0 ) {

                throw new TravelGenerationException('Il preventivo non ha step di viaggio, impossibile proseguire');

            }

            if ( $quote->travels->count() > 0 ) {

                foreach ( $quote->travels as $t ) {

                    if ( $t->boughtSteps() > 0 ) {

                        throw new TravelGenerationException('Il preventivo ha già dei posti venduti, impossibile rigenerare');

                    }

                    TravelStep::where('travel_id', '=', $t->id)->delete();

                }

                Travel::where('quote_id', '=', $quote->id)->delete();

            }

            if (!in_array($status, Travel::s(), true)) {

                throw new TravelGenerationException('Lo status del viaggio ' . $status . ' non è valido');

            }

            $maxPeople = (int) setting('general.max_per_bus');

            $thisPeopleMax = ($quote->people > $maxPeople) ? $maxPeople : $quote->people;

            $rounds = ceil( $quote->people / $thisPeopleMax );

            $this->line('Veicoli Necessari: ' . $rounds);

            for($i = 1; $i <= $rounds; $i++) {

                $busPeople = ($i === (int) $rounds) ? ( $quote->people - ( ($i - 1) * $thisPeopleMax ) ) : $thisPeopleMax;

                if ( $busPeople < (int) setting('general.min_per_bus') ) {

                    $busPeople = (int) setting('general.min_per_bus');

                }

                $budget = round( ($busPeople / $quote->people) * $quote->budget );

                $this->info('Inserisco per veicolo da ' . $busPeople . ' persone con budget ' . $budget . ' €');

                $travel = new Travel();

                $travel->quote_id = $quote->id;
                $travel->owner_id = null;
                $travel->retailer_id = $quote->retailer_id;
                $travel->description = null;
                $travel->status = $status;
                $travel->active = false;
                $travel->shared = (bool) $quote->request->sharable;
                $travel->retailer_status = Travel::RETAILER_STATUS_ACCEPTED;
                $travel->budget = $budget;
                $travel->notified = false;
                $travel->package_id = $quote->package_id;
                $travel->travel_type = $quote->travel_type;
                $travel->people = $quote->people;
                $travel->save();

                $this->info('Viaggio salvato correttamente, ora preparo gli step...');

                TravelStep::where('travel_id', '=', $travel->id)->delete();

                $this->info('Step preparati, controllo e inserisco...');

                foreach ($quote->markers as $marker) {

                    $slots = 0;

                    if ($marker->slots >= 0) {

                        $slots = $marker->slots;

                    } elseif ($quote->people > 0 && $quote->bus_seats > 0) {

                        $slots = $quote->bus_seats - $quote->people;

                    }

                    if ($slots < 0) {

                        $this->warn('Posti disponibili per una tratta minori di 0 (' . $slots . '), setto a 0');

                        $slots = 0;

                    }

                    $travelStep = new TravelStep();

                    $travelStep->travel_id = $travel->id;
                    $travelStep->start_time = $marker->start_time;
                    $travelStep->start_marker_id = $marker->marker_id;
                    $travelStep->end_time = $marker->endTime();
                    $travelStep->end_marker_id = $marker->end_marker_id;
                    $travelStep->slots = $slots;
                    $travelStep->distribusion_code = $marker->distribusion_code;
                    $travelStep->estimated_km = $marker->estimated_km;
                    $travelStep->estimated_time = $marker->estimated_time;
                    $travelStep->price = ($price && $price > 0) ? $price : $travelStep->calculateAutoPrice();
                    $travelStep->save();

                    if ( $travelStep->endMarker->region_id === null ) {

                        $travelStep->endMarker->recalculateRegion();

                    }

                    if ( $travelStep->startMarker->region_id === null ) {

                        $travelStep->startMarker->recalculateRegion();

                    }

                }

                $this->info('Step salvati correttamente, passo ai servizi...');

                $services = [];

                if ( $quote->services->count() > 0 ) {

                    foreach ($quote->services as $service) {

                        $services[] = $service->id;

                    }

                }

                $travel->services()->sync($services);
                $this->info('Synced ' . count($services) . ' servizi');

                $this->info('Working on infos...');
                if ( $quote->infos->count() > 0 ) {

                    TravelInfos::where('travel_id', '=', $travel->id)->delete();

                    foreach( $quote->infos as $info ) {

                        $ti = new TravelInfos();
                        $ti->travel_id = $travel->id;
                        $ti->infos = $info->infos;
                        $ti->included = $info->included;

                        $ti->save();

                    }

                }
                $this->info('Added ' . $quote->infos->count() . ' infos');

                $this->info('Working on prices...');
                if ( $quote->prices->count() > 0 ) {

                    TravelPrice::where('travel_id', '=', $travel->id)->delete();

                    foreach( $quote->prices as $price ) {

                        $ti = new TravelPrice();
                        $ti->travel_id = $travel->id;
                        $ti->reason = $price->reason;
                        $ti->price = $price->price;

                        $ti->save();

                    }

                }
                $this->info('Added ' . $quote->prices->count() . ' prices');

                $this->info('Travel Added');

            }

            $this->info('DONE!');

        } catch (TravelGenerationException $e) {

            $this->error('[Travel Generation Error] ' . $e->getMessage());

            if ( $travel && $travel->id ) {

                $travel->delete();

            }

            return 2;

        } catch (Exception $e) {

            $this->error('[Generic Error] ' . $e->getMessage());

            return 1;
        }

        return 0;
    }
}
