<?php

namespace App\Console\Commands;

use App\Models\Quote;
use App\Models\RentRequest;
use Illuminate\Console\Command;

class SetRequestStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rentRequest:setStatus 
                            {rentRequestId : The id of the rentRequest}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets the rent request status based on quotes and payment';

    protected $statusesConversion = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->statusesConversion = [
            Quote::STATUS_DRAFT => [
                'value' => 0,
                'to' => RentRequest::STATUS_COMPLETED
            ],
            Quote::STATUS_COMPLETED => [
                'value' => 1,
                'to' => RentRequest::STATUS_CLOSED
            ],
            Quote::STATUS_ACCEPTED => [
                'value' => 2,
                'to' => RentRequest::STATUS_CLOSED
            ],
            Quote::STATUS_PAYED => [
                'value' => 3,
                'to' => RentRequest::STATUS_SOLD
            ],
            Quote::STATUS_DELETED => [
                'value' => 0,
                'to' => RentRequest::STATUS_COMPLETED
            ]
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$rentRequest = RentRequest::with('quotes')->find($this->argument('rentRequestId'))) {

            $this->error('Rent Request ID: ' . $this->argument('rentRequestId') . ' Not found! => Aborted');

            return false;
        }

        $status = $rentRequest->status ?? RentRequest::STATUS_DRAFT;
        $maxQuoteStatus = Quote::STATUS_DRAFT;
        $rentRequestStatuses = $rentRequest->statuses();

        $this->info('Rent Request original status: ' . $status);

        if (!empty($rentRequest->quotes)) {

            $this->info('Found ' . count($rentRequest->quotes) . ' Quotes');

            foreach ($rentRequest->quotes as $quote) {

                if ($this->statusesConversion[$quote->status]['value'] > $this->statusesConversion[$maxQuoteStatus]['value']) {

                    $maxQuoteStatus = $quote->status;

                }

            }

            $this->info('Max quote status: ' . $maxQuoteStatus);

            if ($rentRequest->newRequestStatus($this->statusesConversion[$maxQuoteStatus]['to'])) {

                $this->info('Rent Request status changed to: ' . $this->statusesConversion[$maxQuoteStatus]['to']);

            } else {

                $this->info('Rent Request Status is unchanged');

            }

        }
    }
}
