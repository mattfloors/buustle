<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $cognome;
    public $telefono;
    public $email;
    public $messaggio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $nome, string $cognome, string $email, string $telefono, string $messaggio)
    {
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->messaggio = $messaggio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact-us', ['nome' => $this->nome, 'cognome' => $this->cognome, 'email' => $this->email, 'telefono' => $this->telefono, 'messaggio' => $this->messaggio]);
    }
}
