<?php

namespace App;

use App\Models\NoleggiatoriProfile;
use App\Models\Travel;
use App\Models\UserProfile;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Carbon\Carbon;
use Cmgmyr\Messenger\Traits\Messagable;
use Gabievi\Promocodes\Traits\Rewardable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use Rewardable;
    use HasManyNotes;
    use Messagable;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile'
    ];

    protected $listOfRegions = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'provider_name', 'provider_id', 'password', 'remember_token',
    ];

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getRetailers()
    {
        return self::role(config('permission.retailer_group'))->get();
    }

    /**
     * @return mixed
     */
    public static function getAdmins()
    {
        return self::role('Admin')->get();
    }

    /**
     * @return bool
     */
    public function isValidNoleggiatore() :bool
    {

        if ( empty( $this->noleggiatoreProfile ) || $this->noleggiatoreProfile->retailer_id === null || !$this->noleggiatoreProfile->retailer->active ) {

            return false;

        }

        return true;

    }

    /**
     * @param bool $owned
     * @return Collection
     */
    public function getPartnerRequests( bool $owned = false )
    {

        if ( empty( $this->regions ) || count ( $this->regions ) === 0 || !$this->can('see partner') ) {

            return collect();

        }

        if ( empty( $this->listOfRegions ) ) {

            foreach( $this->regions as $region ) {

                $this->listOfRegions[] = $region->id;

            }

        }

        $firstAvailableDate = Carbon::now()->addDays(setting('general.min_days_for_retailer_offer') ?? 30);

        $query = Travel::select('travels.*')
                         ->whereIn('travels.status', [Travel::STATUS_ACCEPTED, Travel::STATUS_COMPLETED])
                         ->join('travel_steps', function($join) use ($firstAvailableDate) {
                             $join->on('travels.id', '=', 'travel_steps.travel_id')
                                  ->where('travel_steps.start_time', '>', $firstAvailableDate);
                         })
                         ->join('markers as startMarker', 'travel_steps.start_marker_id', '=', 'startMarker.id')
                         ->join('markers as endMarker', 'travel_steps.end_marker_id', '=', 'endMarker.id');

        if( !empty( $this->listOfRegions ) ) {
            $regions = $this->listOfRegions;
            $query->where(function($w) use ($regions) {
                $w->whereIn('startMarker.region_id', $regions)
                    ->orWhereIn('endMarker.region_id', $regions);
            });

        }

        if ( !$owned ) {

            $query->whereNull('travels.retailer_id');
            $query->where('travels.active', '=', 1);

        } else {

            $query->where('travels.retailer_id', '=', $this->noleggiatoreProfile->retailer->id);

        }

        $travels = $query->distinct()
                        ->orderBy('travel_steps.start_time', 'DESC')
                        ->get();

        return $travels;
    }

    /**
     * @param $value
     * @return UserProfile|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|object|null
     */
    public function getProfileAttribute($value)
    {
        if (!empty($this->profile()->first())) {
            return $this->profile()->first();
        }

        $profile = new UserProfile();

        if ($this->id !== null) {
            $profile->user_id = $this->id;

            $profile->save();

            $this->attributes['profile'] = $profile;
        }


        return $profile;
    }

    /**
     * @param $value
     * @return NoleggiatoriProfile|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|object|null
     */
    public function getNoleggiatoreProfileAttribute($value)
    {
        if (!empty($this->noleggiatoreProfile()->first())) {
            return $this->noleggiatoreProfile()->first();
        }

        $profile = new NoleggiatoriProfile();

        if ($this->id !== null) {
            $profile->user_id = $this->id;

            $profile->save();

            $this->attributes['noleggiatoreProfile'] = $profile;
        }


        return $profile;
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile();
    }

    /**
     * @return HasOne
     */
    public function profile() :HasOne
    {
        return $this->hasOne('App\Models\UserProfile');
    }

    /**
     * @return HasOne
     */
    public function noleggiatoreProfile() :HasOne
    {
        return $this->hasOne('App\Models\NoleggiatoriProfile');
    }

    /**
     * @return BelongsToMany
     */
    public function regions() :BelongsToMany
    {
        return $this->belongsToMany('App\Models\Region', 'noleggiatore_region', 'noleggiatore_id');
    }
}
