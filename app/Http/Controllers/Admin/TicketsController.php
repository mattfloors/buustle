<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ticket;
use App\Models\TravelStep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TicketsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'tickets' );
        View::share( 'subMenuActive', '' );

    }

    public function list( Request $request )
    {

        View::share( 'openMenu', false );

        if( $request->input('travel_step_id') ) {

            $step = TravelStep::find($request->input('travel_step_id'));
            $tickets = Ticket::where('travel_step_id', '=', $request->input('travel_step_id') )->get();

        } else {

            $step = null;
            $tickets = Ticket::all();

        }

        return view('admin.tickets.list', ['tickets' => $tickets, 'step' => $step]);

    }

    public function view( Ticket $ticket )
    {

        return view('admin.tickets.view', ['ticket' => $ticket, 'step' => $ticket->step]);

    }

    public function create( TravelStep $travelStep )
    {

        return view('admin.tickets.create', ['ticket' => new Ticket(), 'step' => $travelStep]);

    }

    public function update( Ticket $ticket )
    {

        return view('admin.tickets.create', ['ticket' => $ticket, 'step' => $ticket->step]);

    }

    public function store( TravelStep $step, Request $request )
    {

        $validator = Validator::make($request->all(), [
            'id'        => ['sometimes', 'nullable', 'numeric', 'exists:tickets,id'],
            'slots'     => ['required', 'numeric', 'min:1'],
            'price'     => ['required', 'numeric', 'min:0'],
            'through'   => ['sometimes', 'string', 'max:64', 'nullable'],
            'reference' => ['sometimes', 'nullable'],
            'comment'   => ['sometimes', 'nullable', 'string', 'max:2048'],
            'owner'     => ['sometimes', 'nullable', 'string', 'max:256']
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {

            if ( !$ticket = Ticket::find($request->input('id')) ) {

                $ticket = new Ticket();

            }

            $ticket->travel_step_id = $step->id;
            $ticket->through        = $request->input('through');
            $ticket->reference      = $request->input('reference');
            $ticket->slots          = $request->input('slots');
            $ticket->price          = $request->input('price');
            $ticket->comment        = $request->input('comment');
            $ticket->owner          = $request->input('owner');

            $ticket->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.tickets.update', ['ticket' => $ticket])->with('success', 'Biglietto / i salvato / i correttamente');

    }

    public function check ( string $ticketUuid )
    {

        if ( !$ticket = Ticket::where('uuid', '=', $ticketUuid)->first() ) {

            abort(404);

        }

        return redirect()->route('admin.tickets.view', ['ticket' => $ticket]);

    }
}
