<?php

namespace App\Http\Controllers\Admin;

use App\Events\QuoteCompleted;
use App\Http\Requests\StoreQuote;
use App\Models\Package;
use App\Models\Quote;
use App\Models\QuoteInfos;
use App\Models\QuoteMarker;
use App\Models\QuotePrice;
use App\Models\RentRequest;
use App\Models\Retailer;
use App\Models\Service;
use Illuminate\Support\Facades\View;

class QuotesController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'rentRequests' );
        View::share( 'subMenuActive', '' );

    }

    public function listFor( RentRequest $rentRequest )
    {

        $quotes = Quote::where('request_id', '=', $rentRequest->id)->get();

        return view('admin.quotes.list', ['rentRequest' => $rentRequest, 'quotes' => $quotes]);

    }

    public function create( RentRequest $rentRequest )
    {

        $packages = Package::all();
        $retailers = Retailer::all();
        $quote = new Quote();
        $services = Service::all();

        return view('admin.quotes.create', ['services' => $services, 'retailers' => $retailers, 'packages' => $packages, 'rentRequest' => $rentRequest, 'quote' => $quote]);

    }

    public function edit( RentRequest $rentRequest, Quote $quote )
    {

        $packages = Package::all();
        $retailers = Retailer::all();
        $services = Service::all();

        return view('admin.quotes.create', ['services' => $services, 'retailers' => $retailers, 'packages' => $packages, 'rentRequest' => $rentRequest, 'quote' => $quote]);

    }

    public function store( RentRequest $rentRequest, StoreQuote $request )
    {

        try {

            $quote = Quote::findOrNew($request->input('id'));

            $quote->user_id = $rentRequest->user_id;
            $quote->request_id = $rentRequest->id;
            $quote->price = $request->input('price');
            $quote->status = $request->input('status');
            $quote->people = $request->input('people');
            $quote->comment = $request->input('comment');
            $quote->valid_until = $request->input('valid_until');
            $quote->retailer_price = $request->input('retailer_price');
            $quote->retailer_id = $request->input('retailer');
            $quote->sku = $request->input('sku');
            $quote->bus_seats = $request->input('bus_seats') ? $request->input('bus_seats') : $request->input('people');
            $quote->package_id = $request->input('package_id');
            $quote->budget = $request->input('budget');

            $quote->save();

            QuoteInfos::where('quote_id', '=', $quote->id)->delete();

            if( !empty( $request->input('info-content') ) ) {

                foreach ( $request->input('info-content') as $key => $info ) {

                    $included = false;

                    if ($request->input('info-included') && is_array($request->input('info-included'))) {

                        $included = array_key_exists($key, $request->input('info-included')) ? true : false;

                    }
                    $quoteInfo = new QuoteInfos();
                    $quoteInfo->quote_id = $quote->id;
                    $quoteInfo->included = $included;
                    $quoteInfo->infos = $info;
                    $quoteInfo->save();

                }

            }

            QuotePrice::where('quote_id', '=', $quote->id)->delete();

            if( !empty( $request->input('price-price') ) ) {

                foreach ( $request->input('price-price') as $key => $info ) {

                    $reason = '';

                    if ( $request->input('price-reason') && is_array($request->input('price-reason')) && array_key_exists($key, $request->input('price-reason')) ) {

                        $reason = $request->input('price-reason')[$key];

                    }

                    $quotePrice = new QuotePrice();
                    $quotePrice->quote_id = $quote->id;
                    $quotePrice->reason = $reason;
                    $quotePrice->price = $info;
                    $quotePrice->save();

                    $quote->price += $info;

                }

                $quote->save();

            }

            if ( !empty( $request->input('start_marker') ) ) {

                $quote->markers()->delete();

                foreach( $request->input('start_marker') as $key => $markerId ) {

                    if ( array_key_exists( $key, $request->input('time') ) ) {

                        $markerToAdd = new QuoteMarker();
                        $markerToAdd->quote_id = $quote->id;
                        $markerToAdd->marker_id = $markerId;
                        $markerToAdd->start_time = $request->input('time')[$key];
                        $markerToAdd->end_marker_id = $request->input('end_marker')[$key];

                        $markerToAdd->save();

                    }

                }

            }

            $quote->services()->sync( $request->input('services') ?? [] );

            $quote->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        try {

            $quote->createPdf();

            event( new QuoteCompleted($quote) );

        } catch (\Exception $e) {

            return redirect()->back()->with('warning', 'Preventivo salvato ma pdf non generato: ' . $e->getMessage());

        }

        return redirect()->route('admin.quotes.listFor', ['rentRequest' => $rentRequest])->with('success', 'Preventivo Salvato');

    }

    public function pdf( RentRequest $rentRequest, Quote $quote )
    {

        $pdf = $quote->getInvoicePdf();

        return response()->download($pdf->getPath(), $pdf->file_name);

    }
}
