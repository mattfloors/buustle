<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\RentalPrice;
use App\Models\Algorithm;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

class AlgorithmController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travel_settings' );
        View::share( 'subMenuActive', 'travel_alg' );

    }

    public function index()
    {

        $commissione = Algorithm::commissione();
        $tariffa = Algorithm::tariffa();
        $finale = Algorithm::finale();
        $predizione = Algorithm::predizione();

        return view('admin.algorithm.index', ['previsione' => $predizione, 'commissione' => $commissione, 'tariffa' => $tariffa, 'finale' => $finale]);

    }

    public function test( Request $request )
    {

        try {

            Validator::make($request->all(), [
                RentalPrice::KM_TRATTA => ['required', 'numeric', 'min:1'],
                RentalPrice::DAYS_OF_TRAVEL => ['required', 'numeric', 'min:0.1'],
                RentalPrice::DAYS_FROM_START => ['required', 'numeric', 'min:1'],
                RentalPrice::PEOPLE => ['required', 'numeric', 'min:1']
            ])->validate();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore, mancano dei dati: ' . $e->getMessage());

        }

        try {

            $priceService = new RentalPrice();

            $priceService->setKm($request->input(RentalPrice::KM_TRATTA));

            $priceService->setDaysFromStart($request->input(RentalPrice::DAYS_FROM_START));

            if ( !$km_per_day = $request->input('km_per_day') ) {

                $km_per_day = $request->input('km_tratta') / $request->input('days_of_travel');

            }

            if ( !$commissione = Algorithm::commissione() ) {

                throw new \Exception('Manca il calcolo Commissione, impossibile proseguire');

            }

            $commissioneVal = $priceService->calculateCommissione();

            $commissioneVariables = $priceService->getCurrentVars();

            if ( !$tariffa = Algorithm::tariffa() ) {

                throw new \Exception('Manca il calcolo Tariffa, impossibile proseguire');

            }

            if ( !$priceService->setPeople( $request->input(RentalPrice::PEOPLE ) ) ) {

                throw new \Exception('Impossibile trovare una fascia di prezzo per ' . $request->input(RentalPrice::PEOPLE ) . ' persone');

            }

            $priceService->setDaysOfTravel($request->input(RentalPrice::DAYS_OF_TRAVEL));
            $priceService->setKmPerDay();

            $tariffaVariables = $priceService->getCurrentVars();
            $tariffaVal = $priceService->calculateTariffa();

            if ( !$predizione = Algorithm::predizione() ) {

                throw new \Exception('Manca il calcolo Predizione, impossibile proseguire');

            }

            $predizioneVariables = $priceService->getCurrentVars();
            $predizioneVal = $priceService->calculatePartialPrice();

            if ( !$finale = Algorithm::finale() ) {

                throw new \Exception('Manca il calcolo Prezzo Finale, impossibile proseguire');

            }

            $packages = Package::where('active', '=', 1)->get();
            $listOfPrices = [];

            if ( !empty( $packages ) ) {

                foreach ( $packages as $package ) {

                    $priceService->setPackage( $package );

                    $listOfPrices[] = [
                        'package' => $package,
                        'vars' => $priceService->getCurrentVars(),
                        'value' => $priceService->calculatePrice()
                    ];

                }

            }

            return view('admin.algorithm.test', [
                'km_tratta' => $request->input(RentalPrice::KM_TRATTA),
                'days_of_travel' => $request->input(RentalPrice::DAYS_OF_TRAVEL),
                'days_from_start' => $request->input(RentalPrice::DAYS_FROM_START),
                'km_per_day' => $km_per_day,
                'people' => $request->input(RentalPrice::PEOPLE),
                'commissioneVariables' => $commissioneVariables,
                'commissioneVal' => $commissioneVal,
                'commissione' => $commissione,
                'tariffaVal' => $tariffaVal,
                'tariffa' => $tariffa,
                'tariffaVariables' => $tariffaVariables,
                'finale' => $finale,
                'finalePrices' => $listOfPrices,
                'predizione' => $predizione,
                'predizioneVariables' => $predizioneVariables,
                'predizioneVal' => $predizioneVal
            ]);

        } catch ( \Exception $e ) {
dd($e->getMessage());
            return redirect()->back()->with( 'errors', $e->getMessage() );

        }

    }

    public function store( Request $request )
    {

        try {

            Validator::make( $request->all(), [
                'name'      => ['required', 'string', Rule::in( Algorithm::validNames() )],
                'algorithm' => ['required', 'string']
            ])->validate();

            if ( !$alg = Algorithm::where('name', '=', $request->input('name'))->first() ) {

                $alg = new Algorithm();
                $alg->name = $request->input('name');

            }

            $alg->algorithm = $request->input('algorithm');

            $alg->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Salvato Algoritmo Prezzo per ' . $request->input('name'));

    }
}
