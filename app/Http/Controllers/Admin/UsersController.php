<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Role;

class UsersController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'users' );
        View::share( 'subMenuActive', 'users' );

    }

    public function list()
    {

        View::share( 'openMenu', false );
        View::share( 'menuActive', '' );

        $users = User::all();

        return view('admin.users.list', ['users' => $users]);

    }

    public function create()
    {

        $roles = Role::all();

        return view('admin.users.create', ['user' => new User(), 'roles' => $roles]);

    }

    public function update( User $user )
    {

        $roles = Role::all();

        return view('admin.users.create', ['user' => $user, 'roles' => $roles]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'                    => 'nullable|numeric',
            'name'                  => 'nullable|string',
            'email'                 => 'required|email',
            'email_verified'        => 'nullable|numeric',
            'password'              => 'nullable|string|min:8|confirmed',
            'roles'                 => 'required|array',
            'roles.*'               => 'required|string',
            'first_name'            => 'sometimes|nullable|string',
            'last_name'             => 'sometimes|nullable|string',
            'phone'                 => 'sometimes|nullable|string'
        ]);

        $user = User::firstOrNew(['email' => $validatedData['email']]);

        $user->name           = $validatedData['name'] ?? '';
        $user->email          = $validatedData['email'];

        if ( $user->email_verified_at === null && array_key_exists('email_verified', $validatedData) && (bool) $validatedData['email_verified'] ) {

            $user->email_verified_at = Carbon::now();

        } elseif ( $user->email_verified_at !== null && (!array_key_exists('email_verified', $validatedData) || !(bool) $validatedData['email_verified'] ) ) {

            $user->email_verified_at = null;

        }

        if( $validatedData['password'] ) {

            $user->password = Hash::make($validatedData['password']);

        } elseif( $validatedData['id'] === null ) {

            return redirect()->back()->with('error', 'Se stai creando un nuovo utente la password è richiesta');

        }

        $user->save();

        if( in_array('Super Admin', $validatedData['roles'], false) && !Auth::user()->can('assign super admin') ) {

            return redirect()->back()->with('error', 'Non puoi assegnare il ruolo di Super Admin ad un Utente');

        }

        $user->syncRoles( [$validatedData['roles']] );

        $user->save();

        $profile = $user->profile;

        $profile->user_id    = $user->id;
        $profile->first_name = $validatedData['first_name'];
        $profile->last_name  = $validatedData['last_name'];
        $profile->phone      = $validatedData['phone'];

        $profile->save();

        return redirect()->route('admin.users.update', ['user' => $user])->with('success', 'L\'utente ' . $validatedData['email'] . ' è stato salvato con successo');

    }

    public function delete( User $user )
    {

        $user->delete();

        return redirect()->back()->with('success', 'Utente Eliminato');

    }
}
