<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\SendPromoCode;
use Gabievi\Promocodes\Facades\Promocodes;
use Gabievi\Promocodes\Models\Promocode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\View;

class PromocodesController extends AdminBaseController
{

    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'promocodes' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {

        $promocodes = Promocode::all();

        return view('admin.promocodes.list', ['promocodes' => $promocodes]);

    }

    public function create()
    {

        $promocode = new Promocode();

        return view('admin.promocodes.create', ['promocode' => $promocode]);

    }

    public function update( Promocode $promocode )
    {

        return view('admin.promocodes.create', ['promocode' => $promocode]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'         => 'nullable|numeric',
            'type'       => 'required|string',
            'emailed'    => 'nullable|email',
            'reward'     => 'required|numeric|min:0.1',
            'disposable' => 'nullable|boolean',
            'expires_at' => 'nullable|date'
        ]);

        try {

            $returnCode = '';
            $oldEmailed = '';

            if ( $validatedData['id'] === null ) {

                if( $validatedData['disposable'] ) {

                    $promocode = Promocodes::createDisposable(1, $validatedData['reward'], ['type' => $validatedData['type'], 'emailed' => $validatedData['emailed']], $validatedData['expires_at']);
                    $returnCode = $promocode[0]['code'];

                } else {

                    $promocode = Promocodes::create(1, $validatedData['reward'], ['type' => $validatedData['type'], 'emailed' => $validatedData['emailed']], $validatedData['expires_at']);
                    $returnCode = $promocode[0]['code'];

                }

            } else {

                $promocode = Promocode::findOrFail($validatedData['id']);

                $oldEmailed = $promocode->data['emailed'];

                $promocode->reward = $validatedData['reward'];
                $promocode->data = [
                    'type' => $validatedData['type'],
                    'emailed' => $validatedData['emailed']
                ];
                $promocode->expires_at = $validatedData['expires_at'];

                $promocode->save();

                $returnCode = $promocode->code;

            }

            if ( $validatedData['emailed'] !== null && $oldEmailed !== $validatedData['emailed'] ) {

                Notification::route('mail', $validatedData['emailed'])
                    ->notify(new SendPromoCode($returnCode));

            }

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.promocodes.list')->with('success', 'Codice Generato con Successo: ' . $returnCode);

    }

}
