<?php

namespace App\Http\Controllers\Admin;

use App\Models\PeopleRange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PeopleRangesController extends AdminBaseController
{

    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travel_settings' );
        View::share( 'subMenuActive', 'travel_people_ranges' );

    }

    public function list()
    {

        $ranges = PeopleRange::all();

        return view('admin.people_ranges.list', ['ranges' => $ranges]);

    }

    public function create()
    {
        return view('admin.people_ranges.create', ['range' => new PeopleRange()]);

    }

    public function edit( PeopleRange $range )
    {

        return view('admin.people_ranges.create', ['range' => $range]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'               => 'nullable|numeric',
            'min'              => 'required|numeric|min:0',
            'max'              => 'required|numeric|gte:min',
            'multiplier'       => 'required|numeric|min:0.01',
            'price_multiplier' => 'required|numeric|min:0.01',
            'coeff_predizione' => 'required|numeric|min:0'
        ]);

        if( empty($validatedData['id']) ) {

            $range = new PeopleRange();

        } else {

            $range = PeopleRange::findOrFail($validatedData['id']);

        }

        $range->min = $validatedData['min'];
        $range->max = $validatedData['max'];
        $range->multiplier = $validatedData['multiplier'];
        $range->price_multiplier = $validatedData['price_multiplier'];
        $range->coeff_predizione = $validatedData['coeff_predizione'];

        $range->save();

        return redirect()->route('admin.people_ranges.list')->with('success', 'Range persone salvato con successo');

    }

    public function delete( PeopleRange $range )
    {

        $range->delete();

        return redirect()->back()->with('success', 'Il Range è stato eliminato');

    }
}
