<?php

namespace App\Http\Controllers\Admin;

use App\Models\Region;
use App\Models\Retailer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NoleggiatoriController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'noleggiatori' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {

        View::share( 'openMenu', false );
        View::share( 'menuActive', '' );

        $users = User::getRetailers();

        return view('admin.noleggiatori.list', ['users' => $users]);

    }

    public function update( User $user )
    {

        $regions = Region::all();
        $retailers = Retailer::all();

        return view('admin.noleggiatori.create', ['user' => $user, 'regions' => $regions, 'retailers' => $retailers]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'                    => 'required|numeric|exists:users,id',
            'partita_iva'           => 'nullable|string',
            'paypal_account'        => 'nullable|string',
            'stripe_account'        => 'nullable|string',
            'regions'               => 'nullable|array',
            'regions.*'             => 'numeric|exists:regions,id',
            'retailer_id'           => 'required|numeric|exists:retailers,id'
        ]);

        try {

            $user = User::findOrFail( $validatedData['id'] );

            $profile = $user->noleggiatoreProfile;

            $profile->user_id        = $validatedData['id'];
            $profile->partita_iva    = $validatedData['partita_iva'];
            $profile->paypal_account = $validatedData['paypal_account'];
            $profile->stripe_account = $validatedData['stripe_account'];
            $profile->retailer_id    = $validatedData['retailer_id'];

            $profile->save();

            $user->regions()->sync($validatedData['regions']);

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Noleggiatore salvato con Successo');

    }

    public function create( User $user )
    {

        $user->assignRole(config('permission.retailer_group'));

        return redirect()->route('admin.noleggiatori.update', ['user' => $user])->with('success', 'Utente è ora ' . config('permission.retailer_group'));

    }

    public function delete( User $user )
    {

        $user->removeRole(config('permission.retailer_group'));

        return redirect()->back()->with('success', 'Rimosso status ' . config('permission.retailer_group') . ' dall\'utente');

    }
}
