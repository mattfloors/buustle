<?php
/**
 * Created by PhpStorm.
 * User: Алёна
 * Date: 07.01.2020
 * Time: 19:07
 */

namespace App\Http\Controllers\Admin;


use App\Models\Stops;
use View;
use Illuminate\Http\Request;

class StopsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'stops' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {

        View::share( 'openMenu', false );
        View::share( 'menuActive', '' );

        $stops = Stops::all();

        return view('admin.stops.list', ['stops' => $stops]);

    }

    public function create(){
        return view('admin.stops.create', ['stop' => new Stops()]);
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'city_name'               => 'required',
            'stop_name'              => 'required',
            'link'              => 'required',
        ]);

        $stop = new Stops();

        $stop->city_name = $validatedData['city_name'];
        $stop->stops_name = $validatedData['stop_name'];
        $stop->link = $validatedData['link'];

        $stop->save();

        return redirect()->route('admin.stops.list')->with('success', 'New stop was added successfully');    }

    public function delete( Stops $stop )
    {

        $stop->delete();

        return redirect()->back()->with('success', 'Stop was deleted successfully');

    }
}