<?php

namespace App\Http\Controllers\Admin;

use App\Models\RentRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RentRequestsController extends AdminBaseController
{

    /**
     * RentRequestsController constructor.
     */
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'rentRequests' );
        View::share( 'subMenuActive', '' );

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listNew()
    {

        View::share( 'subMenuActive', 'rent_requests_new' );

        $rentRequests = RentRequest::where('status', '=', 'completed')->get();

        return view('admin.rent-requests.list', ['rentRequests' => $rentRequests]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listCompleted()
    {

        View::share( 'subMenuActive', 'rent_requests_completed' );

        $rentRequests = RentRequest::where('status', '=', 'closed')->get();

        return view('admin.rent-requests.list-closed', ['rentRequests' => $rentRequests]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listSold()
    {

        View::share( 'subMenuActive', 'rent_requests_sold' );

        $rentRequests = RentRequest::where('status', '=', 'sold')->get();

        return view('admin.rent-requests.list-sold', ['rentRequests' => $rentRequests]);

    }

    /**
     * @param RentRequest $rentRequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view( RentRequest $rentRequest )
    {

        return view('admin.rent-requests.view', ['rentRequest' => $rentRequest]);

    }

    /**
     * @param RentRequest $rentRequest
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus( RentRequest $rentRequest, Request $request ) :\Illuminate\Http\RedirectResponse
    {

        try {

            $rentRequest->newRequestStatus( $request->input('status') );

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', 'Impossibile modificare lo status: ' . $e->getMessage());

        }

        return redirect()->back()->with('success', 'Status cambiato a ' . ucfirst($rentRequest->status));

    }
}
