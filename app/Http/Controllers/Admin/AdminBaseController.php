<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RentRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

Class AdminBaseController extends Controller
{

    protected $notifications;

    public function __construct() {

        View::share( 'openMenu', true );
        View::share( 'menuActive', 'dashboard' );
        View::share( 'subMenuActive', '' );

        $requestsToEvade = RentRequest::where('status', '=', 'completed')->count();
        View::share( 'requestsToEvade', $requestsToEvade );

        $requestsWithOffers = RentRequest::where('status', '=', 'closed')->count();
        View::share( 'requestsWithOffers', $requestsWithOffers );

        $requestsSold = RentRequest::where('status', '=', 'sold')->count();
        View::share( 'requestsSold', $requestsSold );

        $config = array(
            'driver'     => setting('email.driver', 'smtp'),
            'host'       => setting('email.host', 'smtp.mailgun.org'),
            'port'       => setting('email.port', 587),
            'from'       => array('address' => setting('general.sender_email', 'hello@example.com'), 'name' => setting('general.sender_name', 'Example')),
            'encryption' => setting('email.encryption', 'tls'),
            'username'   => setting('email.username', ''),
            'password'   => setting('email.password', ''),
        );
        Config::set('mail', $config);

    }

}