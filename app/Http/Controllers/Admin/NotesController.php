<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class NotesController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'    => 'required|numeric',
            'model' => 'required|string',
            'text'  => 'required|string|min:1'
        ]);

        try {

            $singleModel = $validatedData['model']::findOrFail($validatedData['id']);

            $singleModel->createNote($validatedData['text']);

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore durante il salvataggio della nota: ' . $e->getMessage());

        }

        return redirect()->back()->with('success', 'Nota salvata con successo');

    }
}
