<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SettingsController extends AdminBaseController
{

    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'configurations' );
        View::share( 'subMenuActive', '' );

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listPayments()
    {

        View::share( 'subMenuActive', 'payments' );

        return view('admin.configurations.payments');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listDistribusion()
    {

        View::share( 'subMenuActive', 'distribusion' );

        return view('admin.configurations.distribusion');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listGeneral()
    {

        View::share( 'subMenuActive', 'general' );

        return view('admin.configurations.general');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listMail()
    {

        View::share( 'subMenuActive', 'mail' );

        return view('admin.configurations.mail');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store( Request $request ) :\Illuminate\Http\RedirectResponse
    {

        $validatedData = $request->validate([
            'group'  => 'required|string',
            'config' => 'required|array'
        ]);

        try {

            foreach( $validatedData['config'] as $key => $value ) {

                $fullKey = $validatedData['group'] . '.' . $key;

                setting([$fullKey => $value]);

            }

            setting()->save();

        } catch ( \Exception $e ) {

            redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Configurazione Salavata con successo');

    }
}
