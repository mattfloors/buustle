<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\TravelGenerationException;
use App\Http\Requests\TravelFileUploads;
use App\Models\Quote;
use App\Models\Retailer;
use App\Models\Travel;
use App\Models\TravelStep;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\RedirectResponse;
use Spatie\MediaLibrary\Models\Media;

class TravelsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travels' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {
        $this->shareLang();

        View::share( 'openMenu', false );

        $travels = Travel::with('steps')->get();

        return view('admin.travels.list', ['travels' => $travels]);

    }

    public function generate( Quote $quote )
    {

        try {

            if ( !empty($quote->travel) ) {

                throw new TravelGenerationException('Il preventivo ha già un viaggio collegato');

            }

            if ( $quote->status !== Quote::STATUS_PAYED ) {

                throw new TravelGenerationException('Il preventivo non è stato pagato, impossibile generare un viaggio');

            }

            $exitCode = Artisan::call('travel:generate', [
                'quoteId' => $quote->id,
                'status'  => Travel::STATUS_DRAFT
            ]);

            if ( $exitCode !== 0 ) {

                throw new TravelGenerationException('Errore nella generazione del viaggio con exit code ' . $exitCode . ' (atteso: 0)');

            }

            $quote->load('travel');

        } catch ( TravelGenerationException | \Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.travels.update', ['travel' => $quote->travels[0]])->with('success', 'Viaggio creato con status "Draft"');

    }

    public function create()
    {

        $this->shareLang();

        View::share( 'openMenu', false );

        $owners = User::getRetailers();
        $users = User::all();
        $quotes = Quote::where('status', '=', Quote::STATUS_PAYED)->get();
        $retailers = Retailer::all();

        return view('admin.travels.create', ['users' => $users, 'travel' => new Travel(), 'retailers' => $retailers, 'owners' => $owners, 'quotes' => $quotes]);

    }

    public function update( Travel $travel )
    {

        $this->shareLang();

        View::share( 'openMenu', false );

        $owners = User::getRetailers();
        $users = User::all();
        $quotes = Quote::where('status', '=', Quote::STATUS_PAYED)->get();
        $retailers = Retailer::all();

        return view('admin.travels.create', ['users' => $users, 'travel' => $travel, 'retailers' => $retailers, 'owners' => $owners, 'quotes' => $quotes]);

    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store( Request $request ) :RedirectResponse
    {

        try {

            $v = Validator::make($request->all(),[
                'id' => ['sometimes', 'nullable', 'numeric', 'exists:travels,id'],
                'relazione' => ['required', 'string', Rule::in(['quote', 'owner', 'user'])],
                'status' => ['required', 'string', Rule::in( Travel::s() )],
                'retailer_status' => ['required', 'string', Rule::in( Travel::rs() )],
                'shared' => ['sometimes', 'nullable', 'boolean'],
                'notified' => ['sometimes', 'nullable', 'boolean'],
                'active' => ['sometimes', 'nullable', 'boolean'],
                'description' => ['sometimes', 'nullable', 'string'],
                'retailer_id' => ['sometimes', 'nullable', 'numeric', 'exists:retailers,id'],
                'budget' => ['required', 'numeric', 'min:0'],
                'targa_mezzo' => ['sometimes', 'nullable', 'string'],
                'telefono_autista' => ['sometimes', 'nullable', 'string'],
            ]);

            $v->sometimes('quote_id', ['required', 'numeric', 'exists:quotes,id'], function ( $input ) {
                return ($input->relazione === 'quote');
            });

            $v->sometimes('owner_id', ['required', 'numeric', 'exists:users,id'], function ( $input ) {
                return ($input->relazione === 'owner');
            });

            $v->sometimes('user_id', ['required', 'numeric', 'exists:users,id'], function ( $input ) {
                return ($input->relazione === 'user');
            });

            $v->validate();

        } catch ( ValidationException $e ) {

            return redirect()->back()->with('errors', 'Validation Exception: ' . $e->getMessage());

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore Generico: ' . $e->getMessage());

        }

        try {

            if ( !$travel = Travel::find( $request->id ) ) {

                $travel = new Travel();

            }

            $travel->quote_id = ( $request->relazione === 'quote' ) ? $request->input('quote_id') : null;
            $travel->owner_id = ( $request->relazione === 'owner' ) ? $request->input('owner_id') : null;
            $travel->user_id = ( $request->relazione === 'user' ) ? $request->input('user_id') : null;
            $travel->retailer_id = $request->input('retailer_id');
            $travel->setTranslation('description', \Request::get('language'), $request->input('description') );
            $travel->status = $request->input( 'status' );
            $travel->retailer_status = $request->input( 'retailer_status' );
            $travel->budget = $request->input( 'budget' );
            $travel->shared = $request->input( 'shared' ) ? true : false;
            $travel->active = $request->input( 'active' ) ? true : false;
            $travel->notified = $request->input( 'notified' ) ? true : false;
            $travel->targa_mezzo = $request->input('targa_mezzo');
            $travel->telefono_autista = $request->input('telefono_autista');

            $travel->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore Generico: ' . $e->getMessage());

        }

        return redirect()->route('admin.travels.update', ['travel' => $travel, 'lang' => \Request::get('language')])->with('success', 'Viaggio Salvato');

    }

    public function fileUploads( Travel $travel, TravelFileUploads $request )
    {

        try {

            $folder = $request->input('file_upload_folder') . '_files';

            foreach( $request->file('files') as $file ) {

                $travel->addMedia($file)->toMediaCollection($folder);

            }

        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getmessage());

        }

        return redirect()->back()->with('success', count($request->file('files')) . ' File Caricati correttamente nella cartella ' . $request->input('file_upload_folder'));

    }

    public function deleteAttachment( Media $media )
    {

        try {

            $media->delete();

        } catch ( \Exception $e ) {

            return redirect()->back()->withErrors($e->getmessage());

        }

        return redirect()->back()->with('success', 'File cancellato con successo');

    }

    public function stepUpdate( TravelStep $step )
    {

        if ( empty ($step->travel) ) {

            return redirect()->back()->with('errors', 'Lo step non ha un viaggio collegato');

        }

        return view('admin.travels.steps.create', ['step' => $step, 'travel' => $step->travel]);

    }

    public function stepCreate( Travel $travel )
    {

        return view('admin.travels.steps.create', ['step' => new TravelStep(), 'travel' => $travel]);

    }

    public function stepStore( Travel $travel, Request $request )
    {
        try {

            $v = Validator::make( $request->all(), [
                'id' => ['sometimes', 'nullable', 'numeric', 'exists:travel_steps,id'],
                'start_time' => ['required', 'date'],
                'start_marker_id' => ['required', 'numeric', 'exists:markers,id'],
                'end_marker_id' => ['required', 'numeric', 'exists:markers,id'],
                'slots' => ['required', 'numeric', 'min:0', function ($attribute, $value, $fail) use ($request) {
                    if ( $request->input('id') && $step = TravelStep::find($request->input('id') ) ) {

                        if ($step->boughtSlots() > $value) {
                            $fail($attribute.' non può essere minore degli slot già acquistati (' . $step->boughtSlots() . ')');
                        }

                    }

                }],
                'distribusion_code' => ['sometimes', 'nullable', 'string'],
                'price' => ['required', 'numeric', 'min:0'],
            ]);

            $v->validate();

        } catch ( ValidationException $e ) {

            return redirect()->back()->with('errors', 'Validation Error: ' . $e->getMessage());

        } catch ( \Exception $e ) {

            dd($e->getmessage());
            return redirect()->back()->with('errors', 'Generic Error: ' . $e->getMessage());

        }

        try {

            if ( !$step = TravelStep::find($request->input('id') ) ) {

                $step = new TravelStep();

            }

            $step->travel_id = $travel->id;

            $step->start_time = $request->input('start_time');
            $step->start_marker_id = $request->input('start_marker_id');
            $step->end_marker_id = $request->input('end_marker_id');
            $step->estimated_time = $step->calculateTime();
            $step->estimated_km = $step->calculateKm();
            $step->slots = $request->input('slots');
            $step->distribusion_code = $request->input('distribusion_code');
            $step->price = $request->input('price');

            $step->save();

        } catch ( \Exception $e ) {

            dd($e->getmessage());
            return redirect()->back()->with('errors', 'Generic Error: ' . $e->getMessage());

        }

        return redirect()->route('admin.travels.update', ['travel' => $travel])->with('success', 'Step Salvato con Successo');

    }

    protected function shareLang()
    {

        View::share( 'language', \Request::get('language') );
        View::share( 'app_langs', config('translatable.app_locales') );

    }

}
