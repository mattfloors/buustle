<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Quote;
use App\Models\Ticket;
use App\User;
use Gabievi\Promocodes\Models\Promocode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

class PaymentsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'payments' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {

        View::share( 'openMenu', false );

        $orders = Order::all();

        return view('admin.payments.list', ['orders' => $orders]);

    }

    public function view( Order $order )
    {

        return view('admin.payments.view', ['order' => $order]);

    }

    public function create( Quote $quote )
    {

        $users = User::all();
        $quotes = Quote::all();
        $tickets = Ticket::all();
        $promocodes = Promocode::all();
        $rentRequest = $quote->request;

        return view('admin.payments.create', ['order' => new Order(), 'tickets' => $tickets, 'quote' => $quote, 'rentRequest' => $rentRequest, 'users' => $users, 'quotes' => $quotes, 'promocodes' => $promocodes, 'quote_id' => $quote->id]);

    }

    public function edit( Order $order )
    {

        $users = User::all();
        $quotes = Quote::all();
        $promocodes = Promocode::all();
        $tickets = Ticket::all();
        $quote = $order->isForQuote() ? $order->product : null;
        $quote_id = $order->isForQuote() ? $order->product->id : null;
        $rentRequest = $order->isForQuote() ? $order->product->request : null;
        $ticket = $order->isForTicket() ? $order->product : null;
        $ticket_id = $order->isForTicket() ? $order->product->id : null;

        return view('admin.payments.create', ['order' => $order, 'ticket_id' => $ticket_id, 'tickets' => $tickets, 'ticket' => $ticket, 'quote' => $quote, 'rentRequest' => $rentRequest, 'users' => $users, 'quotes' => $quotes, 'promocodes' => $promocodes, 'quote_id' => $quote_id]);

    }

    public function store( Request $request )
    {

        $orderForValidation = new Order();

        Validator::make($request->all(), [
            'id' => ['sometimes', 'nullable', 'numeric', 'exists:orders,id'],
            'amount' => ['required', 'numeric', 'min:0'],
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'quote_id' => ['sometimes', 'numeric', 'exists:quotes,id'],
            'ticket_id' => ['sometimes', 'numeric', 'exists:tickets,id'],
            'promocode_id' => ['sometimes', 'nullable', 'numeric', 'exists:promocodes,id'],
            'payment_status' => ['required', 'string', Rule::in($orderForValidation->statuses())],
            'transaction_id' => ['sometimes', 'nullable', 'string'],
            'selected_source' => ['required', 'string'],
            'method' => ['required', 'string', Rule::in($orderForValidation->methods())]
        ])->validate();

        try {

            if ( $request->input('selected_source') === 'App\Models\Quote' ) {

                if ( empty($request->input('quote_id')) ) {

                    throw new \Exception('Selezionato pagamento per Quote ma nessuna Quote selezionata');

                }

                $product_id = $request->input('quote_id');

            } elseif ( $request->input('selected_source') === 'App\Models\Ticket' ) {

                if ( empty($request->input('ticket_id')) ) {

                    throw new \Exception('Selezionato pagamento per Ticket ma nessun Ticket selezionato');

                }

                $product_id = $request->input('ticket_id');

            } else {

                throw new \Exception('Pagamento non ha un motivo di pagamento selezionato valido');

            }

            if ( $request->input('id') ) {

                $order = Order::findOrFail($request->input('id'));

            } else {

                $order = new Order();

            }

            $order->amount = $request->input('amount');
            $order->user_id = $request->input('user_id');
            $order->promocode_id = $request->input('promocode_id');
            $order->payment_status = $request->input('payment_status');
            $order->transaction_id = $request->input('transaction_id');
            $order->method = $request->input('method');
            $order->product_type = $request->input('selected_source');
            $order->product_id = $product_id;

            $order->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.payments.view', ['order' => $order])->with('success', 'Ordine di Pagamento modificato con successo');

    }
}
