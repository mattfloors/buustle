<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\DistribusionSync;
use App\Models\DistribusionRaw;
use App\Models\DistribusionToAdd;
use App\Models\Marker;
use App\Models\RawMarker;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use League\Csv\Writer;

class MarkersController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'markers' );
        View::share( 'subMenuActive', '' );

    }

    public function list()
    {
        $this->shareLang();

        $markers = Marker::all();

        return view('admin.markers.list', ['markers' => $markers]);

    }

    public function orphanedDownload(Request $request)
    {

        $this->shareLang();
        $language = $request->input('language');

        $markers = Marker::whereNull('distribusion_id')->get();

        $headers = ['Language', 'Name', 'Description', 'Street and Number', 'Zip Code', 'Longitude', 'Latitude', 'Full Address', 'City Name'];
        $records = [];

        foreach ( $markers as $marker ) {

            $records[] = [
                $language,
                $marker->getTranslation('name', $language),
                $marker->getTranslation('description', $language),
                $marker->getTranslation('street_and_number', $language),
                $marker->zip_code,
                $marker->lng,
                $marker->lat,
                $marker->full_address,
                $marker->getTranslation('city_name', $language)
            ];

        }

        $csv = Writer::createFromString('');
        $csv->insertOne($headers);
        $csv->insertAll($records);

        $csv->output('export-' . $language . '.csv');

        die;

    }

    public function orphaned()
    {
        $this->shareLang();

        $markers = Marker::whereNull('distribusion_id')->get();

        return view('admin.markers.orphaned', ['markers' => $markers]);

    }

    public function getList( Request $request )
    {

        $perpage = $request->input('length') ?? 10;

        if ( !$request->input('lang') || !in_array($request->input('lang'), config('translatable.app_locales'))) {

            $lang = config('translatable.fallback_locale');

        } else {

            $lang = $request->input('lang');

        }

        $start = ($request->input('start') && is_numeric($request->input('start'))) ? $request->input('start') : 0;

        $array = [];

        if ( $search = $request->input('search')['value'] ) {

            $marker = Marker::whereRaw('UPPER(name) LIKE \'%' . strtoupper($search) . '%\'')
                   ->orWhereRaw('UPPER(street_and_number) LIKE \'%' . strtoupper($search) . '%\'');

            $count = $marker->count();

            $res = $marker->skip($start)->take($perpage)->get();

        } else {

            $count = Marker::count();

            $res = Marker::skip($start)->take($perpage)->get();

        }

        $array['recordsTotal'] = $count;
        $array['data'] = [];
        $array['draw'] = $request->input('draw') ?? 0;
        $array['recordsFiltered'] = $count;

        if ( !empty( $res ) ) {

            foreach ( $res as $row ) {

                $array['data'][] = [
                    'id' => $row->id,
                    'nome' => $row->getTranslation('name', $lang),
                    'indirizzo' => $row->getTranslation('street_and_number', $lang),
                    'azioni' => '<a href="' . route('admin.markers.update', ['marker' => $row, 'lang' => $lang]) . '" class="btn btn-outline-warning btn-sm"><i class="fa fa-edit"></i> Modifica</a>
                            <a href="' . route('admin.markers.delete', ['marker' => $row, 'lang' => $lang]) . '" class="btn btn-outline-danger btn-sm"><i class="fa fa-times-octagon"></i> Elimina</a>'
                ];

            }

        }

        return response()->json($array);

    }

    public function create()
    {
        $this->shareLang();

        return view('admin.markers.create', ['marker' => new Marker()]);

    }

    public function update( Marker $marker )
    {

        $this->shareLang();

        return view('admin.markers.create', ['marker' => $marker]);

    }

    public function distribusionSingleSync( Request $request )
    {

        $id = $request->input('id');
        $lang = $request->input('language') ?? 'en';

        if ( !$distribusion_id = $request->input('distribusion_id') ) {

            return response()->json([
                'status' => 'failed',
                'message' => 'Distribusion ID is Missing'
            ]);

        }

        if ( !$distribusion = DistribusionRaw::where('id', '=', $distribusion_id)->where('locale', '=', $lang)->first() ) {

            return response()->json([
                'status' => 'failed',
                'message' => 'Distribusion Data not found for ' . $distribusion_id
            ]);

        }

        $marker = Marker::firstOrNew(['id' => $id]);
        $marker->distribusion_id = $distribusion_id;

        $marker->save();

        $distSync = new DistribusionSync($lang);
        $distSync->syncSingleStation( $distSync->formatRaw($distribusion) );

        return response()->json([
            'status' => 'success',
            'message' => 'Synced!'
        ]);

    }

    public function store( Request $request )
    {

        $this->shareLang();
        $language = \Request::get('language');

        $validatedData = $request->validate([
            'id'                => 'nullable|numeric',
            'name'              => 'required|string',
            'active'            => 'sometimes|nullable|boolean',
            'value_type'        => 'required|string',
            'value'             => 'nullable|numeric',
            'description'       => 'nullable|string|max:1024',
            'full_address'      => 'required|string',
            'raw_data'          => 'nullable|json',
            'lat'               => 'required|numeric',
            'lng'               => 'required|numeric',
            'street_and_number' => 'required|string|max:255',
            'city_name'         => 'required|string',
            'zip_code'          => 'required|string',
            'country'           => 'required|string',
            'country_code'      => 'required|string',
            'type'              => 'required|string'
        ]);

        try {

            $marker = Marker::firstOrNew(['id' => $validatedData['id']]);

            $marker->value_type = $validatedData['value_type'];
            $marker->value = $validatedData['value'];
            $marker->lat = $validatedData['lat'];
            $marker->lng = $validatedData['lng'];
            $marker->zip_code = $validatedData['zip_code'];
            $marker->full_address = $validatedData['full_address'];
            $marker->active = $validatedData['active'] ?? 0;
            $marker->country_code = $validatedData['country_code'];
            $marker->type = $validatedData['type'];

            $marker->setTranslation('name', $language, $validatedData['name']);
            $marker->setTranslation('street_and_number', $language, $validatedData['street_and_number']);
            $marker->setTranslation('description', $language, $validatedData['description']);
            $marker->setTranslation('city_name', $language, $validatedData['city_name']);
            $marker->setTranslation('country', $language, $validatedData['country']);

            $marker->save();

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore durante il salvataggio del marker: ' . $e->getMessage());

        }

        try {

            if ( $validatedData['raw_data'] !== null ) {

                $rawMarker = RawMarker::firstOrNew(['marker_id' => $marker->id, 'locale' => $language]);

                $rawMarker->locale = $language;
                $rawMarker->data = $validatedData['raw_data'];

                $rawMarker->save();

            }

        } catch (\Exception $e) {

            return redirect()->back()->with('warnings', 'Marker salvato correttamente, ma si è verificato un errore durante il salvataggio dei dati completi del marker: ' . $e->getMessage());

        }


        return redirect()->back()->with('success', 'Il marker è stato salvato con successo');

    }

    public function delete( Marker $marker )
    {

        $this->shareLang();

        $marker->delete();

        return redirect()->back()->with('success', 'Marker Eliminato');

    }

    public function search( Request $request )
    {

        if ( empty ( $request->input('q') || strlen ( $request->input('q') ) < 3 ) ) {

            return response()->json([
                'status' => 'error',
                'message' => 'empty string'
            ], 400);

        }

        $q = $request->input('q');

        $results = Marker::whereRaw('lower(zip_code) like lower(?)', ["%{$q}%"])
            ->orWhereRaw('lower(full_address) like lower(?)', ["%{$q}%"])
            ->orWhereRaw('lower(name) like lower(?)', ["%{$q}%"])
            ->orWhereRaw('lower(street_and_number) like lower(?)', ["%{$q}%"])
            ->orWhereRaw('lower(city_name) like lower(?)', ["%{$q}%"])
            ->orderBy('full_address')
            ->limit(20)
            ->get();

        foreach( $results as $r ) {

            $r->text = $r->getTranslation('name', App::getLocale()) . ' - ' . $r->full_address;

        }

        return response()->json([
            'status' => 'success',
            'message' => '',
            'results' => $results->toArray()
        ]);

    }

    public function distribusion()
    {

        View::share( 'menuActive', 'distribusion' );

        $missingIT = DistribusionToAdd::where('locale', '=', 'it')->count();
        $missingEN = DistribusionToAdd::where('locale', '=', 'en')->count();

        return view('admin.markers.distribusion', ['missingIT' => $missingIT, 'missingEN' => $missingEN]);

    }

    /**
     * @param string $locale
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function distribusionDownload( string $locale ) :\Illuminate\Http\JsonResponse
    {

        $client = new Client(['base_uri' => (bool) setting('distribusion.distribusion_test_mode') ? setting('distribusion.api_url_test') : setting('distribusion.api_url_live')]);
        $response = $client->request('GET', 'stations', [
            'headers' => [
                'api-key' => (bool) setting('distribusion.distribusion_test_mode') ? setting('distribusion.api_key_test') : setting('distribusion.api_key_live'),
                'content-type' => 'application/json'
            ],
            'query' => [
                'locale' => $locale
            ]
        ]);

        $data = json_decode((string) $response->getBody())->data;

        $service = new DistribusionSync($locale);

        try {

            $service->downloadData( $data );

        } catch ( \Exception $e ) {

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);

        }

        return response()->json([
            'status' => 'success',
            'message' => 'Scaricati ' . count($data) . ' Markers per il locale ' . $locale
        ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function distribusionAddCounter( ) :\Illuminate\Http\JsonResponse
    {

        $total = DistribusionRaw::select('distribusion_raw.*', 'markers.id')
                ->leftJoin('markers', function( $join ) {
            $join->on( 'markers.distribusion_id', '=', 'distribusion_raw.id' )
                 ->where('distribusion_raw.locale', '=', 'en');
        })->where('distribusion_raw.locale', '=', 'en')
        ->whereNull('markers.id')
        ->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Found ' . count($total) . ' stations to add'
        ]);

    }

    /**
     * @param string $lang
     * @return \Illuminate\Http\JsonResponse
     */
    public function prepareFor( string $lang ) :\Illuminate\Http\JsonResponse
    {

        if( !in_array($lang, config('translatable.app_locales'), false) ) {

            $lang = config('translatable.fallback_locale');

        }

        $findAll = DistribusionRaw::where('locale', '=', $lang)->get();
        $storedAll = Marker::select('distribusion_id')->get();
        $listOfIds = [];
        $listOfStoredIds = [];

        if ( !empty( $storedAll ) ) {

            foreach( $storedAll as $stored ) {

                if( $stored->distribusion_id !== null ) {

                    $listOfStoredIds[] = $stored->distribusion_id;

                }

            }

        }

        if ( !empty( $findAll ) ) {

            foreach( $findAll as $find ) {

                if ( $find->id !== null && !in_array($find->id, $listOfStoredIds) ) {

                    $listOfIds[] = $find->id;

                }

            }

        }

        if ( !empty($listOfIds) ) {

            $sync = new DistribusionSync($lang);
            $sync->prepareNew( $listOfIds );

        }

        return response()->json([
            'status' => 'success',
            'lang' => $lang,
            'new' => DistribusionToAdd::where('locale', '=', $lang)->count()
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function distribusionAdd( Request $request ) :\Illuminate\Http\JsonResponse
    {

        $start = (int) ($request->input('start') ?? 0);
        $take = (int) ($request->input('take') ?? 25);

        $all = DistribusionToAdd::skip($start)->take($take)->orderBy('id', 'ASC')->get();
        $totalToAdd = DistribusionToAdd::count();

        if ( !empty( $all ) ) {

            foreach( $all as $a ) {

                $raw = DistribusionRaw::where('locale', '=', $a->locale)->where('id', '=', $a->distribusion_id)->first();

                if ( !empty ( $raw ) ) {

                    $sync = new DistribusionSync( $a->locale );
                    $sync->syncSingleStation( $sync->formatRaw( $raw ) );

                }

                //$a->delete();

            }

        }

        if ( $totalToAdd === 0 ) {

            $percent = 100;

        } else {

            $percent = round((($start + $take) / $totalToAdd) * 100, 2);

        }

        return response()->json([
            'status' => 'success',
            'newRoute' => route('admin.markers.distribusionAdd', ['start' => $start + $take, 'take' => $take]),
            'totalToSync' => $totalToAdd,
            'start' => $start,
            'newStart' => $start + $take,
            'take' => $take,
            'barProgress' => ($percent <= 100) ? $percent : 100,
        ]);

    }

    public function removeToAdd()
    {

        DistribusionToAdd::truncate();

        return response()->json([
            'status' => 'success'
        ]);

    }

    public function distribusionSync( Request $request )
    {

        $allowedLangs = config('translatable.app_locales');

        $lang = $request->input('lang');

        if ( !in_array( $lang, $allowedLangs, true ) ) {

            $lang = 'en';

        }

        $start = (int) ($request->input('start') ?? 0);
        $take = (int) ($request->input('take') ?? 25);

        $allData = DistribusionRaw::where('locale', '=', $lang)->skip($start)->take($take)->get();

        $service = new DistribusionSync($lang);

        $service->multipleSync($allData);

        $totalToSync = DistribusionRaw::where('locale', '=', $lang)->count();
        $route = route('admin.markers.distribusionSync', ['lang' => $lang, 'start' => $start + $take, 'take' => $take]);

        if ( $totalToSync === 0 ) {

            $percent = 100;

        } else {

            $percent = round((($start + $take) / $totalToSync) * 100, 2);

        }

        return response()->json([
            'status' => 'success',
            'newRoute' => $route,
            'totalToSync' => $totalToSync,
            'lang' => $lang,
            'allData' => count( $allData ),
            'start' => $start,
            'newStart' => $start + $take,
            'take' => $take,
            'barProgress' => ($percent <= 100) ? $percent : 100,
        ]);

    }

    protected function shareLang() :void
    {

        View::share( 'language', \Request::get('language') );
        View::share( 'app_langs', config('translatable.app_locales') );

    }
}
