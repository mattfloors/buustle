<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class LanguagesController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'languages' );
        View::share( 'subMenuActive', '' );

    }

    public function listTranslations()
    {

        View::share( 'subMenuActive', 'translations' );

        $listOfLocaleFiles = [];

        try {

            foreach ( config('translatable.app_locales') as $language ) {

                $path = $this->checkLangDir( $language );

                $dirScanned = scandir( $path, SCANDIR_SORT_ASCENDING);

                if ( !empty($dirScanned) ) {

                    $listOfLocaleFiles[$language] = [];

                    foreach ( $dirScanned as $file ) {

                        if ( $file !== '.' && $file !== '..' ) {

                            $listOfLocaleFiles[$language][] = str_replace('.php', '', $file);

                        }

                    }

                }

            }

        } catch ( \Exception $e ) {

            return redirect()->route('admin.dashboard')->with('errors', $e->getMessage());

        }

        return view('admin.languages.translations.list', ['languages' => $listOfLocaleFiles]);

    }

    /**
     * @param string $language
     * @param string $translation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function editTranslation( string $language, string $translation ) {

        View::share( 'subMenuActive', 'translations' );

        try {

            $path = $this->translationFilePath($language, $translation);
            $data = include $path;
            $fullData = file_get_contents($path);

        } catch ( FileNotFoundException $e ) {

            return redirect()->route('admin.languages.listTranslations')->with('errors', $e->getMessage());

        }

        return view('admin.languages.translations.create', ['data' => $data, 'file' => $translation, 'language' => $language, 'rawData' => $fullData]);

    }

    /**
     * @param string $language
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTranslation( string $language, Request $request )
    {

        $validatedData = $request->validate([
            'name' => 'required|string',
            'data' => 'required|array',
            'key'  => 'required|array'
        ]);

        try {

            $path = $this->checkLangDir( $language );
            $filePath = $this->findOrNew( $path, $validatedData['name'] );

            if ( !$handle = fopen( $filePath, 'w+') ) {

                throw new \Exception('Impossibile scrivere il file ' . $filePath);

            }

            $handle = $this->writeFileHeaders( $handle );

            $handle = $this->writeFileData( $handle, $validatedData['key'], $validatedData['data'] );

            fclose( $handle );

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Traduzioni Salvate con Successo');

    }

    public function newTranslation( string $language )
    {

        return view('admin.languages.translations.create', ['data' => [], 'file' => '', 'language' => $language, 'rawData' => '']);

    }

    public function duplicateTranslation( string $language,  string $translation )
    {

        return view('admin.languages.translations.duplicate', ['file' => $translation, 'language' => $language]);

    }

    public function doDuplicateTranslation( string $newLang, string $translation, string $oldLang )
    {

        try {

            $folderPath = $this->checkLangDir( $newLang );
            $newFilePath = $folderPath . '/' . $translation . '.php';
            $oldFilePath = $this->translationFilePath( $oldLang, $translation );

            copy( $oldFilePath, $newFilePath );

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.languages.listTranslations')->with('success', 'File traduzioni copiato');

    }

    /**
     * @param string $language
     * @param string $translation
     * @return string
     * @throws \Exception
     */
    protected function translationFilePath( string $language, string $translation ) :string
    {

        $langPath = App::langPath();
        $filePath = $langPath . '/' . $language . '/' . $translation . '.php';

        if ( !file_exists($langPath) ) {

            throw new \Exception('File ' . $filePath . ' non trovato');

        }

        return $filePath;

    }

    /**
     * @param string $language
     * @return string
     * @throws \Exception
     */
    protected function checkLangDir( string $language ) :string
    {
        $langPath = App::langPath();
        $path = $langPath . '/' . $language;

        if ( !file_exists( $path ) && (!mkdir( $path, 0755 ) && !is_dir( $path )) ) {

            throw new \Exception('Impossibile Creare la directory ' . $path);

        }

        return $path;

    }

    /**
     * @param string $languagePath
     * @param string $name
     * @return string
     * @throws \Exception
     */
    protected function findOrNew( string $languagePath, string $name ) :string
    {

        $fullPath = $languagePath . '/' . $name . '.php';

        if ( !file_exists($fullPath) && !touch($fullPath) ) {

            throw new \Exception('Impossibile creare il file ' . $fullPath);

        }

        return $fullPath;

    }

    /**
     * @param $handle
     * @return mixed
     */
    protected function writeFileHeaders( $handle )
    {

        fwrite($handle, '<?php' . PHP_EOL);
        fwrite($handle, '//File is autogenerated, do not edit or it will be overwritten' . PHP_EOL);
        fwrite($handle, '//Generated on: ' . Carbon::now()->format('d/m/Y H:i') . ' By ' . Auth::user()->email . PHP_EOL . PHP_EOL);

        return $handle;

    }

    protected function writeFileData( $handle, array $keys, array $data )
    {

        fwrite($handle, 'return [' . PHP_EOL);

        if ( empty($data) || empty($keys) ) {

            fwrite($handle, '];' . PHP_EOL);

            return $handle;

        }

        foreach ( $keys as $k => $v ) {

            if ( !empty($v) ) {

                fwrite($handle, "\t" . '\'' . $v . '\' => ');

                if ( array_key_exists($k, $data) && !empty( $data[$k] ) ) {

                    fwrite( $handle, '\'' . $this->cleanString($data[$k])  . '\',' . PHP_EOL);

                } else {

                    fwrite( $handle, '\'\',' . PHP_EOL);

                }

            }

        }

        fwrite($handle, '];' . PHP_EOL);

        return $handle;

    }

    private function cleanString( string $string ) {

        return addslashes( $string );

    }
}
