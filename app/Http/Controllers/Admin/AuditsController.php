<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class AuditsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function view( Request $request )
    {

        $validatedData = $request->validate([
            'id'    => 'required|numeric',
            'model' => 'required|string'
        ]);

        try {

            $singleModel = $validatedData['model']::findOrFail($validatedData['id']);

            $audits = $singleModel->audits;

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Audits non trovati: ' . $e->getMessage());

        }

        return view('admin.shared.view-audit', ['audits' => $audits, 'model' => $singleModel, 'modelText' => $validatedData['model'], 'modelId' => $validatedData['id']]);

    }
}
