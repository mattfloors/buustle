<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends AdminBaseController
{

    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'users' );
        View::share( 'subMenuActive', 'roles' );

    }

    public function list()
    {

        $roles = Role::all();

        return view('admin.users.roles.list', ['roles' => $roles]);

    }

    public function create()
    {

        $permissions = Permission::all();

        return view('admin.users.roles.create', ['role' => new Role(), 'permissions' => $permissions]);

    }

    public function update( Role $role )
    {

        $permissions = Permission::all();

        return view('admin.users.roles.create', ['role' => $role, 'permissions' => $permissions]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'          => 'nullable|numeric',
            'name'        => 'required|string',
            'guard_name'  => 'required|string',
            'permissions' => 'nullable|array'
        ]);

        $role = Role::firstOrNew(['name' => $validatedData['name'], 'guard_name' => $validatedData['guard_name']]);

        $role->name = $validatedData['name'];
        $role->guard_name = $validatedData['guard_name'];

        $role->save();

        if ( $validatedData['permissions'] !== null ) {

            $role->syncPermissions( $validatedData['permissions'] );

        } else {

            $role->syncPermissions( [] );

        }

        $role->save();

        return redirect()->back()->with('success', 'Ruolo ' . $validatedData['name'] . ' per la guardia ' . $validatedData['guard_name'] . ' salvato con successo');

    }

    public function delete( Role $role )
    {

        $role->delete();

        return redirect()->back()->with('success', 'Il ruolo è stato eliminato');

    }
}
