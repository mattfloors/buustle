<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\RentalPrice;
use App\Models\Quote;
use App\Models\QuoteMarker;
use App\Models\RentRequest;
use App\Models\SoldMarker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class StepsController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'rentRequests' );
        View::share( 'subMenuActive', 'rent_requests_sold' );

    }

    /**
     * @param RentRequest $rentRequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function distribusionSteps( RentRequest $rentRequest )
    {
        try {

            if ( $rentRequest->status !== 'sold' ) {

                throw new \Exception('the request status is ' . $rentRequest->status . ' instead of sold');

            }

            if ( empty( $quote = Quote::where('request_id', '=', $rentRequest->id)->where('status', '=', 'payed')->firstOrFail() ) ) {

                throw new \Exception('the request has no valid payed quotes');

            }

            return view('admin.steps.list', ['rentRequest' => $rentRequest, 'quote' => $quote]);

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

    }

    /**
     * @param QuoteMarker $quoteMarker
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit( QuoteMarker $quoteMarker )
    {

        return view('admin.steps.edit', ['quoteMarker' => $quoteMarker]);

    }

    /**
     * @param QuoteMarker $quoteMarker
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addTicket( QuoteMarker $quoteMarker )
    {

        return view('admin.steps.ticket.create', ['quoteMarker' => $quoteMarker, 'soldMarker' => new SoldMarker()]);

    }

    /**
     * @param QuoteMarker $quoteMarker
     * @param SoldMarker $soldMarker
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editTicket( QuoteMarker $quoteMarker, SoldMarker $soldMarker )
    {

        return view('admin.steps.ticket.create', ['quoteMarker' => $quoteMarker, 'soldMarker' => $soldMarker]);

    }

    /**
     * @param QuoteMarker $quoteMarker
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTicket( QuoteMarker $quoteMarker, Request $request ) :\Illuminate\Http\RedirectResponse
    {

        $validator = Validator::make($request->all(), [
            'id'        => ['sometimes', 'nullable', 'numeric', 'exists:sold_markers,id'],
            'slots'     => ['required', 'numeric', 'min:1'],
            'price'     => ['required', 'numeric', 'min:0'],
            'through'   => ['sometimes', 'string', 'max:64', 'nullable'],
            'reference' => ['sometimes', 'nullable'],
            'comment'   => ['sometimes', 'nullable', 'string', 'max:2048'],
            'owner'     => ['sometimes', 'nullable', 'string', 'max:256']
        ]);

        if ($validator->fails()) {
            return redirect()
                    ->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {

            if ( $request->input('id') ) {

                $soldMarker = SoldMarker::find($request->input('id'));

            } else {

                $soldMarker = new SoldMarker();

            }

            $soldMarker->quote_marker_id = $quoteMarker->id;
            $soldMarker->through         = $request->input('through');
            $soldMarker->reference       = $request->input('reference');
            $soldMarker->slots           = $request->input('slots');
            $soldMarker->price           = $request->input('price');
            $soldMarker->comment         = $request->input('comment');
            $soldMarker->owner            = $request->input('owner');

            $soldMarker->save();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('admin.steps.edit', ['quoteMarker' => $quoteMarker])->with('success', 'Biglietto / i salvato / i correttamente');

    }

    public function checkTicket( string $soldMarkerUuid )
    {

        if ( !$soldMarker = SoldMarker::where('uuid', '=', $soldMarkerUuid)->first() ) {

            abort(404);

        }

        return redirect()->route('admin.steps.editTicket', ['quoteMarker' => $soldMarker->quoteMarker, 'soldMarker' => $soldMarker]);

    }

    /**
     * @param RentRequest $rentRequest
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function autoGenerate( RentRequest $rentRequest, Request $request ) :\Illuminate\Http\RedirectResponse
    {

        try {

            if ( !$rentRequest->hasPayedQuote() ) {

                throw new \Exception('Rent Request has no valid payed quotes');

            }

            $quote = $rentRequest->payedQuote();

            $exitCode = Artisan::call('steps:generate', [
                'quoteId' => $quote->id,
                'slots'   => (int) $request->input('slots'),
                '--force' => true
            ]);

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Reinitialized the steps for a vehicle of ' . $request->input('slots') . ' people');

    }
}
