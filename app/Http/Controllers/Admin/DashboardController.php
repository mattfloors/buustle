<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\View;

class DashboardController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'dashboard' );
        View::share( 'subMenuActive', '' );

    }

    public function index()
    {

        return view('admin.index');

    }
}
