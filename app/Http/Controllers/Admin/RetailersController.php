<?php

namespace App\Http\Controllers\Admin;

use App\Models\Retailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RetailersController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travel_settings' );
        View::share( 'subMenuActive', 'travel_retailers' );

    }

    public function list()
    {
        $this->shareLang();

        $retailers = Retailer::all();

        return view('admin.retailers.list', ['retailers' => $retailers]);

    }

    public function create()
    {
        $this->shareLang();

        return view('admin.retailers.create', ['retailer' => new Retailer()]);

    }

    public function update( Retailer $retailer )
    {

        $this->shareLang();

        return view('admin.retailers.create', ['retailer' => $retailer]);

    }

    public function store( Request $request )
    {

        $this->shareLang();
        $language = \Request::get('language');

        $validatedData = $request->validate([
            'id'                => 'nullable|numeric',
            'name'              => 'required|string',
            'active'            => 'sometimes|nullable|boolean',
            'description'       => 'nullable|string|max:1024'
        ]);

        try {

            $retailer = Retailer::firstOrNew(['id' => $validatedData['id']]);

            $retailer->setTranslation('name', $language, $validatedData['name']);
            $retailer->setTranslation('description', $language, $validatedData['description']);
            $retailer->active = $validatedData['active'] ?? 0;

            $retailer->save();

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore durante il salvataggio del fornitore: ' . $e->getMessage());

        }

        return redirect()->route('admin.retailers.update', ['retailer' => $retailer, 'lang' => $language])->with('success', 'Il servizio è stato salvato con successo');

    }

    public function delete( Retailer $retailer )
    {

        $this->shareLang();

        $retailer->delete();

        return redirect()->back()->with('success', 'Fornitore Eliminato');

    }

    protected function shareLang()
    {

        View::share( 'language', \Request::get('language') );
        View::share( 'app_langs', config('translatable.app_locales') );

    }
}
