<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Permission;

class PermissionsController extends AdminBaseController
{

    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'users' );
        View::share( 'subMenuActive', 'permissions' );

    }

    public function list()
    {

        $permissions = Permission::all();

        return view('admin.users.permissions.list', ['permissions' => $permissions]);

    }

    public function create()
    {

        return view('admin.users.permissions.create', ['permission' => new Permission()]);

    }

    public function update( Permission $permission )
    {

        return view('admin.users.permissions.create', ['permission' => $permission]);

    }

    public function store( Request $request )
    {

        $validatedData = $request->validate([
            'id'         => 'nullable|numeric',
            'name'       => 'required|string',
            'guard_name' => 'required|string'
        ]);

        $permission = Permission::firstOrNew(['name' => $validatedData['name'], 'guard_name' => $validatedData['guard_name']]);

        $permission->name = $validatedData['name'];
        $permission->guard_name = $validatedData['guard_name'];

        $permission->save();

        return redirect()->back()->with('success', 'Permesso ' . $validatedData['name'] . ' per la guardia ' . $validatedData['guard_name'] . ' salvato con successo');

    }

    public function delete( Permission $permission )
    {

        $permission->delete();

        return redirect()->back()->with('success', 'Il permesso è stato eliminato');

    }
}
