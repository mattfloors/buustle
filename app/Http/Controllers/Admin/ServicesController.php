<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ServicesController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travel_settings' );
        View::share( 'subMenuActive', 'travel_services' );

    }

    public function list()
    {
        $this->shareLang();

        $services = Service::all();

        return view('admin.services.list', ['services' => $services]);

    }

    public function create()
    {
        $this->shareLang();

        return view('admin.services.create', ['service' => new Service()]);

    }

    public function update( Service $service )
    {

        $this->shareLang();

        return view('admin.services.create', ['service' => $service]);

    }

    public function store( Request $request )
    {

        $this->shareLang();
        $language = \Request::get('language');

        $validatedData = $request->validate([
            'id'                => 'nullable|numeric',
            'name'              => 'required|string',
            'active'            => 'sometimes|nullable|boolean',
            'description'       => 'nullable|string|max:1024',
            'price'             => 'nullable|numeric',
            'icon'              => 'nullable|string'
        ]);

        try {

            $service = Service::firstOrNew(['id' => $validatedData['id']]);

            $service->setTranslation('name', $language, $validatedData['name']);
            $service->setTranslation('description', $language, $validatedData['description']);

            $service->active = $validatedData['active'] ?? 0;
            $service->icon = $validatedData['icon'];
            $service->price = $validatedData['price'];

            $service->save();

            if ( $request->file('icon-checked') ) {

                $service->addMedia($request->file('icon-checked'))->toMediaCollection('icon-checked');

            }

            if ( $request->file('icon-unchecked') ) {

                $service->addMedia($request->file('icon-unchecked'))->toMediaCollection('icon-unchecked');

            }

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore durante il salvataggio del servizio: ' . $e->getMessage());

        }

        return redirect()->route('admin.services.update', ['service' => $service, 'lang' => $language])->with('success', 'Il servizio è stato salvato con successo');

    }

    public function delete( Service $service )
    {

        $this->shareLang();

        $service->delete();

        return redirect()->back()->with('success', 'Servizio Eliminato');

    }

    protected function shareLang()
    {

        View::share( 'language', \Request::get('language') );
        View::share( 'app_langs', config('translatable.app_locales') );

    }
}
