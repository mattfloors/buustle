<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PackagesController extends AdminBaseController
{
    public function __construct() {

        parent::__construct();

        View::share( 'menuActive', 'travel_settings' );
        View::share( 'subMenuActive', 'travel_packages' );

    }

    public function list()
    {
        $this->shareLang();

        $packages = Package::all();

        return view('admin.packages.list', ['packages' => $packages]);

    }

    public function create()
    {
        $this->shareLang();

        $services = Service::all();

        return view('admin.packages.create', ['package' => new Package(), 'availableClasses' => $this->loadAvailableClasses(), 'services' => $services]);

    }

    public function update( Package $package )
    {

        $this->shareLang();

        $services = Service::all();

        return view('admin.packages.create', ['package' => $package, 'services' => $services, 'availableClasses' => $this->loadAvailableClasses()]);

    }

    public function store( Request $request )
    {

        $this->shareLang();
        $language = \Request::get('language');

        $validatedData = $request->validate([
            'id'                => 'nullable|numeric',
            'sku'               => 'required|string',
            'name'              => 'required|string',
            'active'            => 'sometimes|nullable|boolean',
            'description'       => 'required|string|max:2048',
            'min_km'            => 'sometimes|nullable|numeric|min:0',
            'max_km'            => 'sometimes|nullable|numeric|gt:min_km',
            'min_people'        => 'sometimes|nullable|numeric|min:0',
            'max_people'        => 'sometimes|nullable|numeric|gt:min_people',
            'icon'              => 'sometimes|nullable|string',
            'class'             => 'required|string|min:2',
            'services'          => 'sometimes|array',
            'services.*'        => 'numeric',
            'multiplier'        => 'required|numeric|min:1',
            'supplement'        => 'required|numeric|min:0',
            'env_class'         => 'sometimes|nullable|string'
        ]);

        try {

            $package = Package::firstOrNew(['id' => $validatedData['id']]);

            $package->setTranslation('name', $language, $validatedData['name']);
            $package->setTranslation('description', $language, $validatedData['description']);

            $package->active     = $validatedData['active'] ?? 0;
            $package->min_km     = $validatedData['min_km'] ?? null;
            $package->max_km     = $validatedData['max_km'] ?? null;
            $package->min_people = $validatedData['min_people'] ?? null;
            $package->max_people = $validatedData['max_people'] ?? null;
            $package->sku        = $validatedData['sku'];
            $package->icon       = $validatedData['icon'] ?? '';
            $package->class      = $validatedData['class'];
            $package->multiplier = $validatedData['multiplier'] / 100;
            $package->env_class  = $validatedData['env_class'];
            $package->supplement = $validatedData['supplement'];

            $package->save();

            $package->services()->sync($validatedData['services']);

            $package->save();

            if( $request->file('banner-image') ) {

                $package->addMedia( $request->file('banner-image') )->toMediaCollection('banner-image');

            }

        } catch( \Exception $e ) {

            return redirect()->back()->with('errors', 'Errore durante il salvataggio del Pacchetto Viaggio: ' . $e->getMessage());

        }

        return redirect()->route('admin.packages.update', ['package' => $package, 'lang' => $language])->with('success', 'Il pacchetto viaggio è stato salvato con successo');

    }

    public function delete( Package $package )
    {

        $this->shareLang();

        $package->delete();

        return redirect()->back()->with('success', 'Pacchetto Viaggio Eliminato');

    }

    protected function shareLang()
    {

        View::share( 'language', \Request::get('language') );
        View::share( 'app_langs', config('translatable.app_locales') );

    }

    protected function loadAvailableClasses()
    {

        $basePath = app_path() . '/Packages';

        $dirScanned = scandir( $basePath, SCANDIR_SORT_ASCENDING);

        $classes = [];

        if ( !empty($dirScanned) ) {

            foreach ( $dirScanned as $file ) {

                if ( $file !== '.' && $file !== '..' && $file !== 'AbstractPackage.php') {

                    $classes[] = str_replace('.php', '', $file);

                }

            }

        }

        return $classes;

    }
}
