<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\RentRequest;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $redirect = null;

    protected $requestUuid = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver, Request $request)
    {
        if( $request->input('redirect') ) {

            if( $request->input('redirect') === 'rent-request' && session('request_id') ) {

                $this->requestUuid = session('request_id');
                $this->redirect = $request->input('redirect');

            }

        }

        return Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->user();
        } catch (\Exception $e) {
            return redirect()->route('login')->with('errors', $e->getMessage());
        }

        $existingUser = User::where('email', $user->getEmail())->first();

        if( session('request_id') ) {

            $this->requestUuid = session('request_id');
            $this->redirect = 'rent-request';

        }

        if ($existingUser) {
            auth()->login($existingUser, true);
        } else {
            $newUser                    = new User;
            $newUser->provider_name     = $driver;
            $newUser->provider_id       = $user->getId();
            $newUser->name              = $user->getName();
            $newUser->email             = $user->getEmail();
            $newUser->email_verified_at = now();
            $newUser->avatar            = $user->getAvatar();
            $newUser->save();

            auth()->login($newUser, true);
        }

        return redirect($this->redirectTo());
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if( $request->input('redirect') ) {

            if( $request->input('redirect') === 'rent-request' && session('request_id') ) {

                $this->requestUuid = session('request_id');
                $this->redirect = $request->input('redirect');

            }

        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @return string
     */
    protected function redirectTo() :string
    {
        $redirect = $this->redirectTo;

        if ($this->redirect === 'rent-request' && $this->requestUuid) {
            $requestById = RentRequest::where('uuid', '=', $this->requestUuid)->first();

            if ($requestById) {
                $requestById->bindToUser(Auth::id());

                $redirect = route('rent.noleggi.offer', ['rentRequest' => $requestById->uuid]);
            }
        }

        return $redirect;
    }

    /**
     * Show the application's login form.
     *
     * @return Response|View
     */
    public function showLoginForm()
    {
        return view('auth.login', ['redirect' => Input::get('redirect')]);
    }
}
