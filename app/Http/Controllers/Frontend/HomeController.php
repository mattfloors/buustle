<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class HomeController extends FrontendBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.home');
    }

    public function setLang( string $locale )
    {

        Session::put('locale', $locale);
        App::setLocale($locale);
        return redirect()->back()->with('success', __('global.new_locale_saved') . ': ' . strtoupper($locale));

    }
}
