<?php

namespace App\Http\Controllers\Frontend;


use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        return view('frontend.contact.index');

    }

    public function submit( Request $request )
    {

        $validatedData = $request->validate([
            'nome'  => 'required|string|max:128|min:1',
            'cognome' => 'required|string|max:128|min:1',
            'telefono' => 'required|string|max:64|min:6',
            'content' => 'required|string|min:20|max:8000',
            'email' => 'required|string|email'
        ]);

        $email = new ContactForm($validatedData['nome'], $validatedData['cognome'], $validatedData['email'], $validatedData['telefono'], $validatedData['content']);

        Mail::to(setting('general.contact_email'))->send($email);

        return redirect()->back()->with('success', __('contact-us.mail_sent'));

    }

}
