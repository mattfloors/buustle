<?php

namespace App\Http\Controllers\Frontend;

class BecomePartnerController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        return view('frontend.become-partner.index');

    }

}
