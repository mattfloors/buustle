<?php

namespace App\Http\Controllers\Frontend;

class ComingSoonController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        return view('frontend.coming-soon.index');

    }

}
