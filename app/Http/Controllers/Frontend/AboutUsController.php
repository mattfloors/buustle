<?php

namespace App\Http\Controllers\Frontend;

class AboutUsController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        return view('frontend.about-us.index');

    }

}
