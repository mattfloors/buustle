<?php

namespace App\Http\Controllers\Frontend;

class FaqController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        return view('frontend.faq.index');

    }

}
