<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Services\Stripe;
use Illuminate\Http\Request;

class StripeController extends FrontendBaseController
{

    /**
     * @param $order_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkout($order_id, Request $request) :\Illuminate\Http\RedirectResponse
    {
        try {

            if ( !$request->input('stripeToken') ) {

                throw new \Exception(__('payments.stripe_token_missing'));

            }

            $order = Order::findOrFail(decrypt($order_id));

            $order->payment_status = Order::PAYMENT_PENDING;
            $order->method = Order::STRIPE;
            $order->save();

            $stripe = new Stripe();

            $response = $stripe->purchase([
                'amount' => $stripe->formatAmount($order->amount),
                'currency' => 'EUR',
                'token' => $request->input('stripeToken'),
                'metadata' => ['order_id' => $order_id]
            ]);

            $order->storeRawData( $response->getData() );

            if ( !$response->isSuccessful() ) {

                $order->fail( $response->getData(), $response->getMessage() );

                return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $order->product->request->uuid, 'quote' => $order->product])->with('errors', 'Error: ' . $response->getMessage());

            }

            $order->complete( $response->getTransactionReference() );

        } catch ( \Exception $e ) {

            $order->fail( ['error' => 'before payment'], $e->getMessage() );

            return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $order->product->request->uuid, 'quote' => $order->product])->with('errors', $e->getMessage());

        }

        return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $order->product->request->uuid, 'quote' => $order->product])->with([
            'success' => __('quote.payment_complete'),
        ]);
    }

}
