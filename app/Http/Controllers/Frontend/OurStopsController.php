<?php

namespace App\Http\Controllers\Frontend;


use App\Mail\ContactForm;
use App\Models\Stops;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OurStopsController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {
        $stops = Stops::all();
        $grouped_stops = array();
        foreach ($stops as $stop){
            if(array_key_exists($stop->city_name, $grouped_stops)){
                array_push($grouped_stops[$stop->city_name], array('city_name'=>$stop->city_name,'stop_name'=>$stop->stops_name, 'link'=>$stop->link));
            }
            else {
                $grouped_stops[$stop->city_name] = array();
                array_push($grouped_stops[$stop->city_name], array('city_name'=>$stop->city_name,'stop_name'=>$stop->stops_name, 'link'=>$stop->link));

            }
        }

        return view('frontend.our-stops.index', compact('grouped_stops'));

    }

}
