<?php

namespace App\Http\Controllers\Frontend;

use Spatie\MediaLibrary\Models\Media;

class DownloadsController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function travelAttachment( Media $media )
    {

        return response()->download($media->getPath(), $media->file_name);

    }

}
