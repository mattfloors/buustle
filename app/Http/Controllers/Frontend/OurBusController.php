<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\ContactForm;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OurBusController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index()
    {

        $packages = Package::all();

        return view('frontend.our-bus.index', ['packages' => $packages]);

    }

}
