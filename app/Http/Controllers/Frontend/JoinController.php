<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\JoinSearch;
use App\Models\Travel;
use App\Models\TravelStep;
use Carbon\Carbon;

class JoinController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    public function index( JoinSearch $request )
    {
        /** Analizyng request */
        if( $request->input('start_date') ) {

            $dateFrom = Carbon::createFromFormat('d/m/Y', $request->input('start_date') );

        } else {

            $dateFrom = Carbon::now();

        }

        $people = $request->input('people') ?: 1;

        /** @var $query | Stores the query */
        $query = TravelStep::select('travel_steps.*')
                           ->where('travel_steps.start_time', '>=', $dateFrom)
                           ->where('travel_steps.slots', '>=', $people)
                           ->with('endMarker')
                           ->with('startMarker')
                           ->with('travel');

        /** Working on the join */
        $query->join('travels', function($join) {
                 $join->on('travels.id', '=', 'travel_steps.travel_id')
                      ->where('travels.status', '=', Travel::STATUS_ACCEPTED)
                      ->where('travels.active', '=', true)
                      ->where('travels.shared', '=', true);
             });
        $query->join('markers as s', function($join) use ($request) {
            $join->on('s.id', '=', 'travel_steps.start_marker_id');

            if ( $request->input('start_lat') ) {

                $join->whereBetween('s.lat', [ $request->input('start_lat') - 0.1, $request->input('start_lat') + 0.1 ]);

            }

            if ( $request->input('start_lng') ) {

                $join->whereBetween('s.lng', [ $request->input('start_lng') - 0.3, $request->input('start_lng') + 0.3 ]);

            }
        });

        $query->join('markers as e', function($join) use ($request) {
            $join->on('e.id', '=', 'travel_steps.end_marker_id');

            if ( $request->input('end_lat') ) {

                $join->whereBetween('e.lat', [$request->input('end_lat') - 0.1, $request->input('end_lat') + 0.1 ]);

            }

            if ( $request->input('end_lng') ) {

                $join->whereBetween('e.lng', [ $request->input('end_lng') - 0.3, $request->input('end_lng') + 0.3 ]);

            }
        });

        /** Order */
        $query->orderBy('travel_steps.start_time', 'ASC');

        /** @var $travels | Closes the query */
        $travels = $query->get();

        return view('frontend.join.index', ['travelSteps' => $travels, 'request' => $request]);

    }

    public function buyStep( TravelStep $travelStep )
    {

        return view('frontend.join.buy-step', ['travelStep' => $travelStep]);

    }

}
