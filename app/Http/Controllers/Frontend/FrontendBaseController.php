<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

Class FrontendBaseController extends Controller
{

    public function __construct() {

        $config = array(
            'driver'     => setting('email.driver', 'smtp'),
            'host'       => setting('email.host', 'smtp.mailgun.org'),
            'port'       => setting('email.port', 587),
            'from'       => array('address' => setting('general.sender_email', 'hello@example.com'), 'name' => setting('general.sender_name', 'Example')),
            'encryption' => setting('email.encryption', 'tls'),
            'username'   => setting('email.username', ''),
            'password'   => setting('email.password', ''),
        );
        Config::set('mail', $config);

    }

}