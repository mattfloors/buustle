<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Services\PayPal;
use Illuminate\Http\Request;

class PayPalController extends FrontendBaseController
{

    /**
     * @param $order_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkout($order_id) :\Illuminate\Http\RedirectResponse
    {
        try {

            $order = Order::findOrFail(decrypt($order_id));

            $order->payment_status = Order::PAYMENT_PENDING;
            $order->method = Order::PAYPAL;
            $order->save();

            $paypal = new PayPal();

            $response = $paypal->purchase([
                'amount' => $paypal->formatAmount($order->amount),
                'transactionId' => $order->id,
                'currency' => 'EUR',
                'cancelUrl' => $paypal->getCancelUrl($order),
                'returnUrl' => $paypal->getReturnUrl($order),
            ]);

            $order->storeRawData($response->getData());

            if ($response->isRedirect()) {

                $response->redirect();

            } else {

                $order->fail( ['error' => 'no redirect after purchase data was set'], $response->getMessage() );

            }

        } catch ( \Exception $e ) {

            $order->fail( ['error' => 'before payment'], $e->getMessage() );

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     * @return mixed
     */
    public function completed($order_id, Request $request)
    {
        $order = Order::findOrFail($order_id);

        $paypal = new PayPal;

        $response = $paypal->complete([
            'amount' => $paypal->formatAmount($order->amount),
            'transactionId' => $order->id,
            'currency' => 'EUR',
            'cancelUrl' => $paypal->getCancelUrl($order),
            'returnUrl' => $paypal->getReturnUrl($order),
            'notifyUrl' => $paypal->getNotifyUrl($order),
        ]);

        if ($response->isSuccessful()) {

            $order->complete($response->getTransactionReference());

            return redirect()->route('home', encrypt($order_id))->with([
                'message' => 'You recent payment is sucessful with reference code ' . $response->getTransactionReference(),
            ]);
        } else {

            $order->fail( $response->getData(), $response->getMessage() );

        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);
    }

    /**
     * @param $order_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelled($order_id) :\Illuminate\Http\RedirectResponse
    {
        try {

            $order = Order::findOrFail($order_id);

            $order->cancel();

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $order->product->request->uuid, 'quote' => $order->product])->with([
            'message' => 'You have cancelled your recent PayPal payment !',
        ]);

    }

    /**
     * @param $order_id
     * @param $env
     */
    public function webhook($order_id, $env)
    {

    }

}
