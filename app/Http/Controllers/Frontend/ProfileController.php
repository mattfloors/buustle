<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Models\Quote;
use App\Models\RentRequest;
use App\Models\Travel;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends FrontendBaseController
{
    public function __construct() {

        parent::__construct();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {

        return view('frontend.profile.edit', ['user' => Auth::user()]);

    }

    public function transactions()
    {

        $orders = Order::where('user_id', '=', Auth::user()->id)->get();

        return view('frontend.profile.transactions', ['orders' => $orders]);

    }

    public function saveChangeImage(string $avatar)
    {
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $avatar));
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
        file_put_contents($tmpFilePath, $fileData);
        return $tmpFilePath;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store( Request $request )
    {

        try {
            $validatedData = $request->validate([
                'first_name' => ['required', 'string', 'min:1', 'max:128'],
                'last_name' => ['required', 'string', 'min:1', 'max:128'],
                'phone' => ['sometimes', 'nullable', 'string', 'min:6', 'max:24'],
                'address' => ['sometimes', 'nullable', 'string', 'min:6', 'max:128'],
            ]);
            $profile = UserProfile::firstOrNew(['user_id' => Auth::user()->id]);

            $img = $request->input('avatar');
            $avatar = is_string($img) ? $this->saveChangeImage($img) : $img;

            $profile->user_id = Auth::user()->id;
            $profile->first_name = $validatedData['first_name'];
            $profile->last_name = $validatedData['last_name'];
            $profile->phone = $validatedData['phone'];
            $profile->address = $validatedData['address'];

            $profile->save();

            if ($avatar) {
                Auth::user()->addMedia($avatar)->toMediaCollection('avatar');
            }

        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getmessage());

        }

        return redirect()->route('profile.requests')->with('success', __('profile.edit_successful'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function requests()
    {

        $requests = RentRequest::where('user_id', '=', Auth::id())
                               ->whereIn('status', [RentRequest::STATUS_COMPLETED, RentRequest::STATUS_CLOSED, RentRequest::STATUS_SOLD])
                               ->with('requestSteps')
                               ->orderBy('id', 'DESC')
                               ->get();

        return view('frontend.profile.requests', ['requests' => $requests, 'userAvatar' => Auth::user()->getFirstMedia('avatar')]);

    }

    public function viewTravel( string $rentRequestUuid )
    {

        try {

            $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->first();

            if ( !$rentRequest->hasPayedQuote() ) {

                throw new \Exception('profile.invalid_travel_no_quote_payed');

            }

            if ( !$quote = $rentRequest->payedQuote() ) {

                throw new \Exception('profile.invalid_travel_quote_not_found');

            }

            if ( !$travel = Travel::where('quote_id', '=', $quote->id)->first() ) {

                throw new \Exception('profile.invalid_travel_no_travel_found');

            }

        } catch ( \Exception $e ) {

            return redirect()->back()->withErrors($e->getMessage());

        }

        return view('frontend.profile.travel-single', ['travel' => $travel, 'quote' => $quote, 'rentRequest' => $rentRequest, 'userAvatar' => Auth::user()->getFirstMedia('avatar')]);

    }

    /**
     * @param string $rentRequestUuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function requestQuotes( string $rentRequestUuid )
    {

        $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->first();

        if ( count($rentRequest->quotes) < 1 ) {

            return redirect()->back()->with('errors', __('profile.rent_requests_no_associated_quotes'));

        }

        if( count($rentRequest->quotes) === 1 ) {

            return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $rentRequestUuid, 'quote' => $rentRequest->quotes[0]]);

        }

        return view('frontend.profile.request-quotes', ['rentRequest' =>  $rentRequest]);

    }

    /**
     * @param string $rentRequestUuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewRequest( string $rentRequestUuid )
    {

        $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->first();

        return view('frontend.profile.request-view', ['rentRequest' =>  $rentRequest]);

    }

    /**
     * @param string $rentRequestUuid
     * @param Quote $quote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function requestSingleQuote( string $rentRequestUuid, Quote $quote )
    {

        $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->first();

        return view('frontend.profile.request-single-quote', ['request' =>  $rentRequest, 'quote' => $quote]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteRequest(Request $request)
    {

        $rentRequest = RentRequest::where('uuid', '=', $request->input('requestUuid'))->first();

        if ( $rentRequest->status === RentRequest::STATUS_COMPLETED || $rentRequest->status === RentRequest::STATUS_DRAFT ) {

            try {

                $rentRequest->delete();

                return redirect()->back()->with('success', __('profile.rent_request_deleted'));

            } catch (\Exception $e) {

                return redirect()->back()->withErrors('errors', __('profile.rent_request_delete_failed'));

            }

        }

        return redirect()->back()->with('errors', __('profile.rent_request_delete_failed'));
    }

    /**
     * @param string $rentRequestUuid
     * @param Quote $quote
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Gabievi\Promocodes\Exceptions\AlreadyUsedException
     */
    public function acceptQuote( string $rentRequestUuid, Quote $quote, Request $request )
    {

        $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->first();

        if ( $quote->status !== Quote::STATUS_COMPLETED && $quote->status !== Quote::STATUS_ACCEPTED && $quote->status ) {

            return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $rentRequestUuid, 'quote' => $quote])->withErrors(__('quote.quote_status_not_valid'));

        }

        if ( $oldOrder = Order::where('product_type', '=', 'App\Models\Quote')->where('product_id', '=', $quote->id)->first() ) {

            if ( $oldOrder->payment_status !== Order::PAYMENT_CREATED ) {

                return redirect()->route('profile.requestSingleQuote', ['rentRequestUuid' => $rentRequestUuid, 'quote' => $quote])->withErrors(__('profile.order_payment_processing'));

            }

            $order = $oldOrder;

        } else {

            $order = new Order();

        }

        $order->user_id = Auth::id();
        $order->amount = $quote->price;
        $order->payment_status = Order::PAYMENT_CREATED;
        $order->method = ( $request->input('payment_type') && in_array($request->input('payment_type'), $order->methods(), true) ) ? $request->input('payment_type') : Order::BONIFICO;

        $order->product()->associate($quote);

        $order->save();

        $quote->accept();

        if ( $request->input('code') !== null ) {

            $user = Auth::user();

            $user->applyCode($request->input('code'), function ( $promocode ) use ( $user, $order ) {

                $order->applyPromoCode( $promocode );

            });

        }

        return view('frontend.profile.accept-quote', ['order' => $order, 'rentRequest' =>  $rentRequest, 'quote' => $quote]);

    }

}
