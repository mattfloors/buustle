<?php

namespace App\Http\Controllers\Frontend;

use App\Events\QuoteAssigned;
use App\Http\Requests\TravelFileUploads;
use App\Models\Order;
use App\Models\Quote;
use App\Models\Region;
use App\Models\RentRequest;
use App\Models\Retailer;
use App\Models\Travel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Spatie\MediaLibrary\Models\Media;

class PartnerController extends FrontendBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        $travels = Auth::user()->getPartnerRequests();

        return view('frontend.partner.index', ['travels' => $travels]);
    }

    public function edit()
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        $regions = Region::all();

        return view('frontend.partner.edit', ['regions' => $regions]);

    }

    public function store( Request $request )
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        $validatedData = $request->validate([
            'partita_iva'           => 'nullable|string',
            'paypal_account'        => 'nullable|string',
            'stripe_account'        => 'nullable|string',
            'regions'               => 'nullable|array',
            'regions.*'             => 'numeric|exists:regions,id',
            'name'                  => 'required|string|max:164',
            'description'           => 'sometimes|string|nullable|max:2048',
            'phone'                 => 'sometimes|string|nullable|max:64',
            'address'               => 'sometimes|string|nullable|max:64',
            'logo'                  => ['sometimes', 'image', 'max:2048']
        ]);

        try {

            $user = User::findOrFail( Auth::user()->id );

            $profile = $user->noleggiatoreProfile;

            $profile->user_id        = Auth::user()->id;
            $profile->partita_iva    = $validatedData['partita_iva'];
            $profile->paypal_account = $validatedData['paypal_account'];
            $profile->stripe_account = $validatedData['stripe_account'];
            $profile->phone          = $validatedData['phone'];
            $profile->address        = $validatedData['address'];

            $profile->save();

            $retailer = Retailer::where('id', '=', $profile->retailer_id)->firstOrFail();

            $retailer->setTranslation('name', App::getLocale(), $validatedData['name']);
            $retailer->setTranslation('description', App::getLocale(), $validatedData['description']);

            $retailer->save();

            if( array_key_exists('regions', $validatedData) && count($validatedData['regions']) > 0 ) {

                $user->regions()->sync($validatedData['regions']);

            } else {

                $user->regions()->sync([]);

            }


            if ( !empty($validatedData['logo']) ) {

                $profile->addMedia($validatedData['logo'])->toMediaCollection('logo');

            }

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        return redirect()->back()->with('success', 'Noleggiatore salvato con Successo');

    }

    public function opportunity( Travel $travel )
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        return view('frontend.partner.opportunities.single', ['travel' => $travel]);

    }

    public function takeOpportunity( Travel $travel )
    {

        $user = Auth::user();

        if ( !$user->isValidNoleggiatore() ) {

            return redirect()->back()->with('errors', __('partner.retailer_inactive'));

        }

        if ( $travel->retailer_id === null ) {

            $travel->retailer_id = $user->noleggiatoreProfile->retailer->id;

            $travel->save();

            event( new QuoteAssigned($travel) );

        }

        return redirect()->route('partner.ownedQuote', ['travel' => $travel])->with('success', __('partner.travel_owned'));

    }

    public function ownedTravels()
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        $travels = Auth::user()->getPartnerRequests(true);

        return view('frontend.partner.opportunities.owned', ['travels' => $travels]);

    }

    public function ownedTravel( Travel $travel )
    {

        if ( !$this->shareNoleggiatore() ) {

            return redirect()->route('home')->withErrors(__('partner.not_valid'));

        }

        return view('frontend.partner.opportunities.owned-single', ['travel' => $travel]);

    }

    public function ownedTravelStore( Travel $travel, Request $request )
    {

        try {

            $v = Validator::make($request->all(),[
                'targa_mezzo' => ['sometimes', 'nullable', 'string', 'max:164'],
                'telefono_autista' => ['sometimes', 'nullable', 'string', 'max:164']
            ]);

            $v->validate();

            $travel->telefono_autista = $request->input('telefono_autista');
            $travel->targa_mezzo = $request->input('targa_mezzo');

            $travel->save();

            return redirect()->back()->with('success', __('partner.travel_edited'));

        } catch ( \Exception $e ) {

            return redirect()->back()->withErrors($e->getmessage());

        }

    }

    public function fileUploads( Travel $travel, TravelFileUploads $request )
    {

        try {

            $folder = $request->input('file_upload_folder') . '_files';

            foreach( $request->file('files') as $file ) {

                $travel->addMedia($file)->toMediaCollection($folder);

            }

        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getmessage());

        }

        return redirect()->back()->with('success', count($request->file('files')) . ' File Caricati correttamente nella cartella ' . $request->input('file_upload_folder'));

    }

    public function deleteAttachment( Travel $travel, Media $media )
    {

        try {

            $media->delete();

        } catch ( \Exception $e ) {

            return redirect()->back()->withErrors($e->getmessage());

        }

        return redirect()->back()->with('success', 'File cancellato con successo');

    }

    /**
     * @return bool
     */
    protected function shareNoleggiatore() :bool
    {

        if ( !Auth::user()->isValidNoleggiatore() ) {

            return false;

        }

        View::share('noleggiatore', Auth::user()->noleggiatoreProfile);

        return true;

    }
}
