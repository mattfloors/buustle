<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\RentFirstStep;
use App\Http\Requests\RentLastStep;
use App\Http\Services\Maps;
use App\Http\Services\RentalPrice;
use App\Models\Package;
use App\Models\PeopleRange;
use App\Models\RentRequest;
use App\Models\RequestStep;
use App\Models\Service;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\RedirectResponse;

class RentController extends FrontendBaseController
{

    protected static $allowedFormType = ['a', 'r'];

    /**
     * Step 1: routes and people range
     *
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function request( $type = 'a' )
    {

        if ( !$type || !in_array($type, self::$allowedFormType, true) ) {

            $type = 'a';

        }

        $peopleRanges = PeopleRange::orderBy('min', 'ASC')->get();

        return view('frontend.rent.request-' . $type, ['peopleRanges' => $peopleRanges, 'type' => $type]);

    }

    /**
     * Saving request and redirecting towards step 2
     *
     * @param RentFirstStep $request
     * @return RedirectResponse
     */
    public function saveRequest( RentFirstStep $request ) :RedirectResponse
    {

        try {

            if( (string) $request->input('people') == "more" ) {

                Alert::warning(__('rent.more_people'), __('rent.contact_us_more_people'))->flash();

                return redirect()->route('contactUs');

            }

            if ( count($request->input('w')) !== count($request->input('lat_w')) || count($request->input('w')) !== count($request->input('lng_w')) ) {

                throw new \Exception(__('rent.validation_count_mismatch'));

            }

            $rentRequest = new RentRequest();
            $rentRequest->user_id = Auth::id();
            $rentRequest->people = $request->input('people');
            $rentRequest->kilometers = $request->input('km_tratta');
            $rentRequest->sharable = false;
            $rentRequest->session_id = Session::getId();
            $rentRequest->status = RentRequest::STATUS_DRAFT;

            $rentRequest->save();

            $totSteps = count( $request->input('w') );

            for ( $i = 0; $i < ($totSteps - 1); $i++ ) {

                if ( !empty($request->input('lng_w')[$i]) && !empty($request->input('lat_w')[$i]) && !empty($request->input('w')[$i]) ) {

                    $step = new RequestStep();
                    $step->request_id = $rentRequest->id;
                    $step->start_address = $request->input('w')[$i];
                    $step->start_lat = $request->input('lat_w')[$i];
                    $step->start_lng = $request->input('lng_w')[$i];
                    $step->end_address = $request->input('w')[$i+1];
                    $step->end_lat = $request->input('lat_w')[$i+1];
                    $step->end_lng = $request->input('lng_w')[$i+1];

                    $step->save();

                }

            }

            if ( $request->input('type') === 'r' ) {

                $step = new RequestStep();
                $step->request_id = $rentRequest->id;
                $step->start_address = $request->input('w')[$totSteps - 1];
                $step->start_lat = $request->input('lat_w')[$totSteps - 1];
                $step->start_lng = $request->input('lng_w')[$totSteps - 1];
                $step->end_address = $request->input('w')[0];
                $step->end_lat = $request->input('lat_w')[0];
                $step->end_lng = $request->input('lng_w')[0];

                $step->save();

                $rentRequest->kilometers += Maps::loadManualDistanceFloat( $step->start_lat, $step->start_lng, $step->end_lat, $step->end_lng);

                $rentRequest->save();

            }

        } catch ( \Exception $e ) {

            return redirect()->back()->withErrors(__('rent.request_error : ' . $e->getMessage()));

        }

        return redirect()->route('rent.noleggi.offer', ['rentRequestUuid' => $rentRequest->uuid])->with('success', __('rent.offer_successfully_generated'));

    }

    /**
     * Step 2: sharable
     *
     * @param string $rentRequestUuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateOffer( string $rentRequestUuid )
    {
        $rentRequest = $this->loadAndCheckRequestFromUuid( $rentRequestUuid );

        try {

            $kmTotali = $rentRequest->kilometers + $rentRequest->addBackKm();
            $peopleRange = PeopleRange::findOrFail($rentRequest->people);

            $rentalPriceService = new RentalPrice();
            $rentalPriceService->setKm( $kmTotali );
            $rentalPriceService->setPeopleRange( $peopleRange );

            $suggestedPrice = $rentalPriceService->calculatePartialPrice();

        } catch ( \Exception $e ) {

            dd($e->getMessage());

        }

        return view('frontend.rent.step-2', ['peopleRange' => $peopleRange, 'rentRequest' => $rentRequest, 'suggestedPrice' => $suggestedPrice]);

    }

    /**
     * Step 3: dates, comment and offers
     *
     * @param string $rentRequestUuid
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\View\View
     */
    public function rentDetails( string $rentRequestUuid, Request $request )
    {

        $rentRequest = $this->loadAndCheckRequestFromUuid( $rentRequestUuid );

        if ( !Auth::check() ) {

            $rules = [
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'email' => ['required', 'email', 'unique:users,email'],
                'phone' => ['required', 'string'],
                'password' => ['required',
                    'min:8',
                    'confirmed']
            ];

            $validator = Validator::make($request->all(), $rules);

            if ( $validator->fails() ) {

                return redirect()->back()->with('errors', $validator->messages());

            }

            $user = new User();
            $user->name = $request->input('first_name');
            $user->password = Hash::make($request->input('password'));
            $user->email = $request->input('email');
            $user->save();

            $profile = new UserProfile();

            $profile->user_id = $user->id;
            $profile->first_name = $request->input('first_name');
            $profile->last_name = $request->input('last_name');
            $profile->phone = $request->input('phone');

            $profile->save();

            $rentRequest->user_id = $user->id;

            Auth::login($user, true);

            Alert::success(__('auth.register_success'));

        }

        if ( Auth::guest() ) {

            return redirect()->back()->with('errors', __('rent.confirm_unauthorized'));

        }
        $rentRequest->contact_email = $request->input('email');
        if( $request->input('sharable') ) {

            $validatedData = $request->validate([
                'sharable' => 'required|boolean'
            ]);

            try {

                $rentRequest->sharable = $validatedData['sharable'];

                $rentRequest->save();

            } catch ( \Exception $e ) {

                return redirect()->back()->with('errors', $e->getMessage());

            }

        }

        //try {

            $packages = Package::where('active', '=', true)->get();
            $peopleRange = PeopleRange::findOrFail($rentRequest->people);
            $services = Service::where('active', '=', 1)->get();

            if ( !empty( $packages ) ) {

                $rentalPrice = new RentalPrice();

                $rentalPrice->setKm( $rentRequest->kilometers + $rentRequest->addBackKm() );
                $rentalPrice->setPeopleRange( $peopleRange );
                $rentalPrice->setShared( $rentRequest->sharable );

            $rentRequest->save();

            foreach ( $packages as $package ) {

                    $rentalPrice->setPackage( $package );

                    $package->predictedPrice = $rentalPrice->calculatePartialPrice();

                }

            }

        /*} catch (\Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());

        }*/
        $isWayBack = false;
        $startAddress = $rentRequest->getFirstStepById()->start_address;
        $lastAddress = $rentRequest->getLastStepById()->end_address;
        if ($startAddress === $lastAddress) {
            $isWayBack = true;
        }

        return view('frontend.rent.request-a-3', ['peopleRange' => $peopleRange, 'rentRequest' => $rentRequest, 'isWayBack' => $isWayBack, 'packages' => $packages, 'services' => $services]);

    }

    /**
     * Saving data and redirecting for user informations
     *
     * @param string $rentRequestUuid
     * @param RentLastStep $request
     * @return RedirectResponse
     */
    public function finalStep( string $rentRequestUuid, RentLastStep $request ) :RedirectResponse
    {

        $rentRequest = $this->loadAndCheckRequestFromUuid( $rentRequestUuid );

        try {

            $package = Package::findOrFail( $request->input('package') );

            $services = $request->input('services-' . $request->input('package'));

            $totW = count($request->input('w'));
            $totLat = count($request->input('lat_w'));
            $totLng = count($request->input('lng_w'));
            $totDates = count($request->input('start-date'));
            $startAddress = $rentRequest->getFirstStepById()->start_address;
            $lastAddress = $rentRequest->getLastStepById()->end_address;
            if ($startAddress === $lastAddress) {
                $totDates--;
            }
            if ( $totW !== $totLat || $totLat !== $totLng || $totLng !== ($totDates + 1) ) {

                throw new \Exception(__('rent.tot_step_and_dates_mismatch'));

            }

            RequestStep::where('request_id', '=', $rentRequest->id)->delete();

            $totKm = 0;

            /** For all But last step we create a travel */
            for ( $i = 0; $i < ($totW - 1); $i++ ) {

                $step = new RequestStep();
                $step->request_id = $rentRequest->id;
                $step->start_address = $request->input('w')[$i];
                $step->start_time = Carbon::createFromFormat('d/m/Y H:i', $request->input('start-date')[$i] . ' ' . $request->input('start-time')[$i])->format('Y-m-d h:i:s');
                $step->start_lat = $request->input('lat_w')[$i];
                $step->start_lng = $request->input('lng_w')[$i];
                $step->end_address = $request->input('w')[$i+1];
                $step->end_lat = $request->input('lat_w')[$i+1];
                $step->end_lng = $request->input('lng_w')[$i+1];

                $step->save();

                $totKm += Maps::loadManualDistanceFloat( $step->start_lat, $step->start_lng, $step->end_lat, $step->end_lng );

            }

            $rentRequest->kilometers = $totKm;
            $rentRequest->save();

            $rentRequest = $rentRequest->fresh();
            $rentalPrice = new RentalPrice();

            $rentalPrice->setKm( $rentRequest->kilometers + $rentRequest->addBackKm() );
            $rentalPrice->setPeopleRangeFromPeople( $request->input('people') );
            $rentalPrice->setShared( $rentRequest->sharable );
            $rentalPrice->setPackage( $package );
            $rentalPrice->setRentRequest( $rentRequest );

            $price = $rentalPrice->setDaysOfTravel($rentRequest->totalDays())->setKmPerDay()->calculatePrice( $request->input('people') );

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', $e->getMessage());

        }

        $rentRequest->people = $request->input('people');
        $rentRequest->kilometers = $totKm;
        $rentRequest->comment = $request->input('comment') ?? '';
        $rentRequest->package_id = $package->id;
        $rentRequest->accepted_price = $price;
        $rentRequest->travel_type = $request->input('travel_type');

        $rentRequest->save();

        $rentRequest->services()->sync($services ?? []);

        return redirect()->route('rent.noleggi.confirm', ['rentRequestUuid' => $rentRequest->uuid])->with('success', __('rent.offer_generated_successfully'));

    }

    /**
     * Step 4: recap and user login
     *
     * @param string $rentRequestUuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm( string $rentRequestUuid )
    {

        $rentRequest = $this->loadAndCheckRequestFromUuid( $rentRequestUuid );
        $steps = $rentRequest->requestSteps()->get();
        $startAddress = $rentRequest->getFirstStep()->start_address;
        $lastStep = $rentRequest->getLastStep();
        $isWayBack = false;
        if (null !== $lastStep) {
            $lastAddress = $lastStep->end_address;
            $lastLat = $lastStep->end_lat;
            $lastLon = $lastStep->end_lng;
            $lastTime = $lastStep->end_time;
            if ($startAddress === $lastAddress) {
                $isWayBack = true;
                $lastAddress = $lastStep->start_address;
                $lastLat = $lastStep->start_lat;
                $lastLon = $lastStep->start_lng;
                $lastTime = $lastStep->start_time;
            }
        }

        $data = [
            'rentRequest' => $rentRequest,
            'steps' => $steps,
            'isWayBack' => $isWayBack,
            'startAddress' => $startAddress,
            'lastAddress' => $lastAddress ?? '',
            'lastTime' => $lastTime ?? '',
            'lastLat' => $lastLat ?? null,
            'lastLon' => $lastLon ?? null
        ];

        return view('frontend.rent.confirm', $data);

    }

    public function complete( string $rentRequestUuid, Request $request )
    {

        $rentRequest = $this->loadAndCheckRequestFromUuid( $rentRequestUuid );

        try {

            if( $rentRequest->status !== RentRequest::STATUS_DRAFT ) {

                throw new \Exception ( __('rent.request_already_confirmed') );

            }

            $validatedData = $request->validate([
                'nome'  => 'required|string|max:128|min:1',
                'email' => 'required|string|email'
            ]);

            $rentRequest->user_id = Auth::id();
            $rentRequest->status = RentRequest::STATUS_COMPLETED;
            $rentRequest->contact_name = $validatedData['nome'];
            $rentRequest->contact_email = $validatedData['email'];
            $rentRequest->save();

            return view('frontend.rent.thank-you', ['rentRequestUuid' => $rentRequest->uuid]);

        } catch ( \Exception $e ) {

            return redirect()->back()->with('errors', __('rent.confirm_error'));

        }

    }

    public function loadPartialPrice( Request $request )
    {

        $people = $request->input('people');
        $km = $request->input('km');
        $end_lat = $request->input('end_lat');
        $end_lng = $request->input('end_lng');
        $start_lat = $request->input('start_lat');
        $start_lng = $request->input('start_lng');
        $backKm = Maps::loadManualDistanceFloat($end_lat, $end_lng, $start_lat, $start_lng);

        if( !$people || !$km || !$peopleRange = PeopleRange::find($people) ) {

            return response()->json([
                'status' => 'error',
                'message' => 'missing Data'
            ]);

        }

        $rentalPrice = new RentalPrice();
        $value = $rentalPrice->setKm( $km + $backKm )->setPeopleRange( $peopleRange )->calculatePartialPrice();

        return response()->json([
            'status' => 'success',
            'value' => $value,
            'valueRounded' => round($value, 2),
        ]);

    }

    /**
     * @param string $rentRequestUuid
     * @return RentRequest
     */
    protected function loadAndCheckRequestFromUuid( string $rentRequestUuid ) :RentRequest
    {

        try {

            $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->firstOrFail();

            Session::put('request_id', $rentRequestUuid);

        } catch (\Exception $e) {

            abort(404);

        }
        return $this->loadAndCheckRequest( $rentRequest );


    }

    /**
     * @param RentRequest $rentRequest
     * @return RentRequest
     */
    protected function loadAndCheckRequest( RentRequest $rentRequest ) :RentRequest
    {

        if ( ($rentRequest->user_id !== Auth::id() && $rentRequest->session_id !== Session::getId()) || $rentRequest->status !== 'draft' ) {

            abort( 404 );

        }

        return $rentRequest;

    }

    /**
     * @param string $data
     * @param string $time
     * @return string
     */
    protected function generateTime( string $data, string $time ) :string
    {

        $dateString = $data . ' ' . $time;
        return Carbon::parse($dateString)->format('Y-m-d H:i:s');

    }
}
