<?php

namespace App\Http\Middleware;

use App\Models\Quote;
use App\Models\Travel;
use Closure;

class NoleggiatoreQuote
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !$request->user()->can('see partner') ) {

            return redirect()->to('login')->with('errors', __('auth.failed'));

        }

        if ( !$travel = $request->route('travel') ) {

            return redirect()->back()->with('errors', __('partner.travel_not_found'));

        }

        if( $travel->retailer_id !== null && $travel->retailer_id !== $request->user()->noleggiatoreProfile->retailer_id ) {

            return redirect()->back()->with('errors', __('partner.travel_not_yours'));

        }

        return $next($request);
    }
}
