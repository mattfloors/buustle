<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckNofitications
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('from')) {

            if ($request->input('from') === 'notification' && Auth::check()) {

                $user = Auth::user();

                $user->unreadNotifications->markAsRead();

            }

        }

        return $next($request);
    }
}
