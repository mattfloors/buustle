<?php

namespace App\Http\Middleware;

use Closure;

class CheckDownload
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {

        if ( $type === 'travel' ) {

            // Admin Permissions
            if ( $request->user()->can('edit travels') ) {

                return $next($request);

            }

            if ( !$travel = $request->route('travel') ) {

                return redirect()->back()->withErrors(__('downloads.travel_not_found'));

            }

            //retailer permissions
            if ( $travel->retailer_id !== null && !empty($request->user()->noleggiatoreProfile) && $travel->retailer_id === $request->user()->noleggiatoreProfile->retailer_id ) {

                return $next($request);

            }

            //user permissions (only public)
            if ( $travel->user_id === $request->user()->id && $request->route('media')->collection_name === 'public_files') {

                return $next($request);

            }

            return redirect()->back()->withErrors(__('downloads.not_allowed'));

        } else {

            return redirect()->back()->withErrors(__('downloads.type_undefined'));

        }

    }
}
