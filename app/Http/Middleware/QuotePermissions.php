<?php

namespace App\Http\Middleware;

use App\Models\RentRequest;
use Closure;
use Illuminate\Support\Facades\Auth;

class QuotePermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $uuid = null;
        if ( array_key_exists('rentRequestUuid', $request->route()->parameters() ) ) {

            $uuid = $request->route()->parameters()['rentRequestUuid'];

        } elseif ( $request->input('rentRequestUuid') ) {

            $uuid = $request->input('rentRequestUuid');

        }

        if ( $uuid === null && !Auth::user()->can('see all quotes') ) {

            return redirect()->back()->with('errors', __('profile.cant_see_this_request'));

        }

        if ( !$this->validateRequestFromUuid( $uuid ) ) {

            return redirect()->back()->with('errors', __('profile.cant_see_this_request'));

        }

        return $next($request);
    }

    protected function validateRequestFromUuid( string $rentRequestUuid )
    {
        try {

            $rentRequest = RentRequest::where('uuid', '=', $rentRequestUuid)->firstOrFail();

            if ( !$this->validateRequest( $rentRequest ) ) {

                throw new \Exception( __('profile.not_allowed') );

            }

        } catch (\Exception $e) {

            return false;

        }

        return $this->validateRequest( $rentRequest );

    }

    protected function validateRequest( RentRequest $rentRequest )
    {

        if ( !Auth::check() || $rentRequest->user_id !== Auth::id() ) {

            return false;

        }

        return true;

    }
}
