<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class MultiLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = $request->input('lang');

        if ( $language === null || !in_array( $language, config('translatable.app_locales') ) ) {

            $language = App::getLocale();

        }

        $request->attributes->add(['language' => $language]);

        return $next($request);
    }
}
