<?php

namespace App\Http\Middleware;

use Closure;

class FromErrorToErrors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->session()->has('errors') && !$request->session()->has('error') ) {

            $error = $request->session()->get('errors');

            if (!is_string($error)) {
                $error = $this->getErrors($error->getMessages());
            }

            alert()->error($error)->toHtml();

        }

        return $next($request);
    }

    /**
     *
     *
     * @param $errors
     *
     * @return string
     */
    private function getErrors($errors)
    {
        $errors = collect($errors);

        return $errors->flatten()->implode('<br />');
    }
}
