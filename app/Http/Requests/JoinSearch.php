<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JoinSearch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_lat' => [
                'sometimes',
                'nullable',
                'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'
            ],
            'start_lng' => [
                'sometimes',
                'nullable',
                'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'
            ],
            'end_lat' => [
                'sometimes',
                'nullable',
                'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'
            ],
            'end_lng' => [
                'sometimes',
                'nullable',
                'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'
            ],
            'start_date' => [
                'sometimes',
                'nullable',
                'date_format:d/m/Y'
            ],
            'people' => [
                'sometimes',
                'nullable',
                'numeric',
                'min:1'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_lat.regex' => __('join.validator_start_lat_not_valid_format'),
            'start_lng.regex' => __('join.validator_start_lng_not_valid_format'),
            'end_lat.regex' => __('join.validator_end_lat_not_valid_format'),
            'end_lng.regex' => __('join.validator_end_lng_not_valid_format'),
            'start_date.date_format' => __('join.validator_start_date_not_valid_format'),
        ];
    }
}
