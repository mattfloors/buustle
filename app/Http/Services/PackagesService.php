<?php

namespace App\Http\Services;

use App\Models\Package;
use App\Models\PeopleRange;
use App\Models\RentRequest;

class PackagesService
{

    protected $km = 0;

    protected $people = 10;

    protected $selectedPackage;

    protected $onlyActive = false;

    protected $withPrices = true;

    protected $startDate;

    protected $endDate;

    protected $prediction = false;

    /**
     * @return mixed
     * @throws \Exception
     */
    public function loadAvailablePackages()
    {

        if ( $this->onlyActive ) {

            $results = Package::where('active', '=', true)->get();

        } else {

            $results = Package::all();

        }

        if ( $this->withPrices && !empty($results) ) {

            foreach( $results as $result ) {

                if( $this->isPrediction() ) {

                    $result->price = $this->loadPartialPricesForPackage( $result );

                } else {

                    $result->price = $this->loadPricesForPackage( $result );

                }

            }

        }

        return $results;

    }

    /**
     * @param Package $package
     * @return mixed
     * @throws \Exception
     */
    public function loadPartialPricesForPackage( Package $package )
    {

        $class = $package->loadClass();

        $class->setMultiplier( $package->multiplier )->setKm( $this->getKm() )->setPeopleRange( PeopleRange::findOrFail($this->getPeople()) );

        return $class->calculatePartialPrice();

    }

    /**
     * @param Package $package
     * @return mixed
     * @throws \Exception
     */
    public function loadPricesForPackage( Package $package )
    {

        $class = $package->loadClass();

        $class->setStartDate( $this->getStartDate() )->setEndDate( $this->getEndDate() )->setPeople( $this->getPeople() )->setKm( $this->getKm() );

        return $class->calculatePrice();

    }

    /**
     * @param RentRequest $rentRequest
     * @return $this
     */
    public function setDataFromRequest( RentRequest $rentRequest ): self
    {

        $this->km = $rentRequest->kilometers ?? 0;
        $this->people = $rentRequest->people ?? 0;

        return $this;

    }

    /**
     * @return PackagesService
     */
    public function withPrices() :self
    {

        $this->withPrices = true;

        return $this;

    }

    /**
     * @return float
     */
    public function getKm(): float
    {
        return $this->km;
    }

    /**
     * @param float $km
     */
    public function setKm(float $km): void
    {
        $this->km = $km;
    }

    /**
     * @return int
     */
    public function getPeople(): int
    {
        return $this->people;
    }

    /**
     * @param int $people
     */
    public function setPeople(int $people): void
    {
        $this->people = $people;
    }

    /**
     * @return int
     */
    public function getOnlyActive(): int
    {
        return $this->onlyActive;
    }

    /**
     * @param int $onlyActive
     */
    public function setOnlyActive(int $onlyActive): void
    {
        $this->onlyActive = $onlyActive;
    }

    /**
     * @return string
     */
    public function getStartDate() :string
    {
        return $this->startDate ?? '';
    }

    /**
     * @param string $startDate
     */
    public function setStartDate( string $startDate ): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return string
     */
    public function getEndDate() :string
    {
        return $this->endDate ?? '';
    }

    /**
     * @param string $endDate
     */
    public function setEndDate( string $endDate ): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return bool
     */
    public function isPrediction(): bool
    {
        return $this->prediction;
    }

    /**
     * @param bool $prediction
     * @return PackagesService
     */
    public function setPrediction(bool $prediction): self
    {
        $this->prediction = $prediction;

        return $this;
    }

}