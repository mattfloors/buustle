<?php

namespace App\Http\Services;

class Maps
{

    public const API_KEY = 'AIzaSyDvFmEA0NWFv2Celnf_2qPmhAGcQjQReSw';

    public const LANGUAGE = 'it';

    /**
     * @param float $lat
     * @param float $lng
     * @return string|null
     */
    public static function getRegion( float $lat, float $lng ) :?string
    {

        $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lng . '&sensor=false&key=' . self::API_KEY . '&language=' . self::LANGUAGE);

        $decoded = json_decode($content);

        if ( $results = $decoded->results ) {

            if ( count( $results ) > 0 ) {

                foreach( $results as $key => $result ) {

                    if ( is_array($result->types) && $result->types[0] === 'administrative_area_level_1' ) {

                        return $result->address_components[0]->long_name;

                    }

                }

            }

        }

        return null;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return string
     */
    public static function loadCityName(float $lat, float $lng) :string
    {

        $response = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='. $lat . ',' . $lng . '&key=' . self::API_KEY . '&sensor=false&language=' . self::LANGUAGE);

        $decoded = json_decode($response);

        if ( $results = $decoded->results ) {

            if ( count( $results ) > 0 ) {

                foreach( $results as $key => $result ) {

                    if ( is_array($result->types) && $result->types[0] === 'administrative_area_level_3' ) {

                        return $result->address_components[0]->long_name;

                    }

                }

            }

        }

        return '';
    }

    /**
     * @param float $startLat
     * @param float $startLng
     * @param float $endLat
     * @param float $endLng
     * @return mixed
     */
    public static function loadManualDistance(float $startLat, float $startLng, float $endLat, float $endLng)
    {

        $content = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&transit_mode=bus&origins=' . $startLat . ',' . $startLng . '&destinations=' . $endLat . ',' . $endLng . '&key=' . self::API_KEY . '&language=' . self::LANGUAGE);

        return json_decode($content);

    }

    public static function loadManualDistanceFloat(float $startLat, float $startLng, float $endLat, float $endLng) :float
    {

        $decoded = self::loadManualDistance( $startLat, $startLng, $endLat, $endLng);

        if(strtoupper($decoded->status) === 'OK') {

            return (float) $decoded->rows[0]->elements[0]->distance->value / 1000;

        }

        return (float) 0;

    }

}