<?php

namespace App\Services;

use Omnipay\Omnipay;

class Stripe
{

    /**
     * @return mixed
     */
    public function gateway()
    {
        $gateway = Omnipay::create('Stripe');

        $gateway->setApiKey(setting('payments.stripe_test_mode') ? setting('payments.stripe_test_private') : setting('payments.stripe_live_private'));

        return $gateway;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function purchase(array $parameters)
    {
        $response = $this->gateway()
            ->purchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function complete(array $parameters)
    {
        $response = $this->gateway()
            ->completePurchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param $amount
     * @return string
     */
    public function formatAmount($amount) :string
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * @param $order
     * @return string
     */
    public function getCancelUrl($order) :string
    {
        return route('paypal.checkout.cancelled', $order->id);
    }

    /**
     * @param $order
     * @return string
     */
    public function getReturnUrl($order) :string
    {
        return route('paypal.checkout.completed', $order->id);
    }

    /**
     * @param $order
     * @return string
     */
    public function getNotifyUrl($order) :string
    {
        $env = env('STRIPE_TEST_MODE') ? 'sandbox' : 'live';

        return route('webhook.paypal.ipn', [$order->id, $env]);
    }

}