<?php

namespace App\Http\Services;

use App\Exceptions\TravelPriceCalculationException;
use App\Models\Algorithm;
use App\Models\Package;
use App\Models\PeopleRange;
use App\Models\RentRequest;
use Webit\Util\EvalMath\EvalMath;

class RentalPrice
{

    protected $km = 0.00;

    protected $people = 1;

    protected $peopleRange;

    protected $package;

    protected $rentRequest;

    protected $shared = false;

    protected $days_from_start = 1;

    protected $days_of_travel = 1;

    protected $km_per_day;

    public $gmData = [];

    public const PEOPLE = 'people';

    public const KM_TRATTA = 'km_tratta';

    public const DAYS_OF_TRAVEL = 'days_of_travel';

    public const DAYS_FROM_START = 'days_from_start';

    public const COMMISSIONE = 'commission';

    public const RATE = 'rate';

    public const KM_PER_DAY = 'km_per_day';

    public const PEOPLE_RANGE_MULTIPLIER = 'people_range_multiplier';

    public const PEOPLE_RANGE_PREDICT_MULTIPLIER = 'people_range_predict_multiplier';

    public const PEOPLE_RANGE_PREDICT_COEFF = 'coeff_predizione';

    public const PACKAGE_MULTIPLIER = 'package_multiplier';

    protected $em;

    /**
     * RentalPrice constructor.
     */
    public function __construct()
    {

        $this->em = new EvalMath();

    }

    /**
     * @return array
     */
    public static function variables() :array
    {

        return [
            self::PEOPLE,
            self::KM_TRATTA,
            self::DAYS_OF_TRAVEL,
            self::DAYS_FROM_START,
            self::COMMISSIONE,
            self::RATE,
            self::KM_PER_DAY,
            self::PEOPLE_RANGE_MULTIPLIER,
            self::PACKAGE_MULTIPLIER
        ];

    }

    /**
     * @param float $startLat
     * @param float $startLng
     * @param float $endLat
     * @param float $endLng
     * @return float
     */
    public function loadManualDistance(float $startLat, float $startLng, float $endLat, float $endLng) :float
    {

        $decoded = Maps::loadManualDistance( $startLat, $startLng, $endLat, $endLng);

        if(strtoupper($decoded->status) === 'OK') {

            $this->gmData = $decoded->rows[0]->elements[0];

            return (float) $decoded->rows[0]->elements[0]->distance->value / 1000;

        }

        return (float) 0;

    }

    /**
     * @param null $travelers
     * @return float
     * @throws TravelPriceCalculationException
     * @throws \Exception
     */
    public function calculatePrice( $travelers = null ) :float
    {
        /** @var float $price */
        $price = 0.00;

        /** Setting travelers as total people if first round */
        if ( $travelers === null ) {

            $travelers = $this->getPeople();

        }

        if ( !$this->isKmValid() ) {

            throw new TravelPriceCalculationException(__('exceptions.price_calc_missing_data'));

        }

        /** If we have more than MAX_PEOPLE we have to do multiple calculations */
        if ( $travelers > setting('general.max_per_bus') ) {

            while ( $travelers > setting('general.max_per_bus') )
            {

                $travelers -= setting('general.max_per_bus');

                $price += $this->calculatePrice( setting('general.max_per_bus') );

            }

        }

        if ( $travelers < (int) setting('general.min_per_bus') ) {

            $travelers = (int) setting('general.min_per_bus');

        }

        /** We have to recalculate the people range for each cycle, it will reset the var in the function */
        $this->setPeopleRangeFromPeople( $travelers );

        $tariffa = $this->calculateTariffa();

        $this->toEv( self::RATE, $tariffa );

        if ( !$this->isPackageValid() ) {

            throw new TravelPriceCalculationException(__('exceptions.price_calculation_no_valid_package'));

        }

        $commissione = $this->calculateCommissione();

        $this->toEv( self::COMMISSIONE, $commissione );

        $priceWithPackage = $this->em->evaluate(Algorithm::finale()->parseCalculation());

        /** General discount if shared */
        if ( $this->isShared() ) {

            $priceWithPackage *= ( ( 100 - setting('general.shared_discount') ) / 100 );

        }

        /** We add to other calculations if needed */
        $price += $priceWithPackage;

        return $price;

    }

    /**
     * @param int|null $travelers
     * @return float
     * @throws TravelPriceCalculationException
     */
    public function calculateTariffa( $travelers = null ) : float
    {

        /** @var float $price */
        $price = 0.00;

        /** Setting travelers as total people if first round */
        if ( $travelers === null ) {

            $travelers = $this->getPeople();

        }

        if ( !$this->isKmValid() ) {

            throw new TravelPriceCalculationException(__('exceptions.price_calc_missing_data'));

        }

        /** If we have more than MAX_PEOPLE we have to do multiple calculations */
        if ( $travelers > setting('general.max_per_bus') ) {

            while ( $travelers > setting('general.max_per_bus') )
            {

                $travelers -= setting('general.max_per_bus');

                $price += $this->calculateTariffa( setting('general.max_per_bus') );

            }

        }

        if ( $travelers < (int) setting('general.min_per_bus') ) {

            $travelers = (int) setting('general.min_per_bus');

        }

        $this->setPeopleRangeFromPeople( $travelers );

        if ( !$this->isPeopleRangeValid() ) {

            throw new TravelPriceCalculationException(__('exceptions.price_calculation_tarif_no_people_range'));

        }

        $price += $this->em->evaluate(Algorithm::tariffa()->parseCalculation());

        return $price;

    }

    /**
     * @return float
     */
    public function calculateCommissione() : float
    {

        return ($this->days_from_start > ( setting('general.days_before_start_limit') ?? 0 ) ) ? ( setting('general.days_before_start_limit_price') ?? 1 ) : $this->em->evaluate(Algorithm::commissione()->parseCalculation());

    }

    /**
     * @return float
     */
    public function calculatePartialPrice() :float
    {

        $predictedPrice = (float) 0;

        if ( $this->isKmValid() && $this->isPeopleRangeValid() ) {

            $predictedPrice = $this->em->evaluate(Algorithm::predizione()->parseCalculation());

        }

        if ( $this->isPackageValid() ) {

            $package = $this->getPackage();

            $predictedPrice *= $package->multiplier;

        }

        if ( $this->isShared() ) {

            $predictedPrice *= ( (100 - setting('general.shared_discount') ) / 100 );

        }

        return $predictedPrice;

    }

    /**
     * @return bool
     */
    protected function isRentRequestValid() :bool
    {

        $rentRequest = $this->rentRequest;

        if ( $rentRequest === null ) {

            return false;

        }

        if ( !($rentRequest instanceof RentRequest) ) {

            return false;

        }

        return true;

    }

    /**
     * @return bool
     */
    protected function isPackageValid() :bool
    {

        $package = $this->getPackage();

        if ( $package === null ) {

            return false;

        }

        if ( !($package instanceof Package) ) {

            return false;

        }

        return true;

    }

    /**
     * @return bool
     */
    protected function isPeopleRangeValid() :bool
    {

        $peopleRange = $this->getPeopleRange();

        if ( $peopleRange === null ) {

            return false;

        }

        if ( !($peopleRange instanceof PeopleRange) ) {

            return false;

        }

        return true;

    }

    /**
     * @return bool
     */
    protected function isKmValid() :bool
    {

        $km = $this->getKm();

        if ( !$km ) {

            return false;

        }

        if ( $km <= 0 ) {

            return false;

        }

        return true;

    }

    /**
     * @param int $people
     * @return bool
     * @throws \Exception
     */
    public function setPeopleRangeFromPeople( int $people ) :bool
    {

        $this->setPeople( $people );

        if ( $people > 0 ) {

            $range = PeopleRange::where('min', '<=', $people)
                                ->where('max', '>=', $people)
                                ->orderBy('min', 'ASC')
                                ->first();

            if ( !empty($range) ) {

                $this->setPeopleRange( $range );

                return true;

            }

        }

        return false;

    }

    /**
     * @return float
     */
    public function getKm() :float
    {
        return $this->km;
    }

    /**
     * @param float $km
     * @return RentalPrice
     * @throws \Exception
     */
    public function setKm(float $km): self
    {
        $this->km = $km;

        $this->toEv( self::KM_TRATTA, (string) $km );

        return $this;
    }

    /**
     * @return int
     */
    public function getPeople() :int
    {
        return $this->people;
    }

    /**
     * @param int $people
     * @return RentalPrice
     * @throws \Exception
     */
    public function setPeople(int $people): self
    {
        $this->people = $people;

        $this->toEv( self::PEOPLE, (string) $people );

        return $this;
    }

    /**
     * @return PeopleRange
     */
    public function getPeopleRange() :?PeopleRange
    {
        return $this->peopleRange;
    }

    /**
     * @param PeopleRange $peopleRange
     * @return RentalPrice
     * @throws \Exception
     */
    public function setPeopleRange(PeopleRange $peopleRange): self
    {
        $this->peopleRange = $peopleRange;

        $this->toEv( self::PEOPLE_RANGE_MULTIPLIER, (string) $peopleRange->price_multiplier );
        $this->toEv( self::PEOPLE_RANGE_PREDICT_MULTIPLIER, (string) $peopleRange->multiplier );
        $this->toEv( self::PEOPLE_RANGE_PREDICT_COEFF, (string) $peopleRange->coeff_predizione );

        return $this;
    }

    /**
     * @return Package
     */
    public function getPackage() :?Package
    {
        return $this->package;
    }

    /**
     * @param Package $package
     * @return RentalPrice
     * @throws \Exception
     */
    public function setPackage(Package $package): self
    {
        $this->package = $package;

        $this->toEv( self::PACKAGE_MULTIPLIER, $package->multiplier );

        return $this;
    }

    /**
     * @return RentRequest
     */
    public function getRentRequest() :?RentRequest
    {
        return $this->rentRequest;
    }

    /**
     * @param RentRequest $rentRequest
     * @return RentalPrice
     * @throws \Exception
     */
    public function setRentRequest(RentRequest $rentRequest): self
    {
        $this->rentRequest = $rentRequest;

        $this->toEv( self::DAYS_FROM_START, (string) $rentRequest->daysBeforeStart() );

        return $this;
    }

    /**
     * @return bool
     */
    public function isShared(): bool
    {
        return $this->shared;
    }

    /**
     * @param bool $shared
     * @return RentalPrice
     */
    public function setShared(bool $shared): self
    {
        $this->shared = $shared;

        return $this;
    }

    /**
     * @param float $days_from_start
     * @return RentalPrice
     * @throws \Exception
     */
    public function setDaysFromStart( float $days_from_start ) :self
    {

        $this->days_from_start = $days_from_start;

        $this->toEv( self::DAYS_FROM_START, (string) $days_from_start );

        return $this;

    }

    /**
     * @return float
     */
    public function getDaysFromStart() :float
    {

        return (float) $this->days_from_start;

    }

    /**
     * @return array
     */
    public function getCurrentVars() :array
    {

        return $this->em->vars() ?? [];

    }

    /**
     * @param float $days_of_travel
     * @return RentalPrice
     * @throws \Exception
     */
    public function setDaysOfTravel( float $days_of_travel ) :self
    {

        $this->days_of_travel = $days_of_travel;

        $this->toEv( self::DAYS_OF_TRAVEL, (string) $days_of_travel );

        return $this;

    }

    /**
     * @return float
     */
    public function getDaysOfTravel() :float
    {

        return $this->days_of_travel;

    }

    public function setKmPerDay() :self
    {

        $this->km_per_day = (float) ($this->getKm() / $this->getDaysOfTravel());

        $this->toEv( self::KM_PER_DAY, (string) $this->km_per_day );

        return $this;

    }

    /**
     * @param string $varName
     * @param string $value
     * @throws \Exception
     */
    protected function toEv ( string $varName, string $value ) :void
    {
        if ( $this->em->evaluate( $varName . ' = ' . $value ) === false ) {

            throw new \Exception( 'Error setting var: ' . $varName . ' to ' . $value . ' => ' . $this->em->last_error );

        }

    }

}