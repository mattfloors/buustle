<?php

namespace App\Http\Services;

use App\Models\DistribusionMarker;
use App\Models\DistribusionRaw;
use App\Models\DistribusionToAdd;
use App\Models\Marker;

class DistribusionSync
{

    protected $locale = 'en';

    protected $countryCodes = [
        'it' => [
            'it' => 'Italia',
            'de' => 'Germania',
            'fr' => 'Francia',
            'be' => 'Belgio',
            'es' => 'Spagna'
        ],

        'en' => [
            'it' => 'Italy',
            'de' => 'Germany',
            'fr' => 'France',
            'be' => 'Belgium',
            'es' => 'Spain'
        ]
    ];

    public function __construct( string $locale )
    {

        $this->locale = $locale;

    }

    /**
     * @param $data
     * @param array $sync
     */
    public function addFromRaw( $data, array $sync )
    {

        if ( !empty($data) ) {

            $this->multipleSync( $data );

            if ( !empty( $sync ) ) {

                $oldLocale = $this->locale;

                foreach ( $sync as $lang => $newData ) {

                    $this->locale = $lang;

                    $this->multipleSync( $newData );

                }

                $this->locale = $oldLocale;

            }

        }

    }

    /**
     * @param $objects
     */
    public function multipleSync( $objects )
    {

        if ( !empty ( $objects ) ) {

            foreach ( $objects as $object ) {

                $station = $this->formatRaw($object);

                $this->syncSingleStation( $station );

            }

        }

    }

    /**
     * @param $object
     * @return object
     */
    public function formatRaw($object)
    {

        $station = (object) $object->data;
        $station->attributes = (object) $station->attributes;
        $station->relationships = (object) $station->relationships;
        $station->relationships->area = (object) $station->relationships->area;
        $station->relationships->city = (object) $station->relationships->city;

        return $station;

    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public function downloadData( array $data ) :void
    {

        if ( empty($data) ) {

            throw new \Exception('Data is Empty');

        }

        DistribusionRaw::where('locale', '=', $this->locale)->delete();

        $dataToInsert = [];

        foreach( $data as $d ) {

            $dataToInsert[] = [
                'id' => $d->id,
                'locale' => $this->locale,
                'data' => json_encode($d)
            ];

        }

        DistribusionRaw::insert($dataToInsert);

    }

    public function syncSingleStation( $data )
    {

        $marker = $this->findDistribusionMarker( $data->id );

        $marker->distribusion_id = $data->id;
        $marker->lat = $data->attributes->latitude;
        $marker->lng = $data->attributes->longitude;
        $marker->zip_code = $data->attributes->zip_code;
        $marker->full_address = $data->attributes->street_and_number;
        $marker->setTranslation('name', $this->locale, $data->attributes->name);
        $marker->setTranslation('street_and_number', $this->locale, $data->attributes->street_and_number);
        $marker->setTranslation('description', $this->locale, $data->attributes->description);

        if( empty($marker->getTranslation('city_name', $this->locale)) ) {

            $marker->setTranslation('city_name', $this->locale, substr($data->id, 2, 3));

        }

        if( empty($marker->country_code) ) {

            $marker->country_code = strtolower(substr($data->id, 0, 2));

        }

        if( empty( $marker->getTranslation('country', $this->locale) ) ) {

            if ( array_key_exists($this->locale, $this->countryCodes) && array_key_exists(strtolower($marker->country_code), $this->countryCodes[$this->locale]) ) {

                $marker->setTranslation('country', $this->locale, $this->countryCodes[$this->locale][strtolower($marker->country_code)]);

            } else {

                $marker->setTranslation('country', $this->locale, strtolower($marker->country_code));

            }

        }

        $marker->type = 'address';

        $marker->save();

        $this->syncDistribusionMarker($marker, $data);

    }

    public function singleSync( array $data ) {



    }

    /**
     * @param array $data
     * @return bool
     */
    public function prepareNew( array $data ) :bool
    {
        if ( empty( $data ) ) {

            return true;

        }

        $newList = [];

        foreach ( $data as $dId ) {

            $newList[] = [
                'distribusion_id' => $dId,
                'locale' => $this->locale
            ];

        }

        DistribusionToAdd::where('locale', '=', $this->locale)->delete();

        DistribusionToAdd::insert($newList);

        return true;
    }

    protected function objToArray($obj, &$arr){

        if(!is_object($obj) && !is_array($obj)){
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }

        return $arr;
    }

    protected function syncDistribusionMarker( Marker $marker, $data )
    {

        $distribusionMarker = DistribusionMarker::where('marker_id', '=', $marker->id)->first();

        if ( empty( $distribusionMarker ) ) {

            $distribusionMarker = new DistribusionMarker();

        }

        $distribusionMarker->marker_id = $marker->id;
        $distribusionMarker->distribusion_id = $data->id;
        $distribusionMarker->locale = $this->locale;
        $distribusionMarker->data = $data;

        $distribusionMarker->save();

    }

    protected function findDistribusionMarker( string $id )
    {

        $marker = Marker::where('distribusion_id', '=', $id)->first();

        if ( empty($marker) ) {

            return new Marker();

        }

        return $marker;

    }

}