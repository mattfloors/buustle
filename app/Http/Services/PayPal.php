<?php

namespace App\Services;

use Omnipay\Omnipay;

class PayPal
{

    /**
     * @return mixed
     */
    public function gateway()
    {
        $gateway = Omnipay::create('PayPal_Express');

        $gateway->initialize(array(
            'username' => setting('payments.paypal_sandbox_mode') ? setting('payments.paypal_username_sandbox') : setting('payments.paypal_username_live'),
            'password' => setting('payments.paypal_sandbox_mode') ? setting('payments.paypal_password_sandbox') : setting('payments.paypal_password_live'),
            'signature' => setting('payments.paypal_sandbox_mode') ? setting('payments.paypal_signature_sandbox') : setting('payments.paypal_signature_live'),
            'testMode' => (bool) setting('payments.paypal_sandbox_mode'),
        ));

        return $gateway;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function purchase(array $parameters)
    {
        $response = $this->gateway()
            ->purchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param array $parameters
     * @return mixed
     */
    public function complete(array $parameters)
    {
        $response = $this->gateway()
            ->completePurchase($parameters)
            ->send();

        return $response;
    }

    /**
     * @param $amount
     * @return string
     */
    public function formatAmount($amount) :string
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * @param $order
     * @return string
     */
    public function getCancelUrl($order) :string
    {
        return route('paypal.checkout.cancelled', $order->id);
    }

    /**
     * @param $order
     * @return string
     */
    public function getReturnUrl($order) :string
    {
        return route('paypal.checkout.completed', $order->id);
    }

    /**
     * @param $order
     * @return string
     */
    public function getNotifyUrl($order) :string
    {
        $env = env('PAYPAL_TEST_MODE') ? 'sandbox' : 'live';

        return route('webhook.paypal.ipn', [$order->id, $env]);
    }

}