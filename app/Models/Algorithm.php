<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Algorithm extends Model implements Auditable
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'algorithms';

    protected $fillable = ['id', 'name', 'algorithm'];

    public const COMMISSIONE = 'commissione';

    public const TARIFFA = 'tariffa';

    public const FINALE = 'total';

    public const PREVISIONE = 'prediction';

    /**
     * My Functions
     */

    public static function validNames() :array
    {

        return [
            self::COMMISSIONE,
            self::TARIFFA,
            self::PREVISIONE,
            self::FINALE
        ];

    }

    /**
     * @return Algorithm|null
     */
    public static function commissione() :?self
    {

        return self::where('name', '=', self::COMMISSIONE)->first();

    }

    /**
     * @return Algorithm|null
     */
    public static function tariffa() :?self
    {

        return self::where('name', '=', self::TARIFFA)->first();

    }

    /**
     * @return Algorithm|null
     */
    public static function predizione() :?self
    {

        return self::where('name', '=', self::PREVISIONE)->first();

    }

    /**
     * @return Algorithm|null
     */
    public static function finale() :?self
    {

        return self::where('name', '=', self::FINALE)->first();

    }

    /**
     * @return string
     */
    public function parseCalculation() :string
    {

        if ( empty( $this->algorithm ) ) {

            return '';

        }

        $string = (string) $this->algorithm;
        //$string = str_replace('^', '**', $string);
        $string = str_replace('{', '', $string);
        $string = str_replace('}', '', $string);

        return $string;

    }

    /**
     * Relations
     */

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }
}
