<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Retailer extends Model
{
    use HasTranslations;
    use HasManyNotes;

    protected $table = 'retailers';

    protected $fillable = ['id'];

    public $translatable = ['name', 'description'];

    /*
     * Functions
     */
    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }

    /*
     * Relationship
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotes()
    {

        return $this->hasMany('App\Models\Quote');

    }
}
