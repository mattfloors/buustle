<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Region extends Model
{

    protected $table = 'regions';

    protected $fillable = ['id', 'name_it', 'name_en'];


    /**
     * My Functions
     */

    /**
     * Relationships
     */

    /**
     * @return BelongsToMany
     */
    public function noleggiatori() :BelongsToMany
    {

        return $this->belongsToMany('App\User', 'noleggiatore_region', 'region_id');

    }
}
