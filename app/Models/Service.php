<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;

class Service extends Model implements HasMedia
{
    use HasTranslations;
    use HasManyNotes;
    use HasMediaTrait;

    protected $table = 'services';

    protected $fillable = ['id'];

    public $translatable = ['name', 'description'];

    /*
     * Functions
     */
    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('icon-unchecked')->singleFile();
        $this->addMediaCollection('icon-checked')->singleFile();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function requests()
    {

        return $this->belongsToMany('requests', 'request_service');

    }

    public function packages()
    {

        return $this->belongsToMany('App\Models\Package', 'package_service');

    }

    public function travels()
    {

        return $this->belongsToMany('App\Models\Travel', 'travel_service');

    }
}
