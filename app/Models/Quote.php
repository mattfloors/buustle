<?php

namespace App\Models;

use App\Models\Interfaces\ProductInterface;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Facades\App;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Quote extends Model implements Auditable, ProductInterface, HasMedia
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;
    use HasMediaTrait;

    protected $table = 'quotes';

    protected $fillable = ['id'];

    public const STATUS_DRAFT = 'draft';

    public const STATUS_COMPLETED = 'completed';

    public const STATUS_ACCEPTED = 'accepted';

    public const STATUS_PAYED = 'payed';

    public const STATUS_DELETED = 'deleted';

    public const PDF_MEDIA_NAME = 'invoice';

    /**
     * My Functions
     */

    public function propagateRetailerToTravel() :void
    {
        if (!empty($this->travel) && $this->travel->retailer_id !== $this->retailer_id) {
            $this->travel->retailer_id = $this->retailer_id;

            $this->travel->save();
        }
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection(self::PDF_MEDIA_NAME)
            ->acceptsFile(function (File $file) {
                return $file->mimeType === 'application/pdf';
            })
            ->singleFile();
    }

    public function getInvoicePdf()
    {
        if (! $this->hasMedia(self::PDF_MEDIA_NAME)) {
            $this->createPdf();
        }

        return $this->getFirstMedia(self::PDF_MEDIA_NAME);
    }

    public function createPdf() :void
    {
        $pdfTmpFilePath = sys_get_temp_dir() . '/quote_invoice_' . $this->id . '.pdf';

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.quote.quote', ['quote' => $this]);
        $pdf->save($pdfTmpFilePath);

        $this->addMedia($pdfTmpFilePath)->toMediaCollection(self::PDF_MEDIA_NAME);

        @unlink($pdfTmpFilePath);
    }

    /**
     * @param Order $order
     */
    public function sold(Order $order) :void
    {
        if ($this->order->id !== $order->id) {

            /** TODO: order error handling */
        }

        $this->setNewStatus(self::STATUS_PAYED);
    }

    /**
     * @param string $status
     * @return bool
     */
    public function setNewStatus(string $status) :bool
    {
        if ($status !== $this->status && in_array($status, $this->statuses(), true)) {
            $this->status = $status;

            $this->save();

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function statuses() :array
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_COMPLETED,
            self::STATUS_ACCEPTED,
            self::STATUS_PAYED,
            self::STATUS_DELETED
        ];
    }

    public function accept() :void
    {
        $this->status = self::STATUS_ACCEPTED;

        $this->save();
    }

    /**
     * @return int
     */
    public function boughtSteps() :int
    {
        $boughtSteps = 0;

        if (empty($this->markers)) {
            return $boughtSteps;
        }

        foreach ($this->markers as $marker) {
            $boughtSteps += $marker->boughtSlots();
        }

        return $boughtSteps;
    }

    /**
     * @return int
     */
    public function totalFreeSlots() :int
    {

        $freeSlots = 0;

        if (empty($this->markers)) {
            return $freeSlots;
        }

        foreach($this->markers as $marker) {
            $freeSlots += $marker->slots;
        }

        return $freeSlots;

    }

    /**
     * Relations
     */

    /**
     * @return BelongsTo
     */
    public function request() :BelongsTo
    {
        return $this->belongsTo('App\Models\RentRequest');
    }

    /**
     * @return BelongsTo
     */
    public function user() :BelongsTo
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return BelongsToMany
     */
    public function services() :BelongsToMany
    {
        return $this->belongsToMany('App\Models\Service', 'quote_service');
    }

    /**
     * @return HasMany
     */
    public function prices() :HasMany
    {
        return $this->hasMany('App\Models\QuotePrice');
    }

    /**
     * @return HasMany
     */
    public function infos() :HasMany
    {
        return $this->hasMany('App\Models\QuoteInfos');
    }

    /**
     * @return HasMany
     */
    public function markers() :HasMany
    {
        return $this->hasMany('App\Models\QuoteMarker');
    }

    /**
     * @return BelongsTo
     */
    public function retailer() :BelongsTo
    {
        return $this->belongsTo('App\Models\Retailer');
    }

    /**
     * @return HasOne
     */
    public function secretData() :HasOne
    {
        return $this->hasOne('App\Models\QuoteSecretData');
    }

    /**
     * @return MorphOne
     */
    public function order() :MorphOne
    {
        return $this->morphOne('App\Models\Order', 'product');
    }

    /**
     * @return BelongsTo
     */
    public function package() :BelongsTo
    {
        return $this->belongsTo('App\Models\Package');
    }

    /**
     * @return HasMany
     */
    public function travels() :HasMany
    {
        return $this->hasMany('App\Models\Travel');
    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }
}
