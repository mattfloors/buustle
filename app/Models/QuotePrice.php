<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable;

class QuotePrice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'quote_prices';

    /**
     * @return BelongsTo
     */
    public function quote() :BelongsTo
    {
        return $this->belongsTo('App\Models\Quote');
    }
}
