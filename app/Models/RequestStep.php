<?php

namespace App\Models;

use App\Http\Services\RentalPrice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RequestStep extends Model
{
    protected $table = 'request_steps';

    protected $fillable = ['id'];

    protected $dates = [
        'start_time',
        'end_time'
    ];

    /**
     * Functions
     */

    public function onlyCityNameStart()
    {

        return str_replace(',', '', explode(' ', trim($this->start_address))[0]);

    }

    public function onlyCityNameEnd()
    {

        return str_replace(',', '', explode(' ', trim($this->end_address))[0]);

    }

    public function calculateTime()
    {

        $estimatedTime = null;
        $rp = new RentalPrice();
        $rp->loadManualDistance($this->start_lat, $this->start_lng, $this->end_lat, $this->end_lng);
        $estimatedTime = (int) $rp->gmData->duration->value;


        return $estimatedTime;

    }

    public function getEndTimeAttribute()
    {

        if ( $this->attributes['end_time'] === null ) {

            $et = Carbon::parse($this->start_time)->addSeconds($this->calculateTime())->toDateTimeString();

            $this->end_time = $et;

            $this->save();

        }

        return $this->attributes['end_time'];

    }

    /**
     * @param $date
     */
    public function setStartTimeAttribute($date) :void
    {
        $this->attributes['start_time'] = empty($date) ? null : Carbon::parse($date);
    }

    /**
     * @param $date
     */
    public function setEndTimeAttribute($date) :void
    {
        $this->attributes['end_time'] = empty($date) ? null : Carbon::parse($date);
    }

    /**
     * Relations
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rentRequest()
    {

        return $this->belongsTo('App\Models\RentRequest');

    }
}
