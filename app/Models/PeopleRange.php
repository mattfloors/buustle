<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeopleRange extends Model
{
    protected $table = 'people_ranges';

    protected $fillable = ['min', 'max', 'multiplier'];

}
