<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RawMarker extends Model
{
    protected $table = 'raw_markers';

    protected $fillable = ['marker_id'];

    public function marker()
    {
        return $this->belongsTo('App\Models\Marker');
    }
}
