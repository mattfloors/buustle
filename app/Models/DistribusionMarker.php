<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistribusionMarker extends Model
{
    protected $table = 'distribusion_markers';

    protected $casts = [
        'data' => 'array'
    ];

    public function marker()
    {
        return $this->belongsTo('App\Models\Marker');
    }
}
