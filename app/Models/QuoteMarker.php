<?php

namespace App\Models;

use App\Http\Services\RentalPrice;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class QuoteMarker extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasManyNotes;

    protected $table = 'quote_marker';

    //public $timestamps = false;

    /**
     * Functions
     */

    /**
     * @return int
     */
    public function boughtSlots() :int
    {
        $return = 0;

        if ( !empty( $this->soldMarkers ) ) {

            foreach ( $this->soldMarkers as $sm ) {

                $return += $sm->slots;

            }

        }

        return $return;

    }

    /**
     * @return float|null
     */
    public function calculateKm() :?float
    {

        $estimatedKm = null;

        if ( !empty( $this->start_marker ) && ! empty( $this->end_marker ) ) {

            $rp = new RentalPrice();
            $estimatedKm = $rp->loadManualDistance($this->start_marker->lat, $this->start_marker->lng, $this->end_marker->lat, $this->end_marker->lng);

        }

        return $estimatedKm;

    }

    public function calculateTime()
    {

        $estimatedTime = null;

        if ( !empty( $this->start_marker ) && ! empty( $this->end_marker ) ) {

            $rp = new RentalPrice();
            $rp->loadManualDistance($this->start_marker->lat, $this->start_marker->lng, $this->end_marker->lat, $this->end_marker->lng);
            $estimatedTime = (int) $rp->gmData->duration->value;

        }

        return $estimatedTime;

    }

    public function endTime()
    {

        return Carbon::parse($this->start_time)->addSeconds($this->estimated_time)->toDateTimeString();

    }

    /**
     * Accessors
     */

    public function getEstimatedKmAttribute( $value )
    {
        if ( $value === null ) {

            $value = $this->calculateKm();
            $this->estimated_km = $value;
            $this->save();

        }

        return $value;

    }

    public function getEstimatedTimeAttribute( $value )
    {
        if ( $value === null ) {

            $value = $this->calculateTime();
            $this->estimated_time = $value;
            $this->save();

        }

        return $value;

    }

    /**
     * Relationships
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quote() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Quote');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function start_marker() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Marker', 'marker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function end_marker() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Marker', 'end_marker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function soldMarkers() :\Illuminate\Database\Eloquent\Relations\HasMany
    {

        return $this->hasMany('App\Models\SoldMarker', 'quote_marker_id');

    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId() :?int
    {
        return auth()->id();
    }
}
