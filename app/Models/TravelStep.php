<?php

namespace App\Models;

use App\Http\Services\RentalPrice;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Contracts\Auditable;

class TravelStep extends Model implements Auditable
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'travel_steps';

    protected $fillable = ['id'];

    protected $dates = [
        'start_time',
        'end_time'
    ];

    /********************************* Mutators **************************/

    /**
     * @param $date
     */
    public function setStartTimeAttribute($date) :void
    {
        $this->attributes['start_time'] = empty($date) ? null : Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /**
     * @param $date
     */
    public function setEndTimeAttribute($date) :void
    {
        $this->attributes['end_time'] = empty($date) ? null : Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /********************************* Accessors **************************/

    /**
     * @param $value
     * @return mixed
     */
    public function getEstimatedKmAttribute( $value )
    {
        if ( $value === null ) {

            $this->recalculateAutomatedData();
            $value = $this->estimated_km;

        }

        return $value;

    }

    /**
     * @param $value
     * @return mixed
     */
    public function getEstimatedTimeAttribute( $value )
    {
        if ( $value === null && $this->id !== null ) {

            $this->recalculateAutomatedData();
            $value = $this->estimated_time;

        }

        return $value;

    }

    /**
     * @param $value
     * @return mixed
     */
    public function getEndTimeAttribute( $value )
    {

        if ( $value === null && $this->id !== null ) {

            $this->recalculateAutomatedData();
            $value = $this->end_time;

        }

        return $value;

    }

    /****************** Functions ***********************/

    /**
     * @return string
     */
    public function endTime() :string
    {

        return Carbon::parse($this->start_time)->addSeconds($this->estimated_time)->toDateTimeString();

    }

    /**
     * @return float
     */
    public function calculateAutoPrice() :float
    {

        return (float) 10;

    }

    /**
     * @return int
     */
    public function boughtSlots() :int
    {

        $total = 0;

        if( !empty( $this->tickets ) ) {

            foreach( $this->tickets as $ticket ) {

                $total += $ticket->slots;

            }

        }

        return $total;

    }

    /**
     * @return float|null
     */
    public function calculateKm() :?float
    {

        $estimatedKm = null;

        if ( !empty( $this->startMarker ) && ! empty( $this->endMarker ) ) {

            $rp = new RentalPrice();
            $estimatedKm = $rp->loadManualDistance($this->startMarker->lat, $this->startMarker->lng, $this->endMarker->lat, $this->endMarker->lng);

        }

        return $estimatedKm;

    }

    /**
     * @return float|null
     */
    public function calculateTime() :?float
    {

        $estimatedTime = null;

        if ( !empty( $this->startMarker ) && ! empty( $this->endMarker ) ) {

            $rp = new RentalPrice();
            $rp->loadManualDistance($this->startMarker->lat, $this->startMarker->lng, $this->endMarker->lat, $this->endMarker->lng);
            $estimatedTime = (int) $rp->gmData->duration->value;

        }

        return $estimatedTime;

    }

    public function recalculateAutomatedData()
    {

        $dispatcher = self::getEventDispatcher();

        self::unsetEventDispatcher();

        $this->estimated_km = $this->calculateKm();
        $this->estimated_time = $this->calculateTime();
        $this->end_time = $this->endTime();

        $this->save();

        $this->fresh();

        self::setEventDispatcher($dispatcher);

    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId() :?int
    {
        return auth()->id();
    }

    /**
     * Relations
     */

    /**
     * @return BelongsTo
     */
    public function travel() :BelongsTo
    {

        return $this->belongsTo('App\Models\Travel');

    }

    /**
     * @return BelongsTo
     */
    public function startMarker() :BelongsTo
    {

        return $this->belongsTo('App\Models\Marker', 'start_marker_id');

    }

    /**
     * @return BelongsTo
     */
    public function endMarker() :BelongsTo
    {

        return $this->belongsTo('App\Models\Marker', 'end_marker_id');

    }

    /**
     * @return HasMany
     */
    public function tickets() :HasMany
    {

        return $this->hasMany('App\Models\Ticket', 'travel_step_id');

    }

}
