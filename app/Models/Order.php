<?php

namespace App\Models;

use App\Events\OrderCompleted;
use App\Events\OrderFailed;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Gabievi\Promocodes\Models\Promocode;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Order extends Model implements Auditable
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'orders';

    public const PAYMENT_CREATED = 'created';

    public const PAYMENT_COMPLETED = 'completed';

    public const PAYMENT_PENDING = 'pending';

    public const PAYMENT_CANCELLED = 'cancelled';

    public const PAYMENT_FAILED = 'failed';

    public const PAYPAL = 'paypal';

    public const STRIPE = 'stripe';

    public const BONIFICO = 'bonifico';

    public const TICKET = 'App\Models\Ticket';

    public const QUOTE = 'App\Models\Quote';

    public $casts = [
        'raw_data' => 'array'
    ];

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }

    /**
     * My Functions
     */

    public function storeRawData( $data )
    {

        $this->raw_data = $data;
        $this->save();

    }

    /**
     * @return array
     */
    public function statuses() :array
    {

        return [
            self::PAYMENT_CREATED,
            self::PAYMENT_PENDING,
            self::PAYMENT_COMPLETED,
            self::PAYMENT_CANCELLED,
            self::PAYMENT_FAILED
        ];

    }

    /**
     * @return array
     */
    public function methods() :array
    {

        return [
            self::PAYPAL,
            self::STRIPE,
            self::BONIFICO
        ];

    }

    /**
     * @param Promocode $promocode
     * @return bool
     */
    public function applyPromoCode( Promocode $promocode ) :bool
    {

        if ( $promocode->data['type'] === 'flat' ) {

            $this->price -= $promocode->reward;

            if ( $this->price < 0 ) {

                $this->price = 0;

            }

        } else {

            $this->price -= ( ( $this->price / 100 ) * $this->reward );

        }

        $this->promocode_id = $promocode->id;

        return true;

    }

    public function cancel()
    {

        $this->payment_status = self::PAYMENT_CANCELLED;

        $this->save();

        /** TODO: event */

    }

    /**
     * @param string $transaction_id
     */
    public function complete( string $transaction_id ) :void
    {

        $this->transaction_id = $transaction_id;
        $this->payment_status = self::PAYMENT_COMPLETED;

        $this->save();

    }

    /**
     * @param $data
     * @param string $message
     */
    public function fail( $data, string $message ) :void
    {

        $this->raw_data = $data;
        $this->payment_status = self::PAYMENT_FAILED;
        $this->fail_message = $message;

        $this->save();

        event( new OrderFailed( $this ) );

    }

    /**
     * @return bool
     */
    public function isForQuote() :bool
    {

        if ( $this->product_type === self::QUOTE ) {

            return true;

        }

        return false;

    }

    /**
     * @return bool
     */
    public function isForTicket() :bool
    {

        if ( $this->product_type === self::TICKET ) {

            return true;

        }

        return false;

    }

    /**
     * Relations
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo('App\User');

    }

    /**
     * @return MorphTo
     */
    public function product() :MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promocode() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo('Gabievi\Promocodes\Models\Promocode');

    }
}
