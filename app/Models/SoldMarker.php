<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Webpatser\Uuid\Uuid;

class SoldMarker extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasManyNotes;

    protected $table = 'sold_markers';

    /**
     * Functions
     */

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * Relationships
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quoteMarker() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo('App\Models\QuoteMarker', 'quote_marker_id');

    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId() :?int
    {
        return auth()->id();
    }
}
