<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;
use Webpatser\Uuid\Uuid;

class Travel extends Model implements Auditable, HasMedia
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;
    use HasTranslations;
    use HasMediaTrait;

    protected $table = 'travels';

    protected $fillable = ['id'];

    public $translatable = ['description'];

    public const STATUS_DRAFT = 'draft';

    public const STATUS_COMPLETED = 'completed';

    public const STATUS_ACCEPTED = 'accepted';

    public const STATUS_PROPOSED = 'proposed';

    public const RETAILER_STATUS_OFFERED = 'offered';

    public const RETAILER_STATUS_ACCEPTED = 'confirmed';

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * My Functions
     */

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('public_files');

        $this
            ->addMediaCollection('hidden_files');
    }

    /**
     * @param string $status
     * @return bool
     */
    public function setNewStatus( string $status ) :bool
    {

        if ( $status !== $this->status && in_array( $status, $this->statuses(), true ) ) {

            $this->status = $status;

            $this->save();

            return true;

        }

        return false;

    }

    /**
     * @return int
     */
    public function totalDays()
    {
        if ( empty( $this->requestSteps ) || count($this->requestSteps) === 1 ) {

            return 1;

        }

        $firstStep = $this->getFirstStep();
        $lastStep  = $this->getLastStep();

        $firstStepDay = Carbon::parse($firstStep->start_time);
        $lastStepDay = Carbon::parse($lastStep->start_time);

        return $lastStepDay->diffInDays($firstStepDay);

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getFirstStep()
    {

        return $this->steps()->orderBy('start_time', 'ASC')->orderBy('id', 'ASC')->first();

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getLastStep()
    {

        return $this->steps()->orderBy('start_time', 'DESC')->orderBy('id', 'DESC')->first();

    }

    /**
     * @return array
     */
    public static function s() :array
    {

        return [
            self::STATUS_DRAFT,
            self::STATUS_COMPLETED,
            self::STATUS_ACCEPTED,
            self::STATUS_PROPOSED
        ];

    }

    /**
     * @return array
     */
    public static function rs() :array
    {

        return [
            self::RETAILER_STATUS_OFFERED,
            self::RETAILER_STATUS_ACCEPTED
        ];

    }

    /**
     * @return array
     */
    public function statuses() :array
    {

        return self::s();

    }

    /**
     * @return int
     */
    public function boughtSteps() :int
    {
        $boughtSteps = 0;

        if ( empty ( $this->steps ) ) {

            return $boughtSteps;

        }

        foreach ( $this->steps as $step ) {

            $boughtSteps += $step->boughtSlots();

        }

        return $boughtSteps;

    }

    public function orderedInfos()
    {

        return TravelInfos::where('travel_id', '=', $this->id)->orderBy('included', 'DESC')->get();

    }

    /**
     * Relations
     */

    /**
     * If travel belongs to a quote
     *
     * @return BelongsTo
     */
    public function quote() :BelongsTo
    {

        return $this->belongsTo('App\Models\Quote');

    }

    /**
     * If a retailer suggested the travel
     *
     * @return BelongsTo
     */
    public function owner() :BelongsTo
    {

        return $this->belongsTo('App\User');

    }

    /**
     * If a user suggested the travel
     *
     * @return BelongsTo
     */
    public function user() :BelongsTo
    {

        return $this->belongsTo('App\User');

    }

    /**
     * @return BelongsTo
     */
    public function retailer() :BelongsTo
    {

        return $this->belongsTo('App\Models\Retailer');

    }

    /**
     * @return BelongsToMany
     */
    public function services() :BelongsToMany
    {

        return $this->belongsToMany('App\Models\Service', 'travel_service');

    }

    /**
     * @return HasMany
     */
    public function steps() :HasMany
    {

        return $this->hasMany('App\Models\TravelStep');

    }

    /**
     * @return BelongsTo
     */
    public function package() :BelongsTo
    {

        return $this->belongsTo('App\Models\Package');

    }

    /**
     * @return HasMany
     */
    public function prices() :HasMany
    {
        return $this->hasMany('App\Models\TravelPrice');
    }

    /**
     * @return HasMany
     */
    public function infos() :HasMany
    {
        return $this->hasMany('App\Models\TravelInfos');
    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId() :?int
    {
        return auth()->id();
    }
}
