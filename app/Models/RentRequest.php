<?php

namespace App\Models;

use App\Events\RequestClosed;
use App\Events\RequestPayed;
use App\Http\Services\RentalPrice;
use App\User;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Webpatser\Uuid\Uuid;

class RentRequest extends Model implements Auditable, HasMedia
{
    use HasManyNotes;
    use \OwenIt\Auditing\Auditable;
    use HasMediaTrait;

    protected $table = 'requests';

    protected $fillable = ['id'];

    public const STATUS_DRAFT = 'draft'; //request created and still collecting informations

    public const STATUS_COMPLETED = 'completed'; //request completed, waiting for quotes

    public const STATUS_CLOSED = 'closed'; //quote accepted, waiting for payment

    public const STATUS_SOLD = 'sold'; //quote payed

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * My Functions
     */

    /**
     * @return array
     */
    public function statuses() :array
    {

        return [
            self::STATUS_DRAFT,
            self::STATUS_COMPLETED,
            self::STATUS_CLOSED,
            self::STATUS_SOLD
        ];

    }

    public function getStupidStatus()
    {

        if ( $this->daysBeforeStart() <= 0 ) {

            return __('requests-andati_e_tornati');

        }

        if ( $this->status === self::STATUS_COMPLETED && empty( $this->quotes ) ) {

            return __('requests.in_attesa_del_preventivo_finale');

        }

        if ( $this->status === self::STATUS_COMPLETED && !empty( $this->quotes ) ) {

            return __('requests.prenotazione_da_confermare');

        }

        if ( $this->status === self::STATUS_CLOSED ) {

            return __('requests.prenotazione_da_confermare');

        }

        if ( $this->status === self::STATUS_SOLD && $this->sharable ) {

            return __('requests.stiamo_vendendo_posti_vuoti');

        }

        if ( $this->status === self::STATUS_SOLD && !$this->sharable ) {

            return __('requests.pronti_a_partire');

        }

    }

    public function getBaseFreeSlots() :int
    {

        if ( !$this->people || $this->people === null || $this->people <= 0 ) {

            return 0;

        }

        $maxPerBus = setting('general.max_per_bus') ?? 55;

        if ( $this->people > setting('general.max_per_bus') ) {

            $peopleLeft = $this->people / $maxPerBus;

        } else {

            $peopleLeft = $this->people;

        }

        $min = setting('general.min_per_bus') ?? 4;

        if ( $peopleLeft < $min ) {

            return $min - $peopleLeft;

        }

        $peopleRange = PeopleRange::where('min', '<=', $peopleLeft)
                                  ->where('max', '>=', $peopleLeft)
                                  ->first();

        if ( !$peopleRange ) {

            return 0;

        }

        return ( $peopleRange->max - $peopleLeft );

    }

    /**
     * @return bool
     */
    public function hasPayedQuote() :bool
    {

        return ($this->payedQuote() !== null);

    }

    public function payedQuote() :?Quote
    {

        return Quote::where('request_id', '=', $this->id)->where('status', '=', 'payed')->first();

    }

    /**
     * @param int $userId
     * @return bool
     */
    public function bindToUser( int $userId ) :bool
    {

        $user = User::find($userId);

        if ( $user ) {

            $this->user_id = $user->id;

            $this->save();

            return true;

        }

        return false;

    }

    /**
     * @return float
     */
    public function addBackKm() :float
    {

        $firstStep = $this->getFirstStep();
        $lastStep = $this->getLastStep();

        if($firstStep->start_lat === $lastStep->end_lat && $firstStep->start_lng === $lastStep->end_lng) {

            return (float) 0;

        }

        $rentalPrice = new RentalPrice();

        return $rentalPrice->loadManualDistance($lastStep->end_lat, $lastStep->end_lng, $firstStep->start_lat, $firstStep->start_lng);

    }

    /**
     * @param null $compareDate
     * @return int
     */
    public function daysBeforeStart( $compareDate = null ) : int
    {
        if ( empty( $this->requestSteps ) ) {

            return 1;

        }

        $firstStep = $this->getFirstStep();
        $firstStepDay = Carbon::parse($firstStep->start_time);
        if ( $compareDate === null ) {

            $today = Carbon::now();

        } else {

            $today = Carbon::parse($compareDate);

        }

        return $firstStepDay->diffInDays($today);

    }

    /**
     * @return int
     */
    public function totalDays()
    {
        if ( empty( $this->requestSteps ) || count($this->requestSteps) === 1 ) {

            return 1;

        }

        $firstStep = $this->getFirstStep();
        $lastStep  = $this->getLastStep();

        $firstStepDay = Carbon::parse($firstStep->start_time);
        $lastStepDay = Carbon::parse($lastStep->start_time);

        return ($lastStepDay->diffInDays($firstStepDay) > 0) ? $lastStepDay->diffInDays($firstStepDay) : 1;

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getFirstStep()
    {

        return $this->requestSteps()->orderBy('start_time', 'ASC')->first();

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getLastStep()
    {

        return $this->requestSteps()->orderBy('start_time', 'DESC')->first();

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getFirstStepById()
    {

        return $this->requestSteps()->orderBy('id', 'ASC')->first();

    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getLastStepById()
    {

        return $this->requestSteps()->orderBy('id', 'DESC')->first();

    }

    /**
     * @return string
     */
    public function userEmail() :string
    {

        if( !empty( $this->user_id ) ) {

            return $this->user->email;

        }

        return 'anonymous';

    }

    public function completeRequest()
    {

        $this->newRequestStatus( self::STATUS_CLOSED );

    }

    /**
     * @param string $status
     * @return bool
     */
    public function newRequestStatus( string $status )
    {

        if ( $this->status !== $status && in_array( $status, $this->statuses(), true ) ) {

            $this->status = $status;

            $this->save();

            return true;

        }

        return false;

    }

    public function registerMediaCollections() :void
    {
        $this
            ->addMediaCollection('pdf')
            ->singleFile();
    }

    public function getPdf()
    {

        if ( $this->getMedia('pdf')->count() === 0 ) {

            $this->generatePdf();

        }

    }

    public function generatePdf()
    {



    }

    /**
     * Relations
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requestSteps()
    {

        return $this->hasMany('App\Models\RequestStep', 'request_id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {

        return $this->belongsTo('App\User');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {

        return $this->belongsToMany('App\Models\Service', 'request_service', 'request_id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotes()
    {

        return $this->hasMany('App\Models\Quote', 'request_id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {

        return $this->belongsTo('App\Models\Package');

    }
}
