<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class NoleggiatoriProfile extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'noleggiatori_profiles';

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('logo')
            ->singleFile();
    }

    /**
     * @return BelongsTo
     */
    public function user() :BelongsTo
    {

        return $this->belongsTo('App\User');

    }

    /**
     * @return BelongsTo
     */
    public function retailer() :BelongsTo
    {

        return $this->belongsTo('App\Models\Retailer');

    }

}
