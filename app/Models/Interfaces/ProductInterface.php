<?php

namespace App\Models\Interfaces;

use App\Models\Order;

interface ProductInterface {

    /**
     * @param Order $order
     * @return void
     */
    public function sold( Order $order ) :void;

}