<?php

namespace App\Models;

use App\Models\Interfaces\ProductInterface;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use OwenIt\Auditing\Contracts\Auditable;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Ticket extends Model implements Auditable, ProductInterface
{
    use \OwenIt\Auditing\Auditable;
    use HasManyNotes;

    protected $table = 'tickets';

    protected const STATUS_DRAFT = 'draft';

    protected const STATUS_PAYED = 'payed';

    /**
     * Functions
     */

    /**
     * @param Order $order
     */
    public function sold( Order $order ) :void
    {

        if ( $this->order->id !== $order->id ) {

            /** TODO: order error handling */

        }

        $this->status = self::STATUS_PAYED;

        $this->save();

    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * Relationships
     */

    /**
     * @return BelongsTo
     */
    public function step() :BelongsTo
    {

        return $this->belongsTo('App\Models\TravelStep', 'travel_step_id');

    }

    /**
     * @return MorphOne
     */
    public function order() :MorphOne
    {
        return $this->morphOne('App\Models\Order', 'product');
    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId() :?int
    {
        return auth()->id();
    }
}
