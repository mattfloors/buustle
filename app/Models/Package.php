<?php

namespace App\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Package extends Model implements HasMedia
{
    use HasTranslations;
    use HasManyNotes;
    use HasMediaTrait;

    protected $table = 'packages';

    protected $fillable = ['id'];

    public $translatable = ['name', 'description'];

    /**
     * Relationships
     */

    public function registerMediaCollections()
    {
        $this->addMediaCollection('banner-image')->singleFile();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services() :\Illuminate\Database\Eloquent\Relations\BelongsToMany
    {

        return $this->belongsToMany('App\Models\Service', 'package_service');

    }

    /**
     * Functions
     */

    /**
     * @return mixed
     * @throws \Exception
     */
    public function loadClass()
    {

        if ( empty($this->class) ) {

            throw new \Exception('Class not Found for Package # ' . $this->id);

        }

        $className = 'App\Packages\\' . $this->class;

        return new $className();

    }

    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }
}
