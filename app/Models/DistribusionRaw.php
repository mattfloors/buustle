<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistribusionRaw extends Model
{
    protected $table = 'distribusion_raw';

    protected $casts = [
        'data' => 'array'
    ];

    public $timestamps = false;

    public $incrementing = false;
}
