<?php

namespace App\Models;

use App\Http\Services\Maps;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Marker extends Model
{
    use HasTranslations;
    use HasManyNotes;

    protected $table = 'markers';

    protected $fillable = ['id'];

    public $translatable = ['name', 'street_and_number', 'description', 'city_name', 'country'];

    public function getRegionAttribute()
    {

        if ( $this->region_id === null ) {

            $this->recalculateRegion();

        }

        $this->refresh();

        return Region::find($this->region_id);

    }

    /*
     * Relationships
     */
    public function distribusionMarkers()
    {
        return $this->hasMany('App\Models\DistribusionMarker');
    }

    public function rawMarkers()
    {
        return $this->hasMany('App\Models\RawMarker');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /****************** Functions ***************/
    /**
     * Get the current author's id.
     *
     * @return int|null
     */
    protected function getCurrentAuthorId()
    {
        return auth()->id();
    }

    public function recalculateRegion()
    {
        if ($this->lat !== null && $this->lng !== null && $this->region_id === null && $r = Maps::getRegion($this->lat, $this->lng)) {
            if ($region = Region::where('name_it', '=', $r)->first()) {
                $this->region_id = $region->id;

                $this->save();
            }
        }
    }
}
