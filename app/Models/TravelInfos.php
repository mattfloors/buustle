<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable;

class TravelInfos extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'travel_infos';

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function quote() :BelongsTo
    {
        return $this->belongsTo('App\Models\Travel');
    }
}
