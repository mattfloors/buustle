<?php

namespace App\Observers;

use App\Models\TravelStep;

class TravelStepObserver
{
    /**
     * Handle the travel step "created" event.
     *
     * @param  TravelStep  $travelStep
     * @return bool
     */
    public function created(TravelStep $travelStep) :bool
    {

        $travelStep->recalculateAutomatedData();

        return false;

    }

    /**
     * Handle the travel step "updated" event.
     *
     * @param  TravelStep  $travelStep
     * @return bool
     */
    public function updated(TravelStep $travelStep) :bool
    {

        if (
            $travelStep->isDirty('start_marker_id') ||
            $travelStep->isDirty('end_marker_id') ||
            $travelStep->isDirty('start_time')
        ) {

            $travelStep->recalculateAutomatedData();

            return false;

        }

        return true;

    }

    /**
     * Handle the travel step "deleted" event.
     *
     * @param  TravelStep  $travelStep
     * @return void
     */
    public function deleted(TravelStep $travelStep) :void
    {
        //
    }

    /**
     * Handle the travel step "restored" event.
     *
     * @param  TravelStep  $travelStep
     * @return void
     */
    public function restored(TravelStep $travelStep) :void
    {
        //
    }

    /**
     * Handle the travel step "force deleted" event.
     *
     * @param  TravelStep  $travelStep
     * @return void
     */
    public function forceDeleted(TravelStep $travelStep) :void
    {
        //
    }
}
