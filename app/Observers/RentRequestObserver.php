<?php

namespace App\Observers;

use App\Events\RequestClosed;
use App\Events\RequestCompleted;
use App\Events\RequestPayed;
use App\Models\RentRequest;

class RentRequestObserver
{
    /**
     * Handle the rent request "created" event.
     *
     * @param  RentRequest  $rentRequest
     * @return void
     */
    public function created(RentRequest $rentRequest) :void
    {
        if ( $rentRequest->status === RentRequest::STATUS_COMPLETED ) {

            event( new RequestCompleted( $rentRequest ) );

        }
    }

    /**
     * Handle the rent request "updated" event.
     *
     * @param  RentRequest  $rentRequest
     * @return void
     */
    public function updated(RentRequest $rentRequest) :void
    {
        if ( $rentRequest->isDirty( 'status' ) ) {

            if ( $rentRequest->status === RentRequest::STATUS_COMPLETED ) {

                event( new RequestCompleted( $rentRequest ) );

            }

            if ( $rentRequest->status === RentRequest::STATUS_CLOSED ) {

                event ( new RequestClosed( $rentRequest ) );

            }

            if ( $rentRequest->status === RentRequest::STATUS_SOLD ) {

                event ( new RequestPayed( $rentRequest ) );

            }

        }
    }

    /**
     * Handle the quote "deleted" event.
     *
     * @param  RentRequest  $rentRequest
     * @return void
     */
    public function deleted(RentRequest $rentRequest) :void
    {
        //
    }

    /**
     * Handle the quote "restored" event.
     *
     * @param  RentRequest  $rentRequest
     * @return void
     */
    public function restored(RentRequest $rentRequest) :void
    {
        //
    }

    /**
     * Handle the quote "force deleted" event.
     *
     * @param  RentRequest  $rentRequest
     * @return void
     */
    public function forceDeleted(RentRequest $rentRequest) :void
    {
        //
    }
}
