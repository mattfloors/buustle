<?php

namespace App\Observers;

use App\Events\QuoteAccepted;
use App\Events\QuoteCompleted;
use App\Events\QuotePayed;
use App\Models\Quote;
use Illuminate\Support\Facades\Artisan;

class QuoteObserver
{
    /**
     * Handle the quote "created" event.
     *
     * @param  Quote  $quote
     * @return void
     */
    public function created(Quote $quote) :void
    {
        if ( $quote->status === Quote::STATUS_COMPLETED ) {



        }

        Artisan::call('rentRequest:setStatus', [
            'rentRequestId' => $quote->request->id,
        ]);
    }

    /**
     * Handle the quote "updated" event.
     *
     * @param  Quote  $quote
     * @return void
     */
    public function updated(Quote $quote) :void
    {

        if ( $quote->isDirty( 'status' ) ) {

            if ( $quote->status === Quote::STATUS_COMPLETED ) {

            }

            if ( $quote->status === Quote::STATUS_ACCEPTED ) {

                event( new QuoteAccepted($quote) );

            }

            if ( $quote->status === Quote::STATUS_PAYED ) {

                event( new QuotePayed( $quote ) );

            }

            Artisan::call('rentRequest:setStatus', [
                'rentRequestId' => $quote->request->id,
            ]);

        }

        Artisan::call('rentRequest:setStatus', [
            'rentRequestId' => $quote->request->id,
        ]);

        if ( $quote->isDirty( 'retailer_id' ) ) {

            $quote->propagateRetailerToTravel();

        }
    }

    /**
     * Handle the quote "deleted" event.
     *
     * @param  Quote  $quote
     * @return void
     */
    public function deleted(Quote $quote) :void
    {
        //
    }

    /**
     * Handle the quote "restored" event.
     *
     * @param  Quote  $quote
     * @return void
     */
    public function restored(Quote $quote) :void
    {
        //
    }

    /**
     * Handle the quote "force deleted" event.
     *
     * @param  Quote  $quote
     * @return void
     */
    public function forceDeleted(Quote $quote) :void
    {
        //
    }
}
