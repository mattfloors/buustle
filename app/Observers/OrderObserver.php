<?php

namespace App\Observers;

use App\Events\OrderCancelled;
use App\Events\OrderCompleted;
use App\Events\OrderFailed;
use App\Models\Order;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {

    }

    /**
     * Handle the quote "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        if ( $order->isDirty( 'payment_status' ) ) {

            if ( $order->status === Order::PAYMENT_COMPLETED ) {

                event( new OrderCompleted($order) );

            }

            if ( $order->status === Order::PAYMENT_CANCELLED ) {

                event( new OrderCancelled( $order ) );

            }

            if ( $order->status === Order::PAYMENT_FAILED ) {

                event( new OrderFailed( $order ) );

            }

        }
    }

    /**
     * Handle the quote "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $quote)
    {
        //
    }

    /**
     * Handle the quote "restored" event.
     *
     * @param  \App\Models\Order  $quote
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the quote "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
