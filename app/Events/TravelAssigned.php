<?php

namespace App\Events;

use App\Models\Travel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class TravelAssigned
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $travel;

    /**
     * QuoteAccepted constructor.
     * @param Travel $travel
     */
    public function __construct( Travel $travel )
    {
        $this->travel = $travel;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('travels');
    }
}
