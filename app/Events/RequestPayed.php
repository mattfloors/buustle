<?php

namespace App\Events;

use App\Models\RentRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class RequestPayed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $rentRequest;

    /**
     * RequestPayed constructor.
     * @param RentRequest $rentRequest
     */
    public function __construct( RentRequest $rentRequest )
    {

        $this->rentRequest = $rentRequest;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('requests');
    }
}
