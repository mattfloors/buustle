<?php

namespace App\Events;

use App\Models\RentRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RequestNewStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $rentRequest;

    /**
     * RequestNewStatus constructor.
     * @param RentRequest $rentRequest
     */
    public function __construct( RentRequest $rentRequest )
    {

        $this->rentRequest = $rentRequest;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('requests');
    }
}
