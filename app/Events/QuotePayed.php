<?php

namespace App\Events;

use App\Models\Quote;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class QuotePayed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * QuoteAccepted constructor.
     * @param Quote $quote
     */
    public function __construct( Quote $quote )
    {
        $this->quote = $quote;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('quotes');
    }
}
