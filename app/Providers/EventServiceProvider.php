<?php

namespace App\Providers;

use App\Events\OrderCancelled;
use App\Events\OrderCompleted;
use App\Events\OrderFailed;
use App\Events\QuoteAccepted;
use App\Events\QuoteAssigned;
use App\Events\QuoteCompleted;
use App\Events\QuotePayed;
use App\Events\RequestClosed;
use App\Events\RequestCompleted;
use App\Events\RequestNewStatus;
use App\Events\RequestPayed;
use App\Events\TravelAssigned;
use App\Listeners\NotifyAdminOfFailedPayment;
use App\Listeners\Orders\AdminNotifyOrderClosed;
use App\Listeners\Orders\CloseOrder;
use App\Listeners\Quotes\UserNotifyQuoteCompleted;
use App\Listeners\RentRequests\AdminNotifyCompleted;
use App\Listeners\RentRequests\UserNotifyCompleted;
use App\Listeners\Travels\AdminNotifyTravelAssigned;
use App\Listeners\UserNotifyOnRequestChange;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        RequestNewStatus::class => [
            UserNotifyOnRequestChange::class,
        ],
        OrderCompleted::class => [
            CloseOrder::class,
            AdminNotifyOrderClosed::class
        ],
        OrderFailed::class => [
            NotifyAdminOfFailedPayment::class,
        ],
        OrderCancelled::class => [
        ],
        QuoteAccepted::class => [
        ],
        QuotePayed::class => [
        ],
        RequestCompleted::class => [
            UserNotifyCompleted::class,
            AdminNotifyCompleted::class
        ],
        RequestPayed::class => [
        ],
        RequestClosed::class => [
        ],
        QuoteCompleted::class => [
            UserNotifyQuoteCompleted::class
        ],
        QuoteAssigned::class => [
        ],
        TravelAssigned::class => [
            AdminNotifyTravelAssigned::class
        ]
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
