<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\Quote;
use App\Models\RentRequest;
use App\Models\TravelStep;
use App\Observers\OrderObserver;
use App\Observers\QuoteObserver;
use App\Observers\RentRequestObserver;
use App\Observers\TravelStepObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        RentRequest::observe(RentRequestObserver::class);
        Quote::observe(QuoteObserver::class);
        TravelStep::observe(TravelStepObserver::class);
        Order::observe(OrderObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
