<?php

namespace App\Notifications\User\RentRequests;

use App\Models\RentRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class UserRentRequestCompleted extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    protected $rentRequest;

    /**
     * AddAdminNotify constructor.
     * @param array $data
     */
    public function __construct( RentRequest $rentRequest )
    {

        $this->rentRequest = $rentRequest;

        $this->notificationArrayMessage = [
            'url' => route('profile.viewRequest', ['rentRequestUuid' => $rentRequest->uuid]),
            'message' => __('rent.request_completed_notification')
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('mail.request_completed_title'))
            ->view(
            'emails.user.rent-request-completed', ['rentRequest' => $this->rentRequest]
            )
            ->from(setting('general.sender_email'), setting('general.sender_name'))
            ->replyTo(setting('general.contact_email'), setting('general.sender_name'));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
