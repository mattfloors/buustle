<?php

namespace App\Notifications\User\Quotes;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class UserQuoteCompleted extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    protected $quote;

    /**
     * AddAdminNotify constructor.
     * @param Quote $quote
     */
    public function __construct( Quote $quote )
    {

        $this->quote = $quote;

        $this->notificationArrayMessage = [
            'url' => route('profile.viewRequestQuotes', ['rentRequestUuid' => $quote->request->uuid]),
            'message' => __('rent.new_quote')
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * @param $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable) :MailMessage
    {
        return (new MailMessage)
            ->subject(__('mail.quote_completed_title'))
            ->view(
                'emails.user.quote-completed', ['quote' => $this->quote]
            )
            ->from(setting('general.sender_email'), setting('general.sender_name'))
            ->replyTo(setting('general.contact_email'), setting('general.sender_name'))
            ->attach($this->quote->getInvoicePdf()->getPath());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
