<?php

namespace App\Notifications\Admin\Quotes;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class AdminQuoteAssignedNotification extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    protected $quote;

    /**
     * AdminQuoteAssignedNotification constructor.
     * @param Quote $quote
     */
    public function __construct( Quote $quote )
    {

        $this->quote = $quote;

        $this->notificationArrayMessage = [
            'url' => '',
            'message' => ''
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('mail.quote_assigned'))
            ->view(
                'emails.admin.quote-assigned', ['rentRequest' => $this->quote]
            )
            ->from(setting('general.sender_email'), setting('general.sender_name'))
            ->replyTo(setting('general.contact_email'), setting('general.sender_name'));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
