<?php

namespace App\Notifications\Admin\Quotes;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class AdminQuoteAccepted extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    protected $quote;

    /**
     * AdminQuoteAccepted constructor.
     * @param Quote $quote
     */
    public function __construct( Quote $quote )
    {

        $this->quote = $quote;

        $this->notificationArrayMessage = [
            'url' => '',
            'message' => 'Quote Accepted'
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('mail.admin_quote_completed_title'))
            ->view(
                'emails.admin.quote-accepted', ['quote' => $this->quote]
            )
            ->from(setting('general.sender_email'), setting('general.sender_name'))
            ->replyTo(setting('general.contact_email'), setting('general.sender_name'))
            ->attach($this->quote->getInvoicePdf()->getPath());

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
