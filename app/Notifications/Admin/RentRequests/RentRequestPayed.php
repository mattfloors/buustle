<?php

namespace App\Notifications\Admin\RentRequests;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RentRequestPayed extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    /**
     * AddAdminNotify constructor.
     * @param array $data
     */
    public function __construct( array $data )
    {

        $this->notificationArrayMessage = $data;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('mail.request_payed_title'))
                    ->greeting(__('mail.request_status_payed'))
                    ->line(__('mail.request_payed_mail_message'))
                    ->action(__('mail.view_rent_request_payed_action'), url($this->notificationArrayMessage['url']))
                    ->line(__('mail.request_payed_footer'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
