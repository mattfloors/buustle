<?php

namespace App\Notifications\Noleggiatori\Travels;

use App\Models\Travel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NoleggiatoriTravelInRegion extends Notification
{
    use Queueable;

    protected $notificationArrayMessage = [
        'url'     => '',
        'message' => ''
    ];

    protected $travel;

    /**
     * AddAdminNotify constructor.
     * @param Travel $travel
     */
    public function __construct( Travel $travel )
    {

        $this->travel = $travel;

        $this->notificationArrayMessage = [
            'url' => '',
            'message' => __('travel.new_travel_your_region')
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) :array
    {
        return ['database', 'mail'];
    }

    /**
     * @param $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable) :MailMessage
    {
        return (new MailMessage)
            ->subject(__('mail.travel_notification_noleggiatori_title'))
            ->view(
                'emails.noleggiatori.travel-notification', ['travel' => $this->travel]
            )
            ->from(setting('general.sender_email'), setting('general.sender_name'))
            ->replyTo(setting('general.contact_email'), setting('general.sender_name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url'     => $this->notificationArrayMessage['url'],
            'message' => $this->notificationArrayMessage['message'],
        ];
    }
}
