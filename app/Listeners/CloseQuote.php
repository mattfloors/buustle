<?php

namespace App\Listeners;

use App\Events\OrderCompleted;
use App\Models\Quote;

class CloseQuote
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCompleted  $event
     * @return void
     */
    public function handle(OrderCompleted $event) :void
    {

        if ( ! empty( $event->order->quote ) && ! empty( $event->order->quote->request ) ) {

            $event->order->quote->status = Quote::STATUS_PAYED;
            $event->order->quote->save();

            $event->order->quote->request->newRequestStatus('sold');

        }

    }
}
