<?php

namespace App\Listeners;

use App\Notifications\AddAdminNotify;
use App\User;
use Illuminate\Support\Facades\Auth;

class AddAdminNotificationSubscriber
{
    /**
     * Handle user login events.
     */
    public function onQuoteAccepted($event) {

        $users = $this->getAdmins();

        foreach( $users as $user ) {

            $user->notify( new AddAdminNotify(['url' => route('admin.rentRequests.listCompleted'), 'message' => 'Un preventivo è stato accettato']));

        }

    }

    /**
     * Handle user login events.
     */
    public function onQuotePayed($event) {

        $users = $this->getAdmins();

        foreach( $users as $user ) {

            $user->notify( new AddAdminNotify(['url' => route('admin.rentRequests.listSold'), 'message' => 'Un preventivo è stato pagato']));

        }

    }

    /**
     * Handle user login events.
     */
    public function onRequestCompleted($event) {

        $users = $this->getAdmins();

        foreach( $users as $user ) {

            $user->notify( new AddAdminNotify( [ 'url' => route('admin.rentRequests.listNew'), 'message' => 'Una nuova richiesta è da Evadere' ] ) );

        }

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        /*$events->listen(
            'App\Events\QuoteAccepted',
            'App\Listeners\AddAdminNotificationSubscriber@onQuoteAccepted'
        );
        $events->listen(
            'App\Events\RequestCompleted',
            'App\Listeners\AddAdminNotificationSubscriber@onRequestCompleted'
        );
        $events->listen(
            'App\Events\QuotePayed',
            'App\Listeners\AddAdminNotificationSubscriber@onQuotePayed'
        );*/
    }

    private function getAdmins()
    {

        return User::role('Admin')->get();

    }
}