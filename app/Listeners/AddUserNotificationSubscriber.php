<?php

namespace App\Listeners;

use App\Notifications\RentRequests\RentRequestClosed;
use App\Notifications\RentRequests\RentRequestCompleted;
use App\Notifications\Quotes\UserQuoteCompleted;
use App\Notifications\RentRequests\RentRequestPayed;
use App\User;
use Illuminate\Support\Facades\Auth;

class AddUserNotificationSubscriber
{
    /**
     * Rent request has been confirmed
     * @param $event
     */
    public function onRequestCompleted($event) :void
    {

        $user = $event->rentRequest->user;

        $user->notify( new RentRequestCompleted(
            [
                'url' => route('profile.viewRequest', ['rentRequestUuid' => $event->rentRequest->uuid]),
                'message' => __('rent.request_completed_notification')
            ]
        ) );

    }

    /**
     * Quote has been completed
     * @param $event
     */
    public function onQuoteCompleted($event) :void
    {
        $user = $event->quote->user;

        $user->notify( new UserQuoteCompleted(
            [
                'url' => route('profile.viewRequestQuotes', ['rentRequestUuid' => $event->quote->request->uuid]),
                'message' => __('rent.new_quote')
            ]
        ) );

    }

    /**
     * Request has been closed (quotes have been added)
     *
     * @param $event
     */
    public function onRequestClosed($event) :void
    {

        $user = $event->rentRequest->user;

        $user->notify( new RentRequestClosed(
            [
                'url' => route('profile.viewRequestQuotes', ['rentRequestUuid' => $event->rentRequest->uuid]),
                'message' => __('rent.new_quote')
            ]
        ) );

    }

    /**
     * Request has been closed (quotes have been added)
     *
     * @param $event
     */
    public function onRequestPayed($event) :void
    {

        $user = $event->rentRequest->user;

        $user->notify( new RentRequestPayed(
            [
                'url' => route('profile.viewRequestQuotes', ['rentRequestUuid' => $event->rentRequest->uuid]),
                'message' => __('rent.request_payed')
            ]
        ) );

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        /*$events->listen(
            'App\Events\RequestCompleted',
            'App\Listeners\AddUserNotificationSubscriber@onRequestCompleted'
        );

        $events->listen(
            'App\Events\QuoteCompleted',
            'App\Listeners\AddUserNotificationSubscriber@onQuoteCompleted'
        );

        $events->listen(
            'App\Events\RequestClosed',
            'App\Listeners\AddUserNotificationSubscriber@onRequestClosed'
        );

        $events->listen(
            'App\Events\RequestPayed',
            'App\Listeners\AddUserNotificationSubscriber@onRequestPayed'
        );*/
    }
}