<?php

namespace App\Listeners\Quotes;

use App\Events\QuoteSaved;
use App\Notifications\Admin\Quotes\AdminQuoteAccepted;
use App\Notifications\UserQuoteDispatch;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminNotifyQuoteAccepted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteSaved  $event
     * @return void
     */
    public function handle(QuoteSaved $event)
    {
        if ( !$event->quote->avoidAdminNotify ) {

            $users = User::getAdmins();

            if( !empty($users) ) {

                foreach( $users as $admin ) {

                    $admin->notify(new AdminQuoteAccepted( $event->quote ));

                }

            }


        }
    }
}
