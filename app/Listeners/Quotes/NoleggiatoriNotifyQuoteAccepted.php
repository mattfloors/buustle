<?php

namespace App\Listeners\Quotes;

use App\Events\QuoteAccepted;
use App\Notifications\Noleggiatori\Quotes\NoleggiatoriQuoteAccepted;
use App\User;

class NoleggiatoriNotifyQuoteAccepted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteAccepted  $event
     * @return void
     */
    public function handle(QuoteAccepted $event)
    {

        if ( !$event->quote->avoidNoleggiatoriNotify ) {

            $listOfRegionIds = [];

            if ( !empty( $event->quote->markers ) ) {

                foreach( $event->quote->markers as $step ) {

                    if ( $step->start_marker->region_id === null ) {

                        $step->start_marker->recalculateRegion();

                    }

                    if ( $step->end_marker->region_id === null ) {

                        $step->end_marker->recalculateRegion();

                    }

                    if ( !in_array( (int) $step->start_marker->region_id, $listOfRegionIds, true ) ) {

                        $listOfRegionIds[] = $step->start_marker->region_id;

                    }

                    if ( !in_array( (int) $step->end_marker->region_id, $listOfRegionIds, true ) ) {

                        $listOfRegionIds[] = $step->end_marker->region_id;

                    }

                }

            }

            $usersToNotify = [];

            if ( count( $listOfRegionIds ) > 0 ) {

                $usersToNotify = User::select(['users.id', 'users.email'])->role(config('permission.retailer_group'))
                                     ->join('noleggiatore_region', function ($join) use ($listOfRegionIds) {
                                         $join->on('users.id', '=', 'noleggiatore_region.noleggiatore_id')
                                              ->whereIn('noleggiatore_region.region_id', $listOfRegionIds);
                                     })
                                     ->groupBy(['users.id', 'users.email'])
                                     ->get();
            }

            if ( count( $usersToNotify ) > 0 ) {

                foreach( $usersToNotify as $user ) {

                    $user->notify(new NoleggiatoriQuoteAccepted( $event->quote ) );

                }

            }

        }

    }
}
