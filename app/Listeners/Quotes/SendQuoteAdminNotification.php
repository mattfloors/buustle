<?php

namespace App\Listeners;

use App\Events\QuoteSaved;

class SendQuoteAdminNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteSaved  $event
     * @return void
     */
    public function handle(QuoteSaved $event)
    {
        if ( $event->quote->status === 'draft' ) {

            return;

        }

        if ( $event->quote->status === 'completed' && $event->quote->request->status ) {

            $event->quote->request->completeRequest();

        }
    }
}
