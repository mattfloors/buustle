<?php

namespace App\Listeners\Quotes;

use App\Events\QuoteAssigned;
use App\Notifications\Admin\Quotes\AdminQuoteAssignedNotification;
use App\Notifications\Admin\RentRequests\AdminRentRequestCompleted;
use App\User;

class AdminNotifyQuoteAssigned
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param QuoteAssigned $event
     */
    public function handle(QuoteAssigned $event)
    {

        $users = User::getAdmins();

        if( !empty($users) ) {

            foreach( $users as $admin ) {

                $admin->notify(new AdminQuoteAssignedNotification( $event->quote ));

            }

        }

    }
}
