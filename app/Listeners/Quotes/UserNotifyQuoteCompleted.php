<?php

namespace App\Listeners\Quotes;

use App\Events\QuoteCompleted;
use App\Notifications\User\Quotes\UserQuoteCompleted;

class UserNotifyQuoteCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuoteCompleted  $event
     * @return void
     */
    public function handle(QuoteCompleted $event)
    {

        if ( !$event->quote->avoidUserNotify ) {

            $user = $event->quote->user;

            $user->notify( new UserQuoteCompleted( $event->quote ) );

        }

    }
}
