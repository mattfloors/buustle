<?php

namespace App\Listeners;

use App\Events\RequestNewStatus;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserNotifyOnRequestChange
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestNewStatus  $event
     * @return void
     */
    public function handle(RequestNewStatus $event)
    {
        //send emails and stuff
    }
}
