<?php

namespace App\Listeners\Travels;

use App\Events\QuoteAssigned;
use App\Events\TravelAssigned;
use App\Notifications\Admin\Travels\AdminTravelAssignedNotification;
use App\User;

class AdminNotifyTravelAssigned
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param QuoteAssigned $event
     */
    public function handle(TravelAssigned $event)
    {
        $users = User::getAdmins();

        if (!empty($users)) {
            foreach ($users as $admin) {
                $admin->notify(new AdminTravelAssignedNotification($event->travel));
            }
        }
    }
}
