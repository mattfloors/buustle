<?php

namespace App\Listeners\RentRequests;

use App\Events\RequestCompleted;
use App\Notifications\User\RentRequests\UserRentRequestCompleted;

class UserNotifyCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RequestCompleted $event
     */
    public function handle(RequestCompleted $event)
    {

        if ( !$event->rentRequest->avoidUserNotify ) {

            $user = $event->rentRequest->user;

            $user->notify( new UserRentRequestCompleted( $event->rentRequest ) );

        }

    }
}
