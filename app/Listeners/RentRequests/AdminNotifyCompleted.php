<?php

namespace App\Listeners\RentRequests;

use App\Events\RequestCompleted;
use App\Notifications\Admin\RentRequests\AdminRentRequestCompleted;
use App\User;

class AdminNotifyCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RequestCompleted $event
     */
    public function handle(RequestCompleted $event)
    {

        if ( !$event->rentRequest->avoidAdminNotify ) {

            $users = User::getAdmins();

            if( !empty($users) ) {

                foreach( $users as $admin ) {

                    $admin->notify(new AdminRentRequestCompleted( $event->rentRequest ));

                }

            }


        }

    }
}
