<?php

namespace App\Listeners\Orders;

use App\Events\OrderCompleted;
use App\Notifications\Admin\Orders\AdminOrderCompleted;
use App\User;

class AdminNotifyOrderClosed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCompleted  $event
     * @return void
     */
    public function handle(OrderCompleted $event)
    {
        if ( !$event->order->avoidAdminNotify ) {

            $users = User::getAdmins();

            if( !empty($users) ) {

                foreach( $users as $admin ) {

                    $admin->notify(new AdminOrderCompleted( $event->order ));

                }

            }


        }
    }
}
