<?php

namespace App\Listeners\Orders;

use App\Events\OrderCompleted;

class CloseOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCompleted  $event
     * @return void
     */
    public function handle(OrderCompleted $event) :void
    {
        if ($product = $event->order->product_type::find($event->order->product_id)) {
            $product->sold($event->order);
        }
    }
}
