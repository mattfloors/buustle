<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            'name_it' => 'Abruzzo',
            'name_en' => 'Abruzzo'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Basilicata',
            'name_en' => 'Basilicata'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Calabria',
            'name_en' => 'Calabria'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Campania',
            'name_en' => 'Campania'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Emilia Romagna',
            'name_en' => 'Emilia Romagna'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Friuli Venezia Giulia',
            'name_en' => 'Friuli Venezia Giulia'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Lazio',
            'name_en' => 'Lazio'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Liguria',
            'name_en' => 'Liguria'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Lombardia',
            'name_en' => 'Lombardy'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Marche',
            'name_en' => 'Marche'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Molise',
            'name_en' => 'Molise'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Piemonte',
            'name_en' => 'Piedmont'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Puglia',
            'name_en' => 'Puglia'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Sardegna',
            'name_en' => 'Sardinia'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Sicilia',
            'name_en' => 'Sicily'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Toscana',
            'name_en' => 'Tuscany'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Trentino Alto Adige',
            'name_en' => 'Trentino Alto Adige'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Umbria',
            'name_en' => 'Umbria'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Valle d\'Aosta',
            'name_en' => 'Valle d\'Aosta'
        ]);

        DB::table('regions')->insert([
            'name_it' => 'Veneto',
            'name_en' => 'Veneto'
        ]);

    }
}
