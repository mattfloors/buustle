<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoldMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_markers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_marker_id')->unsigned();
            $table->string('through')->nullable();
            $table->string('reference')->nullable();
            $table->integer('slots')->unsigned()->default(1);
            $table->float('price')->unsigned()->default(1.00);
            $table->text('comment')->nullable();

            $table->timestamps();

            $table->foreign('quote_marker_id')->references('id')->on('quote_marker')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_markers');
    }
}
