<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRetailerToNoleggiatoriProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('noleggiatori_profiles', function (Blueprint $table) {
            $table->integer('retailer_id')->unsigned()->nullable()->after('user_id');

            $table->foreign('retailer_id')->references('id')->on('retailers')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('noleggiatori_profiles', function (Blueprint $table) {
            $table->dropColumn('retailer_id');
        });
    }
}
