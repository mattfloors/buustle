<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnQuoteMarkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'quote_marker', function( Blueprint $table ) {
            $table->integer('slots')->nullable()->default(0)->comment('total free slots for the step');
            $table->string('distribusion_code')->nullable()->comment('distribusion code to keep track of trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'quote_marker', function( Blueprint $table ) {
            $table->dropColumn('slots');
            $table->dropColumn('distribusion_code');
        });
    }
}
