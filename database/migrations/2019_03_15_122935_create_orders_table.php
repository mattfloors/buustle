<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_id')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->float('amount');
            $table->integer('quote_id')->unsigned()->nullable();
            $table->string('payment_status');
            $table->enum('method', ['bonifico', 'stripe', 'paypal'])->default('paypal');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
