<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuotesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_prices', function( $table ) {
            $table->increments('id');
            $table->integer('travel_id')->unsigned();
            $table->string('reason');
            $table->float('price');

            $table->foreign('travel_id')->references('id')->on('travels')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('travel_infos', function( $table ) {
            $table->increments('id');
            $table->integer('travel_id')->unsigned();
            $table->string('infos');
            $table->float('included');

            $table->foreign('travel_id')->references('id')->on('travels')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_prices');
        Schema::dropIfExists('travel_infos');
    }
}
