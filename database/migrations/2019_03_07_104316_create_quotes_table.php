<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->integer('request_id')->unsigned();

            $table->float('price');
            $table->enum('status', ['draft', 'completed', 'accepted', 'payed', 'deleted'])->default('draft');
            $table->integer('people')->unsigned();
            $table->text('comment')->nullable();
            $table->dateTime('valid_until')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('quote_marker', function( Blueprint $table ) {

            $table->increments('id');

            $table->integer('quote_id')->unsigned();
            $table->integer('marker_id')->unsigned();
            $table->dateTime('start_time');

            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::create('quote_service', function ( Blueprint $table ) {
            $table->increments('id');

            $table->integer('quote_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('quote_infos', function( Blueprint $table ) {

            $table->increments('id');

            $table->integer('quote_id')->unsigned()->unique();

            $table->json('infos')->nullable();

            $table->timestamps();

            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_infos');
        Schema::dropIfExists('quote_service');
        Schema::dropIfExists('quote_marker');
        Schema::dropIfExists('quotes');
    }
}
