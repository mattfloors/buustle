<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPackagesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people_ranges', function ( Blueprint $table ) {

            $table->float('price_multiplier', 8, 4)->default(1)->unsigned()->after('multiplier');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people_ranges', function ( Blueprint $table ) {

            $table->dropColumn('price_multiplier');

        });
    }
}
