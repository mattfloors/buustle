<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markers', function (Blueprint $table) {
            $table->increments('id');

            /** non-translatable */
            $table->enum('value_type', ['flat', 'percent'])->default('flat')->comment('discount type for the marker value');
            $table->float('value')->nullable()->comment('value for the marker if selected by the user');
            $table->string('distribusion_id')->nullable()->comment('ref id from the distribusion API');
            $table->decimal('lat', 20, 16)->comment('latitude');
            $table->decimal('lng', 20, 16)->comment('longitude');
            $table->string('zip_code', 32)->nullable();
            $table->string('full_address')->comment('street and number');
            $table->json('google_data')->comment('stored google data from api')->nullable();
            $table->boolean('active')->default(1)->comment('is marker active or not');

            /** translatable */
            $table->json('name')->comment('name of the place to show or search for');
            $table->json('street_and_number')->comment('only street and (eventually) number');
            $table->json('description')->nullable()->comment('for user use, can be lengthy');
            $table->json('city_name')->comment('name of the city');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markers');
    }
}
