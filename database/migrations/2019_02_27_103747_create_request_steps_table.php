<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_steps', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('request_id')->unsigned();

            $table->string('start_address');
            $table->dateTime('start_time');
            $table->decimal('start_lat', 20, 16);
            $table->decimal('start_lng', 20, 16);

            $table->string('end_address');
            $table->dateTime('end_time');
            $table->decimal('end_lat', 20, 16);
            $table->decimal('end_lng', 20, 16);

            $table->timestamps();

            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_steps');
    }
}
