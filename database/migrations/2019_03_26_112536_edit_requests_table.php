<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function( Blueprint $table ) {
            $table->string('uuid')->after('id');
        });

        Schema::table('request_steps', function( Blueprint $table ) {
            $table->datetime('start_time')->nullable()->change();
            $table->datetime('end_time')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function( Blueprint $table ) {
            $table->dropColumn('uuid');
        });

        Schema::table('request_steps', function( Blueprint $table ) {
            $table->datetime('start_time')->change();
            $table->datetime('end_time')->change();
        });
    }
}
