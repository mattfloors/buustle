<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKmTimeQuoteMarkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_marker', function( Blueprint $table ) {
            $table->float('estimated_km', 11,4)->nullable()->defaut(0);
            $table->integer('estimated_time')->nullable()->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_marker', function( Blueprint $table ) {
            $table->dropColumn('estimated_km');
            $table->dropColumn('estimated_time');

            $table->dropTimestamps();
        });
    }
}
