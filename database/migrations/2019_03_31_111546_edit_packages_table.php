<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function( Blueprint $table ) {
            $table->string('icon')->nullable()->after('description');
            $table->string('class')->nullable()->after('id');
        });

        Schema::create('package_service', function( Blueprint $table ) {
            $table->increments('id');

            $table->integer('package_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function( Blueprint $table ) {
            $table->dropColumn('icon');
            $table->dropColumn('class');
        });

        Schema::dropIfExists('package_service');
    }
}
