<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPackagesTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function( Blueprint $table ) {

            $table->integer('min_people')->nullable()->unsigned()->default(0)->change();
            $table->integer('max_people')->nullable()->unsigned()->default(0)->change();
            $table->integer('min_km')->nullable()->unsigned()->default(0)->change();
            $table->integer('max_km')->nullable()->unsigned()->default(0)->change();

            $table->float('multiplier')->default(1)->after('sku');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function( Blueprint $table ) {

            $table->integer('min_people')->unsigned()->default(0)->change();
            $table->integer('max_people')->unsigned()->default(0)->change();
            $table->integer('min_km')->unsigned()->default(0)->change();
            $table->integer('max_km')->unsigned()->default(0)->change();

            $table->dropColumn('multiplier');

        });
    }
}
