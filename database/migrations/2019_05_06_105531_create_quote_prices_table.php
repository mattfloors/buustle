<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_id')->unsigned();

            $table->string('reason')->comment('reason for the added price');
            $table->float('price')->default(0);

            $table->timestamps();

            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('quote_infos', function( Blueprint $table ) {
            $table->boolean('included')->default(0)->after('infos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_prices');

        Schema::table('quote_infos', function( Blueprint $table ) {
            $table->dropColumn('included');
        });
    }
}
