<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreInfosToMarkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('markers', function (Blueprint $table) {
            $table->string('country_code', 16);
            $table->json('country');
            $table->string('type', 64);
        });

        Schema::create('raw_markers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marker_id')->unsigned();
            $table->string('locale', 16);
            $table->json('data');
            $table->timestamps();

            $table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade')->onUpdate('cascade');
            $table->index('locale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('markers', function (Blueprint $table) {
            $table->dropColumn('country_code');
            $table->dropColumn('country');
            $table->dropColumn('type');
        });

        Schema::dropIfExists('raw_markers');
    }
}
