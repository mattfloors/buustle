<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetToTravels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travels', function (Blueprint $table) {
            $table->float('budget')->default(0)->nullable()->after('retailer_id');
            $table->dropColumn('status');
            $table->enum('retailer_status', ['offered', 'confirmed'])->default('offered')->after('retailer_id');
        });

        Schema::table('travels', function (Blueprint $table) {
            $table->enum('status', ['draft', 'completed', 'accepted', 'proposed'])->default('draft');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travels', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('travels', function (Blueprint $table) {
            $table->dropColumn('budget');
            $table->enum('status', ['draft', 'completed', 'accepted'])->default('draft')->change();
            $table->dropColumn('retailer_status', ['offered', 'confirmed']);
        });
    }
}
