<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');

            $table->json('name');
            $table->json('description');
            $table->boolean('active');
            $table->integer('min_people')->unsigned();
            $table->integer('max_people')->unsigned();
            $table->integer('min_km')->unsigned();
            $table->integer('max_km')->unsigned();
            $table->string('sku');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
