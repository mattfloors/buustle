<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteRetailerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->integer('retailer_id')->after('user_id')->unsigned()->nullable();
            $table->integer('bus_seats')->after('people')->default(0)->unsigned();
            $table->float('retailer_price')->after('people')->default(0)->nullable();
            $table->string('sku')->after('status')->nullable()->unique();

            $table->foreign('retailer_id')->references('id')->on('retailers')->onDelete('set null')->onUpdate('cascade');
        });

        Schema::create( 'quote_secret_data', function ( Blueprint $table ) {
            $table->increments('id');

            $table->integer('quote_id');
            $table->json('data')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_secret_data');
    }
}
