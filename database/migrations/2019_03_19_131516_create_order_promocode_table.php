<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPromocodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'orders', function( Blueprint $table ) {
            $table->integer('promocode_id')->unsigned()->nullable()->after('quote_id');

            $table->foreign('promocode_id')->references('id')->on('promocodes')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'orders', function( Blueprint $table ) {
            $table->dropColumn('promocode_id');
        });
    }
}
