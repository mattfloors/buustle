<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeopleRangeAddedCoeff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'people_ranges', function ( Blueprint $table ) {
            $table->float( 'coeff_predizione' )->after('price_multiplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'people_ranges', function ( Blueprint $table ) {
            $table->dropColumn( 'coeff_predizione' );
        });
    }
}
