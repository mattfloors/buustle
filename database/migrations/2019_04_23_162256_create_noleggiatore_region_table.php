<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoleggiatoreRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noleggiatore_region', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('noleggiatore_id')->unsigned();
            $table->integer('region_id')->unsigned();

            $table->timestamps();

            $table->foreign('noleggiatore_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noleggiatore_region');
    }
}
