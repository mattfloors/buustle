<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDistribusionToAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('distribusion_to_add', function (Blueprint $table) {
            $table->string('locale')->after('distribusion_id');

            $table->index('distribusion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distribusion_to_add', function (Blueprint $table) {
            $table->dropColumn('locale');

            $table->dropIndex('distribusion_id');
        });
    }
}
