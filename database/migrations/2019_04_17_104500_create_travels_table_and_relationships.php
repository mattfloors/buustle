<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelsTableAndRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->increments('id');

            $table->integer( 'quote_id' )->unsigned()->nullable()->comment('If created from a quote');
            $table->integer( 'owner_id' )->unsigned()->nullable()->comment('If created from a user');
            $table->integer( 'retailer_id')->unsigned()->nullable();

            $table->string('uuid')->nullable();
            $table->json('description')->nullable();

            $table->enum('status', ['draft', 'completed', 'accepted'])->default('draft');
            $table->boolean('active')->default(0);
            $table->boolean('shared')->default(0);

            $table->foreign('quote_id')->references('id')->on('quotes')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('owner_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('retailer_id')->references('id')->on('retailers')->onUpdate('cascade')->onDelete('set null');

            $table->timestamps();
        });

        Schema::create( 'travel_steps', function ( Blueprint $table ) {

            $table->increments( 'id' );

            $table->integer( 'travel_id' )->unsigned();
            $table->dateTime('start_time');
            $table->integer('start_marker_id')->unsigned();
            $table->dateTime('end_time')->nullable();
            $table->integer('end_marker_id')->unsigned();
            $table->integer( 'slots')->unsigned()->default(0);
            $table->string('distribusion_code')->nullable();
            $table->float('estimated_km')->unsigned()->nullable();
            $table->integer('estimated_time')->unsigned()->nullable();
            $table->float('price')->unsigned();

            $table->foreign('travel_id')->references('id')->on('travels')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('start_marker_id')->references('id')->on('markers')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('end_marker_id')->references('id')->on('markers')->onUpdate('cascade')->onDelete('restrict');

            $table->timestamps();

        });

        Schema::create( 'travel_service', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->integer( 'travel_id' )->unsigned();
            $table->integer( 'service_id' )->unsigned();

            $table->foreign('travel_id')->references('id')->on('travels')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_service');
        Schema::dropIfExists('travel_steps');
        Schema::dropIfExists('travels');
    }
}
