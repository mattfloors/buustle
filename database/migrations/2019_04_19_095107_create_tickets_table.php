<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('travel_step_id')->unsigned();
            $table->string('through')->nullable();
            $table->string('reference')->nullable();
            $table->integer('slots')->default(1)->unsigned();
            $table->float('price')->deafult(0)->unsigned();
            $table->text('comment')->nullable();

            $table->foreign('travel_step_id')->references('id')->on('travel_steps')->onDelete('restrict')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
