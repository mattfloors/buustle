<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnQuoteMarkerTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'quote_marker', function ( Blueprint $table ) {
            $table->integer('end_marker_id')->unsigned()->nullable();

            $table->foreign('end_marker_id')->references('id')->on('markers')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'quote_marker', function ( Blueprint $table ) {
            $table->dropColumn('end_marker_id');
        });
    }
}
