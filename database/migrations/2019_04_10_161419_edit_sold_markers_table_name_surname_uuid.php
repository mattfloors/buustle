<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSoldMarkersTableNameSurnameUuid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'sold_markers', function ( Blueprint $table ) {
            $table->string('uuid');
            $table->string('owner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'sold_markers', function ( Blueprint $table ) {
            $table->dropColumn('uuid');
            $table->dropColumn('owner');
        });
    }
}
