<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribusionMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribusion_markers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('marker_id')->unsigned()->nullable();

            $table->string('distribusion_id');
            $table->string('locale');
            $table->json('data');

            $table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade')->onUpdate('cascade');
            $table->index('locale');
            $table->index('distribusion_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribusion_markers');
    }
}
