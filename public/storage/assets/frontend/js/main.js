$(document).ready(function () {
  let dateTimePickerSetup1 = {
    pickTime: false,
    onRender: function(date) {
      console.log(date.valueOf());
      console.log(now.valueOf());
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
  };

    let dateTimePickerSetup2 = {
      pickTime: true,
      pickDate: false,
      onRender: function(date) {
        console.log(date.valueOf());
        console.log(now.valueOf());
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    };

	// mobile open menu
  $('.menu-mobile-icon').click(function() {
    $('#sidebar').toggleClass('active');
    $('body').toggleClass('overflow');
    $('.icon-bars').toggleClass('collapsed');
    $('.menu-mobile-overlay').toggleClass('active');
  });
  $('.menu-mobile-overlay').click(function() {
    $('#sidebar').removeClass('active');
    $('body').removeClass('overflow');
    $('.icon-bars').addClass('collapsed');
    $('.menu-mobile-overlay').removeClass('active');
  });

  // search filter
  $('.search-filter .filter').click(function() {
    $('.filter .unclick').toggleClass('active');
    $('.filter .click').toggleClass('active');
    $('.search-filter .bottom').toggleClass('open');
  });

  // scheda01-b
  $('.scheda01-b .inner.step').click(function() {
    $('.scheda01-b .inner.step').removeClass('open');
    $('.scheda01-b .inner.step .itinerario').removeClass('open');
    $(this).addClass('open');
    $('.scheda01-b .inner.step .itinerario').addClass('open');
  });

  // scheda01-b end
  $('.scheda01-b .inner.end').click(function() {
    $(this).toggleClass('open');
    $('.scheda01-b .inner.end .itinerario').toggleClass('open');
  });


  // slide schede proposte homepage
  $('.propone .scheda01 > .row ').slick({
    autoplay: false,
    speed: 300,
    arrows: false,
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: '<span class="customArrow arrowRight"></span>',
    prevArrow: '<span class="customArrow arrowLeft"></span>',
    responsive: [
      {
        breakpoint: 9999,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

  $('.my-select').selectpicker();

  /*$('.mydata').datetimepicker(dateTimePickerSetup1);
  $('.mytime').datetimepicker(dateTimePickerSetup2);*/


  // Change the selector if needed
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler

});
